<?php
class Api_Search_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function searchUsers($input)
    {
    	// $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('gender', 'gender', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Base_model->responseFailed(0,'Please select gender ');
            exit();
        }

    	$user_id = $input['user_id']; //always
    	$gender = $input['gender']; //always
    	$caste = $input['caste'];
    	$marital_status = $input['marital_status'];
    	$manglik = $input['manglik'];
    	$state = $input['state'];
    	$district = $input['district'];
        $name = $input['name'];

        if($user_id != '')
        {

        	if($caste !='' && $marital_status !='' && $manglik !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $manglik !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $manglik !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('manglik',$manglik);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $manglik !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $manglik !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $manglik !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('manglik',$manglik);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($manglik !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($manglik !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($marital_status !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($marital_status !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($manglik !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('manglik',$manglik);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($marital_status !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('marital_status',$marital_status);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $gender !='')
            {
                $this->db->like('name',$name);
            	$this->db->where('caste',$caste);
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($gender !='' && $name !='')
            {
                $this->db->where('gender',$gender);
                $this->db->like('name',$name);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($gender !='')
            {
                $this->db->where('gender',$gender);
                $this->db->where('user_id !=',$user_id);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            
        }
        else
        {

            if($caste !='' && $marital_status !='' && $manglik !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $manglik !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $manglik !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('manglik',$manglik);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $manglik !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $manglik !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $manglik !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('manglik',$manglik);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $marital_status !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($manglik !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($manglik !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('manglik',$manglik);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($marital_status !='' && $state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($marital_status !='' && $state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($state !='' && $district !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('state',$state);
                $this->db->where('district',$district);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($state !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('state',$state);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($manglik !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('manglik',$manglik);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($marital_status !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('marital_status',$marital_status);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($caste !='' && $gender !='')
            {
                $this->db->like('name',$name);
                $this->db->where('caste',$caste);
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($gender !='' && $name !='')
            {
                $this->db->where('gender',$gender);
                $this->db->like('name',$name);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
            else if($gender !='')
            {
                $this->db->where('gender',$gender);
                $this->db->where('status','1');
                $query = $this->db->get('mm_users');
                return $query->result();
            }
        }
    }

}