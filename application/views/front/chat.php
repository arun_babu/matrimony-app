<link rel='stylesheet prefetch' href='<?php echo base_url('front_assets'); ?>/css/reset.min.css'>
<section id="breadcrumb">
   <div class="container">
      <ul class="breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">News</a></li>
      </ul>
   </div>
</section>
<section class="chat-sec">
   <div class="container">
      <div id="frame">
         <div id="sidepanel">
            <div id="profile">
               <div class="wrap">
                  <?php
                   if($user->user_avtar !='')
                   {
                   ?>
                   <img class="online" id="profile-img" width="150" alt="user" src="<?=base_url('assets');?>/images/user/<?=$user->user_avtar; ?>">
                   <?php
                   }else{
                   ?>
                   <img class="online" id="profile-img" width="150" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                  <?php } ?>
                  <p><?=$user->name; ?></p>
               </div>
            </div>
            <div id="search">
               <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
               <input type="text" placeholder="Search contacts..." />
            </div>
            <div id="contacts">
               <ul>
                  <?php
                  $i=0;
                  foreach ($list_user as $lu) 
                  {
                     $i++;
                     // echo "<pre>";
                     // print_r($lu);
                  ?>
                  <li class="contact" onclick="getChatHistory('<?=$lu['user_id']; ?>');">
                     <div class="wrap">
                        <img src="<?=$lu['user_image']; ?>"/ style="height: 43px; width: 42px;">
                        <div class="meta">
                           <p class="name"><?=$lu['user_name']; ?></p>
                           <p class="preview"><?=$lu['message']; ?></p>
                        </div>
                     </div>
                  </li>
                  <?php
                  }
                  ?>
                  <!-- <li class="contact active">
                     <div class="wrap">
                        <img src="<?php echo base_url('front_assets'); ?>/images/chat/harveyspecter.png" alt="" />
                        <div class="meta">
                           <p class="name">Harvey Specter</p>
                           <p class="preview">Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                        </div>
                     </div>
                  </li> -->
                  
               </ul>
            </div>
         </div>
         <div class="content" id="content_msg">
            <div class="contact-profile">
               <!-- <img src="<?php echo base_url('front_assets'); ?>/images/chat/harveyspecter.png" alt="" /> -->
               <p>Please select your friend for chatting</p>
            </div>
            <div class="messages">
              <p><h3>Please choose your friend for start, Good and healthy conevers</h3></p>
               <!-- <ul>
                  <li class="sent">
                     <img src="<?php echo base_url('front_assets'); ?>/images/chat/mikeross.png" alt="" />
                     <p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>
                  </li>
                  <li class="replies">
                     <img src="<?php echo base_url('front_assets'); ?>/images/chat/harveyspecter.png" alt="" />
                     <p>When you're backed against the wall, break the god damn thing down.</p>
                  </li>
                  
               </ul> -->
            </div>
            <!-- <div class="message-input">
               <div class="wrap">
                  <input type="text" placeholder="Write your message..." />
                  <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                  <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
               </div>
            </div> -->
         </div>
      </div>
   </div>
</section>
<!-- <script src='https://phpstack-390430-1543668.cloudwaysapps.com/admin/front_assets/js/jquery.min.js'></script> -->
<script type="text/javascript">
function getChatHistory(user_id) 
{
   // alert(user_id);
   $.ajax({
        type:"post",
        url:"<?php echo site_url('getChatHistory');?>",
        data:"user_id="+user_id,
        success: function(data)
        {
            // alert(data);
            $("#content_msg").html(data);
        }
    });
}   

function setChatMsg()
{
  var to_user_id = $('#to_user_id').val();
  var from_user_id = $('#from_user_id').val();
  var message = $('#message1').val();
  $.ajax({
        type:"post",
        url:"<?php echo site_url('setChatMsg');?>",
        data:"to_user_id="+to_user_id+"&from_user_id="+from_user_id+"&message="+message,
        success: function(data)
        {
            // alert(data);
            $("#content_msg").html(data);
        }
    });
}

 $('body').on('keydown','.send_message',function(e){
        if (e.keyCode == 13) {
           setChatMsg();
        }
    })
</script>