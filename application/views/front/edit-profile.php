<section id="breadcrumb">
   <div class="container">
      <ul class="breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">Profile</a></li>
      </ul>
   </div>
</section>
<section id="edit-personal-profile">
   <div class="container">
   <div class="row detailed-profile">
   <div class="col-md-3">
      <div class="card-6">
         <p class="quick-links">Quick Links</p>
         <div class="line">
         </div>
         <div class="shortlist">
            <a href="<?=site_url('shortlist'); ?>">Shortlists & more</a>
         </div>
         <div class="line-1">
         </div>
         <div class="shortlist">
            <a href="<?=site_url('matches'); ?>">New Matches</a>
         </div>
         <div class="line-1">
         </div>
         <div class="shortlist">
            <a href="<?=site_url('matches'); ?>">My Matches</a>
         </div>
         <div class="line-1">
         </div>
         <div class="shortlist">
            <a href="#">Near Me</a>
         </div>
         <div class="line-1">
         </div>
         <div class="shortlist">
            <a href="#">Add Saved Searches</a>
         </div>
         <div class="line-1">
         </div>
         <div class="shortlist">
            <a href="#">My Help</a>
         </div>
      </div>
    <!--   <div class="profile-id-search">
         <p class="profile-id">Profile ID Search</p>
         <input class="search" type="text" placeholder="Enter Profile ID">&nbsp&nbsp&nbsp<button class="go">Go</button>
      </div> -->
     <!--  <p class="useful-link">Useful Links</p>
      <div class="link">
         <a href="#">Refer A Friend</a><br>
         <a href="#">Need Help ?</a><br>
         <a href="#">Security Tips</a>
      </div> -->
   </div>
   <div class="col-md-8">
      <h5 class="edit-personal">Edit Personal Profile</h5>
      <div class="card-1">
         <form method="post" action="<?=site_url('updateUserRecord'); ?>">
            <div class="bs-example">
               <div class="accordion" id="accordionExample">
                  <div class="card">
                     <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"><i class="fa fa-plus"></i> Basic Info</button>                                  
                        </h2>
                     </div>
                     <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                           <div class="form-content">
                              <div class="form-group">
                                 <label>Profile For </label>
                                 <select id="profile_for" class="form-control" name="profile_for" required="required">
                                    <option value="">--Select product type--</option>
                                    <option value="Self">Self</option>
                                    <option value="Relative">Relative</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Brother">Brother</option>
                                    <option value="Sister">Sister</option>
                                 </select>
                                <script type="text/javascript">
                                    setTimeout(function(){ $("#profile_for option[value='<?=@$user['profile_for']?>']").prop('selected',true)}, 1000);
                                </script>
                              </div>
                              <div class="form-group">
                                 <label>Name  </label>
                                 <input type="hidden" name="user_id" value="<?=$user['user_id']; ?>">
                                 <input class="form-control" type="text" name="name" value="<?=$user['name']; ?>" required>
                              </div>
                              <div class="form-group">
                                 <label>Mobile  </label>
                                 <input class="form-control" type="number" name="mobile" maxlength="10" value="<?=$user['mobile']; ?>" required>
                              </div>
                              <div class="form-group">
                                 <label>Gender  </label>
                                 <select id="heard" class="form-control" name="gender" required="required">
                                    <option value="">--Select product type--</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                 </select>
                                <script type="text/javascript">
                                    setTimeout(function(){ $("#heard option[value='<?=@$user['gender']?>']").prop('selected',true)}, 1000);
                                </script>
                              </div>
                              <div class="form-group">
                                 <label> Height  
                                 </label>
                                 <select id="height" name="height" class="form-control" required>
                                    <option value="">--Select Height--</option>
                                    <?php
                                    foreach ($height as $h) 
                                    {
                                       $selected='';
                                       if($h->id == @$user['height'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$h->id; ?>" <?=$selected?>><?=$h->height; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              
                              <div class="form-group">
                                 <label>Gotra </label>
                                 <input class="form-control"  type="text" name="gotra" value="<?=$user['gotra']; ?>" required>
                              </div>
                              <div class="form-group">
                                 <label>Gotra Nanihal  </label>
                                 <input class="form-control" type="text" name="gotra_nanihal" value="<?=$user['gotra_nanihal']; ?>" required>
                              </div>
                              <div class="form-group">
                                 <label>State</label>
                                 <select id="state" name="state" class="form-control" required>
                                    <option value="">--Select State--</option>
                                    <?php
                                    foreach ($state as $s) 
                                    {
                                       $selected='';
                                       if($s->id == @$user['state'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$s->id; ?>" <?=$selected?>><?=$s->name; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>District </label>
                                 <select id="district" name="district" class="form-control" required>
                                    <option value="">--Select District--</option>
                                    <?php
                                    foreach ($district as $d) 
                                    {
                                       $selected='';
                                       if($d->id == @$user['district'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$d->id; ?>" <?=$selected?>><?=$d->name; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>City </label>
                                 <input class="form-control" type="text" name="city" value="<?=$user['city']; ?>" required>
                              </div>
                              <div class="form-group">
                                 <label>Annual Income </label>
                                 <select id="income" class="form-control" name="income">
                                    <option value="">---Annual Income---</option>
                                    <?php
                                    foreach ($income as $i) 
                                    {
                                       $selected='';
                                       if($i->income_id == @$user['income'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$i->income_id; ?>" <?=$selected?>><?=$i->income; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"><i class="fa fa-plus"></i> Life Style</button>
                        </h2>
                     </div>
                     <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                           <div class="form-content">
                              <div class="form-group">
                                 <label>Date of Birth </label>
                                 <input type="date" class="form-control" name="dob" value="<?=$user['dob']; ?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Marital Status </label>
                                 <select id="cars" class="form-control" name="marital_status" required>
                                    <option value="">--Select Marital Status--</option>
                                    <?php
                                    foreach ($marital as $m) 
                                    {
                                       $selected='';
                                       if($m->id == @$user['marital_status'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$m->id; ?>" <?=$selected?>><?=$m->marital_status; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Aadhaar No. </label>
                                 <input type="number" class="form-control" name="aadhaar" value="<?=$user['aadhaar']; ?>" placeholder="Ex:-0009999000">
                              </div>
                              <div class="form-group">
                                 <label>Manglik </label>
                                 <select id="manglik" name="manglik" class="form-control" required>
                                    <option value="">---Select Manglik---</option>
                                    <?php
                                    foreach ($manglik as $m) 
                                    {
                                       $selected='';
                                       if($m->manglik_id == @$user['manglik'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$m->manglik_id; ?>" <?=$selected?>><?=$m->manglik; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Birth Time </label>
                                 <input type="text" class="form-control" name="birth_time" value="<?=$user['birth_time']; ?>" placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Birth Place </label>
                                 <input type="text" class="form-control" name="birth_place" value="<?=$user['birth_place']; ?>" placeholder="">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"><i class="fa fa-plus"></i> About me</button>                     
                        </h2>
                     </div>
                     <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                           <div class="form-content">
                              <div class="form-group">
                                 <label>About </label>
                                     <textarea class="form-control" name="about_me" id="textarea" rows="3"><?=$user['about_me']; ?></textarea>
                               
                              </div>
                              <div class="form-group">
                                 <label>Weight(kg) </label>
                                 <input type="number" class="form-control" name="weight" value="<?=$user['weight']; ?>" placeholder="Weight in kg.">
                              </div>
                              <div class="form-group">
                                 <label>Body Type </label>
                                 <select id="body_type" name="body_type" class="form-control">
                                    <option value="">---Select Body Type---</option>
                                    <option value="Slim">Slim </option>
                                    <option value="Average">Average </option>
                                    <option value="Athletic">Athletic</option>
                                    <option value="Heavy">Heavy</option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#body_type option[value='<?=@$user['body_type']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Complexion </label>
                                 <select id="complexion" name="complexion" class="form-control">
                                    <option value="">---Select Complexion---</option>
                                    <option value="Very Fair">Very Fair  </option>
                                    <option value="Fair">Fair </option>
                                    <option value="Wheatish">Wheatish</option>
                                    <option value="Wheatish Brown">Wheatish Brown</option>
                                    <option value="Dark">Dark</option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#complexion option[value='<?=@$user['complexion']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Physical Challenged </label>
                                 <select id="manglik" class="form-control">
                                    <option value="None">None  </option>
                                    <option value="Select">Physically handicappe from birth  </option>
                                    <option value="Self">Physically handicappe due to accident  </option>
                                    <option value="Son">Mentally challenged form birth </option>
                                    <option value="Son">Mentally  challenged due to accident</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Blood Group </label>
                                 <select id="blood_group" name="blood_group" class="form-control">
                                    <?php
                                    foreach ($blood as $b) 
                                    {
                                       $selected='';
                                       if($b->id == @$user['blood_group'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$b->id; ?>" <?=$selected?>><?=$b->blood_group; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour"><i class="fa fa-plus"></i> Important  Details</button>                     
                        </h2>
                     </div>
                     <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                           <div class="form-content">
                              <p>Education </p>
                              <div class="form-group">
                                 <label>Highest Education </label>
                                 <input type="text" class="form-control" name="qualification" value="<?=$user['qualification']; ?>" placeholder="">
                              </div>
                              <div class="form-group">
                                 <p>Work Experience</p>
                                 <label>Do you have any work experience? </label>
                                 <input type="radio" name="working" value="1" <?php echo (@$user['working'] == '1') ? 'checked' : ''; ?>>  Yes
                                 <input type="radio" name="working" value="0" <?php echo (@$user['working'] == '0') ? 'checked' : ''; ?>>  No
                              </div>
                              <div class="form-group">
                                 <label>Work place </label>
                                 <input type="text" class="form-control" name="work_place" value="<?=$user['work_place']; ?>" placeholder="Work place">
                              </div>
                              <div class="form-group">
                                 <label>Occupation </label>
                                 <select id="occupation" name="occupation" class="form-control">
                                    <option value="">---Select Occupation---</option>
                                    <?php
                                    foreach ($occupation as $o) 
                                    {
                                       $selected='';
                                       if($o->occupation_id == @$user['occupation'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$o->occupation_id; ?>" <?=$selected?>><?=$o->occupation; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>

                              </div>
                              <div class="form-group">
                                 <label>Organization name </label>
                                 <input type="text" class="form-control" name="organisation_name" value="<?=@$user['organisation_name']?>" placeholder="Organization name">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingFive">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive"><i class="fa fa-plus"></i>About more</button>                     
                        </h2>
                     </div>
                     <div id="collapseFive" class="collapse show" aria-labelledby="headingfive" data-parent="#accordionExample">
                        <div class="card-body">
                           <div class="form-content">
                              <div class="form-group">
                                 <label>Dietary Habits </label>
                                 <select id="dietary" name="dietary" class="form-control">
                                    <option value="Vegetarian">Vegetarian   </option>
                                    <option value="Non Vegetarian">Non Vegetarian </option>
                                    <option value="Eggetarian">Eggetarian</option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#dietary option[value='<?=@$user['dietary']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Drinking Habits </label>
                                 <select id="drinking" name="drinking" class="form-control">
                                    <option value="Yes">Yes  </option>
                                    <option value="No">No </option>
                                    <option value="Occasionally">Occasionally</option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#drinking option[value='<?=@$user['drinking']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Smoking Habits </label>
                                 <select id="smoking" name="smoking" class="form-control">
                                    <option value="Yes">Yes  </option>
                                    <option value="No">No </option>
                                    <option value="Occasionally">Occasionally</option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#smoking option[value='<?=@$user['smoking']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Language/Speak </label>
                                 <select id="language" name="language" class="form-control">
                                    <option value="हिन्दी">हिन्दी </option>
                                    <option value="English">English </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#language option[value='<?=@$user['language']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Hobbies</label>
                                 <select id="hobbies" name="hobbies" class="form-control">
                                    <option value="Collecting Stamps">Collecting Stamps </option>
                                    <option value="Collecting Coins">Collecting Coins </option>
                                    <option value="Collecting Antiques">Collecting Antiques </option>
                                    <option value="Art/Handicraft"> Art/Handicraft </option>
                                    <option value="Painting">Painting </option>
                                    <option value="Cooking">Cooking </option>
                                    <option value="Photography">Photography </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#hobbies option[value='<?=@$user['hobbies']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Interests</label>
                                 <select id="interests" name="interests" class="form-control">
                                    <option value="Writing">Writing </option>
                                    <option value="Reading / Book Clubs">Reading / Book Clubs </option>
                                    <option value="Learning New Languag">Learning New Language </option>
                                    <option value="Listening to music">Listening to music </option>
                                    <option value="Movies">Movies </option>
                                    <option value="Theattre">Theattre </option>
                                    <option value="Watching television">Watching television </option>
                                    <option value="Travel/Sightseeing">Travel/Sightseeing  </option>
                                    <option value="Politics">Politics </option>
                                    <option value="Blogging">Blogging </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#interests option[value='<?=@$user['interests']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingSix">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix"><i class="fa fa-plus"></i>About Family</button>                     
                        </h2>
                     </div>
                     <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#accordionExample">
                        <div class="card-body">
                           <div class="form-content">
                              <div class="form-group">
                                 <label>Grandfather Name </label>
                                 <input type="text" class="form-control" name="grand_father_name" value="<?=@$user['grand_father_name']?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Maternal Grandfather Name</label>
                                 <input type="text" class="form-control" name="maternal_grand_father_name_address" value="<?=@$user['maternal_grand_father_name_address']?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Father Name </label>
                                 <input type="text" class="form-control" name="father_name" value="<?=@$user['father_name']?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Mother Name </label>
                                 <input type="text" class="form-control" name="mother_name" value="<?=@$user['mother_name']?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Mobile </label>
                                 <input type="text" class="form-control" name="mobile2" value="<?=@$user['mobile2']?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Address </label>
                                 <input type="text" class="form-control" name="family_address" value="<?=@$user['family_address']?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Father's  Occupation </label>
                                 <select id="father_occupation" name="father_occupation" class="form-control">
                                    <option value="Business/Entrepreneur">Business/Entrepreneur </option>
                                    <option value="Service-Private">Service - Private</option>
                                    <option value="Service-Govt./PSU">Service - Govt./PSU </option>
                                    <option value="Army/Armed Forces"> Army/Armed Forces </option>
                                    <option value="Civil Services">Civil Services </option>
                                    <option value="Not Employed">Not Employed </option>
                                    <option value="Expired">Expired </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#father_occupation option[value='<?=@$user['father_occupation']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Mother' Occupation </label>
                                 <select id="mother_occupation" name="mother_occupation" class="form-control">
                                    <option value="Housewife">Housewife</option>
                                    <option value="Business/Entrepreneur">Business/Entrepreneur </option>
                                    <option value="Service-Private">Service-Private</option>
                                    <option value="Service-Govt./PSU">Service-Govt./PSU </option>
                                    <option value="Army/Armed Forces"> Army/Armed Forces </option>
                                    <option value="Civil Services">Civil Services </option>
                                    <option value="Teacher">Teacher </option>
                                    <option value="Retired">Retired </option>
                                    <option value="Expired">Expired </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#mother_occupation option[value='<?=@$user['mother_occupation']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Brother(s) </label>
                                 <select id="brother" name="brother" class="form-control">
                                    <option value="None">None </option>
                                    <option value="1">1 </option>
                                    <option value="2">2 </option>
                                    <option value="3">3 </option>
                                    <option value="3+">3+ </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#brother option[value='<?=@$user['brother']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Sister </label>
                                 <select id="sister" name="sister" class="form-control">
                                    <option value="None">None </option>
                                    <option value="1">1 </option>
                                    <option value="2">2 </option>
                                    <option value="3">3 </option>
                                    <option value="3+">3+ </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#sister option[value='<?=@$user['sister']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Family Income </label>
                                 <select id="family_income" name="family_income" class="form-control">
                                    <option value="">---Select Family Income---</option>
                                    <?php
                                    foreach ($income as $i) 
                                    {
                                       $selected='';
                                       if($i->income == @$user['family_income'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$i->income; ?>" <?=$selected?>><?=$i->income; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Family Status </label>
                                 <select id="family_status" name="family_status" class="form-control">
                                    <option value="Rich/Affluent">Rich/Affluent </option>
                                    <option value="Upper Middle Class">Upper Middle Class </option>
                                    <option value="Middle Class">Middle Class </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#family_status option[value='<?=@$user['family_status']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Family Type </label>
                                 <select id="family_type" name="family_type" class="form-control">
                                    <option value="Joint Family">Joint Family </option>
                                    <option value="Nuclear Family">Nuclear Family </option>
                                    <option value="Other">Other </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#family_type option[value='<?=@$user['family_type']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <div class="form-group">
                                 <label>Family Value </label>
                                 <select id="family_value" name="family_value" class="form-control">
                                    <option value="Orthodox">Orthodox</option>
                                    <option value="Conservative">Conservative </option>
                                    <option value="Moderate">Moderate </option>
                                    <option value="Liberal">Liberal </option>
                                 </select>
                                 <script type="text/javascript">
                                    setTimeout(function(){ $("#family_value option[value='<?=@$user['family_value']?>']").prop('selected',true)}, 1000);
                                 </script>
                              </div>
                              <p>Family Basesd out of</p>
                              <div class="form-group">
                                 <label>State  </label>
                                 <select id="family_state" name="family_state" class="form-control">
                                    <option value="">--Select State--</option>
                                    <?php
                                    foreach ($state as $s) 
                                    {
                                       $selected='';
                                       if($s->name == @$user['family_state'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$s->name; ?>" <?=$selected?>><?=$s->name; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Family District</label>
                                 <select id="family_district" name="family_district" class="form-control">
                                    <option value="">--Select District--</option>
                                    <?php
                                    foreach ($district as $d) 
                                    {
                                       $selected='';
                                       if($d->name == @$user['family_district'])
                                       {
                                          $selected = 'selected';
                                       }
                                       ?>
                                       <option value="<?=$d->name; ?>" <?=$selected?>><?=$d->name; ?></option>
                                       <?php
                                    }
                                    ?>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>City </label>
                                 <input type="text" class="form-control" name="family_city" value="<?=$user['family_city']?>"  placeholder="">
                              </div>
                              <div class="form-group">
                                 <label>Pincode </label>
                                 <input type="text" class="form-control" name="family_pin" value="<?=$user['family_pin']?>"  placeholder="">
                              </div>
                              <div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="sub-btn">
                     <input type="submit" class="btn button-1" value="Save" name="">
                  </div>
               </div>
            </div>
         </form>
         <div></div>
         <div class="col-md-2">
         </div>
      </div>
   </div>
</section>
<style type="text/css">
   select.form-control:not([size]):not([multiple]) {
    height: calc(2.25rem + 6px);
}
</style>