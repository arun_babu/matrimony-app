<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| FLASH MESSAGE
|--------------------------------------------------------------------------
*/

define('ALL_FIELD_MANDATORY', "Please fill all fields");
define('ALL_FIELD_MANDATORY_HI', "कृपया सभी स्थाान भरे ");
define('ALL_FIELD_MANDATORY_MR', "कृपया सर्व फील्ड भरा ");
define('DATA_UPDATE',"Data has been update successfully.");
define('DATA_SUBMIT',"Your data has been submitted successfully.");
define('DATA_SUBMIT_HI',"आपका डेटा सफलतापूर्वक सबमिट कर दिया गया है। ");
define('DATA_SUBMIT_MR',"आपला डेटा यशस्वीरित्या सबमिट केला गेला आहे ");
define('STATUS_CHANGE',"Status change successfully.");
define('SEND_NOTIF',"Send notification successfully.");
define('DATA_DELETE',"Data Delete successfully.");
define('IMAGE_DELETE',"Image Delete successfully.");
define('DATA_SAVE',"We have receive your request.");
define('DATA_SAVE_HI',"हमें आपका अनुरोध प्राप्त हुआ है।");
define('DATA_SAVE_MR',"आम्हाला तुमची विनंती प्राप्त झाली आहे.");
define('INTEREST_SENT',"Interest already sent.");
define('INTEREST_ACCEPT',"Interest already accepted.");

define('FAVOURITE', 'We have receive your request.');
define('COMPLETE_PROFILE', 'Please complete your profile to get suitable matches');
define('FORG_PASS', "Forgot password");
define('SUBSCRIPTION_HEAD', "Subscription sucessfull");
define('BLOCKED', "Blocked successfully");
define('UNBLOCKED', "Unblocked successfully");
define('UNBLOCKED_NOTRIGHT', "You have not right to unblock");
define('REPORT', "Report successfully");

define('OTP_SEND',"Otp send successfully");
define('MOBILE_EXIST',"Mobile number already exist");
define('VERIFY_OTP',"Your otp is verified successfully");
define('WRONG_OTP',"Wrong otp");
define('EMAIL_EXIST',"Email Id already exist");
define('EMAIL_EXIST_HI',"ईमेल आईडी पहले से मौजूद है ");
define('EMAIL_EXIST_MR',"ईमेल आयडी आधीपासून विद्यमान आहे ");
define('REGISTERD_SUCCESS', "Reristerd sucessfully, Please check your email and verified your account.");
define('REGISTERD_SUCCESS_HI', "आपने सफलतापूर्वक पंजीकरण किया, कृपया अपना ईमेल देखें और अपना खाता सत्यापित करें। ");
define('REGISTERD_SUCCESS_MR', "यशस्वीरित्या पुनर्प्राप्त केले, कृपया आपले ईमेल तपासा आणि आपले खाते सत्यापित करा.");
define('LOGIN_SUCCESS', "Login successfully.");
define('LOGIN_SUCCESS_HI', "आपने सफलतापूर्वक लॉगिन किया है।");
define('LOGIN_SUCCESS_MR', "आपण लॉगिन यशस्वीरित्या केले आहे.");
define('USER_NOT_FOUND','User not found');
define('USER_NOT_FOUND_HI','उपयोगकर्ता नहीं मिला ');
define('USER_NOT_FOUND_MR','वापरकर्ता आढळला नाही');
define('VALID_PASS',"Please enter valid password");
define('VALID_PASS_HI',"कृपया मान्य पासवर्ड दर्ज करें ");
define('VALID_PASS_MR',"कृपया वैध संकेतशब्द प्रविष्ट करा ");
define('DEACTIVE_NEW',"First verify your mail id using link we have sent on your Email.");
define('DEACTIVE_NEW_HI',"सबसे पहले अपने मेल आईडी का सत्यापन करें जिसका लिंक हमने आपके ईमेल पर भेजा है।");
define('DEACTIVE_NEW_MR',"आम्ही आपल्या ईमेलवर पाठविलेला दुवा वापरुन प्रथम आपला मेल आयडी सत्यापित करा.");
define('PASS_SEND','We have sent you password on your mail Id, please check your email id.');
define('PASS_SEND_HI','हमने आपको आपकी मेल आईडी पर पासवर्ड भेजा है, कृपया अपनी ईमेल आईडी देखें।');
define('PASS_SEND_MR','आम्ही आपल्याला आपल्या मेल आयडीवर संकेतशब्द पाठविला आहे, कृपया आपला ईमेल आयडी तपासा.');
define('VALID_EMAIL',"User not registered.");
define('VALID_EMAIL_HI',"उपयोगकर्ता पंजीकृत नहीं है।");
define('VALID_EMAIL_MR',"वापरकर्ता नोंदणीकृत नाही.");
define('SOMTHING',"Something Went Wrong");
define('SOMTHING_HI',"कुछ गलत हो गया");
define('SOMTHING_MR',"काहीतरी चूक झाली");
define('LOGOUT','Logout successfully');
define('LOGOUT_HI','लॉगआउट सफलतापूर्वक ');
define('LOGOUT_MR','लॉगआउट यशस्वीरित्या');
define('PASS_NOT_MATCH',"Password doesn't match on your old password");
define('PASS_NOT_MATCH_HI',"पासवर्ड आपके पुराने पासवर्ड से मेल नहीं खाता है");
define('PASS_NOT_MATCH_MR',"आपल्या जुन्या संकेतशब्दावर संकेतशब्द जुळत नाही");
define('PASS_CHANGE',"Password change sucessfully");
define('PASS_CHANGE_HI',"आपका पासवर्ड सफलतापूर्वक बदल गया");
define('PASS_CHANGE_MR',"आपला संकेतशब्द यशस्वीरित्या बदलला");
define('DATA_NOT_AVAIABLE',"Data not available");
define('DATA_NOT_AVAIABLE_HI',"डेटा उपलब्ध नहीं");
define('DATA_NOT_AVAIABLE_MR',"डेटा उपलब्ध नाही");
define('MSG_SEND','Successfully message send');
define('MSG_SEND_HI','संदेश सफलतापूर्वक भेजा गया ');
define('MSG_SEND_MR','संदेश यशस्वीरित्या पाठविला गेला ');
define('PROFILE_UPDATE','Profile updated successfully');
define('PROFILE_UPDATE_HI','प्रोफाइल को सफलतापूर्वक अपडेट किया गया');
define('PROFILE_UPDATE_MR','प्रोफाइल यशस्वीरित्या अद्यतनित केले');
define('NO_DATA_FOUND',"No data found");
define('DELETE_ACCOUNT',"Account delete successfully");

define('DELETE_ACCOUNT_CONTACT_ADMIN',"Account is deleted. If you have active once again contact to admin");


/*NOtification Type*/
define('BRODCAST_NOTIFICATION','10014');
define('ADMIN_NOTIFICATION','10016');
define('CHAT_TYPE','7003');
define('INTEREST_SEND_TYPE','7005'); //sendInterest(Api)

define('TOPICS_FOR_USERS','/topics/Users');