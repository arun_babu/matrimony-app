<?php
class Api_Chat_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function set_message($input)
	{
		$this->form_validation->set_rules('from_user_id', 'from_user_id', 'required');
		$this->form_validation->set_rules('to_user_id', 'to_user_id', 'required'); 
		// $this->form_validation->set_rules('message', 'message', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

		$fromUserExst = $this->db->where('user_id',$input['from_user_id'])->get('mm_users')->num_rows();
		if($fromUserExst > 0)
		{
			$toUserExst = $this->db->where('user_id',$input['to_user_id'])->get('mm_users')->num_rows();
			if($toUserExst > 0)
			{
				$from_user_id = $input['from_user_id'];
				$to_user_id = $input['to_user_id'];
				$message = $input['message'];
				$check_block = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id' AND block_status = '0') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id' AND block_status = '0') ")->num_rows();
				if($check_block > 0)
				{
					$checkRow = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id' AND blocked_by = '$from_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id' AND blocked_by = '$from_user_id') ");
					if($checkRow->num_rows() > 0)
					{
						$user = $checkRow->row_array();
						if($from_user_id == $user['blocked_by'])
						{
							$uData = $this->db->where('user_id',$from_user_id)->get('mm_users')->row();
						}else{
							$uData = $this->db->where('user_id',$to_user_id)->get('mm_users')->row();
						}
						$this->Base_model->responseFailed(0, $uData->name.' blocked you');
						exit();
					}else{
						$this->Base_model->responseFailed(0, 'You are blocked');
						exit();
					}
				}
				else
				{ 
					$check = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id') ");
					if($check->num_rows() > 0)
					{
						$msg_head = $check->row_array();
						$message_head_id = $msg_head['message_head_id'];
						$data = array(
							'updated_at' => date('Y-m-d H:i:s')
								);
						$this->db->where('message_head_id',$message_head_id);
						$this->db->update('mm_message_head',$data);
					}else{
						$data1 = array(
							'from_user_id' => $from_user_id,
							'to_user_id' => $to_user_id,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
								);
						$this->db->insert('mm_message_head',$data1);
						$message_head_id = $this->db->insert_id();

					}
					return $message_head_id;
				}
			}
			else
			{
				$this->Base_model->responseFailed(0, 'to_user_id is not exist');
				exit();
			}

		}
		else
		{
			$this->Base_model->responseFailed(0, 'from_user_id is not exist');
			exit();
		}

	}

	public function get_message_history($user_id)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

		$result = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$user_id' OR to_user_id = '$user_id') Order by updated_at DESC")->result();

		$get_chats = array();
		foreach($result as $i => $r)
		{
			$get_chats[$i] = array();
			$message_head_id = $r->message_head_id;
			$get_chats[$i]['message_head_id'] = $message_head_id;
			$get_chats[$i]['updated_at'] = round(strtotime($r->updated_at));
			$messageObjData = $this->db->query("SELECT * FROM mm_message WHERE message_head_id = '$message_head_id' Order by message_id DESC limit 1")->row();
			$get_chats[$i]['message'] = $messageObjData->message;
			if($user_id==$r->from_user_id){
				$userObjData = $this->db->where('user_id',$r->to_user_id)->get('mm_users')->row();
				// $userImgData = $this->db->where('user_id',$r->to_user_id)->get('mm_user_image')->row();
			} else {
				$userObjData = $this->db->where('user_id',$r->from_user_id)->get('mm_users')->row();
				// $userImgData = $this->db->where('user_id',$r->from_user_id)->get('mm_user_image')->row();
			}
			$get_chats[$i]['user_name'] = $userObjData->name;
			$get_chats[$i]['user_id'] = $userObjData->user_id;
			$get_chats[$i]['user_image'] = base_url().'assets/images/user/'.$userObjData->user_avtar;

		}
		$this->Base_model->responseSuccess(1, MSG_SEND, $get_chats);
	}

	public function get_message($input,$lan)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		$this->form_validation->set_rules('to_user_id', 'to_user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

		$user_id = $input['user_id'];
		$to_user_id = $input['to_user_id'];

		$check = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$user_id') ");
		if($check->num_rows() > 0)
		{
			$msg_head = $check->row_array();
			$message_head_id = $msg_head['message_head_id'];
			$this->db->where('message_head_id',$message_head_id);
			$this->db->order_by('message_id', 'Asc');
			$userObjMsg = $this->db->get('mm_message')->result(); 
			foreach ($userObjMsg as $i => $r) 
            {
                $get_msg[$i] = array();
                $get_msg[$i]['message_id'] = $r->message_id;
                $get_msg[$i]['message_head_id'] = $r->message_head_id;
                $get_msg[$i]['from_user_id'] = $r->from_user_id;
                $get_msg[$i]['to_user_id'] = $r->to_user_id;
                $get_msg[$i]['message'] = $r->message;
                $get_msg[$i]['type'] = $r->type;
                $get_msg[$i]['media'] = base_url().$r->media;
                $get_msg[$i]['is_read'] = $r->is_read;
                $get_msg[$i]['created_at'] = round(strtotime($r->created_at));
            }

			if($user_id==$msg_head['from_user_id']){
				$userObjData = $this->db->where('user_id',$msg_head['to_user_id'])->get('mm_users')->row();
				// $userImgData = $this->db->where('user_id',$msg_head['to_user_id'])->get('mm_user_image')->row();
			} else {
				$userObjData = $this->db->where('user_id',$msg_head['from_user_id'])->get('mm_users')->row();
				// $userImgData = $this->db->where('user_id',$msg_head['from_user_id'])->get('mm_user_image')->row();
			}
			$userData['user_name'] = $userObjData->name;
			$userData['user_image'] = base_url().'assets/images/user/'.$userObjData->user_avtar;
			$userData['block_status'] = $msg_head['block_status'];
			//to_user_id detail
			// $toUserData = $this->db->where('user_id',$to_user_id)->get('mm_users')->row();
			// $toUserImgData = $this->db->where('user_id',$to_user_id)->get('mm_user_image')->row();
			// $userData['to_user_name'] = $toUserData->name;
			// $userData['to_user_image'] = base_url().'assets/images/user/'.$toUserImgData->image;

			$check1 = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$user_id' AND to_user_id = '$to_user_id' AND blocked_by = '$to_user_id' AND block_status = '0') OR (from_user_id = '$to_user_id' AND to_user_id = '$user_id' AND blocked_by = '$to_user_id' AND block_status = '0') ");
			if($check1->num_rows() > 0)
			{
				$isBlocked = 1;
			}else{

				$isBlocked = 0;
			}

			$arr = array('status' => 1,'message' => '', 'data'=> $get_msg, 'user'=>$userData, 'is_blocked' => $isBlocked); 
			header('Content-Type: application/json');      
			echo json_encode($arr); 
		}
		else{
		// if($lan == 'en')
  //       {
  //           $USER_NOT_FOUND = USER_NOT_FOUND;
  //       }
  //       else if($lan == 'mr')
  //       {
  //           $USER_NOT_FOUND = USER_NOT_FOUND_MR;
  //       }
  //       else if($lan == 'hi')
  //       {
  //           $USER_NOT_FOUND = USER_NOT_FOUND_HI;
  //       }
		$this->Base_model->responseFailed(0, NO_DATA_FOUND);
		}
	}

	public function blockUser($input)
	{
		$this->form_validation->set_rules('from_user_id', 'from_user_id', 'required');
		$this->form_validation->set_rules('to_user_id', 'to_user_id', 'required'); 
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        $from_user_id = $input['from_user_id'];
		$to_user_id = $input['to_user_id'];

        $check = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id') ");
		if($check->num_rows() > 0)
		{
			$msg_head = $check->row_array();
			$message_head_id = $msg_head['message_head_id'];
			$data = array(
				'block_status' => 0,
				'blocked_by' => $to_user_id,
				'updated_at' => date('Y-m-d H:i:s')
					);
			$this->db->where('message_head_id',$message_head_id);
			$this->db->update('mm_message_head',$data);
		}else{
			$data1 = array(
				'from_user_id' => $from_user_id,
				'to_user_id' => $to_user_id,
				'block_status' => 0,
				'blocked_by' => $to_user_id,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
					);
			$this->db->insert('mm_message_head',$data1);

		}

		$this->Base_model->responseSuccessWethoutData(1, BLOCKED);
		exit();
	}

	public function unblockUser($input)
	{
		$this->form_validation->set_rules('from_user_id', 'from_user_id', 'required');
		$this->form_validation->set_rules('to_user_id', 'to_user_id', 'required'); 
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        $from_user_id = $input['from_user_id'];
		$to_user_id = $input['to_user_id'];

        $check = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id' AND blocked_by = '$to_user_id' AND block_status = '0') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id' AND blocked_by = '$to_user_id' AND block_status = '0') ");
		if($check->num_rows() > 0)
		{
			$msg_head = $check->row_array();
			$message_head_id = $msg_head['message_head_id'];
			$data = array(
				'block_status' => 1,
				'blocked_by' => "",
				'updated_at' => date('Y-m-d H:i:s')
					);
			$this->db->where('message_head_id',$message_head_id);
			$this->db->update('mm_message_head',$data);

			$this->Base_model->responseSuccessWethoutData(1, UNBLOCKED);
			exit();
		}else{

			$this->Base_model->responseFailed(0, UNBLOCKED_NOTRIGHT);
            exit();
		}

		
	}


}