-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `mm_admin`;
CREATE TABLE `mm_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `mm_admin` (`id`, `name`, `email`, `password`) VALUES
(1,	'Admin',	'admin@gmail.com',	'123456');

DROP TABLE IF EXISTS `mm_blood_group`;
CREATE TABLE `mm_blood_group` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `blood_group` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mm_blood_group` (`id`, `blood_group`) VALUES
(1,	'Don\'t Know'),
(2,	'A+'),
(3,	'A-'),
(4,	'B+'),
(5,	'B-'),
(6,	'AB+'),
(7,	'AB-'),
(8,	'O+'),
(9,	'O-');

DROP TABLE IF EXISTS `mm_body_type`;
CREATE TABLE `mm_body_type` (
  `body_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `body_type` tinytext NOT NULL,
  PRIMARY KEY (`body_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_body_type` (`body_type_id`, `body_type`) VALUES
(1,	'Slim'),
(2,	'Average'),
(3,	'Athletic'),
(4,	'Heavy');

DROP TABLE IF EXISTS `mm_brothers_sisters`;
CREATE TABLE `mm_brothers_sisters` (
  `brothers_sisters_id` int(11) NOT NULL AUTO_INCREMENT,
  `brothers_sisters` tinytext NOT NULL,
  PRIMARY KEY (`brothers_sisters_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_brothers_sisters` (`brothers_sisters_id`, `brothers_sisters`) VALUES
(1,	'None'),
(2,	'1'),
(3,	'2'),
(4,	'3'),
(5,	'3+');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `mm_caste`;
CREATE TABLE `mm_caste` (
  `caste_id` int(11) NOT NULL AUTO_INCREMENT,
  `caste` text CHARACTER SET utf8mb4 NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive',
  `language` varchar(11) CHARACTER SET utf8 NOT NULL COMMENT 'language code',
  `created_at` varchar(22) CHARACTER SET utf8 NOT NULL,
  `updated_at` varchar(22) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`caste_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_caste` (`caste_id`, `caste`, `status`, `language`, `created_at`, `updated_at`) VALUES
(1,	'Hindu',	1,	'en',	'',	'2020-10-17 10:59:24'),
(2,	'Muslim',	1,	'en',	'',	'2020-10-17 10:59:05'),
(3,	'Sikh',	1,	'en',	'',	'2020-10-17 10:58:55'),
(4,	'Jain',	1,	'en',	'',	'2020-10-17 10:58:41'),
(5,	'हिन्दू',	1,	'hi',	'2020-10-17 11:00:08',	''),
(6,	'मुस्लिम',	1,	'hi',	'2020-10-17 11:00:24',	''),
(7,	'सिख',	1,	'hi',	'2020-10-17 11:00:43',	''),
(8,	'जैन',	1,	'hi',	'2020-10-17 11:00:55',	''),
(9,	'हिंदू',	1,	'mr',	'2020-10-17 11:01:34',	''),
(10,	'मुसलमान',	1,	'mr',	'2020-10-17 11:02:02',	''),
(11,	'शीख',	1,	'mr',	'2020-10-17 11:02:22',	''),
(12,	'जैन',	1,	'mr',	'2020-10-17 11:02:50',	'');

DROP TABLE IF EXISTS `mm_category`;
CREATE TABLE `mm_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(55) CHARACTER SET utf8 NOT NULL,
  `image` varchar(22) NOT NULL,
  `language` varchar(22) NOT NULL COMMENT 'language code',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1 active, 0 inactive',
  `created_at` varchar(22) NOT NULL,
  `updated_at` varchar(22) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_category` (`category_id`, `cat_name`, `image`, `language`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Catering',	'1611123971.png',	'en',	1,	'2021-01-20 11:56:11',	''),
(2,	'खानपान',	'1611124094.png',	'hi',	1,	'2021-01-20 11:58:14',	''),
(3,	'केटरिंग',	'1611124144.png',	'mr',	1,	'2021-01-20 11:59:04',	''),
(4,	'Decoration',	'1611124234.png',	'en',	1,	'2021-01-20 12:00:34',	'2021-01-20 12:12:48'),
(5,	'सजावट',	'1611124286.png',	'hi',	1,	'2021-01-20 12:01:26',	''),
(6,	'सजावट',	'1611124313.png',	'mr',	1,	'2021-01-20 12:01:53',	''),
(7,	'Garden',	'1611926902.jpg',	'en',	1,	'2021-01-29 18:58:22',	''),
(8,	'Photography',	'1611926971.jpg',	'en',	1,	'2021-01-29 18:59:31',	'');

DROP TABLE IF EXISTS `mm_challanged`;
CREATE TABLE `mm_challanged` (
  `challanged_id` int(11) NOT NULL AUTO_INCREMENT,
  `challanged` tinytext NOT NULL,
  PRIMARY KEY (`challanged_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_challanged` (`challanged_id`, `challanged`) VALUES
(1,	'None'),
(2,	'Physically Handicapped form birth'),
(3,	'Physically Handicappe due to accident'),
(4,	'Mentally Challenged form birth'),
(5,	'Mentally Challenged due to accident');

DROP TABLE IF EXISTS `mm_complexion`;
CREATE TABLE `mm_complexion` (
  `complexion_id` int(11) NOT NULL AUTO_INCREMENT,
  `complexion` tinytext NOT NULL,
  PRIMARY KEY (`complexion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_complexion` (`complexion_id`, `complexion`) VALUES
(1,	'Very fair'),
(2,	'Fair'),
(3,	'Wheatish'),
(4,	'Wheatish brown'),
(5,	'Dark');

DROP TABLE IF EXISTS `mm_diet`;
CREATE TABLE `mm_diet` (
  `diet_id` int(11) NOT NULL AUTO_INCREMENT,
  `diet` tinytext NOT NULL,
  PRIMARY KEY (`diet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_diet` (`diet_id`, `diet`) VALUES
(1,	'Vegetarian'),
(2,	'Non Vegetarian'),
(3,	'Eggetarian');

DROP TABLE IF EXISTS `mm_districts`;
CREATE TABLE `mm_districts` (
  `id` text,
  `state_id` text,
  `name` text,
  `name_hi` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_districts` (`id`, `state_id`, `name`, `name_hi`) VALUES
('1',	'1',	'North Andaman',	'उत्तर अण्डमान'),
('2',	'1',	'South Andaman',	'दक्षिण अण्डमान'),
('3',	'1',	'Nicobar',	'निकोबार'),
('4',	'2',	'Adilabad',	'आदिलाबाद'),
('5',	'2',	'Anantapur',	'अनंतपुर'),
('6',	'2',	'Chittoor',	'चित्तूर'),
('7',	'2',	'East Godavari',	'पूर्व गोदावरी'),
('8',	'2',	'Guntur',	'गुंटूर'),
('9',	'2',	'Hyderabad',	'हैदराबाद'),
('10',	'2',	'Karimnagar',	'करीमनगर'),
('11',	'2',	'Khammam',	'खम्मम'),
('12',	'2',	'Krishna',	'कृष्णा'),
('13',	'2',	'Kurnool',	'कुर्नूल'),
('14',	'2',	'Mahbubnagar',	'महबूबनगर'),
('15',	'2',	'Medak',	'मेडक'),
('16',	'2',	'Nalgonda',	'नालगोंडा'),
('17',	'2',	'Nizamabad',	'निज़ामाबाद'),
('18',	'2',	'Prakasam',	'प्रकाशम'),
('19',	'2',	'Ranga Reddy',	'रंगरेड्डी'),
('20',	'2',	'Srikakulam',	'श्रीकाकुलम'),
('21',	'2',	'Sri Potti Sri Ramulu Nellore',	'श्री पोट्टी श्रीरामुलु नेल्लोर'),
('22',	'2',	'Vishakhapatnam',	'विशाखापट्टनम'),
('23',	'2',	'Vizianagaram',	'विजयनगरम'),
('24',	'2',	'Warangal',	'वारंगल'),
('25',	'2',	'West Godavari',	'पश्चिम गोदावरी'),
('26',	'2',	'Cudappah',	'कडपा'),
('27',	'3',	'Anjaw',	'अंजॉ'),
('28',	'3',	'Changlang',	'चेंगलॉन्ग'),
('29',	'3',	'East Siang',	'पूर्व सियांग'),
('30',	'3',	'East Kameng',	'पूर्व कमेंग '),
('31',	'3',	'Kurung Kumey',	'कुरुंग कुमे'),
('32',	'3',	'Lohit',	'लोहित'),
('33',	'3',	'Lower Dibang Valley',	'निचली दिबांग घाटी'),
('34',	'3',	'Lower Subansiri',	'निचली सुबनसिरी'),
('35',	'3',	'Papum Pare',	'पपुमपारे'),
('36',	'3',	'Tawang',	'तवांग'),
('37',	'3',	'Tirap',	'तिरप'),
('38',	'3',	'Dibang Valley',	'दिबांग घाटी'),
('39',	'3',	'Upper Siang',	'उपरी सियांग'),
('40',	'3',	'Upper Subansiri',	'उपरी सुबनसिरी'),
('41',	'3',	'West Kameng',	'पश्चिम कमेंग जिला'),
('42',	'3',	'West Siang',	'पश्चिम सियांग जिला'),
('43',	'4',	'Baksa',	'बक़सा'),
('44',	'4',	'Barpeta',	'बारपेटा'),
('45',	'4',	'Bongaigaon',	'बंगाईगाँव'),
('46',	'4',	'Cachar',	'काछाड़'),
('47',	'4',	'Chirang',	'चिरांग'),
('48',	'4',	'Darrang',	'दारांग'),
('49',	'4',	'Dhemaji',	'धेमाजी'),
('50',	'4',	'Dima Hasao',	'दीमा हसाओ'),
('51',	'4',	'Dhubri',	'धुबरी'),
('52',	'4',	'Dibrugarh',	'डिब्रूगढ'),
('53',	'4',	'Goalpara',	'गोवालपारा'),
('54',	'4',	'Golaghat',	'गोलाघाट'),
('55',	'4',	'Hailakandi',	'हैलाकांडी'),
('56',	'4',	'Jorhat',	'जोरहाट'),
('57',	'4',	'Kamrup',	'कामरूप'),
('58',	'4',	'Kamrup Metropolitan',	'कामरूप मेट्रोपॉलिटन'),
('59',	'4',	'Karbi Anglong',	'कार्बी ऑन्गलॉन्ग'),
('60',	'4',	'Karimganj',	'करीमगंज'),
('61',	'4',	'Kokrajhar',	'कोकराझार'),
('62',	'4',	'Lakhimpur',	'लखिमपुर'),
('63',	'4',	'Morigaon',	'मारिगांव'),
('64',	'4',	'Nagaon',	'नगांव'),
('65',	'4',	'Nalbari',	'नलबाड़ी'),
('66',	'4',	'Sivasagar',	'शिवसागर'),
('67',	'4',	'Sonitpur',	'शोणितपुर'),
('68',	'4',	'Tinsukia',	'तिनसुकिया'),
('69',	'4',	'Udalguri',	'उदालगुड़ी'),
('70',	'5',	'Araria',	'अररिया'),
('71',	'5',	'Arwal',	'अरवल'),
('72',	'5',	'Aurangabad',	'औरंगाबाद'),
('73',	'5',	'Banka',	'बांका'),
('74',	'5',	'Begusarai',	'बेगूसराय'),
('75',	'5',	'Bhagalpur',	'भागलपुर'),
('76',	'5',	'Bhojpur',	'भोजपुर'),
('77',	'5',	'Buxar',	'बक्सर'),
('78',	'5',	'Darbhanga',	'दरभंगा'),
('79',	'5',	'East Champaran',	'पूर्वी चंपारण'),
('80',	'5',	'Gaya',	'गया'),
('81',	'5',	'Gopalganj',	'गोपालगंज'),
('82',	'5',	'Jamui',	'जमुई'),
('83',	'5',	'Jehanabad',	'जहानाबाद'),
('84',	'5',	'Kaimur',	'कैमूर'),
('85',	'5',	'Katihar',	'कटिहार'),
('86',	'5',	'Khagaria',	'खगरिया'),
('87',	'5',	'Kishanganj',	'किशनगंज'),
('88',	'5',	'Lakhisarai',	'लखीसराय'),
('89',	'5',	'Madhepura',	'मधेपुरा'),
('90',	'5',	'Madhubani',	'मधुबनी'),
('91',	'5',	'Munger',	'मुंगेर'),
('92',	'5',	'Muzaffarpur',	'मुजफ्फरपुर'),
('93',	'5',	'Nalanda',	'नालंदा'),
('94',	'5',	'Nawada',	'नवादा'),
('95',	'5',	'Patna',	'पटना'),
('96',	'5',	'Purnia',	'पूर्णिया'),
('97',	'5',	'Rohtas',	'रोहतास'),
('98',	'5',	'Saharsa',	'सहरसा'),
('99',	'5',	'Samastipur',	'समस्तीपुर'),
('100',	'5',	'Saran',	'सरन'),
('101',	'5',	'Sheikhpura',	'शेखपुरा'),
('102',	'5',	'Sheohar',	'शिवहर'),
('103',	'5',	'Sitamarhi',	'सीतामढ़ी'),
('104',	'5',	'Siwan',	'सिवान'),
('105',	'5',	'Supaul',	'सुपौल'),
('106',	'6',	'Chandigarh',	'चंडीगढ़'),
('107',	'7',	'Bastar',	'बस्तर'),
('108',	'7',	'Bijapur',	'बीजापुर'),
('109',	'7',	'Bilaspur',	'बिलासपुर'),
('110',	'7',	'Dantewada',	'दंतेवाड़ा'),
('111',	'7',	'Dhamtari',	'धमतरी'),
('112',	'7',	'Durg',	'दुर्ग'),
('113',	'7',	'Jashpur',	'जशपुर'),
('114',	'7',	'Janjgir-Champa',	'जांजगीर-चंपा'),
('115',	'7',	'Korba',	'कोरबा'),
('116',	'7',	'Koriya',	'कोरिया'),
('117',	'7',	'Kanker',	'कांकेर'),
('118',	'7',	'Kabirdham (formerly Kawardha)',	'कबीरधाम'),
('119',	'7',	'Mahasamund',	'महासमुंद'),
('120',	'7',	'Narayanpur',	'नारायणपुर'),
('121',	'7',	'Raigarh',	'रायगढ़'),
('122',	'7',	'Rajnandgaon',	'राजनंदगांव'),
('123',	'7',	'Raipur',	'रायपुर'),
('124',	'7',	'Surguja',	'सरगुजा'),
('125',	'8',	'Dadra and Nagar Haveli',	'दादरा और नगर हवेली'),
('126',	'9',	'Daman',	'दमन'),
('127',	'9',	'Diu',	'दीव'),
('128',	'10',	'Central Delhi',	'मध्य दिल्ली'),
('129',	'10',	'East Delhi',	'पूर्वी दिल्ली'),
('130',	'10',	'New Delhi',	'नई दिल्ली'),
('131',	'10',	'North Delhi',	'उत्तर दिल्ली'),
('132',	'10',	'North East Delhi',	'उत्तर पूर्व दिल्ली'),
('133',	'10',	'North West Delhi',	'उत्तर पश्चिम दिल्ली'),
('134',	'10',	'South Delhi',	'दक्षिण दिल्ली'),
('135',	'10',	'South West Delhi',	'दक्षिण पश्चिम दिल्ली'),
('136',	'10',	'West Delhi',	'पश्चिम दिल्ली'),
('137',	'11',	'North Goa',	'उत्तर गोवा'),
('138',	'11',	'South Goa',	'दक्षिण गोवा'),
('139',	'12',	'Ahmedabad',	'अहमदाबाद'),
('140',	'12',	'Amreli district',	'अमरेली जिला'),
('141',	'12',	'Anand',	'आनंद'),
('142',	'12',	'Banaskantha',	'बनासकांठा'),
('143',	'12',	'Bharuch',	'भरूच'),
('144',	'12',	'Bhavnagar',	'भावनगर'),
('145',	'12',	'Dahod',	'दाहोद'),
('146',	'12',	'The Dangs',	'डैंग्स'),
('147',	'12',	'Gandhinagar',	'गांधीनगर'),
('148',	'12',	'Jamnagar',	'जामनगर'),
('149',	'12',	'Junagadh',	'जूनागढ़'),
('150',	'12',	'Kutch',	'कच्छ'),
('151',	'12',	'Kheda',	'खेड़ा'),
('152',	'12',	'Mehsana',	'मेहसाणा'),
('153',	'12',	'Narmada',	'नर्मदा'),
('154',	'12',	'Navsari',	'नवसारी'),
('155',	'12',	'Patan',	'पाटन'),
('156',	'12',	'Panchmahal',	'पंचमहल'),
('157',	'12',	'Porbandar',	'पोरबंदर'),
('158',	'12',	'Rajkot',	'राजकोट'),
('159',	'12',	'Sabarkantha',	'साबरकांठा'),
('160',	'12',	'Surendranagar',	'सुरेंद्रनगर'),
('161',	'12',	'Surat',	'सूरत'),
('162',	'12',	'Tapi',	'तापी'),
('163',	'12',	'Vadodara',	'वडोदरा'),
('164',	'12',	'Valsad',	'वलसाड'),
('165',	'13',	'Ambala',	'अंबाला'),
('166',	'13',	'Bhiwani',	'भिवानी'),
('167',	'13',	'Faridabad',	'फरीदाबाद'),
('168',	'13',	'Fatehabad',	'फतेहाबाद'),
('169',	'13',	'Gurgaon',	'गुडगाँव'),
('170',	'13',	'Hissar',	'हिसार'),
('171',	'13',	'Jhajjar',	'झज्जर'),
('172',	'13',	'Jind',	'जींद'),
('173',	'13',	'Karnal',	'करनाल'),
('174',	'13',	'Kaithal',	'कैथल'),
('175',	'13',	'Kurukshetra',	'कुरुक्षेत्र'),
('176',	'13',	'Mahendragarh',	'महेंद्रगढ़'),
('177',	'13',	'Mewat',	'मेवात'),
('178',	'13',	'Palwal',	'पलवल'),
('179',	'13',	'Panchkula',	'पंचकुला'),
('180',	'13',	'Panipat',	'पानीपत'),
('181',	'13',	'Rewari',	'रेवाड़ी'),
('182',	'13',	'Rohtak',	'रोहतक'),
('183',	'13',	'Sirsa',	'सिरसा'),
('184',	'13',	'Sonipat',	'सोनीपत'),
('185',	'13',	'Yamuna Nagar',	'यमुना नगर'),
('186',	'14',	'Bilaspur',	'बिलासपुर'),
('187',	'14',	'Chamba',	'चंबा'),
('188',	'14',	'Hamirpur',	'हमीरपुर'),
('189',	'14',	'Kangra',	'कांगड़ा'),
('190',	'14',	'Kinnaur',	'किन्नौर'),
('191',	'14',	'Kullu',	'कुल्लू'),
('192',	'14',	'Lahaul and Spiti',	'लाहौल और स्पीति'),
('193',	'14',	'Mandi',	'मंडी'),
('194',	'14',	'Shimla',	'शिमला'),
('195',	'14',	'Sirmaur',	'सिरमौर'),
('196',	'14',	'Solan',	'एक प्रकार का हंस'),
('197',	'14',	'Una',	'ऊना'),
('198',	'15',	'Anantnag',	'अनंतनाग'),
('199',	'15',	'Badgam',	'बड़गाम'),
('200',	'15',	'Bandipora',	'बांदीपुरा'),
('201',	'15',	'Baramulla',	'बारामूला'),
('202',	'15',	'Doda',	'डोडा'),
('203',	'15',	'Ganderbal',	'गांदरबल'),
('204',	'15',	'Jammu',	'जम्मू'),
('205',	'15',	'Kargil',	'कारगिल'),
('206',	'15',	'Kathua',	'कठुआ'),
('207',	'15',	'Kishtwar',	'किश्तवाड़'),
('208',	'15',	'Kupwara',	'कुपवाड़ा'),
('209',	'15',	'Kulgam',	'कुलगाम'),
('210',	'15',	'Leh',	'लेह'),
('211',	'15',	'Poonch',	'पूंछ'),
('212',	'15',	'Pulwama',	'पुलवामा'),
('213',	'15',	'Rajouri',	'राजौरी'),
('214',	'15',	'Ramban',	'रामबन'),
('215',	'15',	'Reasi',	'रियासी'),
('216',	'15',	'Samba',	'सांबा'),
('217',	'15',	'Shopian',	'शोपियां'),
('218',	'15',	'Srinagar',	'श्रीनगर'),
('219',	'15',	'Udhampur',	'उधमपुर'),
('220',	'16',	'Bokaro',	'बोकारो'),
('221',	'16',	'Chatra',	'चतरा'),
('222',	'16',	'Deoghar',	'देवघर'),
('223',	'16',	'Dhanbad',	'धनबाद'),
('224',	'16',	'Dumka',	'दुमका'),
('225',	'16',	'East Singhbhum',	'पूर्वी सिंहभाम'),
('226',	'16',	'Garhwa',	'गढ़वा'),
('227',	'16',	'Giridih',	'गिरिडीह'),
('228',	'16',	'Godda',	'गोड्डा'),
('229',	'16',	'Gumla',	'गुमला'),
('230',	'16',	'Hazaribag',	'हजारीबाग'),
('231',	'16',	'Jamtara',	'जामताड़ा'),
('232',	'16',	'Khunti',	'खूंटी'),
('233',	'16',	'Koderma',	'कोडरमा'),
('234',	'16',	'Latehar',	'लातेहार'),
('235',	'16',	'Lohardaga',	'लोहरदगा'),
('236',	'16',	'Pakur',	'पाकुर'),
('237',	'16',	'Palamu',	'पलामू'),
('238',	'16',	'Ramgarh',	'रामगढ़'),
('239',	'16',	'Ranchi',	'रांची'),
('240',	'16',	'Sahibganj',	'साहिबगंज'),
('241',	'16',	'Seraikela Kharsawan',	'सराइकेला खरसावाँ'),
('242',	'16',	'Simdega',	'सिमडेगा'),
('243',	'16',	'West Singhbhum',	'पश्चिम सिंहभाम'),
('244',	'17',	'Bagalkot',	'बागलकोट'),
('245',	'17',	'Bangalore Rural',	'बैंगलोर ग्रामीण'),
('246',	'17',	'Bangalore Urban',	'बैंगलोर शहरी'),
('247',	'17',	'Belgaum',	'बेलगाम'),
('248',	'17',	'Bellary',	'बेल्लारी'),
('249',	'17',	'Bidar',	'बीदर'),
('250',	'17',	'Bijapur',	'बीजापुर'),
('251',	'17',	'Chamarajnagar',	'चामराजनगर'),
('252',	'17',	'Chikkamagaluru',	'चिक्कामगलुरु'),
('253',	'17',	'Chikkaballapur',	'चिकबलपुर'),
('254',	'17',	'Chitradurga',	'चित्रदुर्ग'),
('255',	'17',	'Davanagere',	'दावनगेरे'),
('256',	'17',	'Dharwad',	'धारवाड़'),
('257',	'17',	'Dakshina Kannada',	'दक्षिणी कन्नड़'),
('258',	'17',	'Gadag',	'गडग'),
('259',	'17',	'Gulbarga',	'गुलबर्गा'),
('260',	'17',	'Hassan',	'हसन'),
('261',	'17',	'Haveri district',	'हावेरी जिला'),
('262',	'17',	'Kodagu',	'कोडागू'),
('263',	'17',	'Kolar',	'कोलार'),
('264',	'17',	'Koppal',	'कोप्पल'),
('265',	'17',	'Mandya',	'मंड्या'),
('266',	'17',	'Mysore',	'मैसूर'),
('267',	'17',	'Raichur',	'रायचूर'),
('268',	'17',	'Shimoga',	'शिमोगा'),
('269',	'17',	'Tumkur',	'तुमकुर'),
('270',	'17',	'Udupi',	'उडुपी'),
('271',	'17',	'Uttara Kannada',	'उत्तरा कन्नड़'),
('272',	'17',	'Ramanagara',	'रामनगर'),
('273',	'17',	'Yadgir',	'यादगीर'),
('274',	'18',	'Alappuzha',	'अलाप्पुझा'),
('275',	'18',	'Ernakulam',	'एर्नाकुलम'),
('276',	'18',	'Idukki',	'इडुक्की'),
('277',	'18',	'Kannur',	'कन्नूर'),
('278',	'18',	'Kasaragod',	'कासरगोड'),
('279',	'18',	'Kollam',	'कोल्लम'),
('280',	'18',	'Kottayam',	'कोट्टायम'),
('281',	'18',	'Kozhikode',	'कोझिकोड'),
('282',	'18',	'Malappuram',	'मलप्पुरम'),
('283',	'18',	'Palakkad',	'पलक्कड़'),
('284',	'18',	'Pathanamthitta',	'पथानामथिट्टा'),
('285',	'18',	'Thrissur',	'त्रिशूर'),
('286',	'18',	'Thiruvananthapuram',	'तिरुवनंतपुरम'),
('287',	'18',	'Wayanad',	'वायनाड'),
('288',	'19',	'Lakshadweep',	'लक्षद्वीप'),
('289',	'20',	'Agar',	'आगर'),
('290',	'20',	'Alirajpur',	'अलीराजपुर'),
('291',	'20',	'Anuppur',	'अनूपपुर'),
('292',	'20',	'Ashok Nagar',	'अशोक नगर'),
('293',	'20',	'Balaghat',	'बालाघाट'),
('294',	'20',	'Barwani',	'बड़वानी'),
('295',	'20',	'Betul',	'बेतुल'),
('296',	'20',	'Bhind',	'भिंड'),
('297',	'20',	'Bhopal',	'भोपाल'),
('298',	'20',	'Burhanpur',	'बुरहानपुर'),
('299',	'20',	'Chhatarpur',	'छतरपुर'),
('300',	'20',	'Chhindwara',	'छिंदवाड़ा'),
('301',	'20',	'Damoh',	'दमोह'),
('302',	'20',	'Datia',	'दतिया'),
('303',	'20',	'Dewas',	'देवास'),
('304',	'20',	'Dhar',	'धार'),
('305',	'20',	'Dindori',	'डिंडोरी'),
('306',	'20',	'Guna',	'गुना'),
('307',	'20',	'Gwalior',	'ग्वालियर'),
('308',	'20',	'Harda',	'हरदा'),
('309',	'20',	'Hoshangabad',	'होशंगाबाद'),
('310',	'20',	'Indore',	'इंदौर'),
('311',	'20',	'Jabalpur',	'जबलपुर'),
('312',	'20',	'Jhabua',	'झाबुआ'),
('313',	'20',	'Katni',	'कटनी'),
('314',	'20',	'Khandwa',	'खंडवा'),
('315',	'20',	'Khargone',	'खरगोन'),
('316',	'20',	'Mandla',	'मंडला'),
('317',	'20',	'Mandsaur',	'मंदसौर'),
('318',	'20',	'Morena',	'मुरैना'),
('319',	'20',	'Narsinghpur',	'नरसिंहपुर'),
('320',	'20',	'Neemuch',	'नीमच'),
('321',	'20',	'Panna',	'पन्ना'),
('322',	'20',	'Raisen',	'रायसेन'),
('323',	'20',	'Rajgarh',	'राजगढ़'),
('324',	'20',	'Ratlam',	'रतलाम'),
('325',	'20',	'Rewa',	'रीवा'),
('326',	'20',	'Sagar',	'सागर'),
('327',	'20',	'Satna',	'सतना'),
('328',	'20',	'Sehore',	'सीहोर'),
('329',	'20',	'Seoni',	'सिवनी'),
('330',	'20',	'Shahdol',	'शाहडोल'),
('331',	'20',	'Shajapur',	'शाजापुर'),
('332',	'20',	'Sheopur',	'श्योपुर'),
('333',	'20',	'Shivpuri',	'शिवपुरी'),
('334',	'20',	'Sidhi',	'सीधी'),
('335',	'20',	'Singrauli',	'सिंगरौली'),
('336',	'20',	'Tikamgarh',	'टीकमगढ़'),
('337',	'20',	'Ujjain',	'उज्जैन'),
('338',	'20',	'Umaria',	'उमरिया'),
('339',	'20',	'Vidisha',	'विदिशा'),
('340',	'21',	'Ahmednagar',	'अहमदनगर'),
('341',	'21',	'Akola',	'अकोला'),
('342',	'21',	'Amravati',	'अमरावती'),
('343',	'21',	'Aurangabad',	'औरंगाबाद'),
('344',	'21',	'Beed',	'बीड'),
('345',	'21',	'Bhandara',	'भंडारा'),
('346',	'21',	'Buldhana',	'बुलढाना'),
('347',	'21',	'Chandrapur',	'चंद्रपुर'),
('348',	'21',	'Dhule',	'धुले'),
('349',	'21',	'Gadchiroli',	'गडचिरोली'),
('350',	'21',	'Gondia',	'गोंदिया'),
('351',	'21',	'Hingoli',	'हिंगोली'),
('352',	'21',	'Jalgaon',	'जलगांव'),
('353',	'21',	'Jalna',	'जलना'),
('354',	'21',	'Kolhapur',	'कोल्हापुर'),
('355',	'21',	'Latur',	'लातूर'),
('356',	'21',	'Mumbai City',	'मुंबई सिटी'),
('357',	'21',	'Mumbai suburban',	'मुंबई उपनगरीय'),
('358',	'21',	'Nanded',	'नांदेड़'),
('359',	'21',	'Nandurbar',	'नंदुरबार'),
('360',	'21',	'Nagpur',	'नागपुर'),
('361',	'21',	'Nashik',	'नासिक'),
('362',	'21',	'Osmanabad',	'उस्मानाबाद'),
('363',	'21',	'Parbhani',	'परभनी'),
('364',	'21',	'Pune',	'पुणे'),
('365',	'21',	'Raigad',	'रायगढ़'),
('366',	'21',	'Ratnagiri',	'रत्नागिरी'),
('367',	'21',	'Sangli',	'सांगली'),
('368',	'21',	'Satara',	'सतारा'),
('369',	'21',	'Sindhudurg',	'सिंधुदुर्ग'),
('370',	'21',	'Solapur',	'सोलापुर'),
('371',	'21',	'Thane',	'थाइन'),
('372',	'21',	'Wardha',	'वर्धा'),
('373',	'21',	'Washim',	'वाशिम'),
('374',	'21',	'Yavatmal',	'यवतमाल'),
('375',	'22',	'Bishnupur',	'बिश्नुपुर'),
('376',	'22',	'Churachandpur',	'छुरछंदपुर'),
('377',	'22',	'Chandel',	'चंदेल'),
('378',	'22',	'Imphal East',	'इम्फाल ईस्ट'),
('379',	'22',	'Senapati',	'सेनापति'),
('380',	'22',	'Tamenglong',	'तामेंगलांग'),
('381',	'22',	'Thoubal',	'थौबल'),
('382',	'22',	'Ukhrul',	'उखरूल'),
('383',	'22',	'Imphal West',	'इम्फाल वेस्ट'),
('384',	'23',	'East Garo Hills',	'पूर्वी गारो हिल्स'),
('385',	'23',	'East Khasi Hills',	'पूर्वी खासी हिल्स'),
('386',	'23',	'Jaintia Hills',	'जेंतिया हिल्स'),
('387',	'23',	'Ri Bhoi',	'री भोई'),
('388',	'23',	'South Garo Hills',	'दक्षिण गारो हिल्स'),
('389',	'23',	'West Garo Hills',	'वेस्ट गारो हिल्स'),
('390',	'23',	'West Khasi Hills',	'पश्चिम खासी हिल्स'),
('391',	'24',	'Aizawl',	'आइजोल'),
('392',	'24',	'Champhai',	'चम्फाई'),
('393',	'24',	'Kolasib',	'कोलासिब'),
('394',	'24',	'Lawngtlai',	'लॉन्गतलाई'),
('395',	'24',	'Lunglei',	'लुंगलेई'),
('396',	'24',	'Mamit',	'मामित'),
('397',	'24',	'Saiha',	'सैहा'),
('398',	'24',	'Serchhip',	'सेरछिप'),
('399',	'25',	'Dimapur',	'दीमापुर'),
('400',	'25',	'Kiphire',	'किफायर'),
('401',	'25',	'Kohima',	'कोहिमा'),
('402',	'25',	'Longleng',	'लोंगलेंग'),
('403',	'25',	'Mokokchung',	'मोकोकचुंग'),
('404',	'25',	'Mon',	'सोम'),
('405',	'25',	'Peren',	'पेरेन'),
('406',	'25',	'Phek',	'फेक'),
('407',	'25',	'Tuensang',	'तुएनसांग'),
('408',	'25',	'Wokha',	'वोखा'),
('409',	'25',	'Zunheboto',	'ज़ुन्हेबोटो'),
('410',	'26',	'Angul',	'अंगुल'),
('411',	'26',	'Boudh',	'बौद्ध'),
('412',	'26',	'Bhadrak',	'भद्रक'),
('413',	'26',	'Balangir',	'बलांगीर'),
('414',	'26',	'Bargarh (Baragarh)',	'बरगढ़ (बारागढ़)'),
('415',	'26',	'Balasore',	'बालासोर'),
('416',	'26',	'Cuttack',	'कटक'),
('417',	'26',	'Debagarh (Deogarh)',	'देबागढ़ (देवगढ़)'),
('418',	'26',	'Dhenkanal',	'ढेंकनाल'),
('419',	'26',	'Ganjam',	'गंजम'),
('420',	'26',	'Gajapati',	'गजपति'),
('421',	'26',	'Jharsuguda',	'झारसुगुडा'),
('422',	'26',	'Jajpur',	'जाजपुर'),
('423',	'26',	'Jagatsinghpur',	'जगतसिंहपुर'),
('424',	'26',	'Khordha',	'खोर्धा'),
('425',	'26',	'Kendujhar (Keonjhar)',	'केंडुहर (केंजर)'),
('426',	'26',	'Kalahandi',	'कालाहांडी'),
('427',	'26',	'Kandhamal',	'कंधमाल'),
('428',	'26',	'Koraput',	'कोरापुट'),
('429',	'26',	'Kendrapara',	'केंद्रपाड़ा'),
('430',	'26',	'Malkangiri',	'मल्कानगिरी'),
('431',	'26',	'Mayurbhanj',	'मयूरभंज'),
('432',	'26',	'Nabarangpur',	'नबरंगपुर'),
('433',	'26',	'Nuapada',	'नुआपाड़ा'),
('434',	'26',	'Nayagarh',	'नयागढ़'),
('435',	'26',	'Puri',	'पुरी'),
('436',	'26',	'Rayagada',	'रायगढ़'),
('437',	'26',	'Sambalpur',	'संबलपुर'),
('438',	'26',	'Subarnapur (Sonepur)',	'सुबरनपुर (सोनपुर)'),
('439',	'26',	'Sundergarh',	'सुंदरगढ़'),
('440',	'27',	'Karaikal',	'कराईकल'),
('441',	'27',	'Mahe',	'माहे'),
('442',	'27',	'Pondicherry',	'पांडिचेरी'),
('443',	'27',	'Yanam',	'यानम'),
('444',	'28',	'Amritsar',	'अमृतसर'),
('445',	'28',	'Barnala',	'बरनाला'),
('446',	'28',	'Bathinda',	'बठिंडा'),
('447',	'28',	'Firozpur',	'फिरोजपुर'),
('448',	'28',	'Faridkot',	'फरीदकोट'),
('449',	'28',	'Fatehgarh Sahib',	'फतेहगढ़ साहिब'),
('450',	'28',	'Fazilka[6]',	'फाजिल्का [6]'),
('451',	'28',	'Gurdaspur',	'गुरदासपुर'),
('452',	'28',	'Hoshiarpur',	'होशियारपुर'),
('453',	'28',	'Jalandhar',	'जालंधर'),
('454',	'28',	'Kapurthala',	'कपूरथला'),
('455',	'28',	'Ludhiana',	'लुधियाना'),
('456',	'28',	'Mansa',	'मानसा'),
('457',	'28',	'Moga',	'मोगा'),
('458',	'28',	'Sri Muktsar Sahib',	'श्री मुक्तासर साहिब'),
('459',	'28',	'Pathankot',	'पठानकोट'),
('460',	'28',	'Patiala',	'पटियाला'),
('461',	'28',	'Rupnagar',	'रूपनगर'),
('462',	'28',	'Ajitgarh (Mohali)',	'अजीतगढ़ (मोहाली)'),
('463',	'28',	'Sangrur',	'संगरूर'),
('464',	'28',	'Shahid Bhagat Singh Nagar',	'शाहिद भगत सिंह नगर'),
('465',	'28',	'Tarn Taran',	'तरण तारण'),
('466',	'29',	'Ajmer',	'अजमेर'),
('467',	'29',	'Alwar',	'अलवर'),
('468',	'29',	'Bikaner',	'बीकानेर'),
('469',	'29',	'Barmer',	'बाड़मेर'),
('470',	'29',	'Banswara',	'बांसवाड़ा'),
('471',	'29',	'Bharatpur',	'भरतपुर'),
('472',	'29',	'Baran',	'बरन'),
('473',	'29',	'Bundi',	'बूंदी'),
('474',	'29',	'Bhilwara',	'भीलवाड़ा'),
('475',	'29',	'Churu',	'चुरू'),
('476',	'29',	'Chittorgarh',	'चित्तौड़गढ़'),
('477',	'29',	'Dausa',	'दौसा'),
('478',	'29',	'Dholpur',	'धौलपुर'),
('479',	'29',	'Dungapur',	'डूँगरपुर'),
('480',	'29',	'Ganganagar',	'गंगानगर'),
('481',	'29',	'Hanumangarh',	'हनुमानगढ़'),
('482',	'29',	'Jhunjhunu',	'झुंझुनू'),
('483',	'29',	'Jalore',	'जालोर'),
('484',	'29',	'Jodhpur',	'जोधपुर'),
('485',	'29',	'Jaipur',	'जयपुर'),
('486',	'29',	'Jaisalmer',	'जैसलमेर'),
('487',	'29',	'Jhalawar',	'झालावाड़'),
('488',	'29',	'Karauli',	'करौली'),
('489',	'29',	'Kota',	'कोटा'),
('490',	'29',	'Nagaur',	'नागौर'),
('491',	'29',	'Pali',	'पाली'),
('492',	'29',	'Pratapgarh',	'प्रतापगढ़'),
('493',	'29',	'Rajsamand',	'राजसमंद'),
('494',	'29',	'Sikar',	'सीकर'),
('495',	'29',	'Sawai Madhopur',	'सवाई माधोपुर'),
('496',	'29',	'Sirohi',	'सिरोही'),
('497',	'29',	'Tonk',	'टोंक'),
('498',	'29',	'Udaipur',	'उदयपुर'),
('499',	'30',	'East Sikkim',	'पूर्वी सिक्किम'),
('500',	'30',	'North Sikkim',	'उत्तर सिक्किम'),
('501',	'30',	'South Sikkim',	'दक्षिण सिक्किम'),
('502',	'30',	'West Sikkim',	'पश्चिम सिक्किम'),
('503',	'31',	'Ariyalur',	'अरियालुर'),
('504',	'31',	'Chennai',	'चेन्नई'),
('505',	'31',	'Coimbatore',	'कोयंबटूर'),
('506',	'31',	'Cuddalore',	'कुड्डालोर'),
('507',	'31',	'Dharmapuri',	'धर्मपुरी'),
('508',	'31',	'Dindigul',	'डिंडीगुल'),
('509',	'31',	'Erode',	'इरोड'),
('510',	'31',	'Kanchipuram',	'कांचीपुरम'),
('511',	'31',	'Kanyakumari',	'कन्याकूमारी'),
('512',	'31',	'Karur',	'करूर'),
('513',	'31',	'Krishnagiri',	'कृष्णागिरी'),
('514',	'31',	'Madurai',	'मदुरै'),
('515',	'31',	'Nagapattinam',	'नागपट्टिनम'),
('516',	'31',	'Nilgiris',	'नीलगिरी'),
('517',	'31',	'Namakkal',	'नमक्कल'),
('518',	'31',	'Perambalur',	'पेरम्बलुर'),
('519',	'31',	'Pudukkottai',	'पुदुक्कोट्टई'),
('520',	'31',	'Ramanathapuram',	'रामनाथपुरम'),
('521',	'31',	'Salem',	'सलेम'),
('522',	'31',	'Sivaganga',	'शिवगंगा'),
('523',	'31',	'Tirupur',	'तिरुपुर'),
('524',	'31',	'Tiruchirappalli',	'तिरुचिरापल्ली'),
('525',	'31',	'Theni',	'तब मैं'),
('526',	'31',	'Tirunelveli',	'तिरुनेलवेली'),
('527',	'31',	'Thanjavur',	'तंजावुर'),
('528',	'31',	'Thoothukudi',	'तूतूकुड़ी'),
('529',	'31',	'Tiruvallur',	'तिरुवल्लुर'),
('530',	'31',	'Tiruvarur',	'तिरुवरुर'),
('531',	'31',	'Tiruvannamalai',	'तिरुवन्नामलाई'),
('532',	'31',	'Vellore',	'वेल्लोर'),
('533',	'31',	'Viluppuram',	'विलुप्पुरम'),
('534',	'31',	'Virudhunagar',	'विरुधुनगर'),
('535',	'32',	'Dhalai',	'धलाई'),
('536',	'32',	'North Tripura',	'उत्तर त्रिपुरा'),
('537',	'32',	'South Tripura',	'दक्षिण त्रिपुरा'),
('538',	'32',	'Khowai[7]',	'खोवाई [7]'),
('539',	'32',	'West Tripura',	'पश्चिम त्रिपुरा'),
('540',	'33',	'Agra',	'आगरा'),
('541',	'33',	'Aligarh',	'अलीगढ़'),
('542',	'33',	'Allahabad',	'इलाहाबाद'),
('543',	'33',	'Ambedkar Nagar',	'अम्बेडकर नगर'),
('544',	'33',	'Auraiya',	'औरैया'),
('545',	'33',	'Azamgarh',	'आजमगढ़'),
('546',	'33',	'Bagpat',	'बागपत'),
('547',	'33',	'Bahraich',	'बहराइच'),
('548',	'33',	'Ballia',	'बलिया'),
('549',	'33',	'Balrampur',	'बलरामपुर'),
('550',	'33',	'Banda',	'बांदा'),
('551',	'33',	'Barabanki',	'बाराबंकी'),
('552',	'33',	'Bareilly',	'बरेली'),
('553',	'33',	'Basti',	'बस्ती'),
('554',	'33',	'Bijnor',	'बिजनौर'),
('555',	'33',	'Budaun',	'शाहजहांपुर'),
('556',	'33',	'Bulandshahr',	'बुलंदशहर'),
('557',	'33',	'Chandauli',	'चंदौली'),
('558',	'33',	'Chhatrapati Shahuji Maharaj Nagar',	'छत्रपति शाहूजी महाराज नगर'),
('559',	'33',	'Chitrakoot',	'चित्रकूट'),
('560',	'33',	'Deoria',	'देवरिया'),
('561',	'33',	'Etah',	'एटा'),
('562',	'33',	'Etawah',	'इटावा'),
('563',	'33',	'Faizabad',	'फैजाबाद'),
('564',	'33',	'Farrukhabad',	'फर्रुखाबाद'),
('565',	'33',	'Fatehpur',	'फतेहपुर'),
('566',	'33',	'Firozabad',	'फिरोजाबाद'),
('567',	'33',	'Gautam Buddh Nagar',	'गौतम बुद्ध नगर'),
('568',	'33',	'Ghaziabad',	'गाज़ियाबाद'),
('569',	'33',	'Ghazipur',	'गाजीपुर'),
('570',	'33',	'Gonda',	'गोंडा'),
('571',	'33',	'Gorakhpur',	'गोरखपुर'),
('572',	'33',	'Hamirpur',	'हमीरपुर'),
('573',	'33',	'Hardoi',	'हरदोई'),
('574',	'33',	'Hathras',	'हाथरस'),
('575',	'33',	'Jalaun',	'जालौन'),
('576',	'33',	'Jaunpur district',	'जौनपुर जिला।'),
('577',	'33',	'Jhansi',	'झांसी'),
('578',	'33',	'Jyotiba Phule Nagar',	'ज्योतिबा फुले नगर'),
('579',	'33',	'Kannauj',	'कन्नौज'),
('580',	'33',	'Kanpur',	'कानपुर'),
('581',	'33',	'Kanshi Ram Nagar',	'कंशी राम नगर'),
('582',	'33',	'Kaushambi',	'कौशाम्बी'),
('583',	'33',	'Kushinagar',	'कुशीनगर'),
('584',	'33',	'Lakhimpur Kheri',	'लखीमपुर खेरी'),
('585',	'33',	'Lalitpur',	'ललितपुर'),
('586',	'33',	'Lucknow',	'लखनऊ'),
('587',	'33',	'Maharajganj',	'महाराजगंज'),
('588',	'33',	'Mahoba',	'महोबा'),
('589',	'33',	'Mainpuri',	'मैनपुरी'),
('590',	'33',	'Mathura',	'मथुरा'),
('591',	'33',	'Mau',	'मऊ'),
('592',	'33',	'Meerut',	'मेरठ'),
('593',	'33',	'Mirzapur',	'मिर्जापुर'),
('594',	'33',	'Moradabad',	'मुरादाबाद'),
('595',	'33',	'Muzaffarnagar',	'मुजफ्फरनगर'),
('596',	'33',	'Panchsheel Nagar district (Hapur)',	'पंचशील नगर जिला (हापुर)'),
('597',	'33',	'Pilibhit',	'पीलीभीत'),
('598',	'33',	'Pratapgarh',	'प्रतापगढ़'),
('599',	'33',	'Raebareli',	'रायबरेली'),
('600',	'33',	'Ramabai Nagar (Kanpur Dehat)',	'रामाबाई नगर (कानपुर देहाट)'),
('601',	'33',	'Rampur',	'रामपुर'),
('602',	'33',	'Saharanpur',	'सहारनपुर'),
('603',	'33',	'Sant Kabir Nagar',	'संत कबीर नगर'),
('604',	'33',	'Sant Ravidas Nagar',	'संत रविदास नगर'),
('605',	'33',	'Shahjahanpur',	'शाहजहांपुर'),
('606',	'33',	'Shamli',	'शामली'),
('607',	'33',	'Shravasti',	'श्रावस्ती'),
('608',	'33',	'Siddharthnagar',	'सिद्धार्थनगर'),
('609',	'33',	'Sitapur',	'सीतापुर'),
('610',	'33',	'Sonbhadra',	'सोनभद्र'),
('611',	'33',	'Sultanpur',	'सुल्तानपुर'),
('612',	'33',	'Unnao',	'उन्नाव'),
('613',	'33',	'Varanasi',	'वाराणसी'),
('614',	'34',	'Almora',	'अल्मोड़ा'),
('615',	'34',	'Bageshwar',	'बागेश्वर'),
('616',	'34',	'Chamoli',	'चमोली'),
('617',	'34',	'Champawat',	'चम्पावत'),
('618',	'34',	'Dehradun',	'देहरादून'),
('619',	'34',	'Haridwar',	'हरिद्वार'),
('620',	'34',	'Nainital',	'नैनीताल'),
('621',	'34',	'Pauri Garhwal',	'पौरी गढ़वाल'),
('622',	'34',	'Pithoragarh',	'पिथोरागढ़'),
('623',	'34',	'Rudraprayag',	'रुद्रप्रयाग'),
('624',	'34',	'Tehri Garhwal',	'तेहरी गढ़वाल'),
('625',	'34',	'Udham Singh Nagar',	'उधम सिंह नगर'),
('626',	'34',	'Uttarkashi',	'उत्तरकाशी'),
('627',	'35',	'Bankura',	'बांकुड़ा'),
('628',	'35',	'Bardhaman',	'बर्धमान'),
('629',	'35',	'Birbhum',	'बीरभूम'),
('630',	'35',	'Cooch Behar',	'कूच बिहार'),
('631',	'35',	'Dakshin Dinajpur',	'दक्षिण दिनाजपुर'),
('632',	'35',	'Darjeeling',	'दार्जिलिंग'),
('633',	'35',	'Hooghly',	'हुगली'),
('634',	'35',	'Howrah',	'हावड़ा'),
('635',	'35',	'Jalpaiguri',	'जलपाईगुड़ी'),
('636',	'35',	'Kolkata',	'कोलकाता'),
('637',	'35',	'Maldah',	'मालदा'),
('638',	'35',	'Murshidabad',	'मुर्शिदाबाद'),
('639',	'35',	'Nadia',	'नादिया'),
('640',	'35',	'North 24 Parganas',	'उत्तर 24 परगना'),
('641',	'35',	'Paschim Medinipur',	'पासिम मेदिनीपुर'),
('642',	'35',	'Purba Medinipur',	'पूरब मेदिनीपुर'),
('643',	'35',	'Purulia',	'पुरुलिया'),
('644',	'35',	'South 24 Parganas',	'दक्षिण 24 परगना'),
('645',	'35',	'Uttar Dinajpur',	'उत्तर दिनाजपुर');

DROP TABLE IF EXISTS `mm_drinking_smoking`;
CREATE TABLE `mm_drinking_smoking` (
  `drinking_smoking_id` int(11) NOT NULL AUTO_INCREMENT,
  `drinking_smoking` tinytext NOT NULL,
  PRIMARY KEY (`drinking_smoking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_drinking_smoking` (`drinking_smoking_id`, `drinking_smoking`) VALUES
(1,	'Yes'),
(2,	'No'),
(3,	'Occasionally');

DROP TABLE IF EXISTS `mm_events`;
CREATE TABLE `mm_events` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `image` varchar(1000) NOT NULL,
  `language` varchar(10) NOT NULL COMMENT 'language code',
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_family_status`;
CREATE TABLE `mm_family_status` (
  `family_statu_id` int(11) NOT NULL AUTO_INCREMENT,
  `family_status` tinytext NOT NULL,
  PRIMARY KEY (`family_statu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_family_status` (`family_statu_id`, `family_status`) VALUES
(1,	'Rich/Affluent'),
(2,	'Upper Middle Class'),
(3,	'Middle Class');

DROP TABLE IF EXISTS `mm_family_type`;
CREATE TABLE `mm_family_type` (
  `family_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `family_type` tinytext NOT NULL,
  PRIMARY KEY (`family_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_family_type` (`family_type_id`, `family_type`) VALUES
(1,	'Joint Family'),
(2,	'Nuclear Family'),
(3,	'Other');

DROP TABLE IF EXISTS `mm_family_values`;
CREATE TABLE `mm_family_values` (
  `family_values_id` int(11) NOT NULL AUTO_INCREMENT,
  `family_values` tinytext NOT NULL,
  PRIMARY KEY (`family_values_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_family_values` (`family_values_id`, `family_values`) VALUES
(1,	'Orthodox'),
(2,	'Conservative'),
(3,	'Moderate'),
(4,	'Liberal');

DROP TABLE IF EXISTS `mm_faq`;
CREATE TABLE `mm_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `language` varchar(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_faq` (`faq_id`, `faq`, `language`, `language_id`, `created_at`, `updated_at`) VALUES
(1,	'<h3 id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><strong><span lang=\"en\">Instructions</span></strong></h3>\r\n<h4 id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><strong><span lang=\"en\">informations</span></strong></h4>\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><span lang=\"en\">By using the services on this website, you [the user] agree to the terms of this privacy policy. <br />When you submit information via this website or directly in restaurants, you consent to the handling, collection, <br />use and disclosure of the information in accordance with this Privacy Policy and the laws currently in effect in the territory of India. <br />Although all efforts have been made to ensure the accuracy and practicality of the content on this portal, <br />it should not be interpreted as a legal statement or used for any legal purposes.</span></pre>',	'en',	1,	'2020-10-15 11:35:50',	'2020-10-16 07:00:05'),
(2,	'<p><strong>अनुदेश </strong></p>\r\n<p><strong>जानकारियां </strong></p>\r\n<p>इस वेबसाइट पर सेवाओं का उपयोग करके, आप [उपयोगकर्ता] इस गोपनीयता नीति की शर्तों से सहमत हैं। जब आप इस वेबसाइट के माध्यम से या रेस्तरां में सीधे जानकारी जमा करते हैं, तो आप हैंडलिंग, संग्रह के लिए सहमति देते हैं, इस गोपनीयता नीति और वर्तमान में भारत के क्षेत्र में लागू कानूनों के अनुसार जानकारी का उपयोग और प्रकटीकरण। यद्यपि इस पोर्टल पर सामग्री की सटीकता और व्यावहारिकता सुनिश्चित करने के लिए सभी प्रयास किए गए हैं, इसे एक कानूनी कथन के रूप में व्याख्यायित नहीं किया जाना चाहिए या किसी कानूनी उद्देश्यों के लिए उपयोग नहीं किया जाना चाहिए।</p>',	'hi',	2,	'2020-10-15 11:36:22',	'2020-10-16 07:13:02'),
(3,	'',	'mr',	3,	'2020-10-17 07:11:40',	'2020-10-17 07:12:21');

DROP TABLE IF EXISTS `mm_fathers_occupation`;
CREATE TABLE `mm_fathers_occupation` (
  `fathers_occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `fathers_occupation` tinytext NOT NULL,
  PRIMARY KEY (`fathers_occupation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_fathers_occupation` (`fathers_occupation_id`, `fathers_occupation`) VALUES
(1,	'Business/Entrepreneur'),
(2,	'Service - Private'),
(3,	'Service - Govt./PSU'),
(4,	'Army/Armed Forces'),
(5,	'Civil Services'),
(6,	'Retired'),
(7,	'Not Employed'),
(8,	'Expired');

DROP TABLE IF EXISTS `mm_firebase_key`;
CREATE TABLE `mm_firebase_key` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `firebase_key` text NOT NULL,
  `updated_at` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_firebase_key` (`id`, `firebase_key`, `updated_at`) VALUES
(1,	'AAAAZ8xb-oI:APA91bGw1_4enflqKMk7laZxMBYgPvkFlrSh8M8Ju1y4SnGpeQv2Bu8u7GbDBU4uuAQZduVNRpU86UgEcYUZGjcgC_HiULyAUoPeSE4lHOFjlIseAx9NkBfg8QhybRatMkFqI7Z-v8Ao',	'2021-01-21 16:31:37');

DROP TABLE IF EXISTS `mm_gender`;
CREATE TABLE `mm_gender` (
  `gender_id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` varchar(22) NOT NULL,
  `looking_for` varchar(22) NOT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_gender` (`gender_id`, `gender`, `looking_for`) VALUES
(1,	'Male',	'Groom'),
(2,	'Female',	'Bride');

DROP TABLE IF EXISTS `mm_happy_story`;
CREATE TABLE `mm_happy_story` (
  `happy_story_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8mb4 NOT NULL,
  `description` text CHARACTER SET utf8mb4 NOT NULL,
  `image` tinytext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1.active, 0.inactive',
  `created_at` tinytext NOT NULL,
  `updated_at` tinytext NOT NULL,
  PRIMARY KEY (`happy_story_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_happy_story` (`happy_story_id`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Kanchan & Venkatesh',	'Thanks to Matrimony team!! I finally got my dream partner with whom i would love to walk a journey of life. She is truly a gods gift for me and Shaad',	'1606818115.jpg',	1,	'2020-12-01 15:51:55',	''),
(2,	'Abhinav & Vertika',	'Thanks to Matrimony team!! I finally got my dream partner with whom i would love to walk a journey of life. She is truly a gods gift for me and Shaad',	'1606819502.jpg',	1,	'2020-12-01 16:15:02',	'2020-12-01 18:02:20');

DROP TABLE IF EXISTS `mm_heights`;
CREATE TABLE `mm_heights` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `height` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mm_heights` (`id`, `height`) VALUES
(1,	'4\' 0\" (1.22 mts)'),
(2,	'4\' 1\" (1.24 mts)'),
(3,	'4\' 2\" (1.28 mts)'),
(4,	'4\' 3\" (1.31 mts)'),
(5,	'4\' 4\" (1.34 mts)'),
(6,	'4\' 5\" (1.35 mts)'),
(7,	'4\' 6\" (1.37 mts)'),
(8,	'4\' 7\" (1.40 mts)'),
(9,	'4\' 8\" (1.42 mts)'),
(10,	'4\' 9\" (1.45 mts)'),
(11,	'4\' 10\" (1.47 mts)'),
(12,	'4\' 11\" (1.50 mts)'),
(13,	'5\' 0\" (1.52 mts)'),
(14,	'5\' 1\" (1.55 mts)'),
(15,	'5\' 2\" (1.58 mts)'),
(16,	'5\' 3\" (1.60 mts)'),
(17,	'5\' 4\" (1.63 mts)'),
(18,	'5\' 5\" (1.65 mts)'),
(19,	'5\' 6\" (1.68 mts)'),
(20,	'5\' 7\" (1.70 mts)'),
(21,	'5\' 8\" (1.73 mts)'),
(22,	'5\' 9\" (1.75 mts)'),
(23,	'5\' 10\" (1.78 mts)'),
(24,	'5\' 11\" (1.80 mts)'),
(25,	'6\' 0\" (1.83 mts)'),
(26,	'6\' 1\" (1.85 mts)'),
(27,	'6\' 2\" (1.88 mts)'),
(28,	'6\' 3\" (1.91 mts)'),
(29,	'6\' 4\" (1.93 mts)'),
(30,	'6\' 5\" (1.96 mts)'),
(31,	'6\' 6\" (1.98 mts)'),
(32,	'6\' 7\" (2.01 mts)'),
(33,	'6\' 8\" (2.03 mts)'),
(34,	'6\' 9\" (2.06 mts)'),
(35,	'6\' 10\" (2.08 mts)'),
(36,	'6\' 11\" (2.11 mts)'),
(37,	'7\' (2.13 mts) plus');

DROP TABLE IF EXISTS `mm_hobbies`;
CREATE TABLE `mm_hobbies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 NOT NULL,
  `name_hi` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_hobbies` (`id`, `name`, `name_hi`) VALUES
(1,	'Collecting Stamps',	'टिकटों का संग्रह'),
(2,	'Collecting Coins',	'सिक्के इकट्ठा करना'),
(3,	'Collecting antiques',	'प्राचीन वस्तुओं को इकट्ठा करना'),
(4,	'Art / Handicraft',	'कला / हस्तशिल्प'),
(5,	'Painting',	'चित्रकार'),
(6,	'Cooking',	'खाना बनाना'),
(7,	'Photography',	'फोटोग्राफी'),
(8,	'Film-making',	'फिल्म-निर्माण'),
(9,	'Model building',	'मॉडल बनाना'),
(10,	'Gardening / Landscaping',	'बागवानी / भूनिर्माण'),
(11,	'Fishing',	'मछली पकड़ना'),
(12,	'Bird watching',	'पंछी देखना'),
(13,	'Taking care of pets',	'पालतू जानवरों की देखभाल करना'),
(14,	'Playing musical instruments',	'संगीत वाद्ययंत्र बजाना'),
(15,	'Singing',	'गायन'),
(16,	'Dancing',	'नृत्य'),
(17,	'Acting',	'अभिनय'),
(18,	'Ham radio',	'हैम रेडियो'),
(19,	'Astrology / Palmistry / Numerology',	'ज्योतिष / हस्तरेखा / संख्याशास्त्र'),
(20,	'Graphology',	'हस्तलेख का विज्ञान'),
(21,	'Solving Crosswords, Puzzles',	'हल करने वाले पहेली, पहेलियाँ');

DROP TABLE IF EXISTS `mm_income`;
CREATE TABLE `mm_income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `income` varchar(191) NOT NULL,
  `language` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1 active, 0 inactive',
  `created_at` varchar(200) NOT NULL,
  `updated_at` varchar(200) NOT NULL,
  PRIMARY KEY (`income_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mm_income` (`income_id`, `income`, `language`, `status`, `created_at`, `updated_at`) VALUES
(0,	'',	'en',	0,	'',	''),
(1,	'Rs. No Income',	'en',	1,	'',	''),
(2,	'Rs.0 - 1 Lakh',	'en',	1,	'',	''),
(3,	'Rs.1 - 2 Lakh',	'en',	1,	'',	''),
(4,	'Rs.2 - 3 Lakh',	'en',	1,	'',	''),
(5,	'Rs.3 - 4 Lakh',	'en',	1,	'',	''),
(6,	'Rs.4 - 5Lakh',	'en',	1,	'',	''),
(7,	'Rs.5 - 8 Lakh',	'en',	1,	'',	''),
(8,	'Rs.8 - 10 Lakh',	'en',	1,	'',	''),
(9,	'Rs.10 - 15 Lakh',	'en',	1,	'',	''),
(10,	'Rs.15 - 20 Lakh',	'en',	1,	'',	''),
(11,	'Rs.20 - 35 Lakh',	'en',	1,	'',	''),
(12,	'Rs.35 - 50 Lakh',	'en',	1,	'',	''),
(13,	'Rs.50 Lakh & above',	'en',	1,	'',	'2020-10-19 09:25:14'),
(14,	'कोई आय नही',	'hi',	1,	'2020-10-19 09:33:41',	''),
(15,	'रु 0 - 1 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(16,	'रु 1 - 2 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(17,	'रु 2 - 3 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(18,	'रु 3 - 4 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(19,	'रु 4 - 5 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(20,	'रु 5 - 8 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(21,	'रु 8 - 10 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(22,	'रु 10 - 15 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(23,	'रु 15 - 20 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(24,	'रु 20 - 35 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(25,	'रु 35 - 50 लाख ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(26,	'रु 50 लाख  के ऊपर ',	'hi',	1,	'2020-10-19 09:33:41',	''),
(27,	'रु. उत्पन्न नाही',	'mr',	1,	'',	''),
(28,	'रु 0 - 1 लाख',	'mr',	1,	'',	''),
(29,	'रु 1 - 2 लाख',	'mr',	1,	'',	''),
(30,	'रु 2 - 3 लाख',	'mr',	1,	'',	''),
(31,	'रु 3 - 4 लाख',	'mr',	1,	'',	''),
(32,	'रु 4 - 5 लाख',	'mr',	1,	'',	''),
(33,	'रु 5 - 8 लाख',	'mr',	1,	'',	''),
(34,	'रु 8 - 10 लाख',	'mr',	1,	'',	''),
(35,	'रु 10 - 15 लाख',	'mr',	1,	'',	''),
(36,	'रु 15 - 20 लाख',	'mr',	1,	'',	''),
(37,	'रु 20 - 35 लाख',	'mr',	1,	'',	''),
(38,	'रु 20 - 35 लाख',	'mr',	1,	'',	''),
(39,	'रु 50 लाख आणि त्याहून अधिक',	'mr',	1,	'',	'');

DROP TABLE IF EXISTS `mm_interest`;
CREATE TABLE `mm_interest` (
  `interest_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(9) NOT NULL,
  `requested_id` varchar(9) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0 pending, 1 approved by request reciever, 2 approved by admin',
  `created_at` tinytext NOT NULL,
  `updated_at` tinytext NOT NULL,
  PRIMARY KEY (`interest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_interest_user`;
CREATE TABLE `mm_interest_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 NOT NULL,
  `name_hi` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_interest_user` (`id`, `name`, `name_hi`) VALUES
(1,	'Writing',	'लेखन'),
(2,	'Reading / Book clubs',	'पढ़ना'),
(3,	'Learning new languages',	'नई भाषा सीखना'),
(4,	'Listening to music',	'संगीत सुनना'),
(5,	'Movies',	'फिल्म'),
(6,	'Theatre',	'थिएटर'),
(7,	'Watching television',	'टेलीविजन देखना'),
(8,	'Travel / Sightseeing',	'यात्रा / पर्यटन स्थलों का भ्रमण'),
(9,	'Sports - Outdoor',	'खेल - आउटडोर'),
(10,	'Sports - Indoor',	'खेल - इंडोर'),
(11,	'Trekking / Adventure sports',	'ट्रेकिंग / एडवेंचर स्पोर्ट्स'),
(12,	'Video / Computer games',	'वीडियो / कंप्यूटर गेम'),
(13,	'Health & Fitness',	'स्वास्थ्य'),
(14,	'Yoga / Meditation',	'योग / ध्यान'),
(15,	'Alternative healing',	'वैकल्पिक उपचार'),
(16,	'Volunteering / Social Service',	'स्वयंसेवी / सामाजिक सेवा'),
(17,	'Politics',	'राजनीति'),
(18,	'Net surfing',	'नेट सर्फिंग'),
(19,	'Blogging',	'ब्लॉगिंग');

DROP TABLE IF EXISTS `mm_language`;
CREATE TABLE `mm_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_code` varchar(55) NOT NULL,
  `language_name` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1. active 0. inactive',
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_language` (`language_id`, `language_code`, `language_name`, `status`, `created_at`, `updated_at`) VALUES
(1,	'en',	'English',	1,	'2020-10-15 11:35:50',	''),
(2,	'hi',	'हिन्दी',	1,	'2020-10-15 11:36:22',	''),
(3,	'mr',	'मराठी',	1,	'2020-10-17 07:11:40',	'2020-10-17 07:12:21');

DROP TABLE IF EXISTS `mm_manglik`;
CREATE TABLE `mm_manglik` (
  `manglik_id` int(11) NOT NULL AUTO_INCREMENT,
  `manglik` varchar(191) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1. active 0. inactive',
  `language` varchar(25) NOT NULL COMMENT 'language code',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  PRIMARY KEY (`manglik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mm_manglik` (`manglik_id`, `manglik`, `status`, `language`, `created_at`, `updated_at`) VALUES
(1,	'Manglik',	1,	'en',	'',	'2020-10-17 11:27:32'),
(2,	'Non Manglik',	1,	'en',	'',	'2020-10-17 11:27:22'),
(3,	'Angshik (Partial Manglik)',	1,	'en',	'',	'2020-10-17 11:27:08'),
(4,	'मांगलिक',	1,	'hi',	'2020-10-17 11:28:03',	''),
(5,	'गैर मंगलिक',	1,	'hi',	'2020-10-17 11:28:22',	''),
(6,	'अंगिक (आंशिक मंगलिक)	',	1,	'hi',	'2020-10-17 11:28:35',	''),
(7,	'मांगलिक',	1,	'mr',	'2020-10-17 11:29:36',	''),
(8,	'नॉन मांगलिक',	1,	'mr',	'2020-10-17 11:31:56',	'');

DROP TABLE IF EXISTS `mm_marital_status`;
CREATE TABLE `mm_marital_status` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(191) NOT NULL,
  `language` varchar(25) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mm_marital_status` (`id`, `marital_status`, `language`, `status`) VALUES
(1,	'Unmarried',	'en',	1),
(2,	'Second Marriage',	'en',	1),
(3,	'अविवाहित',	'hi',	1),
(4,	'दूसरा विवाह',	'hi',	1),
(5,	'अविवाहित',	'mr',	1),
(6,	'दुसरे लग्न',	'mr',	1);

DROP TABLE IF EXISTS `mm_message`;
CREATE TABLE `mm_message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_head_id` int(11) NOT NULL,
  `message` varchar(2555) CHARACTER SET utf8 NOT NULL,
  `from_user_id` varchar(100) NOT NULL,
  `to_user_id` varchar(100) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1. text 2.image',
  `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0. Not Read 1. Read',
  `media` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_message_head`;
CREATE TABLE `mm_message_head` (
  `message_head_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user_id` varchar(11) NOT NULL,
  `to_user_id` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `block_status` int(1) NOT NULL DEFAULT '1',
  `blocked_by` varchar(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`message_head_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_mothers_occupation`;
CREATE TABLE `mm_mothers_occupation` (
  `mothers_occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `mothers_occupation` tinytext NOT NULL,
  PRIMARY KEY (`mothers_occupation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_mothers_occupation` (`mothers_occupation_id`, `mothers_occupation`) VALUES
(1,	'Housewife'),
(2,	'Business/Entrepreneur'),
(3,	'Service-Private'),
(4,	'Service-Govt/PSU'),
(5,	'Army/Armed forces'),
(6,	'Civil Services'),
(7,	'Teacher'),
(8,	'Retired'),
(9,	'Expired');

DROP TABLE IF EXISTS `mm_notification`;
CREATE TABLE `mm_notification` (
  `id` int(111) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(111) NOT NULL DEFAULT '0',
  `title` text CHARACTER SET utf8mb4 NOT NULL,
  `message` text CHARACTER SET utf8mb4 NOT NULL,
  `type` varchar(11) NOT NULL COMMENT '1. broadcast 2. personal ',
  `created_at` varchar(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_occupation`;
CREATE TABLE `mm_occupation` (
  `occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `occupation` varchar(191) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive',
  `language` varchar(25) NOT NULL COMMENT 'language code',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  PRIMARY KEY (`occupation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mm_occupation` (`occupation_id`, `occupation`, `status`, `language`, `created_at`, `updated_at`) VALUES
(0,	'Details Not available',	1,	'en',	'',	''),
(1,	'Looking for a job',	1,	'en',	'',	''),
(2,	'Not working',	1,	'en',	'',	''),
(3,	'Actor/Model',	1,	'en',	'',	''),
(4,	'Advertising Professional',	1,	'en',	'',	''),
(5,	'Agent',	1,	'en',	'',	''),
(6,	'Air Hostess',	1,	'en',	'',	''),
(7,	'Analyst',	1,	'en',	'',	''),
(8,	'Architect',	1,	'en',	'',	''),
(9,	'BPO/ITeS',	1,	'en',	'',	''),
(10,	'Banking Professional',	1,	'en',	'',	''),
(11,	'Beautician',	1,	'en',	'',	''),
(12,	'Businessperson',	1,	'en',	'',	''),
(13,	'Chartered accountant',	1,	'en',	'',	''),
(14,	'Civil Services (IAS/ IFS/ IPS/ IRS)',	1,	'en',	'',	''),
(15,	'Consultant',	1,	'en',	'',	''),
(16,	'Corporate Communication',	1,	'en',	'',	''),
(17,	'Corporate Planning Professional',	1,	'en',	'',	''),
(18,	'Customer Services',	1,	'en',	'',	''),
(19,	'Cyber / Network Security',	1,	'en',	'',	''),
(20,	'Defence',	1,	'en',	'',	''),
(21,	'Doctor',	1,	'en',	'',	''),
(22,	'Education Professional',	1,	'en',	'',	''),
(23,	'Engineer - Non IT',	1,	'en',	'',	''),
(24,	'Farming',	1,	'en',	'',	''),
(25,	'Fashion Designer',	1,	'en',	'',	''),
(26,	'Film Professional',	1,	'en',	'',	''),
(27,	'Financial Services/Accounting',	1,	'en',	'',	''),
(28,	'Fitness Professional',	1,	'en',	'',	''),
(29,	'Govt. Services',	1,	'en',	'',	''),
(30,	'HR Professional',	1,	'en',	'',	''),
(31,	'Hardware/Telecom',	1,	'en',	'',	''),
(32,	'Healthcare Professional',	1,	'en',	'',	''),
(33,	'Hotels/Hospitality Professional',	1,	'en',	'',	''),
(34,	'Interior Designer',	1,	'en',	'',	''),
(35,	'Journalist',	1,	'en',	'',	''),
(36,	'Lawyer/Legal Professional',	1,	'en',	'',	''),
(37,	'Logistics/SCM Professional',	1,	'en',	'',	''),
(38,	'Manager',	1,	'en',	'',	''),
(39,	'Marketing Professional',	1,	'en',	'',	''),
(40,	'Media Professional',	1,	'en',	'',	''),
(41,	'Merchant Navy',	1,	'en',	'',	''),
(42,	'NGO/Social Services',	1,	'en',	'',	''),
(43,	'Nurse',	1,	'en',	'',	''),
(44,	'Office Admin',	1,	'en',	'',	''),
(45,	'Operator/Technician',	1,	'en',	'',	''),
(46,	'Physiotherapist',	1,	'en',	'',	''),
(47,	'Pilot',	1,	'en',	'',	''),
(48,	'Police',	1,	'en',	'',	''),
(49,	'Private Security',	1,	'en',	'',	''),
(50,	'Product manager',	1,	'en',	'',	''),
(51,	'Professor/Lecturer',	1,	'en',	'',	''),
(52,	'Program Manager',	1,	'en',	'',	''),
(53,	'Project Manager - IT',	1,	'en',	'',	''),
(54,	'Project Manager - Non IT',	1,	'en',	'',	''),
(55,	'Psychologist',	1,	'en',	'',	''),
(56,	'Research Professional',	1,	'en',	'',	''),
(57,	'Sales Professional',	1,	'en',	'',	''),
(58,	'Scientist',	1,	'en',	'',	''),
(59,	'Secretary/Front Office',	1,	'en',	'',	''),
(60,	'Security Professional',	1,	'en',	'',	''),
(61,	'Self Employed',	1,	'en',	'',	''),
(62,	'Software Professional',	1,	'en',	'',	''),
(63,	'Sportsperson',	1,	'en',	'',	''),
(64,	'Student',	1,	'en',	'',	''),
(65,	'Teacher',	1,	'en',	'',	''),
(66,	'Top Management (CXO, M.D. etc.)',	1,	'en',	'',	''),
(67,	'UI/UX designer',	1,	'en',	'',	''),
(68,	'Web/Graphic Design',	1,	'en',	'',	''),
(69,	'Others',	1,	'en',	'',	''),
(71,	'विवरण उपलब्ध नहीं है',	1,	'hi',	'',	''),
(72,	'नौकरी की तलाश में',	1,	'hi',	'',	''),
(73,	'कार्य नहीं करते',	1,	'hi',	'',	''),
(74,	'अभिनेता / मॉडल',	1,	'hi',	'',	''),
(75,	'विज्ञापन पेशेवर',	1,	'hi',	'',	''),
(76,	'एजेंट',	1,	'hi',	'',	''),
(77,	'एयर होस्टेस',	1,	'hi',	'',	''),
(78,	'विश्लेषक',	1,	'hi',	'',	''),
(79,	'वास्तुकार',	1,	'hi',	'',	''),
(80,	'बीपीओ / आईटीईएस',	1,	'hi',	'',	''),
(81,	'बैंकिंग ',	1,	'hi',	'',	''),
(82,	'कस्मेटिकस',	1,	'hi',	'',	''),
(83,	'व्यापारी',	1,	'hi',	'',	''),
(84,	'चार्टर्ड एकाउंटेंट',	1,	'hi',	'',	''),
(85,	'सिविल सेवा',	1,	'hi',	'',	''),
(86,	'सलाहकार',	1,	'hi',	'',	''),
(87,	'व्यावसायिक संस्था का संचार तंत्र',	1,	'hi',	'',	''),
(88,	'कॉर्पोरेट योजना',	1,	'hi',	'',	''),
(89,	'ग्राहक सेवाएं',	1,	'hi',	'',	''),
(90,	'साइबर / नेटवर्क सुरक्षा',	1,	'hi',	'',	''),
(91,	'आर्मी',	1,	'hi',	'',	''),
(92,	'डॉक्टर',	1,	'hi',	'',	''),
(93,	'शिक्षा पेशेवर',	1,	'hi',	'',	''),
(94,	'अभियंता - गैर आईटी',	1,	'hi',	'',	''),
(95,	'खेती',	1,	'hi',	'',	''),
(96,	'फैशन डिजाइनर',	1,	'hi',	'',	''),
(97,	'फिल्म',	1,	'hi',	'',	''),
(98,	'वित्तीय सेवाएं / लेखा',	1,	'hi',	'',	''),
(99,	'स्वास्थ्य ',	1,	'hi',	'',	''),
(100,	'सरकारी सेवाएं',	1,	'hi',	'',	''),
(101,	'मानव संसाधन',	1,	'hi',	'',	''),
(102,	'हार्डवेयर / दूरसंचार',	1,	'hi',	'',	''),
(103,	'स्वास्थ्यकर्मी',	1,	'hi',	'',	''),
(104,	'होटल / आतिथ्य',	1,	'hi',	'',	''),
(105,	'आं‍तरिक सज्जाकार',	1,	'hi',	'',	''),
(106,	'पत्रकार',	1,	'hi',	'',	''),
(107,	'वकील',	1,	'hi',	'',	''),
(108,	'रसद / एससीएम',	1,	'hi',	'',	''),
(109,	'मैनेजर',	1,	'hi',	'',	''),
(110,	'मार्केटिंग',	1,	'hi',	'',	''),
(111,	'मीडियाक्स',	1,	'hi',	'',	''),
(112,	'मर्चेंट नेवी',	1,	'hi',	'',	''),
(113,	'एनजीओ / सोशल सर्विसेज',	1,	'hi',	'',	''),
(114,	'नर्स',	1,	'hi',	'',	''),
(115,	'कार्यालय व्यवस्थापक',	1,	'hi',	'',	''),
(116,	'ऑपरेटर / तकनीशियन',	1,	'hi',	'',	''),
(117,	'फ़िज़ियोथेरेपिस्ट',	1,	'hi',	'',	''),
(118,	'पायलट',	1,	'hi',	'',	''),
(119,	'पुलिस',	1,	'hi',	'',	''),
(120,	'निजी सुरक्षा',	1,	'hi',	'',	''),
(121,	'उत्पादन प्रबंधक',	1,	'hi',	'',	''),
(122,	'प्रोफेसर / व्याख्याता',	1,	'hi',	'',	''),
(123,	'कार्यकर्म प्रबंधक',	1,	'hi',	'',	''),
(124,	'परियोजना प्रबंधक - आईटी',	1,	'hi',	'',	''),
(125,	'परियोजना प्रबंधक - गैर आईटी',	1,	'hi',	'',	''),
(126,	'मनोविज्ञानी',	1,	'hi',	'',	''),
(127,	'अनुसंधान',	1,	'hi',	'',	''),
(128,	'सेल्स',	1,	'hi',	'',	''),
(129,	'वैज्ञानिक',	1,	'hi',	'',	''),
(130,	'सचिव / फ्रंट ऑफिस',	1,	'hi',	'',	''),
(131,	'सुरक्षा पेशेवर',	1,	'hi',	'',	''),
(132,	'स्व नियोजित',	1,	'hi',	'',	''),
(133,	'सॉफ्टवेयर इंजीनियर',	1,	'hi',	'',	''),
(134,	'खिलाड़ी',	1,	'hi',	'',	''),
(135,	'छात्र',	1,	'hi',	'',	''),
(136,	'अध्यापक',	1,	'hi',	'',	''),
(137,	'शीर्ष प्रबंधन (सीएक्सओ, एमडी आदि)',	1,	'hi',	'',	''),
(138,	'यूआई / यूएक्स डिजाइनर',	1,	'hi',	'',	''),
(139,	'वेब / ग्राफिक डिजाइन',	1,	'hi',	'',	''),
(140,	'अन्य',	1,	'hi',	'',	''),
(141,	'इतर',	1,	'mr',	'2020-10-19 07:17:58',	''),
(142,	'वेब / ग्राफिक डिझाइन',	1,	'mr',	'2020-10-19 07:18:24',	''),
(143,	'यूआय / यूएक्स डिझायनर',	1,	'mr',	'2020-10-19 07:19:14',	''),
(144,	'शीर्ष व्यवस्थापन (सीएक्सओ, एमडी इ.)',	1,	'mr',	'2020-10-19 07:19:45',	''),
(145,	'शिक्षक',	1,	'mr',	'2020-10-19 07:20:31',	''),
(146,	'विद्यार्थी',	1,	'mr',	'2020-10-19 07:20:50',	'');

DROP TABLE IF EXISTS `mm_packages`;
CREATE TABLE `mm_packages` (
  `packages_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
  `description` varchar(5000) CHARACTER SET utf8mb4 NOT NULL,
  `price` varchar(100) NOT NULL,
  `subscription_type` varchar(100) NOT NULL COMMENT '0. free 1. monthly 2. quarterly 3. halfyearly 4. yearly',
  `no_of_days` varchar(11) NOT NULL,
  `language` varchar(10) NOT NULL COMMENT 'language code',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '1 = active; 0 = deactive',
  `created_at` varchar(33) NOT NULL,
  `updated_at` varchar(33) NOT NULL,
  PRIMARY KEY (`packages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_packages` (`packages_id`, `title`, `description`, `price`, `subscription_type`, `no_of_days`, `language`, `status`, `created_at`, `updated_at`) VALUES
(18,	'1 Year Valid',	'Enjoy 1 year without any issue.',	'999',	'4',	'360',	'en',	1,	'',	'2021-02-26 19:56:51'),
(19,	'6 Month Valid',	'SEND UNLIMITED MESSAGE\r\nVIEW UPTO 100 CONTACT NO\r\nSTANDOUT FROM OTHER PROFILES\r\nLET MATCHES CONTACT YOU DIRECTLY',	'551',	'3',	'180',	'en',	1,	'',	'2021-02-26 19:56:11'),
(20,	'4 Month Valid',	'SEND UNLIMITED MESSAGE\r\nVIEW UPTO 100 CONTACT NO\r\nSTANDOUT FROM OTHER PROFILES',	'451',	'2',	'90',	'en',	1,	'',	'2021-02-26 19:55:59'),
(21,	'Personal Use',	'SEND UNLIMITED MESSAGE\r\nVIEW UPTO 100 CONTACT NO',	'251',	'1',	'30',	'en',	1,	'',	'2021-02-26 19:55:42'),
(22,	'Free Membership',	'you can browse and see all the candidates for free.',	'0',	'0',	'30',	'en',	0,	'',	'2021-02-25 17:32:54'),
(23,	'नि: शुल्क',	'आप सभी उम्मीदवारों को मुफ्त में ब्राउज़ और देख सकते हैं',	'0',	'0',	'30',	'hi',	0,	'2020-10-21 05:57:35',	''),
(24,	' 1 साल',	'1 वर्ष के लिए ऐप का उपयोग करें',	'999',	'4',	'360',	'hi',	1,	'2020-10-21 06:00:14',	'2021-02-26 19:58:41'),
(25,	'वैयक्तिक वापर',	'मर्यादित संदेश पाठवा 100 संपर्क क्रमांक',	'251',	'1',	'30',	'mr',	1,	'2021-03-01 15:42:59',	''),
(26,	'1 वर्ष वैध',	'कोणत्याही समस्येशिवाय 1 वर्षाचा आनंद घ्या.',	'0',	'4',	'360',	'mr',	1,	'2021-03-01 15:43:55',	''),
(27,	'4 महिना वैध',	'मर्यादित संदेश पाठवा 100 इतर संपर्कांमधून संपर्क साधू नका.',	'451',	'2',	'90',	'mr',	1,	'2021-03-01 15:45:46',	''),
(28,	'Test',	'testing version here only to know about test app',	'0',	'0',	'500',	'en',	1,	'2021-03-02 10:32:59',	'');

DROP TABLE IF EXISTS `mm_privacy_policy`;
CREATE TABLE `mm_privacy_policy` (
  `privacy_policy_id` int(11) NOT NULL AUTO_INCREMENT,
  `privacy_policy` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `language` varchar(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL,
  PRIMARY KEY (`privacy_policy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_privacy_policy` (`privacy_policy_id`, `privacy_policy`, `language`, `language_id`, `created_at`, `updated_at`) VALUES
(1,	'<h1 class=\"box-title m-b-0\" style=\"text-align: center;\">PRIVACY</h1>\r\n<p style=\"text-align: left;\">Matrimonix is an online matrimonial portal endeavouring constantly to provide you with matrimonial services. This privacy statement is common to all the matrimonial Website/apps operated under Matrimonix Since we are strongly committed to your right to privacy, we have drawn out a privacy statement with regard to the information we collect from you. You acknowledge that you are disclosing information voluntarily. By accessing /using the apps and/or by providing your information, you consent to the collection and use of the info you disclose on the apps in accordance with this Privacy Policy. If you do not agree for use of your information, please do not use or access this apps.</p>\r\n<p>&nbsp;</p>',	'en',	1,	'2020-10-15 11:35:50',	'2021-01-29 12:28:00'),
(2,	'<h1 id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" style=\"text-align: center;\" data-placeholder=\"Translation\"><span lang=\"hi\">गोपनीयता और नीति</span></h1>\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><span lang=\"hi\">मैट्रिमोनिक्स एक ऑनलाइन मैट्रिमोनियल पोर्टल है, जो आपको मैट्रिमोनियल सर्विसेज प्रदान करने के लिए लगातार प्रयास कर रहा है। <br />यह गोपनीयता कथन सभी वैवाहिक वेबसाइट / मैट्रिमोनिक्स के तहत संचालित ऐप्स के लिए सामान्य है क्योंकि हम गोपनीयता के आपके अधिकार के लिए दृढ़ता से प्रतिबद्ध हैं,<br />हमने आपके द्वारा एकत्र की जाने वाली जानकारी के संबंध में एक गोपनीयता कथन निकाला है। आप स्वीकार करते हैं कि आप स्वेच्छा से जानकारी का खुलासा कर रहे हैं। <br />एप्लिकेशन तक पहुँचने और / या अपनी जानकारी प्रदान करके, आप इस गोपनीयता नीति के अनुसार ऐप्स पर प्रकट होने वाली जानकारी के संग्रह और उपयोग के लिए सहमति देते हैं। <br />यदि आप अपनी जानकारी के उपयोग के लिए सहमत नहीं हैं, तो कृपया इस एप्लिकेशन का उपयोग या उपयोग न करें।</span></pre>\r\n<p style=\"text-align: left;\">&nbsp;</p>',	'hi',	2,	'2020-10-15 11:36:22',	'2021-02-25 16:32:59'),
(3,	'',	'mr',	3,	'2020-10-17 07:11:40',	'2020-10-17 07:12:21');

DROP TABLE IF EXISTS `mm_razorpay_key`;
CREATE TABLE `mm_razorpay_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razorpay_key` text NOT NULL,
  `status` int(1) NOT NULL,
  `updated_at` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_razorpay_key` (`id`, `razorpay_key`, `status`, `updated_at`) VALUES
(1,	'rzp_test_WU7Kfj9LTTV6i6',	1,	'');

DROP TABLE IF EXISTS `mm_report_user`;
CREATE TABLE `mm_report_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL,
  `report_user_id` varchar(11) NOT NULL,
  `created_at` varchar(22) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_service_provider`;
CREATE TABLE `mm_service_provider` (
  `service_provider_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_provider_name` varchar(55) CHARACTER SET utf8mb4 NOT NULL,
  `category_id` varchar(22) CHARACTER SET utf8mb4 NOT NULL,
  `about` text CHARACTER SET utf8mb4 NOT NULL,
  `contact_detail` text CHARACTER SET utf8mb4 NOT NULL,
  `address` text CHARACTER SET utf8mb4 NOT NULL,
  `landmark` text CHARACTER SET utf8mb4 NOT NULL,
  `latitude` varchar(44) CHARACTER SET utf8mb4 NOT NULL,
  `longitude` varchar(44) CHARACTER SET utf8mb4 NOT NULL,
  `service_offer` text CHARACTER SET utf8mb4 NOT NULL,
  `pricing_info` varchar(222) CHARACTER SET utf8mb4 NOT NULL,
  `image` text CHARACTER SET utf8mb4 NOT NULL,
  `banner_image` text CHARACTER SET utf8mb4 NOT NULL,
  `language` varchar(22) CHARACTER SET utf8mb4 NOT NULL COMMENT 'language code',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1 active, 0 inactive',
  `created_at` varchar(22) CHARACTER SET utf8mb4 NOT NULL,
  `updated_at` varchar(22) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`service_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_service_provider` (`service_provider_id`, `service_provider_name`, `category_id`, `about`, `contact_detail`, `address`, `landmark`, `latitude`, `longitude`, `service_offer`, `pricing_info`, `image`, `banner_image`, `language`, `status`, `created_at`, `updated_at`) VALUES
(1,	'JMB Caterers',	'1',	'JMB Caterers in indore, best caterers',	'JMB Caterers detail',	'vijay nagar Indore mp',	'Kali mandir road',	'22.719569',	'75.857726',	'JMB caterers offer ',	'Starting price 1000',	'jmb_img.jpg',	'1611141411.jpg',	'en',	1,	'2021-01-20 16:46:51',	''),
(2,	'JMB Caterers',	'2',	'इंडोर में जेएमबी कैटरर्स, सर्वश्रेष्ठ कैटरर्स',	'जेएमबी कैटरर्स विस्तार',	'vijay nagar इंदौर म.प्र',	'काली मंदिर रोड',	'22.719569',	'75.857726',	'JMB कैटरर्स प्रदान करते हैं',	'Starting price',	'1611142074851210.jpg',	'1611213134.jpg',	'hi',	1,	'2021-01-20 16:57:54',	'2021-01-21 12:42:14'),
(3,	'Kesar Caterers',	'1',	'Kesar caterers indore, best caterers',	'kesar caters detail',	'old palasiya indore mp',	'palasiya squire',	'22.719569',	'75.857726',	'kesar caterers service offer',	'Starting price 100 ',	'1611142479721134.jpg',	'1611142479.jpg',	'en',	1,	'2021-01-20 17:04:39',	''),
(4,	'Anchal photo studio',	'8',	'Best Photo studio in indore',	'anchl photo studio ',	'Indore mp',	'VijAY NAGAR',	'22.719569',	'434.445454',	'Very good service for 20k',	'Starting price 20k',	'1611927193733083.jpg',	'1611927193.jpg',	'en',	1,	'2021-01-29 19:03:13',	''),
(5,	'Garden service',	'7',	'Very good garden decoration in indore ',	'Nice garden &86784787532',	'Indore mp',	'Rajkumar bridge',	'63.5454544',	'75.857726',	'Very cheep cost 10k',	'Starting 10k',	'1611927596823271.jpg',	'1611927596.jpg',	'en',	1,	'2021-01-29 19:09:56',	'');

DROP TABLE IF EXISTS `mm_service_provider_image`;
CREATE TABLE `mm_service_provider_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_provider_id` varchar(22) NOT NULL,
  `gallery_image` text NOT NULL,
  `created_at` varchar(55) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_service_provider_image` (`image_id`, `service_provider_id`, `gallery_image`, `created_at`) VALUES
(1,	'1',	'1182f545727e4872f7ddffa26605792d.jpg',	'2021-01-20 16:46:51'),
(2,	'1',	'e67e95c49fde0822ba05a28abec29738.jpg',	'2021-01-20 16:46:51'),
(3,	'1',	'5d69d02c9ceb1d734e40123d07a053aa.jpg',	'2021-01-20 16:46:51'),
(4,	'2',	'b53029bc5d5897564e7bb886c4743596.jpg',	'2021-01-20 16:57:54'),
(5,	'2',	'4931ad0f55785bfef44d2d9b48d9ac35.jpg',	'2021-01-20 16:57:54'),
(6,	'2',	'fd1e70c1337215eb22f5e379abb9cb6e.jpg',	'2021-01-20 16:57:54'),
(7,	'3',	'6570e8d3e9e536492fc45940e609248f.jpg',	'2021-01-20 17:04:39'),
(8,	'3',	'df80382c7e851fe4acb1250415d647df.jpg',	'2021-01-20 17:04:39'),
(9,	'3',	'cdfdbfe1ee76b6621136bba31145be4c.jpg',	'2021-01-20 17:04:39'),
(10,	'3',	'ecaab4859ed0c3cc7bfb464ca787ac75.jpg',	'2021-01-20 17:04:39'),
(11,	'4',	'1bf6f8522af31497252425d0f757df06.jpg',	'2021-01-29 19:03:13'),
(12,	'4',	'5ae24ef0b0eb38f83f8f3228d58cf897.jpg',	'2021-01-29 19:03:13'),
(13,	'4',	'7c069fd1af270f927178215ebd0fc81f.jpg',	'2021-01-29 19:03:13'),
(14,	'4',	'7b13fba8e59e058d55d6e45fb0ab1053.jpg',	'2021-01-29 19:03:13'),
(15,	'5',	'b12a6333f8cb64be6f1d4b277842692a.jpg',	'2021-01-29 19:09:56'),
(16,	'5',	'9a9fac954392add52dff798119d78e92.jpg',	'2021-01-29 19:09:56'),
(17,	'5',	'3100ed23a7f999b7e4131f9c6bc1a414.jpg',	'2021-01-29 19:09:56'),
(18,	'5',	'44d3c0c215d85dfaca941c4733819d0a.jpg',	'2021-01-29 19:09:56');

DROP TABLE IF EXISTS `mm_shortlists`;
CREATE TABLE `mm_shortlists` (
  `shortlists_id` int(111) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL,
  `short_listed_id` varchar(11) NOT NULL,
  `created_at` varchar(55) NOT NULL,
  PRIMARY KEY (`shortlists_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_social_media`;
CREATE TABLE `mm_social_media` (
  `social_media_id` int(1) NOT NULL AUTO_INCREMENT,
  `about` text CHARACTER SET utf8mb4 NOT NULL,
  `facebook` tinytext NOT NULL,
  `insta` tinytext NOT NULL,
  `linkedin` tinytext NOT NULL,
  `twitter` tinytext NOT NULL,
  `contact` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`social_media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_social_media` (`social_media_id`, `about`, `facebook`, `insta`, `linkedin`, `twitter`, `contact`, `email`) VALUES
(1,	'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',	'https://www.facebook.com/samyotech',	'https://www.instagram.com/samyotech/?hl=en',	'https://www.linkedin.com/in/samyotech',	'https://www.twitter.com/samyotech',	'917869999639',	'samyotech@gmail.com');

DROP TABLE IF EXISTS `mm_states`;
CREATE TABLE `mm_states` (
  `id` text,
  `name` text,
  `name_hi` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_states` (`id`, `name`, `name_hi`) VALUES
('1',	'Andaman and Nicobar (AN)',	'अण्डमान और निकोबार द्वीपसमूह'),
('2',	'Andhra Pradesh (AP)',	'आंध्र प्रदेश'),
('3',	'Arunachal Pradesh (AR)',	'अरुणाचल प्रदेश'),
('4',	'Assam (AS)',	'असम'),
('5',	'Bihar (BR)',	'बिहार'),
('6',	'Chandigarh (CH)',	'चण्डीगढ़'),
('7',	'Chhattisgarh (CG)',	'छत्तीसगढ़'),
('8',	'Dadra and Nagar Haveli (DN)',	'दादरा और नगर हवेली'),
('9',	'Daman and Diu (DD)',	'दमन और दीव'),
('10',	'Delhi (DL)',	'दिल्ली'),
('11',	'Goa (GA)',	'गोवा'),
('12',	'Gujarat (GJ)',	'गुजरात'),
('13',	'Haryana (HR)',	'हरियाणा'),
('14',	'Himachal Pradesh (HP)',	'हिमाचल प्रदेश'),
('15',	'Jammu and Kashmir (JK)',	'जम्मू और कश्मीर'),
('16',	'Jharkhand (JH)',	'झारखण्ड'),
('17',	'Karnataka (KA)',	'कर्नाटक'),
('18',	'Kerala (KL)',	'केरल'),
('19',	'Lakshdweep (LD)',	'लक्षद्वीप'),
('20',	'Madhya Pradesh (MP)',	'मध्य प्रदेश'),
('21',	'Maharashtra (MH)',	'महाराष्ट्र'),
('22',	'Manipur (MN)',	'मणिपुर'),
('23',	'Meghalaya (ML)',	'मेघालय'),
('24',	'Mizoram (MZ)',	'मिज़ोरम'),
('25',	'Nagaland (NL)',	'नागालैण्ड'),
('26',	'Odisha (OD)',	'ओडिशा'),
('27',	'Puducherry (PY)',	'पॉण्डीचेरी'),
('28',	'Punjab (PB)',	'पंजाब'),
('29',	'Rajasthan (RJ)',	'राजस्थान'),
('30',	'Sikkim (SK)',	'सिक्किम'),
('31',	'Tamil Nadu (TN)',	'तमिल नाडु'),
('32',	'Tripura (TR)',	'त्रिपुरा'),
('33',	'Uttar Pradesh (UP)',	'उत्तर प्रदेश'),
('34',	'Uttarakhand (UK)',	'उत्तराखण्ड'),
('35',	'West Bengal (WB)',	'पश्चिम बंगाल');

DROP TABLE IF EXISTS `mm_subscription_history`;
CREATE TABLE `mm_subscription_history` (
  `id` int(111) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(111) NOT NULL,
  `txn_id` varchar(50) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `subscription_type` varchar(11) NOT NULL,
  `price` int(20) NOT NULL,
  `packages_id` int(11) NOT NULL,
  `end_subscription_date` varchar(33) NOT NULL,
  `created_at` varchar(33) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_terms`;
CREATE TABLE `mm_terms` (
  `terms_id` int(11) NOT NULL AUTO_INCREMENT,
  `terms` text CHARACTER SET utf8mb4 NOT NULL,
  `language` varchar(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL,
  PRIMARY KEY (`terms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_terms` (`terms_id`, `terms`, `language`, `language_id`, `created_at`, `updated_at`) VALUES
(1,	'<h1 style=\"text-align: center;\">TERM AND CONDITION</h1>\r\n<h3 style=\"text-align: center;\">CONSENT</h3>\r\n<p style=\"text-align: left;\">By using the services on this site, you [the User] agree to the terms of this Privacy Policy. Whenever you submit information via this site or directly at the restaurants, you consent to the handling, collection, use and disclosure of the information in accordance with this Privacy Policy and the laws applicable for the time being in the territory of India. Though all efforts have been made to ensure the accuracy and currency of the content on this Portal, the same should not be construed as a statement of law or used for any legal purposes.</p>',	'en',	1,	'2020-10-15 11:35:50',	'2021-01-29 12:27:35'),
(2,	'<div id=\"tw-target-text-container\" class=\"tw-ta-container hide-focus-ring tw-lfl\" tabindex=\"0\">\r\n<div id=\"tw-target-text-container\" class=\"tw-ta-container hide-focus-ring tw-lfl\" tabindex=\"0\">\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><strong><span lang=\"hi\">नियम और शर्तें</span></strong></pre>\r\n</div>\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><strong><span lang=\"hi\">सहमति</span></strong></pre>\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><span lang=\"hi\">इस साइट पर सेवाओं का उपयोग करके, आप [उपयोगकर्ता] इस गोपनीयता नीति की शर्तों से सहमत हैं। जब भी आप इस साइट के माध्यम से या सीधे रेस्तरां में जानकारी जमा करते हैं, तो आप इस गोपनीयता नीति और भारत के क्षेत्र में आने वाले समय के लिए लागू कानूनों के अनुसार जानकारी के हैंडलिंग, संग्रह, उपयोग और प्रकटीकरण के लिए सहमति देते हैं। यद्यपि इस पोर्टल पर सामग्री की सटीकता और मुद्रा सुनिश्चित करने के लिए सभी प्रयास किए गए हैं, लेकिन इसे कानून के एक बयान के रूप में नहीं माना जाना चाहिए या किसी कानूनी उद्देश्यों के लिए उपयोग नहीं किया जाना चाहिए।</span></pre>\r\n</div>\r\n<div id=\"tw-target-rmn-container\" class=\"tw-ta-container hide-focus-ring tw-nfl\">&nbsp;</div>',	'hi',	2,	'2020-10-15 11:36:22',	'2020-12-24 11:42:22'),
(3,	'<div id=\"tw-target-text-container\" class=\"tw-ta-container hide-focus-ring tw-lfl\" tabindex=\"0\">\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><strong><span lang=\"mr\">अटी व शर्ती</span></strong></pre>\r\n</div>\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><strong><span lang=\"mr\">संमती</span></strong></pre>\r\n<pre id=\"tw-target-text\" class=\"tw-data-text tw-text-large XcVN5d tw-ta\" dir=\"ltr\" data-placeholder=\"Translation\"><span lang=\"mr\">या साइटवरील सेवा वापरुन, आपण [वापरकर्ता] या गोपनीयता धोरणाच्या अटींशी सहमत आहात. जेव्हा जेव्हा आपण या साइटद्वारे किंवा थेट रेस्टॉरंट्सवर माहिती सबमिट करता तेव्हा आपण या गोपनीयता धोरण आणि त्या काळासाठी लागू असलेल्या कायद्याच्या अनुषंगाने माहिती हाताळणी, संग्रह, वापर आणि उघड करण्यास सहमती देता. जरी या पोर्टलवरील सामग्रीची अचूकता आणि चलन सुनिश्चित करण्यासाठी सर्व प्रयत्न केले गेले असले तरी ते कायद्याचे विधान म्हणून मानले जाऊ नये किंवा कोणत्याही कायदेशीर हेतूंसाठी वापरले जाऊ नये.</span></pre>',	'mr',	3,	'2020-10-17 07:11:40',	'2020-12-24 11:44:00');

DROP TABLE IF EXISTS `mm_users`;
CREATE TABLE `mm_users` (
  `s_no` int(111) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(111) NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `email` text NOT NULL,
  `country_code` varchar(11) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `otp` varchar(15) NOT NULL,
  `gender` varchar(1) NOT NULL COMMENT '1=male, 2=female',
  `profile_for` varchar(99) NOT NULL,
  `password` text CHARACTER SET utf8mb4 NOT NULL,
  `area_of_interest` text CHARACTER SET utf8 NOT NULL,
  `blood_group` varchar(55) NOT NULL,
  `caste` varchar(55) NOT NULL,
  `complexion` varchar(111) CHARACTER SET utf8mb4 NOT NULL,
  `body_type` varchar(151) CHARACTER SET utf8mb4 NOT NULL,
  `weight` varchar(151) NOT NULL,
  `about_me` text CHARACTER SET utf8 NOT NULL,
  `height` varchar(5) NOT NULL,
  `qualification` varchar(161) CHARACTER SET utf8mb4 NOT NULL,
  `occupation` varchar(161) NOT NULL,
  `income` varchar(161) NOT NULL,
  `work_place` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `organisation_name` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `city` varchar(111) NOT NULL,
  `state` varchar(111) NOT NULL,
  `district` varchar(111) NOT NULL,
  `current_address` text CHARACTER SET utf8mb4 NOT NULL,
  `pin` int(11) NOT NULL,
  `whatsapp_no` varchar(15) NOT NULL,
  `challenged` varchar(171) NOT NULL,
  `permanent_address` text CHARACTER SET utf8mb4 NOT NULL,
  `marital_status` varchar(14) NOT NULL,
  `aadhaar` varchar(55) NOT NULL,
  `dob` varchar(55) NOT NULL,
  `birth_place` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `birth_time` varchar(161) NOT NULL,
  `manglik` varchar(161) NOT NULL,
  `gotra` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `gotra_nanihal` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `family_address` text CHARACTER SET utf8mb4 NOT NULL,
  `father_name` varchar(181) CHARACTER SET utf8mb4 NOT NULL,
  `father_occupation` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `family_status` varchar(181) NOT NULL,
  `family_district` varchar(161) NOT NULL,
  `mother_name` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `grand_father_name` varchar(171) NOT NULL,
  `family_state` varchar(151) NOT NULL,
  `family_pin` varchar(22) NOT NULL,
  `mother_occupation` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `family_type` varchar(171) NOT NULL,
  `family_income` varchar(171) NOT NULL,
  `family_city` varchar(171) NOT NULL,
  `maternal_grand_father_name_address` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `family_value` varchar(161) NOT NULL,
  `mobile2` varchar(15) NOT NULL,
  `brother` varchar(171) NOT NULL,
  `sister` varchar(171) NOT NULL,
  `drinking` varchar(171) NOT NULL,
  `dietary` varchar(171) NOT NULL,
  `hobbies` varchar(555) CHARACTER SET utf8mb4 NOT NULL,
  `smoking` varchar(171) NOT NULL,
  `language` varchar(171) CHARACTER SET utf8mb4 NOT NULL,
  `interests` varchar(555) CHARACTER SET utf8mb4 NOT NULL,
  `working` int(11) NOT NULL DEFAULT '0' COMMENT '1=yes, 0=no',
  `shortlisted` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '1=active , 0=inactive, 2=delete',
  `critical` int(11) NOT NULL DEFAULT '0',
  `device_type` varchar(22) NOT NULL,
  `device_token` text NOT NULL,
  `user_avtar` text NOT NULL COMMENT 'profile image',
  `end_subscription_date` text NOT NULL COMMENT 'expiry date of subscription',
  `reasons` text NOT NULL COMMENT 'delete account reasons',
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL,
  PRIMARY KEY (`s_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_user_image`;
CREATE TABLE `mm_user_image` (
  `media_id` int(111) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(111) NOT NULL,
  `image` text NOT NULL,
  `created_at` varchar(22) NOT NULL,
  PRIMARY KEY (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_user_subscription`;
CREATE TABLE `mm_user_subscription` (
  `subsciption_id` int(111) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL,
  `subscription_type` int(11) NOT NULL,
  `start_subscription_date` varchar(44) NOT NULL,
  `end_subscription_date` varchar(44) NOT NULL,
  `created_at` varchar(22) NOT NULL,
  PRIMARY KEY (`subsciption_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_visitors`;
CREATE TABLE `mm_visitors` (
  `id` int(111) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL,
  `visitor_id` varchar(11) NOT NULL COMMENT 'my id',
  `created_at` varchar(22) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mm_website_banner`;
CREATE TABLE `mm_website_banner` (
  `website_banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8mb4 NOT NULL,
  `description` text CHARACTER SET utf8mb4 NOT NULL,
  `image` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1.active, 2.inactive',
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL,
  PRIMARY KEY (`website_banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mm_website_banner` (`website_banner_id`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1,	'We bring people together. Love unites them...',	'Justice . Equality . I have',	'1606814020.jpg',	1,	'2020-12-01 14:43:40',	'2020-12-06 11:52:41'),
(2,	'We bring people together. Love unites them...',	'Justice . Equality . Trust',	'1606814840.jpg',	1,	'2020-12-01 14:57:20',	'');

-- 2021-03-05 07:09:57
