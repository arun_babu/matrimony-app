<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function category()
	{
      	if (isset($_SESSION['id'])) {
            $res['language'] = $this->Setting_model->activeLanguage();
			$res['category'] = $this->Service_model->category();
			echo load_page($this,'service/category',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function submitCategory()
  	{
  		$input = $this->input->post();
        $image = $_FILES['image']['name'];
    	$id = $this->Service_model->submitCategory($input,$image);
  	}

  	public function changeCategoryStatus()
    {
        $id = $this->uri->segment('3');
        $this->Service_model->changeCategoryStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('category');
    }

    public function updateCategory()
    {
        $id = $this->uri->segment('3');
        $data['language'] = $this->Setting_model->activeLanguage();
        $data['row'] = $this->Service_model->getCategoryById($id);
        $data['category'] = $this->Service_model->category();
        echo load_page($this,'service/category',$data);
    }

    public function serviceProvider()
    {
    	$res['language'] = $this->Setting_model->activeLanguage();
		$res['category'] = $this->Service_model->activeCategory();
		$res['service'] = $this->Service_model->serviceProvider();
		echo load_page($this,'service/service_provider',$res);
    }

    public function submitServiceProvider()
    {
    	$input = $this->input->post();
        $image = $_FILES['image']['name'];
        $b_image = $_FILES['banner_image']['name'];
    	$service_provider_id = $this->Service_model->submitServiceProvider($input,$image,$b_image);
    	$g_image = implode("@&@",$_FILES['gallery_image']['name']);
    	if($g_image !='')
    	{
    		$filesCount = count($_FILES['gallery_image']['name']);
			for ($i = 0; $i < $filesCount; $i++) {
				$_FILES['file']['name']     = $_FILES['gallery_image']['name'][$i];
				$_FILES['file']['type']     = $_FILES['gallery_image']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['gallery_image']['tmp_name'][$i];
				$_FILES['file']['error']    = $_FILES['gallery_image']['error'][$i];
				$_FILES['file']['size']     = $_FILES['gallery_image']['size'][$i];
				// File upload configuration                       
				$uploadPath                 = './assets/images/service';
				$config['upload_path']      = $uploadPath;
				$config['allowed_types']    = 'gif|jpg|jpeg|png';
				$config['encrypt_name']     = TRUE;
				// Load and initialize upload library
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('file')) {
					// Uploaded file data
					$fileData = $this->upload->data();
					$fileData['file_name'];
					$uploadData[$i] = $fileData['file_name'];
				} //$this->upload->do_upload('file')
			} //$i = 0; $i < $filesCount; $i++
			foreach ($uploadData as $uploadData) {
				$datas['service_provider_id'] = $service_provider_id;
				$datas['gallery_image']      = $uploadData;
				$datas['created_at'] = date('Y-m-d H:i:s');
				$listID              = $this->db->insert("mm_service_provider_image", $datas);
			}
  		}

  		$this->session->set_flashdata('error',DATA_SAVE);
        redirect('serviceProvider');
    }

    public function changeServiceStatus()
    {
    	$id = $this->uri->segment('3');
        $this->Service_model->changeServiceStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('serviceProvider');
    }

    public function updateServiceProvider()
    {
    	$id = $this->uri->segment('3');
        $res['row'] = $this->Service_model->getServiceProviderById($id);
    	$res['language'] = $this->Setting_model->activeLanguage();
		$res['category'] = $this->Service_model->activeCategory();
		$res['service'] = $this->Service_model->serviceProvider();
		echo load_page($this,'service/service_provider',$res);
    }

}