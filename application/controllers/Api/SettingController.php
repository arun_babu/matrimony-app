<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SettingController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		error_reporting(0);
	}

	public function getCaste()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan, 'status' => '1');
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_caste');
		if ($getdata) {

			$this->Api_Auth_model->responseSuccess(1, 'Caste', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getCasteByReligion()
	{

		$religion_id = $_GET['caste_id'];
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan, 'status' => '1');
		$getdata = $this->Base_model->getDataByCaste($religion_id);
		if ($getdata) {

			$this->Api_Auth_model->responseSuccess(1, 'Caste', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getManglik()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan, 'status' => '1');
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_manglik');
		if ($getdata) {

			$this->Api_Auth_model->responseSuccess(1, 'Manglik', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getOccupation()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan, 'status' => '1');
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_occupation');
		if ($getdata) {

			$this->Api_Auth_model->responseSuccess(1, 'Occupation', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getIncome()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan, 'status' => '1');
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_income');
		if ($getdata) {

			$this->Api_Auth_model->responseSuccess(1, 'Income', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getLanguage()
	{

		$where = array('status' => '1');
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_language');
		if ($getdata) {

			$this->Api_Auth_model->responseSuccess(1, 'Language', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getBloodGroup()
	{
		$getdata = $this->Base_model->getAllData('mm_blood_group');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'Blood Group', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getHeight()
	{
		$getdata = $this->Base_model->getAllData('mm_heights');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'Height', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getStates()
	{
		$getdata = $this->Base_model->getAllData('mm_states');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'States', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getDistricts()
	{
		$state_id = $this->input->post('state_id');
		if ($state_id == '') {
			$this->Api_Auth_model->responseFailed(0, ALL_FIELD_MANDATORY);
			exit();
		} else {
			$where = array('state_id' => $state_id);
			$getdata = $this->Base_model->getAllDataWhere($where, 'mm_districts');
			if ($getdata) {
				$this->Api_Auth_model->responseSuccess(1, 'Districts', $getdata);
			} else {
				$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
			}
		}
	}

	public function getDistrict()
	{
		$state_id = $this->input->post('state_id');
		if ($state_id == '') {
			$this->Api_Auth_model->responseFailed(0, ALL_FIELD_MANDATORY);
			exit();
		} else {
			$where = array('state_id' => $state_id);
			$getdata = $this->Base_model->getAllDataWhere($where, 'mm_districts');
			if ($getdata) {
				$this->Api_Auth_model->responseSuccess(1, 'Districts', $getdata);
			} else {
				$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
			}
		}
	}

	public function getMaritalStatus()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan);
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_marital_status');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'Marital Status', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getPackages()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan, 'status' => '1');
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_packages');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'Package', $getdata);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getNews()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}
		$where = array('language' => $lan, 'status' => '1');
		$getdata = $this->Base_model->getAllDataWhere($where, 'mm_events');
		if ($getdata) {
			$news = array();
			foreach ($getdata as $getdata) {
				$getdata->image = $this->config->base_url() . 'assets/images/news/' . $getdata->image;
				array_push($news, $getdata);
			}
			$this->Api_Auth_model->responseSuccess(1, 'News', $news);
		} else {
			if ($lan == 'en') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
			} else if ($lan == 'hi') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
			} else if ($lan == 'mr') {
				$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
			}
			$this->Api_Auth_model->responseFailed(0, $DATA_NOT_AVAIABLE);
		}
	}

	public function getBodyType()
	{
		$getdata = $this->Base_model->getAllData('mm_body_type');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getComplexion()
	{
		$getdata = $this->Base_model->getAllData('mm_complexion');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getChallanged()
	{
		$getdata = $this->Base_model->getAllData('mm_challanged');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getDiet()
	{
		$getdata = $this->Base_model->getAllData('mm_diet');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getDrinkingSmoking()
	{
		$getdata = $this->Base_model->getAllData('mm_drinking_smoking');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getFathersOccupation()
	{
		$getdata = $this->Base_model->getAllData('mm_fathers_occupation');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getMothersOccupation()
	{
		$getdata = $this->Base_model->getAllData('mm_mothers_occupation');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getBrothersSisters()
	{
		$getdata = $this->Base_model->getAllData('mm_brothers_sisters');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getFamilyStatus()
	{
		$getdata = $this->Base_model->getAllData('mm_family_status');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getFamilyType()
	{
		$getdata = $this->Base_model->getAllData('mm_family_type');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getFamilyValues()
	{
		$getdata = $this->Base_model->getAllData('mm_family_values');
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}

	public function getAllSettingValue()
	{
		foreach (getallheaders() as $name => $value) {
			if ($name == 'Language') {
				$lan = $value;
			}
		}

		$getdata = $this->Base_model->getAllSettingValue($lan);
		if ($getdata) {
			$this->Api_Auth_model->responseSuccess(1, 'List', $getdata);
		} else {
			$this->Api_Auth_model->responseFailed(0, DATA_NOT_AVAIABLE);
		}
	}


}
