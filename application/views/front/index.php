<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/plugins/images/favicon.png">
            <title>Matrimonix</title>
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/bootstrap.min.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/style.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.carousel.min.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.theme.default.min.css" >
      <!--alerts CSS -->
      <link href="<?php echo base_url('assets');?>/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
 -->

</head>
   </head>
   <body>
      <header>
           <nav class="navbar navbar-expand-md navbar-dark">
         <div class="container">
            <!-- Brand -->
            <!-- <a class="navbar-brand" href="index.php"><span>Matrimonix</span></a> -->
            <a class="navbar-brand" href="index.php"><img src="<?=base_url('front_assets'); ?>/images/logoFront.png" style="height: 70px;"><span style="color: #dd1d37; font-size: revert;">Matrimonix</span></a>
            <h5 style="color: black;"><?php echo $this->session->flashdata('error'); ?></h5>
            
            <!-- Toggler/collapsibe Button -->
            <?php
            $sesData = $this->session->userdata;
            
            ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            Login
            </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">

               <ul class="navbar-nav ml-auto">
                <h4 style="color: #c62039;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                  <li class="nav-item">
                    <?php
                    if(@$sesData['user_login'] == '')
                    {
                    ?>
                     <u><a  href="#" class="nav-link" data-toggle="modal" data-target="#myModal">Login</a></u>
                     <?php
                    }else{
                    ?>
                    <u><a class="nav-link" href="<?php echo base_url('signOut'); ?>"> Sign Out</a></u>
                    <?php
                    } ?>
                  </li>
                  <li class="nav-item">
                     <u><a  href="#" class="nav-link ">  |</a></u>
                  </li>
                  <li class="nav-item">
                     <u><a  href="#" class="nav-link">  Help</a></u>
                  </li>
                  <!-- Dropdown -->
                  <!-- <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                     Help
                     </a>
                     
                     </li> -->
               </ul>
            </div>
         </div>
      </nav>
    </header>
      <div id="minimal-bootstrap-carousel" class="carousel slide carousel-fade slider-content-style slider-home-one">
         <!-- Wrapper for slides -->
         <div class="carousel-inner">
          <?php
          $i=0;
          foreach ($banner as $b)
          {
            // echo "<pre>";
            // print_r($b); die;
            $i++;
          ?>
            <div class="carousel-item <?=@$i=='1'? 'active' :'';?> slide-1" style="background-image: url(<?php echo base_url('assets'); ?>/images/website_banner/<?=$b->image; ?>);">
               <div class="carousel-caption">
                  <div class="container">
                     <div class="box valign-middle">
                        <div class="content text-center">
                           <h3 data-animation="animated fadeInUp"> <?=$b->title; ?></h3>
                           <p data-animation="animated fadeInUp"><?=$b->description; ?></p>
                           <a data-animation="animated fadeInDown" href="#" class="thm-btn ">Free Consultation</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
            }
            ?>
            
         </div>
         <!-- Controls -->
         <a class="carousel-control-prev" href="#minimal-bootstrap-carousel" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>

         <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#minimal-bootstrap-carousel" role="button" data-slide="next">
       <i class="fa fa-angle-right" aria-hidden="true"></i>


         <span class="sr-only">Next</span>
         </a>
      </div>



        <section id="about-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
               <div class="section-title text-center">
                   <h2>About Matrimonix</h2>
               </div>
                  <p><?=$social['about']; ?></p>
               </div>
            </div>
         </div>
      </section>



     
      <section>
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
                  <div id="news-slider" class="owl-carousel">
                     <div class="post-slide">
                        <div class="post-img">
                           <img src="<?php echo base_url('front_assets'); ?>/images/gal-1.jpg" alt="">
                           <a href="#" class="over-layer"><i class="fa fa-link"></i></a>
                        </div>
                     </div>
                     <div class="post-slide">
                        <div class="post-img">
                           <img src="<?php echo base_url('front_assets'); ?>/images/gal-2.jpg" alt="">
                           <a href="#" class="over-layer"><i class="fa fa-link"></i></a>
                        </div>
                     </div>
                     <div class="post-slide">
                        <div class="post-img">
                           <img src="<?php echo base_url('front_assets'); ?>/images/gal-3.jpg" alt="">
                           <a href="#" class="over-layer"><i class="fa fa-link"></i></a>
                        </div>
                     </div>
                     <div class="post-slide">
                        <div class="post-img">
                           <img src="<?php echo base_url('front_assets'); ?>/images/gal-4.jpg" alt="">
                           <a href="#" class="over-layer"><i class="fa fa-link"></i></a>
                        </div>
                     </div>
                     <div class="post-slide">
                        <div class="post-img">
                           <img src="<?php echo base_url('front_assets'); ?>/images/gal-5.jpg" alt="">
                           <a href="#" class="over-layer"><i class="fa fa-link"></i></a>
                        </div>
                     </div>
                     <div class="post-slide">
                        <div class="post-img">
                           <img src="<?php echo base_url('front_assets'); ?>/images/gal-6.jpg" alt="">
                           <a href="#" class="over-layer"><i class="fa fa-link"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   <!-----------------end image slider ------------------>
   <section class="why_choose_wapper section_wapper">
           <div class="container">
             <div class="row">
               <div class="col-md-12">
                 <div class="section_title_wapper wow fadeInUp">
                   <div class="section_title">Why Choose First Dating</div>
                   <div class="section_sub_title">Here’s why lots of people choose our website.</div>
                 </div>
               </div>
             </div>
             <div class="row">
               <div class="col-lg-3 col-md-6">
                 <div class="why_choose_boxs wow fadeInUp" >
                   <div class="why_choose_icon"> <img src="<?php echo base_url('front_assets'); ?>/images/icon/why1.png" alt=""></div>
                   <div class="why_choose_title">Protection</div>
                   <div class="why_choose_text">Your safety is provided by leading anti-scam system in the industry.</div>
                 </div>
               </div>
                <div class="col-lg-3 col-md-6">
                 <div class="why_choose_boxs wow fadeInUp" >
                   <div class="why_choose_icon"> <img src="<?php echo base_url('front_assets'); ?>/images/icon/why2.png" alt=""></div>
                   <div class="why_choose_title">VERIFICATION</div>
                   <div class="why_choose_text">All members are personally confirmed by our staff to prove they are real.</div>
                 </div>
               </div>
                <div class="col-lg-3 col-md-6">
                 <div class="why_choose_boxs wow fadeInUp" >
                   <div class="why_choose_icon"> <img src="<?php echo base_url('front_assets'); ?>/images/icon/why3.png" alt=""></div>
                   <div class="why_choose_title">ATTENTION</div>
                   <div class="why_choose_text">Receive lots of attention from attractive members worldwide.</div>
                 </div>
               </div>
               <div class="col-lg-3 col-md-6">
                 <div class="why_choose_boxs wow fadeInUp" >
                   <div class="why_choose_icon"> <img src="<?php echo base_url('front_assets'); ?>/images/icon/why4.png" alt=""></div>
                   <div class="why_choose_title">COMMUNICATION</div>
                   <div class="why_choose_text">Chat, send letters, call, share your photos and make presents.</div>
                 </div>
               </div>
             </div>
           </div>
         </section>
         
<!---------------- FEATURE END -------------------->

<section class="ragister_section_wapper section_wapper">
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-lg-10">
        <div class="ragister_se_title section_title">Receive Lots of Attention from Attractive Members Worldwide</div>
        <div class="ragister_se_text">Join the Secure &amp; Easy Way</div>
        <div class="ragister_button"> 
          <a class="btn01" data-toggle="modal" data-dismiss="modal" href="#myModal2">Create an Account </a>
          <!-- <a class="btn01" href="#"> </a>  -->
        </div>
      </div>
    </div>
     <a href="#0" class="cd-top cd-is-visible cd-fade-out"><i class="fa fa-heart" aria-hidden="true"></i></a>
  </div>
</section>

 <section id="about">
         <h4 class="text-center">Find your Special Someone</h4>
         <div class="container">
            <div class="row">
               <div class="col-md-4 about-1">
                  <div class="circle">
                     <i class="fa fa-user" aria-hidden="true" style="font-size:50px;color:white"></i>
                  </div>
                  <h4 class="text-center sign">Sign Up</h4>
                  <p>Register for free & put up your Profile
                  </p>
               </div>
               <div class="col-md-4 about-1">
                  <div class="circle">
                     <i class="fa fa-comment-o" style="font-size:50px;color:white"></i>
                  </div>
                  <h4 class="text-center sign">Connect</h4>
                  <p>Select & Connect with Matches you like
                  </p>
               </div>
               <div class="col-md-4 about-1">
                  <div class="circle">
                     <i class="fa fa-credit-card" aria-hidden="true" style="font-size:50px;color:white"></i>
                  </div>
                  <h4 class="text-center sign">Interact</h4>
                  <p>Become a Premium Member & Start a Conversation
                  </p>
               </div>
            </div>
         </div>

      </section>
      <section class="marriage-card" id="marriage-card">
       <div class="container">
          <div class="row justify-content">
             <div class="col-lg-10">
               <div class="sec-title">
                 <h2>Happy Stories</h2>
              </div>
                <div class="marriage-card">
                    <div class="owl-carousel m-card-carousel owl-theme">
                          <?php
                          foreach ($story as $s) 
                          {
                            
                          ?>
                            <div class="item">
                                <div class="mm-c">
                                    <div class="row">
                                          <div class="col-lg-5">
                                             <div class="mr-img">
                                                 <img src="<?php echo base_url('assets'); ?>/images/happy_story/<?=$s->image; ?>" class="img-fluid">
                                             </div>
                                          </div>
                                          <div class="col-lg-7">
                                            <div class="right-m">
                                              <i class="fa fa-quote-left" aria-hidden="true"></i>
                                                 <h4><?=$s->title; ?></h4>
                                                 <p><?=$s->description; ?></p>
                                            </div>
                                          </div>
                                      </div> 
                                </div>
                            </div>
                          <?php
                          }
                          ?> 
                        </div>
                </div> 
             </div>
          </div>  
       </div>
   </section>

   <!--------------end testimonials----------------->

     
      <!-------------------- popup ---------------->
      <!-- The Modal login popup -->

      <div class="modal" id="myModal">
         <div class="modal-dialog">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="first">
                  <img class="img-fluid po-logo" src="<?php echo base_url('front_assets'); ?>/images/m-logop.png" >
                  <h5 class="text-center p-welcome">Welcome! Please Login</h5>
                  <h5 class="text-center" id="erroLog"></h5>
                  <form class="text-center" method="post" action="<?=site_url('userLogin'); ?>">
                     <div class="form-group ">
                        <input class=" mobile-no form-control" id="emailIdLog" name="email" type="email" placeholder="Email ID" required>
                     </div>
                     <div class="form-group">
                         <input class=" form-control" type="password" id="passwordLog" name="password" placeholder="Enter password" required>
                     </div>
                      <div class="form-group text-right">
                        <a class="popup-forget" data-toggle="modal" data-dismiss="modal" href="#myModal7">Forget Password ?</a>
                     </div>
                     <div class="form-group mb-10">
                        <button type="submit" class="button-4 ar">Login</button>
                       <!-- <a href="" class="button-1 ar">Submit</a> -->
                     </div>
                       <!-- <p class="or">OR</p>
                      <div class="form-group mb-10">
                         <a  data-toggle="modal" data-dismiss="modal" href="#myModal-otp" class="button-1 otp-btn">Login With OTP</a>
                     </div> -->
                    
                     <a class="create" data-toggle="modal" data-dismiss="modal" href="#myModal2">Create a New Account </a>
                  </form>
               </div>
            </div>
         </div>
      </div>
    <!---------------------------- register popup ------------------->
      <div class="modal" id="myModal2">
         <div class="modal-dialog">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
                 <div class="first">
                  <img class="img-fluid po-logo" src="<?php echo base_url('front_assets'); ?>/images/m-logop.png" >
                  <h5 class="text-center p-welcome">Let's set up your account we'll send you a link to your email address and verified your account.</h5>
                  <form class="text-center" method="post" action="<?=site_url('userSignUp'); ?>">
                     <div class="form-group ">
                        <input class="form-control" type="text" name="name" placeholder="Name" required>
                     </div>
                     <div class="form-group">
                         <input class=" form-control" type="number" name="mobile" placeholder="Mobile-no" required>
                     </div>
                      <div class="form-group ">
                        <input class="form-control" type="email" name="email" placeholder="Email" required>
                     </div>
                     <div class="form-group">
                         <input class=" form-control" type="password" name="password" placeholder="Password" required>
                     </div>
                       
                     <div class="form-group mb-10">
                        <button type="submit" class="verify-btn button-4 ar">Next</button>
                            <!-- <a class="verify-btn button-1 ar" data-toggle="modal" data-dismiss="modal" href="#myModal8">Next</a> -->
                     </div>
                     <p class="all-ready-m">Already a Member? 
                     <a class="all-p" data-toggle="modal" data-dismiss="modal" href="#myModal">Login </a>
                     </p>
                  </form>
               </div>
            </div>
         </div>
      </div>
        <!------------------------------login with otp popup ------------->
            <div class="modal" id="myModal-otp">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <!-- Modal Header -->
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="first">
                         <img class="img-fluid po-logo" src="<?php echo base_url('front_assets'); ?>/images/m-logop.png" >
                        <h4 class="text-center">Enter Mobile No. or Email ID</h4>
                        <h5 class="text-center p-welcome">We will send you an OTP to login</h5>
                     <form class="">
                        <div class="form-group">
                              <input class="form-control" type="text" placeholder="Mobile No. / Email ID">
                        </div>
                        <div class="form-group">
                           <a class="button-three" data-toggle="modal" data-dismiss="modal" href="#myModal8">Send OTP</a>
                        </div>
                     </form> 
                     </div>
                      
                  </div>
               </div>
            </div>
      <!------------------------------ reset password popup ------------->
            <div class="modal" id="myModal7">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <!-- Modal Header -->
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="first">
                         <img class="img-fluid po-logo" src="<?php echo base_url('front_assets'); ?>/images/m-logop.png" >
                        <h4 class="text-center">Reset Password</h4>
                        <h5 class="text-center p-welcome">Please enter your email address and we'll send you a password.</h5>
                     <form class="" method="post" action="<?=site_url('userResetPassword'); ?>">
                        <div class="form-group">
                              <input class="form-control" type="email" name="email" placeholder="Email ID" required>
                        </div>
                        <div class="form-group">
                          <button type="submit" class="verify-btn button-4 ar">Send Password</button>
                           <!-- <a class="button-three" data-toggle="modal" data-dismiss="modal" href="#myModal8">Send Password</a> -->
                        </div>
                     </form> 
                     </div>
                      
                  </div>
               </div>
            </div>
   <!------------------- enter otp password ------------------>
         <div class="modal" id="myModal8">
         <div class="modal-dialog">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <img class="img-fluid po-logo" src="<?php echo base_url('front_assets'); ?>/images/m-logop.png" >
                  <h5 class="text-center p-welcome">Let's set up your account, while
we find Matches for you!</h5>
               <form class="first">
                  <h4 class="text-center">Enter OTP</h4>
                  <div class="form-group">
                     <input type="text" class="form-control"  placeholder="Enter OTP">
                  </div>
                  <div class="form-group">
                       <!--   <button type="submit" class="button-1">Next</button> -->
                      <a href="#" class="resnd">Resend</a>
                     <a class="button-1 ar"  href="membership.php">Verify OTP</a>
                  </div>
                  <div class="text-center">
                     <a class="backto" data-toggle="modal" data-dismiss="modal" href="#myModal">Back to Sign In</a>
                  </div>
               </form>
            </div>
         </div>
      </div>
<style type="text/css">
.button-4.ar {
background-color: #e43f3b;
border: 2px solid #e43f3b;
padding: 6px 0;
color: #fff;
margin: 0px auto 0;
width: 180px;
border-radius: 23px;
display: block;
text-align: center;
}
</style>
<script src='<?php echo base_url('front_assets'); ?>/js/jquery.min.js'></script>