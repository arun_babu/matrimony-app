<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Service Provider</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Service Provider</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        
        <div class="row">
            
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['service_provider_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update Service Provider</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Add Service Provider</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('service/submitServiceProvider'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Service Provider Name</label>
                                    <input type="hidden" name="service_provider_id" value="<?=@$row['service_provider_id'];?>">
                                    <input type="text" name="service_provider_name" class="form-control" value="<?=@$row['service_provider_name']?>" placeholder="Enter Service Provider Name" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Language</label>
                                    <select name="language" id="language" class="form-control" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach($language as $l)
                                        {
                                            $select = '';
                                            if ($l->language_code == @$row['language']) {
                                                $select = 'selected';
                                            }
                                            ?>
                                            <option value="<?=$l->language_code; ?>" <?=$select; ?>><?=$l->language_name; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Category</label>
                                    <select name="category_id" id="category_id" class="form-control" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach($category as $c)
                                        {
                                            $select = '';
                                            if ($c->category_id == @$row['category_id']) {
                                                $select = 'selected';
                                            }
                                            ?>
                                            <option value="<?=$c->category_id; ?>" <?=$select; ?>><?=$c->cat_name; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">About</label>
                                    <textarea class="form-control" name="about" placeholder="Enter about" rows="4" cols="50" required><?=@$row['about'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Contact Detail</label>
                                    <textarea class="form-control" name="contact_detail" placeholder="Enter contact detail" rows="4" cols="50" required><?=@$row['contact_detail'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Address</label>
                                    <textarea class="form-control" name="address" placeholder="Enter address" rows="4" cols="50" required><?=@$row['address'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Landmark</label>
                                    <input class="form-control" name="landmark" value="<?=@$row['landmark'];?>" placeholder="Enter landmark" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Latitude</label>
                                    <input class="form-control" name="latitude" value="<?=@$row['latitude'];?>" placeholder="Enter latitude" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Longitude</label>
                                    <input class="form-control" name="longitude" value="<?=@$row['longitude'];?>" placeholder="Enter longitude" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Service Offer</label>
                                    <textarea class="form-control" name="service_offer" placeholder="Enter service offer" rows="4" cols="50" required><?=@$row['service_offer'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Image</label>
                                    <input type="file" id="input-file-now" class="dropify" name="image" accept="image/*" <?=@$row['image']?@$row['image']:'required' ;?> >
                                </div>
                            </div>
                            <?php
                            if(@$row['image'] !='')
                            {
                            ?>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <img src="<?=base_url()?>assets/images/service/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                </div>
                            </div>
                            <?php
                            }else{}
                            ?>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Banner Image</label>
                                    <input type="file" id="input-file-now" class="dropify" name="banner_image" accept="image/*" <?=@$row['banner_image']?@$row['banner_image']:'required' ;?> >
                                </div>
                            </div>
                            <?php
                            if(@$row['banner_image'] !='')
                            {
                            ?>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <img src="<?=base_url()?>assets/images/service/<?=@$row['banner_image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                </div>
                            </div>
                            <?php
                            }else{}
                            ?>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Pricing Info</label>
                                    <input class="form-control" name="pricing_info" value="<?=@$row['pricing_info'];?>" placeholder="Enter pricing info" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Gallery Image <span style="color: red;">Select only 5 image</span></label>
                                    <input type="file" class="form-control" name="gallery_image[]" multiple accept="image/*" <?=@$row['service_provider_id']?@$row['service_provider_id']:'required' ;?> >
                                </div>
                            </div>

                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['service_provider_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
            
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">All Service Provider</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of Service Provider List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Image</th>
                                    <th>Banner Image</th>
                                    <th>Name</th>
                                    <th>Language</th> 
                                    <th>Category</th>
                                    <th>About</th>
                                    <th>Contact Detail</th>
                                    <th>Address</th>
                                    <th>Lat/Long</th>
                                    <th>Service Offer</th>
                                    <th>Pricing Info</th>
                                    <th>Status</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($service as $n) 
                            { 
                                $i++; 
                            ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <a href="<?= base_url(); ?>assets/images/service/<?php  echo $n->image; ?>" target="_blank">
                                        <img style="width: 50px;height: 40px;" src="<?= base_url(); ?>assets/images/service/<?php echo $n->image; ?>" alt="Image not Available" />
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= base_url(); ?>assets/images/service/<?php  echo $n->banner_image; ?>" target="_blank">
                                        <img style="width: 50px;height: 40px;" src="<?= base_url(); ?>assets/images/service/<?php echo $n->banner_image; ?>" alt="Image not Available" />
                                    </a>
                                </td>
                                <td><?php echo $n->service_provider_name; ?></td>
                                <td><?php echo $n->language; ?></td>
                                <td><?php echo $this->Service_model->getCatNameById($n->category_id); ?></td>
                                <td><?php echo $n->about; ?></td>
                                <td><?php echo $n->contact_detail; ?></td>
                                <td><?php echo $n->address; ?><br><?php echo $n->landmark; ?></td>
                                <td><?php echo $n->latitude; ?><br><?php echo $n->longitude; ?></td>
                                <td><?php echo $n->service_offer; ?></td>
                                <td><?php echo $n->pricing_info; ?></td>
                                <td>
                                    <?php if($n->status==1){ ?>
                                    <label class="badge badge-teal">Active</label>
                                    <?php }else if($n->status==0){ ?>
                                    <label class="badge badge-danger">Deactive</label>
                                    <?php  } ?> 
                                </td>
                                <td >
                                    <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a title="Verified" class="<?=$n->status==1?'disabled':''?>" href="<?php echo base_url('service/changeServiceStatus');?>/<?php echo $n->service_provider_id;?>">Activate</a>
                                        </li>
                                        <li>
                                            <a title="Not Verified" class="<?=$n->status==0?'disabled':''?>" href="<?php echo base_url('service/changeServiceStatus');?>/<?php echo $n->service_provider_id;?>" >Deactivate</a>
                                        </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a title="Edit" href="<?=base_url()?>service/updateServiceProvider/<?php echo $n->service_provider_id; ?>">Edit</a>
                                    </li>
                                     </ul> 
                                    </div>
                                </td>
                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

.dropify-wrapper {
    display: block;
    position: relative;
    cursor: pointer;
    overflow: hidden;
    width: 100%;
    max-width: 100%;
    height: 92px;
    padding: 5px 10px;
    font-size: 14px;
    line-height: 22px;
    color: #777;
    background-color: #FFF;
    background-image: none;
    text-align: center;
    border: 2px solid #E5E5E5;
    -webkit-transition: border-color .15s linear;
    transition: border-color .15s linear;
}
</style>
