<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IncomeController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function income()
	{
      	if (isset($_SESSION['id'])) {
      		$res['language'] = $this->Setting_model->activeLanguage();
			$res['income'] = $this->Income_model->income();
			echo load_page($this,'income/income',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function submitIncome()
  	{
  		$input = $this->input->post();
    	$id = $this->Income_model->submitIncome($input);
  	}

  	public function changeIncomeStatus()
    {
        $id = $this->uri->segment('3');
        $this->Income_model->changeIncomeStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('income');
    }

    public function updateIncome()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Income_model->updateIncome($id);
        $data['language'] = $this->Setting_model->activeLanguage();
        $data['income'] = $this->Income_model->income();
        echo load_page($this,'income/income',$data);
    }

    public function test()
    {
    	$res = $this->db->where('status',1)->get('mm_income')->result();
    	foreach($res as $r)
    	{
    		echo "<pre>";
    		print_r($r->income_hi);
    		$data = array(
    			'income' => $r->income_hi,
    			'created_at' => date('Y-m-d H:i:s')
    		);
    		$this->db->insert('mm_income',$data);
    	}
    }


}