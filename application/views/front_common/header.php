<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/plugins/images/favicon.png">
      <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/bootstrap.min.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/style.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.carousel.min.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.theme.default.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
      <title>Matrimonix</title>
   </head>
   <body>
    <header>
           <nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
         <div class="container">
            <!-- Brand -->
            <a class="navbar-brand" href="index.php"><span>Matrimonix</span></a>
            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            Login
            </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
               <ul class="navbar-nav ml-auto">
                  <!-- <li class="nav-item">
                     <a class="nav-link" href="#popup1">Login</a>              
                     </li> -->
                 
                  <li class="nav-item">
                     <a  href="#" class="nav-link dropdown-toggle" data-toggle="modal" data-target="#myModal" > <img src="<?php echo base_url('front_assets'); ?>/images/call-center.png" class="img-fluid help-center"> Help</a>
                  </li>
                  <!-- Dropdown -->
                  <!-- <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                     Help
                     </a>
                     
                     </li> -->
               </ul>
            </div>
         </div>
      </nav>
    </header>


      <nav class="navbar navbar-expand-md navbar-dark" id="banner-1">
         <div class="container">
            <!-- Brand -->
            <!-- <a class="navbar-brand" href="#"><span>Matrimony.com</span></a> -->
            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler button-one" type="button" data-toggle="collapse" data-target="#collapsibleNavbar-1">
               <span class="navbar-toggler-icon"></span>
             </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar-1">
               <ul class="navbar-nav ml-auto line-text">
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('home'); ?>">My Profile</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('shortlist'); ?>">Shortlisted</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('interest'); ?>">Interest</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('matches'); ?>">My Matches</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('visitor'); ?>">Visitors</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('news'); ?>">News</a>              
                  </li>
                
               </ul>
            </div>
         </div>
      </nav>
   
   