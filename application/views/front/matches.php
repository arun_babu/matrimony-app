<section id="breadcrumb">
   <div class="container">
      <ul class="breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">Interest</a></li>
      </ul>
   </div>
</section>
<section class="matches-sec">
   <div class="container-fluid">
      <?php
      foreach ($user as $u) 
      {
         // $user_img = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $u->user_id ));
      ?>
      <div class="matches-user">
         <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 col-md-12">
               <div class="card-7">
                  <div class="row">
                     <div class="col-md-4">
                        <?php
                            if($u->user_avtar !='')
                            {
                            ?>
                            <img class="img-fluid profile" alt="user" src="<?=base_url('assets');?>/images/user/<?=$u->user_avtar; ?>">
                            <?php
                            }else{
                            ?>
                            <img class="img-fluid profile" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                        <?php } ?>
                     </div>
                     <div class="col-md-5">
                        <div class="padd">
                           <div class="matches-1">
                              <h5> <a href="<?=site_url('about-profile');?>?User-Details=<?=base64_encode($u->user_id); ?>&user-sno=<?=time().md5($u->user_id).time();?>"><?=$u->name; ?> </a></h5>
                              <h6>Online Now</h6>
                              <hr>
                              <div class="match-detail">
                                 <div>
                                    <span><?=$this->User_model->getHeightById($u->height); ?></span>
                                    <span class="never-married"><?=$this->User_model->getMaritalStatusById($u->marital_status); ?></span>
                                 </div>
                                 <div>
                                    <span><?=$u->gotra; ?></span>
                                    <span class="never-married"><?=$u->city; ?></span>
                                 </div>
                                 <div>
                                    <span><?=$u->language; ?></span>
                                    <span class="never-married">
                                       <?php
                                      if($u->working == '1')
                                      {
                                          echo "Yes Working";
                                      }else{
                                          echo "Not Working";
                                      }
                                      ?>
                                    </span>
                                 </div>
                              </div>
                              <p class="description"><?=$u->about_me; ?></p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <p class="click">Like this profile ?</p>
                        <button class="check-button"><i class="fa fa-check" aria-hidden="true"></i></button>
                        <p class="text-center clickone">Connect Now</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-2"></div>
         </div>
      </div>
      <?php
      }
      ?>
        <div class="row">
            <!-- <div class="col-12">
                <div class="pagination-wrap card-pagination">
                    <ul>
                        <li><a href="#">first</a></li>
                        <li class="active"><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a href="#">next</a></li>
                    </ul>
                </div>
            </div> -->
        </div>
   </div>
</section>