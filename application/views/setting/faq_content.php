<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">FAQ</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
              <ol class="breadcrumb">
                <li><a href="#">Dashboard</a></li>
                <li class="active">FAQ</li>
              </ol> 
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">FAQ Update</h3>
                <p class="text-muted m-b-30">Update</p>
                <section class="m-t-40">
                    <div class="sttabs tabs-style-linetriangle">
                        <nav>
                            <ul>
                                <?php
                                $a = 0;
                                foreach ($language as $l) 
                                {
                                $a++;

                                ?>
                                <li><a href="#section-linetriangle-<?=$a; ?>" onclick="<?php if($a > '1'){ echo 'getData('.$a.')'; }else{ } ?>"><span><?=$l->language_name?> Language</span></a></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </nav>
                        <div class="content-wrap">
                            <?php
                            $i = 0;
                            foreach ($faqData as $f) 
                            {
                              $i++;
                              if($i != '1'){ $id = $i; }else{ $id = ''; }
                            ?>
                            <section id="section-linetriangle-<?=$i; ?>">
                                <form method="post" action="<?php echo base_url('faq/submitfaq'); ?>" enctype="multipart/form-data">
                                  <input type="hidden" name="faq_id" value="<?=$f->faq_id?>">
                                  <input type="hidden" name="language" value="<?=$f->language?>">
                                    <textarea id="mymce<?=$id?>" name="faq"><?=$f->faq?></textarea>
                                
                                  <br>
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                                </form>
                            </section>
                            <?php
                            }
                            ?>
                        </div>
                        <!-- /content -->
                    </div>
                    <!-- /tabs -->
                </section>
                <!-- Tabstyle start -->

                
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<script>
function getData(sno)
{
    if ($("#mymce"+sno).length > 0) {
        tinymce.init({
            selector: "textarea#mymce"+sno,
            theme: "modern",
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker", "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking", "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
        });
    }
}
</script>