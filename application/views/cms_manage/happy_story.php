<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Happy Story</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Happy Story</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        
        <div class="row">
            
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['happy_story_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update Happy Story</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Add Happy Story</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('happyStory/submitHappyStory'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Title</label>
                                    <input type="hidden" name="happy_story_id" value="<?=@$row['happy_story_id'];?>">
                                    <input type="text" name="title" class="form-control" value="<?=@$row['title']?>" placeholder="Enter title" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Description</label>
                                    <textarea class="form-control" name="description" placeholder="Enter description" rows="4" cols="50" required><?=@$row['description'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Image <span style="color: red;"> Size 500 * 300 pixels</span></label>
                                    <input type="file" id="input-file-now" class="dropify" name="image" accept="image/*" <?=@$row['image']?@$row['image']:'required' ;?> >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/happy_story/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>

                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['happy_story_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
            
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">All Happy Story</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of Happy Story List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($story as $n) 
                            { 
                                $i++; 
                            ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <a href="<?= base_url(); ?>assets/images/happy_story/<?php  echo $n->image; ?>" target="_blank">
                                        <img style="width: 50px;height: 40px;" src="<?= base_url(); ?>assets/images/happy_story/<?php echo $n->image; ?>" alt="Image not Available" />
                                    </a>
                                </td>
                                <td><?php echo $n->title; ?></td>
                                <td><?php echo $n->description; ?></td>
                                <td>
                                    <?php if($n->status==1){ ?>
                                    <label class="badge badge-teal">Active</label>
                                    <?php }else if($n->status==0){ ?>
                                    <label class="badge badge-danger">Deactive</label>
                                    <?php  } ?> 
                                </td>
                                <td >
                                    <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a title="Verified" class="<?=$n->status==1?'disabled':''?>" href="<?php echo base_url('happyStory/changeHappyStoryStatus');?>/<?php echo $n->happy_story_id;?>">Activate</a>
                                        </li>
                                        <li>
                                            <a title="Not Verified" class="<?=$n->status==0?'disabled':''?>" href="<?php echo base_url('happyStory/changeHappyStoryStatus');?>/<?php echo $n->happy_story_id;?>" >Deactivate</a>
                                        </li>
                                    
                                    <li class="divider"></li>
                                    <li>
                                        <a title="Edit" href="<?=base_url()?>happyStory/updateHappyStory/<?php echo $n->happy_story_id; ?>">Edit</a>
                                    </li>
                                     </ul> 
                                    </div>
                                </td>
                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

.dropify-wrapper {
    display: block;
    position: relative;
    cursor: pointer;
    overflow: hidden;
    width: 100%;
    max-width: 100%;
    height: 92px;
    padding: 5px 10px;
    font-size: 14px;
    line-height: 22px;
    color: #777;
    background-color: #FFF;
    background-image: none;
    text-align: center;
    border: 2px solid #E5E5E5;
    -webkit-transition: border-color .15s linear;
    transition: border-color .15s linear;
}
</style>
