<?php
class Occupation_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function occupation()
    {
    	$res = $this->db->order_by('occupation_id','DESC')->get('mm_occupation')->result();
    	return $res;
    }

    public function changeOccupationStatus($id)
    {
        $this->db->where('occupation_id',$id);
        $query = $this->db->get('mm_occupation');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('occupation_id',$id);
            $this->db->update('mm_occupation',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('occupation_id',$id);
            $this->db->update('mm_occupation',$data);
        }
    }

    public function submitOccupation($input)
    {
    	$this->form_validation->set_rules('occupation', 'occupation', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('occupation');
            exit();
        }

        if($input['occupation_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            $data = array(
                'occupation' => $input['occupation'],
                'language' => $input['language'],
                'updated_at' => date('Y-m-d H:i:s'),
                );
            
            $this->db->where('occupation_id',$input['occupation_id']);
            $this->db->update('mm_occupation',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('occupation');
        }
        else
        {
            
            $data = array(
                'occupation' => $input['occupation'],
                'language' => $input['language'],
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_occupation',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('occupation');
        }
    }

    public function updateOccupation($occupation_id)
    {
    	$query = $this->db->where('occupation_id',$occupation_id)->get('mm_occupation')->row_array();
        return $query;
    }


}