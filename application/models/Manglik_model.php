<?php
class Manglik_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function manglik()
    {
    	$res = $this->db->order_by('manglik_id','DESC')->get('mm_manglik')->result();
    	return $res;
    }

    public function changeManglikStatus($id)
    {
        $this->db->where('manglik_id',$id);
        $query = $this->db->get('mm_manglik');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('manglik_id',$id);
            $this->db->update('mm_manglik',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('manglik_id',$id);
            $this->db->update('mm_manglik',$data);
        }
    }

    public function submitManglik($input)
    {
    	$this->form_validation->set_rules('manglik', 'manglik', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('manglik');
            exit();
        }

        if($input['manglik_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            $data = array(
                'manglik' => $input['manglik'],
                'language' => $input['language'],
                'updated_at' => date('Y-m-d H:i:s'),
                );
            
            $this->db->where('manglik_id',$input['manglik_id']);
            $this->db->update('mm_manglik',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('manglik');
        }
        else
        {
            
            $data = array(
                'manglik' => $input['manglik'],
                'language' => $input['language'],
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_manglik',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('manglik');
        }
    }

    public function updateManglik($manglik_id)
    {
    	$query = $this->db->where('manglik_id',$manglik_id)->get('mm_manglik')->row_array();
        return $query;
    }

}