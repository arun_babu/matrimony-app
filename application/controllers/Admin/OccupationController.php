<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OccupationController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function occupation()
	{
      	if (isset($_SESSION['id'])) {
      		$res['language'] = $this->Setting_model->activeLanguage();
			$res['occupation'] = $this->Occupation_model->occupation();
			echo load_page($this,'occupation/occupation',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function submitOccupation()
  	{
  		$input = $this->input->post();
    	$id = $this->Occupation_model->submitOccupation($input);
  	}

  	public function changeOccupationStatus()
    {
        $id = $this->uri->segment('3');
        $this->Occupation_model->changeOccupationStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('occupation');
    }

    public function updateOccupation()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Occupation_model->updateOccupation($id);
        $data['language'] = $this->Setting_model->activeLanguage();
        $data['occupation'] = $this->Occupation_model->occupation();
        echo load_page($this,'occupation/occupation',$data);
    }


}