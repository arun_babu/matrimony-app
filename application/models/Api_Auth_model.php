<?php

class Api_Auth_model extends CI_Model
{

	function __construct()
	{
		parent:: __construct();
	}

	public function generateOtp($input)
	{
		$mobile = $input['mobile'];
		$country_code = $input['country_code'];
		if ($mobile != '' && $country_code != '') {
			$q = $this->db->where('mobile', $mobile)->where('country_code', $country_code)->get('mm_users')->num_rows();
			if ($q > 0) {
				$otp = rand(1111, 9999);
				$data = array(
					'otp' => $otp,
				);
				$this->db->where('mobile', $mobile);
				$this->db->update('mm_users', $data);

				$mobile_no = $country_code . $mobile;
				$msg = "Use $otp as one time password (OTP) to verify your Matrimony account. Please do not share this OTP to anyone for security reasons.";
				$this->send_msg_by_mobile_number($mobile_no, $msg);
				return $otp;

			} else {

				$otp = rand(1111, 9999);
				$data = array(
					'mobile' => $mobile,
					'country_code' => $country_code,
					'otp' => $otp,
				);
				$this->db->insert('mm_users', $data);

				$mobile_no = $country_code . $mobile;
				$msg = "Use $otp as one time password (OTP) to verify your Matrimony account. Please do not share this OTP to anyone for security reasons.";
				$this->send_msg_by_mobile_number($mobile_no, $msg);
				return $otp;

			}

		} else {
			return 3;
		}
	}

	public function verifyOtp($input)
	{
		$mobile = $input['mobile'];
		$country_code = $input['country_code'];
		$otp = $input['otp'];
		if ($mobile != '' && $country_code != '' && $otp != '') {
			$q = $this->db->where('mobile', $mobile)->where('country_code', $country_code)->where('otp', $otp)->get('mm_users')->num_rows();
			if ($q > 0) {
				return 1;

			} else {

				return 2;

			}

		} else {
			return 3;
		}
	}

	public function signup($input, $lan)
	{
		// $profile_for = $input['profile_for'];
		// $gender = $input['gender'];
		// $dob = $input['dob'];
		// $height = $input['height'];
		// $state = $input['state'];
		// $district = $input['district'];
		// $city = $input['city'];
		// $pin = $input['pin'];
		// $qualification = $input['qualification'];

		// $work_place = $input['work_place'];
		// $occupation = $input['occupation'];
		// $organisation_name = $input['organisation_name'];
		// $income = $input['income'];

		// $working = $input['working'];
		// $blood_group = $input['blood_group'];
		// $aadhaar = $input['aadhaar'];
		// $marital_status = $input['marital_status'];
		// $gotra = $input['gotra'];
		// $gotra_nanihal = $input['gotra_nanihal'];
		// $caste = $input['caste'];
		// $manglik = $input['manglik'];
		// $birth_time = $input['birth_time'];
		// $birth_place = $input['birth_place'];
		// $mobile = $input['mobile'];
		$email = $input['email'];
		$name = $input['name'];
		$password = md5($input['password']);
		$device_type = $input['device_type'];
		$device_token = $input['device_token'];

		if ($name == '' || $email == '' || $password == '' || $device_type == '' || $device_token == '') {
			return '2';

		} else {

			$query = $this->db->where('email', $email)->get('mm_users')->num_rows();
			if ($query > '0') {
				return '1';
			} else {
				$user_id = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50), 2) . substr(md5(time()), 1, 4);

				$data = array(
					'user_id' => $user_id,
					// 'profile_for' => $profile_for,
					// 'gender' => $gender,
					// 'dob' => $dob,
					// 'height' => $height,
					// 'state' => $state,
					// 'district' => $district,
					// 'city' => $city,
					// 'pin' => $pin,
					// 'qualification' => $qualification,
					// 'working' => $working,
					// 'blood_group' => $blood_group,
					// 'aadhaar' => $aadhaar,
					// 'marital_status' => $marital_status,
					// 'gotra' => $gotra,
					// 'gotra_nanihal' => $gotra_nanihal,
					// 'caste' => $caste,
					// 'manglik' => $manglik,
					// 'birth_time' => $birth_time,
					// 'birth_place' => $birth_place,
					// 'mobile' => $mobile,
					'name' => $name,
					'email' => $email,
					'password' => $password,
					'device_type' => $device_type,
					'device_token' => $device_token,
					// 'work_place' =>$work_place,
					// 'occupation' => $occupation,
					// 'organisation_name' => $organisation_name,
					// 'income' => $income,
					'created_at' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('mm_users', $data);
				// return '0';
				$url = base_url() . '/api/userActive?id=' . base64_encode($user_id);
				$subject = "Matrimony Registration";
				//$msg = "Thanks for signing up with Matrimony! Your account has been created, Please verify your account with below link. " . $url;

				$msg = "<!DOCTYPE html>
                    <html lang='en'>
                       <head>
                          <meta charset='utf-8'>
                          <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                          <meta name='viewport' content='width=device-width, initial-scale=1'>
                          <title>Registration to Matrimonix</title>
                          <style type='text/css'>
                             body {
                             background-color: #efefef;
                             font-family: 'Poppins', sans-serif;
                             }
                             .welcome {
                             background-color: #ffffff;
                             max-width: 665px;
                             margin: auto;
                             }
                             header {
                             background: #f7f7f7;
                             }
                             .col-6 {
                             width: 50%;
                             }
                             .col-12 {
                             width: 100%;
                             }
                             .row {
                             display: flex;
                             }
                             .col-4{
                             width: 33.3%;
                             }
                             footer {
                             padding: 20px 30px;
                             background: #bc1c1e;
                             }
                             ul {
                             padding: 0;
                             margin: auto;
                             display: block;
                             text-align: center;
                             }
                             .so-icon ul li {
                             display: inline-block;
                             padding: 14px 5px;display: inline-block;
                             }
                             .so-icon ul li img {
                             width: 31px;
                             }
                             .add p {
                             padding: 0;
                             margin: 0;
                             }
                             .contact-d {
                             padding-top: 10px;
                             }
                             .contact-d  p {
                             padding: 0;
                             margin: 0;
                             }
                             .img-r {
                             width: 100%;
                             display: block;
                             }
                             footer p {
                             font-size: 14px;
                             }

                             a.btn {
                                    background: #bc1c1e;
                                    color: #fff;
                                    padding: 7px 23px;
                                    border-radius: 4px;
                                }
                             @media only screen and (max-width: 781px) {
                             footer .col-4{
                             width: 100%;
                             }
                             .row {
                             display: block;
                             }
                             }
                             @media only screen and (max-width: 767px) {
                             .welcome {
                             max-width: 96%;
                             }
                             }
                          </style>
                       </head>
                       <body>
                          <div class='main'>
                             <div class='top-header'>
                                <h5 style='text-align: center;padding: 14px 0;     font-size: 1.2em;
                                   margin: 0px; color: #595a5d;'>An Important Messsage From  Matrimonix</h5>
                             </div>
                             <section class='welcome'>
                                <div class='container'>
                                   <header>
                                      <div class='logo'>
                                         <img src='https://phpstack-390430-1543668.cloudwaysapps.com/admin/assets/images/logo.png' style='padding: 10px 22px;
                                            width: 135px; display: block;'>
                                      </div>
                                   </header>
                                    
                                   <div class='row' style=' '>
                                      <div class='col-12' style='
                                         text-align: left;'>
                                         <div class='welcome-content-center' style='padding: 30px;'>
                                            <h4 style='font-size: 22px;
                                               color: #bc1c1e;'> Hello " . $name . ", </h4>
                                            <div class='para' style='    color: #555360;
                                               font-size: 18px;
                                               '>
                                               <p>Thank you for registraion with Matrimonix. </p>
                                               <p>Your Acount has been created Succesfully.</p>
                                               <p>Please verify your email id to activate your account.</p>
                                               <p>Just click on given link below </p>
                                               <div class='btn-z' style='margin-top: 40px;'>
                                                  <a href=" . $url . " class='btn'>Click Here For Verification</a>
                                               </div>
                                            </div>
                                            <h4 style='font-size: 18px;
                                               color: #333;     padding-top: 41px;'> Wishing the best</h4>
                                             <h4 style='font-size: 18px;
                                               color: #bc1c1e;     margin-bottom: 0;'> Thanks,</h4>
                                            <h4 style='font-size: 18px;
                                               color: #bc1c1e;margin-top: 0px;'> Matrimonix Team</h4>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <footer>
                                   <div class='row'>
                                      <div class='col-4'>
                                         <div class='add'>
                                            <p> 1st Floor Pramukh Plaza,
                                            Near Sajan Prabha Garden,
                                            Vijay Nagar, Indore,
                                            Madhya Pradesh 452010
                                            India</p>
                                         </div>
                                      </div>
                                      <div class='col-4'>
                                         
                                      </div>
                                      <div class='col-4'>
                                         <div class='contact-d'>
                                            <p> Call :  +91.78699.99639 </p>
                                            <p>Email :  samyotech@gmail.com</p>
                                         </div>
                                      </div>
                                   </div>
                                </footer>
                             </section>
                          </div>
                       </body>
                    </html>";

				$this->send_email_by_msg($email, $subject, $msg);

				if ($lan == 'en') {
					$REGISTERD_SUCCESS = REGISTERD_SUCCESS;
				} else if ($lan == 'ar') {
					$REGISTERD_SUCCESS = REGISTERD_SUCCESS_HI;
				} else if ($lan == 'mr') {
					$REGISTERD_SUCCESS = REGISTERD_SUCCESS_MR;
				}
				$this->responseSuccessWethoutData(1, $REGISTERD_SUCCESS);
				exit();

			}
		}

	}

	public function userActive($user_id)
	{
		$get_user = $this->db->where('user_id', $user_id)->get('mm_users')->row();
		if ($get_user) {
			$data = array(
				'status' => '1',
			);
			$this->db->where('user_id', $get_user->user_id);
			$this->db->update('mm_users', $data);
			return 1;
		}
	}

	public function login($input)
	{
		$email = $input['email'];
		$password = md5($input['password']);
		$device_type = $input['device_type'];
		$device_token = $input['device_token'];

		if ($email == '' || $password == '' || $device_type == '' || $device_token == '') {
			return '4';
		} else {
			$query = $this->db->where('email', $email)->get('mm_users')->num_rows();
			if ($query > '0') {
				$check_pass = $this->db->where('email', $email)->where('password', $password)->get('mm_users')->num_rows();
				if ($check_pass > '0') {
					$this->db->where('email', $email);
					$this->db->where('password', $password);
					$this->db->where('status', '2');
					$check_status_delete = $this->db->get('mm_users')->num_rows();
					if ($check_status_delete > '0') {
						return '5';
					} else {
						$this->db->where('email', $email);
						$this->db->where('password', $password);
						$this->db->where('status', '1');
						$check_status = $this->db->get('mm_users')->num_rows();
						if ($check_status > '0') {
							return '0';
						} else {
							return '3';
						}
					}

				} else {
					return '2';
				}
			} else {
				return '1';
			}
		}
	}

	public function single_row_data($input)
	{
		$email = $input['email'];
		$password = md5($input['password']);
		$device_type = $input['device_type'];
		$device_token = $input['device_token'];

		$query = $this->db->where('email', $email)->where('password', $password)->get('mm_users')->num_rows();
		if ($query) {
			// return 1;
			$data = array(
				'device_type' => $device_type,
				'device_token' => $device_token,
			);
			$this->db->where('email', $email);
			$this->db->where('password', $password);
			$q = $this->db->update('mm_users', $data);
			if ($q) {
				$res = $this->db->where('email', $email)->where('password', $password)->get('mm_users')->row_array();
				$user_img = $this->Base_model->getAllDataWhere(array(
					'user_id' => $res['user_id'],
				), 'mm_user_image');
				$user_imgs = array();
				foreach ($user_img as $user_img) {

					$user_img->image = $this->config->base_url() . 'assets/images/user/' . $user_img->image;

					array_push($user_imgs, $user_img);
				}

				$user_imgs = $user_imgs;

				$userData = array(
					's_no' => $res['s_no'],
					'user_id' => $res['user_id'],
					'name' => $res['name'],
					'email' => $res['email'],
					'mobile' => $res['mobile'],
					'gender' => $this->User_model->getGenderById($res['gender']),
					'profile_for' => $res['profile_for'],
					'area_of_interest' => $res['area_of_interest'],
					'blood_group' => $this->User_model->getBloodGroupById($res['blood_group']),
					'caste' => $this->User_model->getCasteById($res['caste']),
					'complexion' => $res['complexion'],
					'body_type' => $res['body_type'],
					'weight' => $res['weight'],
					'about_me' => $res['about_me'],
					'height' => $this->User_model->getHeightById($res['height']),
					'qualification' => $res['qualification'],
					'occupation' => $this->User_model->getOccupation($res['occupation']),
					'income' => $this->User_model->getIncomeById($res['income']),
					'work_place' => $res['work_place'],
					'organisation_name' => $res['organisation_name'],
					'city' => $res['city'],
					'state' => $this->User_model->getStatesById($res['state']),
					'district' => $this->User_model->getDistrictsById($res['district']),
					'current_address' => $res['current_address'],
					'pin' => $res['pin'],
					'whatsapp_no' => $res['whatsapp_no'],
					'challenged' => $res['challenged'],
					'permanent_address' => $res['permanent_address'],
					'marital_status' => $this->User_model->getMaritalStatusById($res['marital_status']),
					'aadhaar' => $res['aadhaar'],
					'dob' => $res['dob'],
					'birth_place' => $res['birth_place'],
					'birth_time' => $res['birth_time'],
					'manglik' => $this->User_model->getManglikById($res['manglik']),
					'gotra' => $res['gotra'],
					'gotra_nanihal' => $res['gotra_nanihal'],
					'family_address' => $res['family_address'],
					'father_name' => $res['father_name'],
					'father_occupation' => $res['father_occupation'],
					'family_status' => $res['family_status'],
					'family_district' => $res['family_district'],
					'mother_name' => $res['mother_name'],
					'grand_father_name' => $res['grand_father_name'],
					'family_state' => $res['family_state'],
					'family_pin' => $res['family_pin'],
					'mother_occupation' => $res['mother_occupation'],
					'family_type' => $res['family_type'],
					'family_income' => $res['family_income'],
					'family_city' => $res['family_city'],
					'maternal_grand_father_name_address' => $res['maternal_grand_father_name_address'],
					'family_value' => $res['family_value'],
					'mobile2' => $res['mobile2'],
					'brother' => $res['brother'],
					'sister' => $res['sister'],
					'drinking' => $res['drinking'],
					'dietary' => $res['dietary'],
					'hobbies' => $res['hobbies'],
					'smoking' => $res['smoking'],
					'language' => $res['language'],
					'interests' => $res['interests'],
					'working' => $res['working'],
					'shortlisted' => (int)$res['shortlisted'],
					'status' => $res['status'],
					'critical' => $res['critical'],
					'device_type' => $res['device_type'],
					'device_token' => $res['device_token'],
					'end_subscription_date' => $res['end_subscription_date'],
					'user_avtar' => $this->config->base_url() . 'assets/images/user/' . $res['user_avtar'],
					'created_at' => $res['created_at'],
					'updated_at' => $res['updated_at'],
					'user_imgs' => $user_imgs
				);
				return $userData;
				// $user = $this->db->where('mobile',$mobile)->where('password',$password)->get('mm_users')->result();
				// $users = array();
				// foreach ($user as $user) {

				//     $blood_group = $this->User_model->getBloodGroupById($user->blood_group);
				//     $user->blood_group = $blood_group;
				//     $caste = $this->User_model->getCasteById($user->caste);
				//     $user->caste = $caste;
				//     $height = $this->User_model->getHeightById($user->height);
				//     $user->height = $height;
				//     $occupation = $this->User_model->getOccupation($user->occupation);
				//     $user->occupation = $occupation;
				//     $income = $this->User_model->getIncomeById($user->income);
				//     $user->income = $income;
				//     $state = $this->User_model->getStatesById($user->state);
				//     $user->state = $state;
				//     $district = $this->User_model->getDistrictsById($user->district);
				//     $user->district = $district;
				//     $marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
				//     $user->marital_status = $marital_status;
				//     $manglik = $this->User_model->getManglikById($user->manglik);
				//     $user->manglik = $manglik;
				//     $user_img    = $this->Base_model->getAllDataWhere(array(
				//         'user_id' => $user->user_id,
				//     ), 'mm_user_image');
				//     $user_imgs  = array();
				//     foreach ($user_img as $user_img) {

				//         $user_img->image = $this->config->base_url().'assets/images/user/' . $user_img->image;

				//         array_push($user_imgs, $user_img);
				//     }

				//     $user->user_imgs = $user_imgs;
				//     array_push($users, $user);
				// }
				// return $users;
			}

		}

	}

	public function forgotPassword($email)
	{
		if ($email != '') {
			$q = $this->db->where('email', $email)->get('mm_users');
			if ($q->num_rows() > 0) {
				// $password = substr( "abcdefghijklmnopqrstuvwxyz" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
				$length = 6;
				$password = "";
				$codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
				$codeAlphabet .= "0123456789";
				$max = strlen($codeAlphabet); // edited

				for ($i = 0; $i < $length; $i++) {
					$password .= $codeAlphabet[random_int(0, $max - 1)];
				}

				$data = array(
					'password' => md5($password),
				);
				$this->db->where('email', $email);
				$qu = $this->db->update('mm_users', $data);
				if ($qu) {
					$u = $q->row_array();
					$name = $u['name'];
					$subject = FORG_PASS;
					//$msg = "Use $password as password your Matrimony App account. Please do not share this Password to anyone for security reasons.";

					$msg = "<!DOCTYPE html>
                    <html lang='en'>
                       <head>
                          <meta charset='utf-8'>
                          <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                          <meta name='viewport' content='width=device-width, initial-scale=1'>
                          <title>Registration to Matrimonix</title>
                          <style type='text/css'>
                             body {
                             background-color: #efefef;
                             font-family: 'Poppins', sans-serif;
                             }
                             .welcome {
                             background-color: #ffffff;
                             max-width: 665px;
                             margin: auto;
                             }
                             header {
                             background: #f7f7f7;
                             }
                             .col-6 {
                             width: 50%;
                             }
                             .col-12 {
                             width: 100%;
                             }
                             .row {
                             display: flex;
                             }
                             .col-4{
                             width: 33.3%;
                             }
                             footer {
                             padding: 20px 30px;
                             background: #bc1c1e;
                             }
                             ul {
                             padding: 0;
                             margin: auto;
                             display: block;
                             text-align: center;
                             }
                             .so-icon ul li {
                             display: inline-block;
                             padding: 14px 5px;display: inline-block;
                             }
                             .so-icon ul li img {
                             width: 31px;
                             }
                             .add p {
                             padding: 0;
                             margin: 0;
                             }
                             .contact-d {
                             padding-top: 10px;
                             }
                             .contact-d  p {
                             padding: 0;
                             margin: 0;
                             }
                             .img-r {
                             width: 100%;
                             display: block;
                             }
                             footer p {
                             font-size: 14px;
                             }

                             a.btn {
                                    background: #bc1c1e;
                                    color: #fff;
                                    padding: 7px 23px;
                                    border-radius: 4px;
                                }
                             @media only screen and (max-width: 781px) {
                             footer .col-4{
                             width: 100%;
                             }
                             .row {
                             display: block;
                             }
                             }
                             @media only screen and (max-width: 767px) {
                             .welcome {
                             max-width: 96%;
                             }
                             }
                          </style>
                       </head>
                       <body>
                          <div class='main'>
                             <div class='top-header'>
                                <h5 style='text-align: center;padding: 14px 0;     font-size: 1.2em;
                                   margin: 0px; color: #595a5d;'>An Important Messsage From  Matrimonix</h5>
                             </div>
                             <section class='welcome'>
                                <div class='container'>
                                   <header>
                                      <div class='logo'>
                                         <img src='https://phpstack-390430-1543668.cloudwaysapps.com/admin/assets/images/logo.png' style='padding: 10px 22px;
                                            width: 135px; display: block;'>
                                      </div>
                                   </header>
                                    
                                   <div class='row' style=' '>
                                      <div class='col-12' style='
                                         text-align: left;'>
                                         <div class='welcome-content-center' style='padding: 30px;'>
                                            <h4 style='font-size: 22px;
                                               color: #bc1c1e;'> Hello " . $name . ", </h4>
                                            <div class='para' style='    color: #555360;
                                               font-size: 18px;
                                               '>
                                               <p>Thank you for being a part with Matrimonix. </p>
                                               <p>Use '" . $password . "' as password your Matrimonix App account.</p>
                                               <p>Please do not share this Password to anyone for security reasons.</p>
                                               
                                            </div>
                                            <h4 style='font-size: 18px;
                                               color: #333;     padding-top: 41px;'> Wishing the best</h4>
                                             <h4 style='font-size: 18px;
                                               color: #bc1c1e;     margin-bottom: 0;'> Thanks,</h4>
                                            <h4 style='font-size: 18px;
                                               color: #bc1c1e;margin-top: 0px;'> Matrimonix Team</h4>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <footer>
                                   <div class='row'>
                                      <div class='col-4'>
                                         <div class='add'>
                                            <p> 1st Floor Pramukh Plaza,
                                            Near Sajan Prabha Garden,
                                            Vijay Nagar, Indore,
                                            Madhya Pradesh 452010
                                            India</p>
                                         </div>
                                      </div>
                                      <div class='col-4'>
                                         
                                      </div>
                                      <div class='col-4'>
                                         <div class='contact-d'>
                                            <p> Call :  +91.78699.99639 </p>
                                            <p>Email :  samyotech@gmail.com</p>
                                         </div>
                                      </div>
                                   </div>
                                </footer>
                             </section>
                          </div>
                       </body>
                    </html>";

					$this->send_email_by_msg($email, $subject, $msg);
					return 1;
				}
			} else {
				return 2;
			}
		} else {
			$this->responseFailed(0, ALL_FIELD_MANDATORY);
		}

	}

	public function logOut($input)
	{
		$user_id = $input['user_id'];

		$data = array(
			'device_token' => '12345'
		);
		$this->db->where('user_id', $user_id);
		$this->db->update('mm_users', $data);
	}

	public function changePassword($input, $lan)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('new_password', 'new_password', 'required');
		if ($this->form_validation->run() == false) {
			$this->responseFailed(0, ALL_FIELD_MANDATORY);
			exit();
		}

		$password = md5($input['password']);

		$userIdExist = $this->db->where('user_id', $input['user_id'])->get('mm_users')->num_rows();
		if ($userIdExist > 0) {
			$passExist = $this->db->where('user_id', $input['user_id'])->where('password', $password)->get('mm_users')->num_rows();
			if ($passExist > 0) {
				$data = array(
					'password' => md5($input['new_password']),
				);
				$sucess = $this->db->where('user_id', $input['user_id'])->update('mm_users', $data);
				if ($sucess) {
					return 1;
				} else {
					return 0;
				}
			} else {
				if ($lan == 'en') {
					$PASS_NOT_MATCH = PASS_NOT_MATCH;
				} else if ($lan == 'hi') {
					$PASS_NOT_MATCH = PASS_NOT_MATCH_HI;
				} else if ($lan == 'mr') {
					$PASS_NOT_MATCH = PASS_NOT_MATCH_MR;
				}
				$this->responseFailed(0, $PASS_NOT_MATCH);
				exit();
			}
		} else {
			return 2;
		}
	}

	public function deleteAccount($input)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		$this->form_validation->set_rules('reasons', 'reasons', 'required');
		if ($this->form_validation->run() == false) {
			$this->responseFailed(0, 'Please fill all field');
			exit();
		}

		$query = $this->db->where('user_id', $input['user_id'])->get('mm_users');
		if ($query->num_rows() > 0) {
			$user = $query->row_array();
			$data = array(
				'status' => 2,
				'email' => "HARD-DELETED##" . $user['email'],
				'reasons' => $input['reasons'],
			);
			$this->db->where('user_id', $input['user_id']);
			$this->db->update('mm_users', $data);
			$this->responseSuccessWethoutData(2, DELETE_ACCOUNT);
			exit();

		} else {
			$this->responseFailed(0, USER_NOT_FOUND);
			exit();
		}

	}

	public function send_email_by_msg($email_id, $subject, $msg)
	{
		$my_config = $this->config->load('email', true);
		$from_mail = $this->config->item('from_mail', 'email');

		$this->load->library('email');
		$this->email->from($from_mail, 'Matrimony');
		$this->email->to($email_id);
		$this->email->subject($subject);
		$this->email->message($msg);
		try {
			$this->email->send();
			// echo 'Message has been sent.';
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		// $this->load->view('welcome_message');
	}

	public function send_msg_by_mobile_number($mobile_number, $msg)
	{
		// phpinfo(); die;
		$authKey = "205521AaBNspcwGS5ab512aa";
		// $authKey = SMS_AUTH_KEY;

		//Multiple mobiles numbers separated by comma
		$mobileNumber = $mobile_number;
		// echo $mobileNumber; exit;

		//Sender ID,While using route4 sender id should be 6 characters long.
		$senderId = "PETSTAND";
		// $senderId = senderId;

		//Your message to send, Add URL encoding here.
		$message = urlencode($msg);

		//Define route
		$route = "4";
		//Prepare you post parameters
		$postData = array(
			'authkey' => $authKey,
			'mobiles' => $mobileNumber,
			'message' => $message,
			'sender' => $senderId,
			'route' => $route
		);

		//API URL
		$url = "https://api.msg91.com/api/sendhttp.php?authkey='$authKey'&mobiles='$mobileNumber'&message='$message'&sender='$senderId'&route=4&country=0";

		// init the resource
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $postData
			//,CURLOPT_FOLLOWLOCATION => true
		));

		//Ignore SSL certificate verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		//get response
		$output = curl_exec($ch);

		//Print error if any
		if (curl_errno($ch)) {
			echo 'error:' . curl_error($ch);
		}

		curl_close($ch);
	}


	public function responseSuccessWethoutData($status, $message)
	{
		$arr = array('status' => $status, 'message' => $message);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function responseSuccess($status, $message, $data)
	{
		$arr = array('status' => $status, 'message' => $message, 'data' => $data);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function responseSuccessOtp($status, $message, $data)
	{
		$arr = array('status' => $status, 'message' => $message, 'otp' => $data);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function responseFailed($status, $message)
	{
		$arr = array('status' => $status, 'message' => $message);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function checkUser($input)
	{
		$mobile = $input['mobile'];
		$uid = $input['uid'];
		$type = $input['type'];
		$email = $input['email'];
		$facebook = $input['facebook'];

		if ($type == 'email') {
			$query = $this->db->where('email', $email)
				->where('uid', $uid)
				->get('mm_users');
			if ($query->num_rows() > 0) {
				$user = $query->row_array();
				$this->responseSuccess(2, 'user exist', $user);
				exit();
			} else {
				$data = array(
					'uid' => $uid,
					'user_id' => $uid,
					'email' => $email,
					'status' => '1',
					'created_at' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('mm_users', $data);
				$row = $this->db->where('uid', $uid)
					->get('mm_users')->row_array();
				$this->responseSuccess(1, USER_NOT_FOUND, $row);
				exit();
			}

		} elseif ($type == 'facebook') {
			$query = $this->db->where('facebook', $facebook)
				->where('uid', $uid)
				->get('mm_users');
			if ($query->num_rows() > 0) {
				$user = $query->row_array();
				$this->responseSuccess(2, 'user exist', $user);
				exit();
			} else {
				$data = array(
					'uid' => $uid,
					'user_id' => $uid,
					'facebook' => $facebook,
					'status' => '1',

					'created_at' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('mm_users', $data);
				$row = $this->db->where('uid', $uid)
					->get('mm_users')->row_array();
				$this->responseSuccess(1, USER_NOT_FOUND, $row);
				exit();
			}
		} else {
			$query = $this->db->where('mobile', $mobile)
				->where('uid', $uid)
				->get('mm_users');
			if ($query->num_rows() > 0) {
				$user = $query->row_array();
				$this->responseSuccess(2, 'user exist', $user);
				exit();
			} else {
				$data = array(
					'uid' => $uid,
					'user_id' => $uid,
					'mobile' => $mobile,
					'status' => '1',
					'created_at' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('mm_users', $data);
				$insert_id = $this->db->insert_id();
				$row = $this->db->where('uid', $uid)
					->get('mm_users')->row_array();
				$this->responseSuccess(1, USER_NOT_FOUND, $row);
				exit();
			}
		}
	}
}
