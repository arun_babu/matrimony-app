<?php
class News_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function news()
    {
    	$res = $this->db->order_by('news_id','DESC')->get('mm_events')->result();
    	return $res;
    }

    public function changeNewsStatus($id)
    {
        $this->db->where('news_id',$id);
        $query = $this->db->get('mm_events');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('news_id',$id);
            $this->db->update('mm_events',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('news_id',$id);
            $this->db->update('mm_events',$data);
        }
    }

    public function submitNews($input,$img)
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('news');
            exit();
        }

        if($input['news_id'] !='')
        {

            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/news/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = ""; 
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'description' => $input['description'],
                    'language' => $input['language'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else
            {
                $config['upload_path']   = 'assets/images/news/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'description' => $input['description'],
                    'language' => $input['language'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            
            $this->db->where('news_id',$input['news_id']);
            $this->db->update('mm_events',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('news');
        }
        else
        {
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/news/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }

            $data = array(
                'title' => $input['title'],
                'description' => $input['description'],
                'language' => $input['language'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_events',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('news');
        }
    }

    public function updateNews($news_id)
    {
    	$query = $this->db->where('news_id',$news_id)->get('mm_events')->row_array();
        return $query;
    }

    public function deleteNews($news_id)
    {
        $this->db->where('news_id',$news_id);
        $this->db->delete('mm_events');
    }

}