<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Webservice</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Webservice</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    
                        <h3 class="box-title m-b-0">Web Service List</h3>
                    
                  <p class="text-muted m-b-30 font-13">Web Services list with url and descriptions</p>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h4 >Live: <font color="#008b8b">https://phpstack-390430-1543668.cloudwaysapps.com/</font></h4>
                                     
                                </div>
                                <div class="form-group">
                                    <h4 >01) App User Signup (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/signup' <br>
                                        --header 'language: en' \<br>
                                        --form 'profile_for=Self' \<br>
                                        --form 'gender=1' \<br>
                                        --form 'dob=1994-01-05' \<br>
                                        --form 'height=18' \<br>
                                        --form 'state=20' \<br>
                                        --form 'district=310' \<br>
                                        --form 'city=bhopal' \<br>
                                        --form 'pin=275217' \<br>
                                        --form 'qualification=BE' \<br>
                                        --form 'working=1' \<br>
                                        --form 'blood_group=4' \<br>
                                        --form 'aadhaar=987654789' \<br>
                                        --form 'marital_status=1' \<br>
                                        --form 'gotra=hindu' \<br>
                                        --form 'gotra_nanihal=hindu' \<br>
                                        --form 'caste=1' \<br>
                                        --form 'manglik=' \<br>
                                        --form 'birth_time=' \<br>
                                        --form 'birth_place=' \<br>
                                        --form 'mobile=8359947894' \<br>
                                        --form 'email=rahulrajpoot14357@gmail.com' \<br>
                                        --form 'name=Rahul Kumar' \<br>
                                        --form 'password=123456' \<br>
                                        --form 'device_type=android' \<br>
                                        --form 'device_token=kdfgdjfgsf5873456873jhbsjdf'<br>
                                        </font>
                                    </p>
                                </div>
                                
                                <div class="form-group">
                                    <h4 >02) App User login (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/login' <br>
                                        --header 'language: en' \<br>
                                        --form 'email=ramraj123@gmail.com' \<br>
                                        --form 'password=1234567' \<br>
                                        --form 'device_type=android' \<br>
                                        --form 'device_token=jskjbsfksjhrtot58thr'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >03) App User ForgotPassword (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/forgotPassword' <br>
                                        --header 'language: en' \<br>
                                        --form 'email=rahulrajpoot14357@gmail.com'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >04) App User logOut (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/logOut' <br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=1'<br>
                                        </font>
                                    </p>
                                </div>
                                
                                <div class="form-group">
                                    <h4 >05) App User changePassword (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/changePassword' <br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=1' \<br>
                                        --form 'password=123456' \<br>
                                        --form 'new_password=1234567'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >06) Set Message (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/setMessage' \<br>
                                        --header 'language: en' \<br>
                                        --form 'from_user_id=1' \<br>
                                        --form 'to_user_id=xy5835' \<br>
                                        --form 'message=hii' \<br>
                                        --form 'media=@/C:/Users/Rahul/Downloads/1111.png'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >07) Get Message History (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getMessageHistory' \<br>
                                        --form 'user_id=1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >08) Get Message (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getMessage' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=xy5835' \<br>
                                        --form 'to_user_id=1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >09) User Update (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/userUpdate' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=xy5835' \<br>
                                        --form 'name=Rahul ji' \<br>
                                        --form 'email=rahulrajpoot14357@gmail.com' \<br>
                                        --form 'mobile=' \<br>
                                        --form 'gender=1' \<br>
                                        --form 'profile_for=' \<br>
                                        --form 'area_of_interest=' \<br>
                                        --form 'blood_group=' \<br>
                                        --form 'caste=' \<br>
                                        --form 'complexion=' \<br>
                                        --form 'body_type=' \<br>
                                        --form 'weight=67' \<br>
                                        --form 'about_me=most wanted banda' \<br>
                                        --form 'height=' \<br>
                                        --form 'qualification=' \<br>
                                        --form 'occupation=' \<br>
                                        --form 'income=' \<br>
                                        --form 'work_place=Indore' \<br>
                                        --form 'organisation_name=ABIT' \<br>
                                        --form 'city=' \<br>
                                        --form 'state=' \<br>
                                        --form 'district=' \<br>
                                        --form 'current_address=' \<br>
                                        --form 'pin=' \<br>
                                        --form 'whatsapp_no=7656788760' \<br>
                                        --form 'challenged=' \<br>
                                        --form 'permanent_address=bhopal' \<br>
                                        --form 'marital_status=' \<br>
                                        --form 'aadhaar=' \<br>
                                        --form 'dob=' \<br>
                                        --form 'birth_place=' \<br>
                                        --form 'birth_time=' \<br>
                                        --form 'manglik=' \<br>
                                        --form 'gotra=' \<br>
                                        --form 'gotra_nanihal=' \<br>
                                        --form 'family_address=' \<br>
                                        --form 'father_name=' \<br>
                                        --form 'father_occupation=' \<br>
                                        --form 'family_status=' \<br>
                                        --form 'family_district=' \<br>
                                        --form 'mother_name=' \<br>
                                        --form 'grand_father_name=' \<br>
                                        --form 'family_state=' \<br>
                                        --form 'family_pin=' \<br>
                                        --form 'mother_occupation=teacher' \<br>
                                        --form 'family_type=middle class' \<br>
                                        --form 'family_income=4 to 5L' \<br>
                                        --form 'family_city=Ghazipur' \<br>
                                        --form 'maternal_grand_father_name_address=' \<br>
                                        --form 'family_value=' \<br>
                                        --form 'mobile2=' \<br>
                                        --form 'brother=1' \<br>
                                        --form 'sister=' \<br>
                                        --form 'drinking=' \<br>
                                        --form 'dietary=' \<br>
                                        --form 'hobbies=Coading' \<br>
                                        --form 'smoking=no' \<br>
                                        --form 'interests=' \<br>
                                        --form 'working=1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >10) Multiple Image Update (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/imageUpdate' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=xy5835' \<br>
                                        --form 'files[]=@/C:/Users/Rahul/Downloads/admin-logo-dark.png' \<br>
                                        --form 'files[]=@/C:/Users/Rahul/Downloads/clean.png' \<br>
                                        --form 'files[]=@/C:/Users/Rahul/Downloads/laundry.png'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >11) Short List (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/shortlist' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=xy5835' \<br>
                                        --form 'short_listed_id=STada5'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >12) Get Short list (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getShortlist' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >13) Search Users (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/searchUsers' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=xy5835' \<br>
                                        --form 'gender=1' \<br>
                                        --form 'caste=1' \<br>
                                        --form 'marital_status=1' \<br>
                                        --form 'manglik=2' \<br>
                                        --form 'state=20' \<br>
                                        --form 'district=310'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >14) Faq (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/faq' \<br>
                                        --header 'language: en' \<br>
                                        
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >15) Terms Condition (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/termsCondition' \<br>
                                        --header 'language: en' \<br>
                                        
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >16) Privacy Policy (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/privacyPolicy' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >17) Add Visitors (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/addVisitors' \<br>
                                        --header 'language: en' \<br>
                                        --form 'visitor_id=1' \<br>
                                        --form 'user_id=STada5'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >18) Get Visitors (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getVisitors' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >19) User Subscribe (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/userSubscribe' \<br>
                                        --header 'language: en' \<br>
                                        --form 'order_id=343647' \<br>
                                        --form 'txn_id=pay_DDKUmoE1aiDYbo' \<br>
                                        --form 'user_id=1' \<br>
                                        --form 'price=149' \<br>
                                        --form 'packages_id=20'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >20) Subscribe History (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/subscribeHistory' \<br>
                                        --header 'language: en' \<br>
                                        --form 'user_id=1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >21) Get Caste (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getCaste' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >22) Get Manglik (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getManglik' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >23) Get Occupation (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getOccupation' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >24) Get Income (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getIncome' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >25) Get Language (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getLanguage' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >26) Get Blood Group (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getBloodGroup' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >27) Get Height (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getHeight' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >28) Get States (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getStates' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >29) Get Districts (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getDistricts' \<br>
                                        --form 'state_id=33' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >30) Get Marital Status (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getMaritalStatus' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >31) Get Packages (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getPackages' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >32) Get News (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b"> 'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/getNews' \<br>
                                        --header 'language: en' \<br>
                                        </font>
                                    </p>
                                </div>
                                
                            </div>
                            
                        </div>
                            
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        
        </div>
        <!-- /.row -->
    </div>
    </div>