<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function users()
	{
      if (isset($_SESSION['id'])) {

          $res['user'] = $this->User_model->users();
          echo load_page($this,'user/users',$res);

	    } else {
	          redirect('login');
	    }
  	}

  	public function changeUserStatus()
    {
        $id = $this->uri->segment('3');
        $this->User_model->changeUserStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('users');
    }
	public function userDelete()
	{
		$id = $this->uri->segment('3');
		$this->User_model->userDelete($id);
		$this->session->set_flashdata('error', STATUS_CHANGE);
		redirect('users');
	}

    public function viewDetail()
    {
        $id = $this->uri->segment('3');
        $data['user'] = $this->User_model->getUserData($id);
        $data['user_img'] = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $id ));
        echo load_page($this,'user/view_detail',$data);
    }

    public function requestUsers()
    {
        $res['user'] = $this->User_model->requestUsers();
        echo load_page($this,'user/request_users',$res);
    }

    public function changeRequestStatus()
    {
        $id = $this->uri->segment('3');
        $this->User_model->changeRequestStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('requestUsers');
    }

}
