<?php
class Api_Service_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getCategory($lan)
    {
    	$this->db->select('*,CONCAT("'.base_url().'assets/images/service/",image) as image');
    	$res = $this->db->where('language',$lan)->get('mm_category')->result();
    	return $res;
    }

    public function getServiceByCategory($category_id)
    {
    	$this->form_validation->set_rules('category_id', 'category_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        
    	$query = $this->db->where('category_id',$category_id)->get('mm_service_provider');
    	if($query->num_rows() > 0)
    	{
    		$service = $query->result();
    		$services = array();
            foreach ($service as $service) {
                
                $service->image = $this->config->base_url().'assets/images/service/' . $service->image;
                $service->banner_image = $this->config->base_url().'assets/images/service/' . $service->banner_image;

                $service_img    = $this->Base_model->getAllDataWhere(array(
                    'service_provider_id' => $service->service_provider_id,
                ), 'mm_service_provider_image');
                $service_imgs  = array();
                foreach ($service_img as $service_img) {
                    
                    $service_img->gallery_image = $this->config->base_url().'assets/images/service/' . $service_img->gallery_image;
                   
                    array_push($service_imgs, $service_img);
                } 
                
                $service->service_imgs = $service_imgs;
                array_push($services, $service);
            } 
            return $services;
    	}
    	else
    	{
    		return 0;
    	}
    }

    public function reportUser($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('report_user_id', 'report_user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, 'Please login first');
            exit();
        }

        $data = array(
            'user_id' => $input['user_id'],
            'report_user_id' => $input['report_user_id'],
            'created_at' => date('Y-m-d H:i:s')
        );
        $this->db->insert('mm_report_user',$data);
        return 1;
    }

}