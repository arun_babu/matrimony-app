<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">User Detail</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)">Dashboard</a></li>
            <li class="active">User Detail</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- row -->
<div class="row">
    <div class="col-md-4 col-xs-12">
        <div class="white-box">
            <div class="user-bg"> 
                <?php
                if($user_img !='')
                {
                ?>
                <img width="100%" alt="user" src="<?=base_url('assets');?>/images/user/<?=$user_img->image; ?>">
                <?php
                }else{
                ?>
                <img width="100%" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
            <?php } ?>
            </div>
            <div class="user-btm-box">
                <!-- .row -->
                <div class="row text-center m-t-10">
                    <div class="col-md-6 b-r"><strong>Name</strong>
                        <p><?=$user['name']; ?></p>
                    </div>
                    <div class="col-md-6"><strong>Occupation</strong>
                        <p><?=$this->User_model->getOccupation($user['occupation']); ?></p>
                    </div>
                </div>
                <!-- /.row -->
                <hr>
                <!-- .row -->
                <div class="row text-center m-t-10">
                    <div class="col-md-6 b-r"><strong>Email ID</strong>
                        <p><?=$user['email']; ?></p>
                    </div>
                    <div class="col-md-6"><strong>Phone</strong>
                        <p><?=$user['mobile']; ?></p>
                    </div>
                </div>
                <!-- /.row -->
                <hr>
                <!-- .row -->
                <div class="row text-center m-t-10">
                    <div class="col-md-12"><strong>Address</strong>
                        <p><?=$user['permanent_address']; ?></p>
                    </div>
                </div>
                <hr>
                <!-- /.row -->
                <!-- <div class="col-md-4 col-sm-4 text-center">
                    <p class="text-purple"><i class="ti-facebook"></i></p>
                    <h1>258</h1> </div>
                <div class="col-md-4 col-sm-4 text-center">
                    <p class="text-blue"><i class="ti-twitter"></i></p>
                    <h1>125</h1> </div>
                <div class="col-md-4 col-sm-4 text-center">
                    <p class="text-danger"><i class="ti-dribbble"></i></p>
                    <h1>556</h1> </div> -->
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12">
        <div class="white-box">
            <!-- .tabs -->
            <ul class="nav nav-tabs tabs customtab">
                <li class="active tab">
                    <a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">User Info</span> </a>
                </li>
                <li class="tab">
                    <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Important Details</span> </a>
                </li>
                <li class="tab">
                    <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Family Details</span> </a>
                </li>
            </ul>
            <!-- /.tabs -->
            <div class="tab-content">
                <!-- .tabs 1 -->
                <div class="tab-pane active" id="home">
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                            <br>
                            <p class="text-muted"><?=$user['name']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                            <br>
                            <p class="text-muted"><?php
                                        if($user['gender'] == 1)
                                        {
                                            echo "Mail";
                                        }else{
                                            echo "Femail";
                                        }

                                        ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Aadhaar</strong>
                            <br>
                            <p class="text-muted"><?=$user['aadhaar']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Birth Place</strong>
                            <br>
                            <p class="text-muted"><?=$user['birth_place']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Height</strong>
                            <br>
                            <p class="text-muted"><?=$this->User_model->getHeightById($user['name']); ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Gotra</strong>
                            <br>
                            <p class="text-muted"><?=$user['gotra']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Marital Status </strong>
                            <br>
                            <p class="text-muted"><?=$this->User_model->getMaritalStatusById($user['marital_status']); ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Profile For</strong>
                            <br>
                            <p class="text-muted"><?=$user['profile_for']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>State</strong>
                            <br>
                            <p class="text-muted"><?=$this->User_model->getStatesById($user['state']); ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>District</strong>
                            <br>
                            <p class="text-muted"><?=$this->User_model->getDistrictsById($user['district']); ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>City</strong>
                            <br>
                            <p class="text-muted"><?=$user['city']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>DOB</strong>
                            <br>
                            <p class="text-muted"><?=$user['dob']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Birth Time </strong>
                            <br>
                            <p class="text-muted"><?=$user['birth_time']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Income</strong>
                            <br>
                            <p class="text-muted"><?=$this->User_model->getIncomeById($user['income']); ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Gotra Nanihal </strong>
                            <br>
                            <p class="text-muted"><?=$user['gotra_nanihal']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Manglik</strong>
                            <br>
                            <p class="text-muted"><?=$this->User_model->getManglikById($user['manglik']); ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="m-t-20 row">
                            <?php
                            $img = $this->User_model->getUserImageById($user['user_id']);
                            foreach($img as $img)
                            {
                            ?>
                            <img src="<?=base_url('assets')?>/images/user/<?=$img->image;?>" alt="user" class="col-md-3 col-xs-12" />
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.tabs1 -->
                <!-- .tabs2 -->
                <div class="tab-pane" id="profile">
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Education </strong>
                            <br>
                            <p class="text-muted"><?=$user['qualification']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Work Place</strong>
                            <br>
                            <p class="text-muted"><?=$user['work_place']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Occupation </strong>
                            <br>
                            <p class="text-muted"><?=$this->User_model->getOccupation($user['occupation']); ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Organisation Name </strong>
                            <br>
                            <p class="text-muted"><?=$user['organisation_name']; ?></p>
                        </div>
                    </div>
                    <hr>
                    
                    <h4>Life Styles</h4>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Dietry </strong>
                            <br>
                            <p class="text-muted"><?=$user['dietary']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Drinking</strong>
                            <br>
                            <p class="text-muted"><?=$user['drinking']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Smoking </strong>
                            <br>
                            <p class="text-muted"><?=$user['smoking']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Language </strong>
                            <br>
                            <p class="text-muted"><?=$user['language']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Hobbies </strong>
                            <br>
                            <p class="text-muted"><?=$user['hobbies']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Interest </strong>
                            <br>
                            <p class="text-muted"><?=$user['interests']; ?></p>
                        </div>
                    </div>
                    
                </div>
                <!-- /.tabs2 -->
                <!-- .tabs3 -->
                <div class="tab-pane" id="settings">
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Grand Father </strong>
                            <br>
                            <p class="text-muted"><?=$user['grand_father_name']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Maternal Grand Father </strong>
                            <br>
                            <p class="text-muted"><?=$user['maternal_grand_father_name_address']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Father Name </strong>
                            <br>
                            <p class="text-muted"><?=$user['father_name']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Mother Name </strong>
                            <br>
                            <p class="text-muted"><?=$user['mother_name']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Address </strong>
                            <br>
                            <p class="text-muted"><?=$user['permanent_address']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Whatsapp No </strong>
                            <br>
                            <p class="text-muted"><?=$user['whatsapp_no']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile </strong>
                            <br>
                            <p class="text-muted"><?=$user['mobile2']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Family Pin </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_pin']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Father Occupation </strong>
                            <br>
                            <p class="text-muted"><?=$user['father_occupation']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Mother Occupation </strong>
                            <br>
                            <p class="text-muted"><?=$user['father_occupation']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Brother </strong>
                            <br>
                            <p class="text-muted"><?=$user['brother']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Sister</strong>
                            <br>
                            <p class="text-muted"><?=$user['brother']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Family Income </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_income']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Family Status </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_status']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Family Type </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_type']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Family Value </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_value']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Family State </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_state']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Family District </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_district']; ?></p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Family City </strong>
                            <br>
                            <p class="text-muted"><?=$user['family_city']; ?></p>
                        </div>
                        
                    </div>
                </div>
                <!-- /.tabs3 -->
            </div>
        </div>
    </div>
</div>
<!-- /.row -->