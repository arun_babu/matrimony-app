<?php
class Service_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function category()
    {
    	$res = $this->db->order_by('category_id','DESC')->get('mm_category')->result();
    	return $res;
    }

    public function activeCategory()
    {
    	$res = $this->db->where('status','1')->get('mm_category')->result();
    	return $res;
    }

    public function getCatNameById($category_id)
    {
        $q = $this->db->where('category_id',$category_id)->get('mm_category')->row_array();
        return $q['cat_name'];
    }

    public function submitCategory($input,$img)
    {
        $this->form_validation->set_rules('cat_name', 'cat_name', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('category');
            exit();
        }

        if($input['category_id'] !='')
        {

            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = ""; 
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'cat_name' => $input['cat_name'],
                    'language' => $input['language'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else
            {
                $config['upload_path']   = 'assets/images/service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'cat_name' => $input['cat_name'],
                    'language' => $input['language'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            
            $this->db->where('category_id',$input['category_id']);
            $this->db->update('mm_category',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('category');
        }
        else
        {
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/service/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }

            $data = array(
                'cat_name' => $input['cat_name'],
                'language' => $input['language'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_category',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('category');
        }
    }

    public function changeCategoryStatus($id)
    {
        $this->db->where('category_id',$id);
        $query = $this->db->get('mm_category');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('category_id',$id);
            $this->db->update('mm_category',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('category_id',$id);
            $this->db->update('mm_category',$data);
        }
    }

    public function getCategoryById($category_id)
    {
    	$query = $this->db->where('category_id',$category_id)->get('mm_category')->row_array();
        return $query;
    }

    public function serviceProvider()
    {
    	$res = $this->db->order_by('service_provider_id','DESC')->get('mm_service_provider')->result();
    	return $res;
    }

    public function submitServiceProvider($input,$img,$b_img)
    {
    	$this->form_validation->set_rules('service_provider_name', 'service_provider_name', 'required');
        $this->form_validation->set_rules('category_id', 'category_id', 'required');
        $this->form_validation->set_rules('about', 'about', 'required');
        $this->form_validation->set_rules('contact_detail', 'contact_detail', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');
        $this->form_validation->set_rules('landmark', 'landmark', 'required');
        $this->form_validation->set_rules('latitude', 'latitude', 'required');
        $this->form_validation->set_rules('longitude', 'longitude', 'required');
        $this->form_validation->set_rules('service_offer', 'service_offer', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        $this->form_validation->set_rules('pricing_info', 'pricing_info', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('serviceProvider');
            exit();
        }

        if($input['service_provider_id'] !='')
        {

            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = ""; 
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $banner_image = "";
                if ($this->upload->do_upload('banner_image')) {
                    $uploadData = $this->upload->data(); 
                    $banner_image = $uploadData['file_name']; 
                }

                $data = array(
                    'service_provider_name' => $input['service_provider_name'],
	                'category_id' => $input['category_id'],
	                'about' => $input['about'],
	                'contact_detail' => $input['contact_detail'],
	                'address' => $input['address'],
	                'landmark' => $input['landmark'],
	                'latitude' => $input['latitude'],
	                'longitude' => $input['longitude'],
	                'service_offer' => $input['service_offer'],
	                'banner_image' => $banner_image,
	                'language' => $input['language'],
                    'pricing_info' => $input['pricing_info'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else if($b_img == '')
            {
                $config['upload_path']   = 'assets/images/service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = ""; 
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $banner_image = "";
                if ($this->upload->do_upload('banner_image')) {
                    $uploadData = $this->upload->data(); 
                    $banner_image = $uploadData['file_name']; 
                }

                $data = array(
                    'service_provider_name' => $input['service_provider_name'],
	                'category_id' => $input['category_id'],
	                'about' => $input['about'],
	                'contact_detail' => $input['contact_detail'],
	                'address' => $input['address'],
	                'landmark' => $input['landmark'],
	                'latitude' => $input['latitude'],
	                'longitude' => $input['longitude'],
	                'service_offer' => $input['service_offer'],
	                'image' => $image,
	                'language' => $input['language'],
                    'pricing_info' => $input['pricing_info'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else if($img == '' || $b_img == '')
            {
            	$config['upload_path']   = 'assets/images/service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $banner_image = "";
                if($this->upload->do_upload('banner_image')) {
                    $uploadData = $this->upload->data(); 
                    $banner_image = $uploadData['file_name']; 
                }

                $data = array(
                    'service_provider_name' => $input['service_provider_name'],
	                'category_id' => $input['category_id'],
	                'about' => $input['about'],
	                'contact_detail' => $input['contact_detail'],
	                'address' => $input['address'],
	                'landmark' => $input['landmark'],
	                'latitude' => $input['latitude'],
	                'longitude' => $input['longitude'],
	                'service_offer' => $input['service_offer'],
	                'language' => $input['language'],
                    'pricing_info' => $input['pricing_info'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else
            {
            	$config['upload_path']   = 'assets/images/service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $banner_image = "";
                if($this->upload->do_upload('banner_image')) {
                    $uploadData = $this->upload->data(); 
                    $banner_image = $uploadData['file_name']; 
                }

                $data = array(
                    'service_provider_name' => $input['service_provider_name'],
	                'category_id' => $input['category_id'],
	                'about' => $input['about'],
	                'contact_detail' => $input['contact_detail'],
	                'address' => $input['address'],
	                'landmark' => $input['landmark'],
	                'latitude' => $input['latitude'],
	                'longitude' => $input['longitude'],
	                'service_offer' => $input['service_offer'],
	                'image' => $image,
                	'banner_image' => $banner_image,
	                'language' => $input['language'],
                    'pricing_info' => $input['pricing_info'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            
            $this->db->where('service_provider_id',$input['service_provider_id']);
            $this->db->update('mm_service_provider',$data);
            return $input['service_provider_id'];
        }
        else
        {
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/service/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time().rand(110011,999999);
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }

            if($b_img == '')
            {
                $banner_image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/service/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $banner_image = "";
                if($this->upload->do_upload('banner_image')) {
                    $uploadData = $this->upload->data(); 
                    $banner_image = $uploadData['file_name']; 
                }
            }

            $data = array(
                'service_provider_name' => $input['service_provider_name'],
                'category_id' => $input['category_id'],
                'about' => $input['about'],
                'contact_detail' => $input['contact_detail'],
                'address' => $input['address'],
                'landmark' => $input['landmark'],
                'latitude' => $input['latitude'],
                'longitude' => $input['longitude'],
                'service_offer' => $input['service_offer'],
                'language' => $input['language'],
                'image' => $image,
                'banner_image' => $banner_image,
                'pricing_info' => $input['pricing_info'],
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_service_provider',$data);
        	$service_provider_id = $this->db->insert_id();
            return $service_provider_id;
        }
    }

    public function changeServiceStatus($id)
    {
        $this->db->where('service_provider_id',$id);
        $query = $this->db->get('mm_service_provider');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('service_provider_id',$id);
            $this->db->update('mm_service_provider',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('service_provider_id',$id);
            $this->db->update('mm_service_provider',$data);
        }
    }

    public function getServiceProviderById($service_provider_id)
    {
    	$query = $this->db->where('service_provider_id',$service_provider_id)->get('mm_service_provider')->row_array();
        return $query;
    }

}