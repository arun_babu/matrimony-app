<?php
class Api_Visitors_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function addVisitors($input,$lan)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('visitor_id', 'visitor_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Base_model->responseFailed(0,'Please fill user id field');
            exit();
        }

        $favExist = $this->db->where('visitor_id',$input['visitor_id'])->where('user_id',$input['user_id'])->get('mm_visitors');
        if($favExist->num_rows() > 0)
        {
        	if($lan == 'en')
	        {
	            $DATA_SAVE = DATA_SAVE;
	        }else if($lan == 'hi')
	        {
	            $DATA_SAVE = DATA_SAVE_HI;
	        }else if($lan == 'mr')
	        {
	            $DATA_SAVE = DATA_SAVE_MR;
	        }
        }
        else
        {
	        $data = array(
	        	'visitor_id' => $input['visitor_id'], 
	        	'user_id' => $input['user_id'], 
	        	'created_at' => date('Y-m-d H:i:s')
	    	);
	    	$this->db->insert('mm_visitors',$data);
	    	if($lan == 'en')
	        {
	            $DATA_SAVE = DATA_SAVE;
	        }else if($lan == 'hi')
	        {
	            $DATA_SAVE = DATA_SAVE_HI;
	        }else if($lan == 'mr')
	        {
	            $DATA_SAVE = DATA_SAVE_MR;
	        }
    	}

        $this->Base_model->responseSuccessWethoutData(1,$DATA_SAVE);
        exit();
        
    }

    public function getVisitors($input,$lan)
    {
    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        $userExist = $this->db->where('user_id',$input['user_id'])->where('status','1')->get('mm_users')->num_rows();
        if($userExist > 0)
        {
	        $user = $this->getVisitorsUser($input['user_id']);   
            $users = array();
            foreach ($user as $user) {
                
                $user->gender = $this->User_model->getGenderById($user->gender);
                $blood_group = $this->User_model->getBloodGroupById($user->blood_group);
                $user->blood_group = $blood_group;
                $caste = $this->User_model->getCasteById($user->caste);
                $user->caste = $caste;
                $height = $this->User_model->getHeightById($user->height);
                $user->height = $height;
                $occupation = $this->User_model->getOccupation($user->occupation);
                $user->occupation = $occupation;
                $income = $this->User_model->getIncomeById($user->income);
                $user->income = $income;
                $state = $this->User_model->getStatesById($user->state);
                $user->state = $state;
                $district = $this->User_model->getDistrictsById($user->district);
                $user->district = $district;
                $marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
                $user->marital_status = $marital_status;
                $manglik = $this->User_model->getManglikById($user->manglik);
                $user->manglik = $manglik;
                $request = $this->Api_User_model->getRequested($input['user_id'],$user->user_id);
                $user->request = $request;
                $user->shortlisted = (int)$user->shortlisted;
                $user->user_avtar = $this->config->base_url().'assets/images/user/' . $user->user_avtar;
                $user_img    = $this->Base_model->getAllDataWhere(array(
                    'user_id' => $user->user_id,
                ), 'mm_user_image');
                $user_imgs  = array();
                foreach ($user_img as $user_img) {
                    
                    $user_img->image = $this->config->base_url().'assets/images/user/' . $user_img->image;
                   
                    array_push($user_imgs, $user_img);
                } 
                
                $user->user_imgs = $user_imgs;
                array_push($users, $user);
            } 
            return $users;
            
        }
	    else
	    {
            if($lan == 'en')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW;
            }else if($lan == 'hi')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_HI;
            }else if($lan == 'mr')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_MR;
            }
	    	$this->Base_model->responseFailed(3, $DEACTIVE_NEW);
            exit();
	    }
    }

    public function getVisitorsUser($user_id)
    {
        $this->db->select('u.*');
        $this->db->from('mm_users u');
        $this->db->join('mm_visitors v','v.user_id = u.user_id');
        $this->db->where('v.visitor_id',$user_id);
        $this->db->where('u.status',1);
        $this->db->order_by('u.s_no', 'desc');
        $query = $this->db->get();
        $user = $query->result();
        return $user;
    }


}