<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PackageController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function packages()
	  {
      if (isset($_SESSION['id'])) {
      $res['language'] = $this->Setting_model->activeLanguage();
			$res['packages'] = $this->Package_model->packages();
			echo load_page($this,'packages/packages',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function submitPackages()
  	{
  		$input = $this->input->post();
    	$id = $this->Package_model->submitPackages($input);
  	}

  	public function changePackagesStatus()
    {
        $id = $this->uri->segment('3');
        $this->Package_model->changePackagesStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('packages');
    }

    public function updatePackages()
    {
        $id = $this->uri->segment('3');
        $data['language'] = $this->Setting_model->activeLanguage();
        $data['row'] = $this->Package_model->updatePackages($id);
        $data['packages'] = $this->Package_model->packages();
        echo load_page($this,'packages/packages',$data);
    }


}