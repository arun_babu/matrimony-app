<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CasteController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function caste()
	{
      	if (isset($_SESSION['id'])) {
      		$res['language'] = $this->Setting_model->activeLanguage();
			$res['caste'] = $this->Caste_model->caste();
			echo load_page($this,'caste/caste',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function submitCaste()
  	{
  		$input = $this->input->post();
    	$id = $this->Caste_model->submitCaste($input);
  	}

  	public function changeCasteStatus()
    {
        $id = $this->uri->segment('3');
        $this->Caste_model->changeCasteStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('caste');
    }

    public function updateCaste()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Caste_model->updateCaste($id);
        $data['language'] = $this->Setting_model->activeLanguage();
        $data['caste'] = $this->Caste_model->caste();
        echo load_page($this,'caste/caste',$data);
    }

}