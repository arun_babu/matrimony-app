<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SettingController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function apiKeys()
    {
        $data['firebase'] = $this->Setting_model->getFirebaseKey();
        $data['razorpay'] = $this->Setting_model->getRazorpayKey();
        echo load_page($this,'setting/api_keys',$data);
    }

    public function updateFirebaseKey()
    {
      	$input = $this->input->post();
      	$this->Setting_model->updateFirebaseKey($input);
    }

    public function updateRazorpayKey()
    {
        $input = $this->input->post();
        $this->Setting_model->updateRazorpayKey($input);
    }

    public function language()
    {
      $res['language'] = $this->Setting_model->language();
      echo load_page($this,'setting/language',$res);
    }

    public function submitLanguage()
    {
        $input = $this->input->post();
        $this->Setting_model->submitLanguage($input);
    }

    public function changeLanguageStatus()
    {
        $id = $this->uri->segment('3');
        $this->Setting_model->changeLanguageStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('language');
    }

    public function updateLanguage()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Setting_model->updateLanguage($id);
        $data['language'] = $this->Setting_model->language();
        echo load_page($this,'setting/language',$data);
    }

    public function faqContent()
    {
        $res['language'] = $this->Setting_model->activeLanguage();
        $res['faqData'] = $this->Setting_model->faqData();
        echo load_page($this,'setting/faq_content',$res);
    }

    public function submitfaq()
    {
        $faq = $this->input->post('faq');
        $faq_id = $this->input->post('faq_id');
        $language = $this->input->post('language');
        $this->Setting_model->submitfaq($faq,$faq_id,$language);
    }

    public function privacyPolicyContent()
    {
        $res['language'] = $this->Setting_model->activeLanguage();
        $res['ppdata'] = $this->Setting_model->privacyPolicyData();
        echo load_page($this,'setting/privacyPolicyContent',$res);
    }

    public function submitPrivacyPolicy()
    {
        $privacy_policy = $this->input->post('privacy_policy');
        $privacy_policy_id = $this->input->post('privacy_policy_id');
        $language = $this->input->post('language');
        $this->Setting_model->submitPrivacyPolicy($privacy_policy,$privacy_policy_id,$language);
    }

    public function termsContent()
    {
        $res['language'] = $this->Setting_model->activeLanguage();
        $res['ppdata'] = $this->Setting_model->termsContentData();
        echo load_page($this,'setting/termsContent',$res);
    }

    public function submitTerms()
    {
        $terms = $this->input->post('terms');
        $terms_id = $this->input->post('terms_id');
        $language = $this->input->post('language');
        $this->Setting_model->submitTerms($terms,$terms_id,$language);
    }

    public function webservice()
    {
        echo load_page($this,'setting/webservice');
    }

    public function emailAlert()
    {
        echo load_page($this,'setting/email_alert');
    }

}