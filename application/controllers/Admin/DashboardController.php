<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('aws3');
		// error_reporting(0);
	}

	public function upload()
	{

		$config['upload_path'] = './upload';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		            $config['max_size']      = 1000000;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$image = "";
var_dump($this->upload->do_upload('user_avtar'));die();
		$uploadData = $this->upload->data();
		$uploadData['file_name'] = $this->aws3->sendfile('bethelmatrimony-upload', $_FILES['user_avtar']);
		var_dump($uploadData['file_name'] );die();

		$data = array('upload_data' => $uploadData['file_name']);
		var_dump($data);
		die();
		$image = $uploadData['file_name'];


	}

	public function index()
	{
		if (isset($_SESSION['id'])) {

			$res['user_count'] = $this->Dashboard_model->getUserCount();
			$res['packages_count'] = $this->Dashboard_model->getPackagesCount();
			$res['language_count'] = $this->Dashboard_model->getLanguageCount();
			$res['total_income'] = $this->Dashboard_model->getTotalIncome();
			$res['android_user'] = $this->Dashboard_model->getAndroidUser();
			$res['ios_user'] = $this->Dashboard_model->getIosUser();
			$res['admin'] = $this->Dashboard_model->getAdmin();
			$res['user'] = $this->Dashboard_model->getLastUser();
			echo load_page($this, 'admin/dashboard', $res);

		} else {
			redirect('login');
		}
	}

}
