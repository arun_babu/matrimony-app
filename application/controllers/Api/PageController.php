<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function faq()
    {
    	foreach (getallheaders() as $name => $value) 
        { 
            
            if($name == 'Language')
            {
                $lan = $value;
            }
        }

        $url = base_url()."api/faqPage/".$lan;
        $this->Base_model->responseSuccessWethoutData(1, $url);
    }

    public function faqPage()
    {
        $lan = $this->uri->segment('3');
        $res['row'] = $this->Setting_model->faqPage($lan);
        $this->load->view('page/faq',$res);
    }

    public function privacyPolicy()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            
            if($name == 'Language')
            {
                $lan = $value;
            }
        }

        $url = base_url()."api/privacyPolicyPage/".$lan;
        $this->Base_model->responseSuccessWethoutData(1, $url);
    }
    public function privacyPolicyPage()
    {
        $lan = $this->uri->segment('3');
        $res['row'] = $this->Setting_model->privacyPolicyPage($lan);
        $this->load->view('page/privacyPolicy',$res);
    }

    public function termsCondition()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            
            if($name == 'Language')
            {
                $lan = $value;
            }
        }

        $url = base_url()."api/termsConditionPage/".$lan;
        $this->Base_model->responseSuccessWethoutData(1, $url);
    }
    public function termsConditionPage()
    {
        $lan = $this->uri->segment('3');
        $res['row'] = $this->Setting_model->termsConditionPage($lan);
        $this->load->view('page/termsCondition',$res);
    }


}