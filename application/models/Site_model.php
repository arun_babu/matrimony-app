<?php
class Site_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function userLogin($input)
    {
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_red',ALL_FIELD_MANDATORY);
            redirect();
            exit();
        }

        $email = $input['email'];
        $password = md5($input['password']);

        $query = $this->db->where('email',$email)->get('mm_users')->num_rows();
        if($query > '0')
        {
            $check_pass = $this->db->where('email',$email)->where('password',$password)->get('mm_users')->num_rows();
            if($check_pass > '0')
            {
                $sess_array       = array();
                $this->db->where('email',$email);
                $this->db->where('password',$password);
                $this->db->where('status','1');
                $check_status = $this->db->get('mm_users');
                if($check_status->num_rows() > '0')
                {
                    $getdata = $check_status->row();
                    $this->session->unset_userdata($sess_array);
                    $sess_array = array(
                        'user_email' => $getdata->email,
                        'user_id' => $getdata->user_id,
                        'user_name' => $getdata->name,
                        'user_login' => 'user'
                    );
                    
                    $this->session->set_userdata($sess_array);

                    $query = $this->db->where('user_id',$getdata->user_id)->get('mm_user_subscription')->num_rows();
                    if($query > 0)
                    {
                        $this->session->set_flashdata('error',LOGIN_SUCCESS);
                        redirect('profile');
                    }
                    else
                    {
                        $this->session->set_flashdata('error',LOGIN_SUCCESS);
                        redirect('membership');
                    }
                    
                }
                else{
                    return '3';
                }
            }
            else{
                return '2';
            }
        }
        else{
            return '1';
        }
    }

    public function userSignUp($input)
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_red',ALL_FIELD_MANDATORY);
            redirect();
            exit();
        }

        $email = $input['email'];
        $mobile = $input['mobile'];
        $name = $input['name'];
        $password = md5($input['password']);

        $query = $this->db->where('email',$email)->get('mm_users')->num_rows();
        if($query > '0')
        {
            $this->session->set_flashdata('error_red',EMAIL_EXIST);
            redirect();

        }else{

            $user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

            $data = array(
                'user_id' => $user_id,
                'name' => $name,
                'mobile' => $mobile,
                'email' => $email,
                'password' => $password,
                'status' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                );
            $this->db->insert('mm_users',$data);

            $url = base_url().'/api/userActive?id=' . base64_encode($user_id);
            $subject = "Matrimony Registration";
            $msg = "Thanks for signing up with Matrimony!<br><br> Your account has been created, <br><br>Please verify your account with below link. " . $url;

            $this->Api_Auth_model->send_email_by_msg($email, $subject, $msg);
            $this->session->set_flashdata('error',REGISTERD_SUCCESS);
            redirect();
        }
    }

    public function userResetPassword($email)
    {
    	if($email !='')
        {
            $q = $this->db->where('email',$email)->get('mm_users')->num_rows();
            if($q > 0)
            {
                // $password = substr( "abcdefghijklmnopqrstuvwxyz" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
                $length = 6;
                $password = "";
                $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
                $codeAlphabet.= "0123456789";
                $max = strlen($codeAlphabet); // edited

                for ($i=0; $i < $length; $i++) {
                    $password .= $codeAlphabet[random_int(0, $max-1)];
                }

                $data = array(
                    'password' => md5($password),
                );
                $this->db->where('email',$email);
                $q = $this->db->update('mm_users',$data);
                if($q)
                {
                    $subject = FORG_PASS;
                    $msg = "Use $password as password your Matrimony App account. Please do not share this Password to anyone for security reasons.";

                    $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
                    return 1;
                }
            }else{
                return 2;
            }
        }else{
            $this->session->set_flashdata('error_red',ALL_FIELD_MANDATORY);
            redirect();
            exit();
        }
    }

    public function getShortlistUser($user_id)
    {
        $this->db->select('u.*');
        $this->db->from('mm_users u');
        $this->db->join('mm_shortlists s','s.short_listed_id = u.user_id');
        $this->db->where('s.user_id',$user_id);
        $this->db->order_by('u.s_no', 'desc');
        $query = $this->db->get();
        $user = $query->result();
        return $user;
    }

    public function getSendInterest($user_id)
    {
        $this->db->select('u.*');
        $this->db->from('mm_users u');
        $this->db->join('mm_interest i','i.requested_id = u.user_id');
        $this->db->where('i.user_id',$user_id);
        $this->db->where('u.status','1');
        $this->db->order_by('u.s_no', 'desc');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function getReceivedInterest($user_id)
    {
        $this->db->select('u.*');
        $this->db->from('mm_users u');
        $this->db->join('mm_interest i','i.user_id = u.user_id');
        $this->db->where('i.requested_id',$user_id);
        $this->db->where('u.status','1');
        $this->db->order_by('u.s_no', 'desc');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function getShortlistByLoginUserId($user_id,$short_listed_id)
    {
        $row = $this->db->where('user_id',$user_id)->where('short_listed_id',$short_listed_id)->get('mm_shortlists')->row_array();
        if($row !='')
        {
            return 1;
        }else{
            return 0;
        }
    }

    public function shortlistAction($user_id,$short_listed_id)
    {
        
        $favExist = $this->db->where('short_listed_id',$short_listed_id)->where('user_id',$user_id)->get('mm_shortlists');
        if($favExist->num_rows() > 0)
        {
            $this->db->where('short_listed_id',$short_listed_id);
            $this->db->where('user_id',$user_id);
            $this->db->delete('mm_shortlists');
        }
        else
        {
            $data = array(
                'short_listed_id' => $short_listed_id, 
                'user_id' => $user_id, 
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_shortlists',$data);
        }

        redirect('interest');
    }

    public function getUserMatches($user_id)
    {
        $user = $this->db->where('user_id',$user_id)->get('mm_users')->row_array();
        if($user['gender'] == '1')
        {
            //Femail Data Show
            $this->db->where('manglik',$user['manglik']);
            $this->db->where('gender','2');
            $this->db->where('status','1');
            $res = $this->db->get('mm_users')->result();
            return $res;
        }
        else if($user['gender'] == '2')
        {
            //Male Data Show
            $this->db->where('manglik',$user['manglik']);
            $this->db->where('gender','1');
            $this->db->where('status','1');
            $res = $this->db->get('mm_users')->result();
            return $res;
        }
        
    }

    public function getChatList($user_id)
    {
        $result = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$user_id' OR to_user_id = '$user_id') Order by updated_at DESC")->result();
        $get_chats = array();
        foreach($result as $i => $r)
        {
            $get_chats[$i] = array();
            $message_head_id = $r->message_head_id;
            $get_chats[$i]['message_head_id'] = $message_head_id;
            $get_chats[$i]['updated_at'] = round(strtotime($r->updated_at));
            $messageObjData = $this->db->query("SELECT * FROM mm_message WHERE message_head_id = '$message_head_id' Order by message_id DESC limit 1")->row();
            $get_chats[$i]['message'] = $messageObjData->message;
            if($user_id==$r->from_user_id){
                $userObjData = $this->db->where('user_id',$r->to_user_id)->get('mm_users')->row();
                // $userImgData = $this->db->where('user_id',$r->to_user_id)->get('mm_user_image')->row();
            } else {
                $userObjData = $this->db->where('user_id',$r->from_user_id)->get('mm_users')->row();
                // $userImgData = $this->db->where('user_id',$r->from_user_id)->get('mm_user_image')->row();
            }
            $get_chats[$i]['user_name'] = $userObjData->name;
            $get_chats[$i]['user_id'] = $userObjData->user_id;
            $get_chats[$i]['user_image'] = base_url().'assets/images/user/'.$userObjData->user_avtar;

        }
        return $get_chats;
    }

    public function getChatHistory($to_user_id,$user_id)
    {
        $check = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$user_id') ");
        if($check->num_rows() > 0)
        {
            $msg_head = $check->row_array();
            $message_head_id = $msg_head['message_head_id'];
            $this->db->where('message_head_id',$message_head_id);
            $this->db->order_by('message_id', 'Asc');
            $userObjMsg = $this->db->get('mm_message')->result(); 
            foreach ($userObjMsg as $i => $r) 
            {
                $get_msg[$i] = array();
                $get_msg[$i]['message_id'] = $r->message_id;
                $get_msg[$i]['message_head_id'] = $r->message_head_id;
                $get_msg[$i]['from_user_id'] = $r->from_user_id;
                $get_msg[$i]['to_user_id'] = $r->to_user_id;
                $get_msg[$i]['message'] = $r->message;
                $get_msg[$i]['type'] = $r->type;
                $get_msg[$i]['media'] = base_url().$r->media;
                $get_msg[$i]['is_read'] = $r->is_read;
                $get_msg[$i]['created_at'] = round(strtotime($r->created_at));
            }

            return  $get_msg;
        }

    }

    public function setChatMsg($to_user_id,$from_user_id)
    {
        $check = $this->db->query("SELECT * FROM mm_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id') ");
        if($check->num_rows() > 0)
        {
            $msg_head = $check->row_array();
            $message_head_id = $msg_head['message_head_id'];
            $data = array(
                'updated_at' => date('Y-m-d H:i:s')
                    );
            $this->db->where('message_head_id',$message_head_id);
            $this->db->update('mm_message_head',$data);
        }else{
            $data1 = array(
                'from_user_id' => $from_user_id,
                'to_user_id' => $to_user_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
                    );
            $this->db->insert('mm_message_head',$data1);
            $message_head_id = $this->db->insert_id();

        }
        return $message_head_id;
    }

    public function userImageUpdate($input,$img)
    {

        $config['upload_path']   = 'assets/images/user/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['overwrite']     = TRUE;
        $config['max_size']      = 1000000;
        $config['file_name']     = time();
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        $image = "";
        if ($this->upload->do_upload('image')) {
            $uploadData = $this->upload->data(); 
            $image = $uploadData['file_name']; 
        }

            $data = array(
                'user_avtar' => $image,
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $this->db->where('user_id',$input['user_id']);
            $this->db->update('mm_users',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('profile');
            
        }

    // public function userImageUpdate($input,$img)
    // {
    //     $imgExist = $this->db->where('user_id',$input['user_id'])->get('mm_user_image');
    //     if($imgExist->num_rows() > 0)
    //     {
    //         $img_del = $this->db->where('user_id',$input['user_id'])->delete('mm_user_image');

    //         $config['upload_path']   = 'assets/images/user/';
    //         $config['allowed_types'] = 'gif|jpg|jpeg|png';
    //         $config['overwrite']     = TRUE;
    //         $config['max_size']      = 1000000;
    //         $config['file_name']     = time();
    //         $this->load->library('upload',$config);
    //         $this->upload->initialize($config);
    //         $image = "";
    //         if ($this->upload->do_upload('image')) {
    //             $uploadData = $this->upload->data(); 
    //             $image = $uploadData['file_name']; 
    //         }

    //         $data = array(
    //             'image' => $image,
    //             'created_at' => date('Y-m-d H:i:s'),
    //         );
    //         $this->db->where('user_id',$input['user_id']);
    //         $this->db->update('mm_user_image',$data);
    //         $this->session->set_flashdata('error', DATA_UPDATE);
    //         redirect('profile');
    //     }
    //     else
    //     {    
    //         $config['upload_path']   = 'assets/images/user/';
    //         $config['allowed_types'] = 'gif|jpg|jpeg|png';
    //         $config['overwrite']     = TRUE;
    //         $config['max_size']      = 1000000;
    //         $config['file_name']     = time();
    //         $this->load->library('upload',$config);
    //         $this->upload->initialize($config);
    //         $image = "";
    //         if ($this->upload->do_upload('image')) {
    //             $uploadData = $this->upload->data(); 
    //             $image = $uploadData['file_name']; 
    //         }

    //         $data = array(
    //             'user_id' => $input['user_id'],
    //             'image' => $image,
    //             'created_at' => date('Y-m-d H:i:s'),
    //         );
    //         $this->db->insert('mm_user_image',$data);
    //         $this->session->set_flashdata('error', DATA_UPDATE);
    //         redirect('profile');
    //     }
    // }

}