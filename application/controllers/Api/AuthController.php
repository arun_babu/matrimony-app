<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function generateOtp()
    {
        $input = $this->input->post();
        $data = $this->Api_Auth_model->generateOtp($input);
        
        if($data == '2')
        {
            $this->Api_Auth_model->responseFailed(0,MOBILE_EXIST);
        }
        else if($data == '3')
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
        }
        else
        {
            $this->Api_Auth_model->responseSuccessOtp(1, OTP_SEND,$data);
        }
    }

    public function verifyOtp()
    {
        $input = $this->input->post();
        $data = $this->Api_Auth_model->verifyOtp($input);
        if($data == '1')
        {
            $this->Api_Auth_model->responseSuccessWethoutData(1, VERIFY_OTP);
        }
        else if($data == '2')
        {
            $this->Api_Auth_model->responseFailed(0,WRONG_OTP);
        }
        else if($data == '3')
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
        }
    }

    public function signup()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        
        $data = $this->Api_Auth_model->signup($input,$lan);
        if($data == '1')
        {
            if($lan == 'en')
            {
                $EMAIL_EXIST = EMAIL_EXIST;
            }else if($lan == 'hi')
            {
                $EMAIL_EXIST = EMAIL_EXIST_HI;
            }else if($lan == 'mr')
            {
                $EMAIL_EXIST = EMAIL_EXIST_MR;
            }

            $this->Api_Auth_model->responseFailed(0,$EMAIL_EXIST);
            exit();
        }
        else if($data == '2')
        {
            if($lan == 'en')
            {
                $ALL_FIELD_MANDATORY = ALL_FIELD_MANDATORY;
            }else if($lan == 'hi')
            {
                $ALL_FIELD_MANDATORY = ALL_FIELD_MANDATORY_HI;
            }else if($lan == 'mr')
            {
                $ALL_FIELD_MANDATORY = ALL_FIELD_MANDATORY_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$ALL_FIELD_MANDATORY);
        }
        else
        {
            if($lan == 'en')
            {
                $DATA_SUBMIT = DATA_SUBMIT;
            }else if($lan == 'hi')
            {
                $DATA_SUBMIT = DATA_SUBMIT_HI;
            }else if($lan == 'mr')
            {
                $DATA_SUBMIT = DATA_SUBMIT_MR;
            }
            $this->Api_Auth_model->responseSuccess(1, $DATA_SUBMIT, $data);
        }
    }

    public function userActive()
    {
        $user_id = base64_decode($_GET['id']);
        $data = $this->Api_Auth_model->userActive($user_id);
        // redirect('Admin/post');
        $this->load->view('auth/activeUser');
        
    }

    public function login()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $getdata = $this->Api_Auth_model->login($input);
        if($getdata == '0')
        {
            if($lan == 'en')
            {
                $LOGIN_SUCCESS = LOGIN_SUCCESS;
            }else if($lan == 'hi')
            {
                $LOGIN_SUCCESS = LOGIN_SUCCESS_HI;
            }else if($lan == 'mr')
            {
                $LOGIN_SUCCESS = LOGIN_SUCCESS_MR;
            }
            $data = $this->Api_Auth_model->single_row_data($input);
            $this->Api_Auth_model->responseSuccess(1, $LOGIN_SUCCESS, $data);
        }
        else if($getdata == '1')
        {
            if($lan == 'en')
            {
                $USER_NOT = USER_NOT;
            }else if($lan == 'hi')
            {
                $USER_NOT = USER_NOT_HI;
            }else if($lan == 'mr')
            {
                $USER_NOT = USER_NOT_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$USER_NOT);
        }
        else if($getdata == '2')
        {
            if($lan == 'en')
            {
                $VALID_PASS = VALID_PASS;
            }else if($lan == 'hi')
            {
                $VALID_PASS = VALID_PASS_HI;
            }else if($lan == 'mr')
            {
                $VALID_PASS = VALID_PASS_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$VALID_PASS);
        }
        else if($getdata == '3')
        {
            if($lan == 'en')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW;
            }else if($lan == 'hi')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_HI;
            }else if($lan == 'mr')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$DEACTIVE_NEW);
        }
        else if($getdata == '4')
        {
            if($lan == 'en')
            {
                $ALL_FIELD_MANDATORY = ALL_FIELD_MANDATORY;
            }else if($lan == 'hi')
            {
                $ALL_FIELD_MANDATORY = ALL_FIELD_MANDATORY_HI;
            }else if($lan == 'mr')
            {
                $ALL_FIELD_MANDATORY = ALL_FIELD_MANDATORY_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$ALL_FIELD_MANDATORY);
        }
        else if($getdata == '5')
        {
            $this->Api_Auth_model->responseFailed(0,DELETE_ACCOUNT_CONTACT_ADMIN);
        }
        
    }

    public function forgotPassword()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $email = $this->input->post('email');
        $query = $this->Api_Auth_model->forgotpassword($email);
        if($query == 1)
        {
            if($lan == 'en')
            {
                $PASS_SEND = PASS_SEND;
            }else if($lan == 'hi')
            {
                $PASS_SEND = PASS_SEND_HI;
            }else if($lan == 'mr')
            {
                $PASS_SEND = PASS_SEND_MR;
            }
            $this->Api_Auth_model->responseSuccess(1, $PASS_SEND, '');
        }
        else if($query == 2)
        {
            if($lan == 'en')
            {
                $VALID_EMAIL = VALID_EMAIL;
            }else if($lan == 'hi')
            {
                $VALID_EMAIL = VALID_EMAIL_HI;
            }else if($lan == 'mr')
            {
                $VALID_EMAIL = VALID_EMAIL_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$VALID_EMAIL);
        }
        else{
            if($lan == 'en')
            {
                $SOMTHING = SOMTHING;
            }else if($lan == 'hi')
            {
                $SOMTHING = SOMTHING_HI;
            }else if($lan == 'mr')
            {
                $SOMTHING = SOMTHING_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$SOMTHING);
        }
    }

    public function logOut()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        }

        if($lan == 'en')
        {
            $LOGOUT = LOGOUT;
        }else if($lan == 'hi')
        {
            $LOGOUT = LOGOUT_HI;
        }else if($lan == 'mr')
        {
            $LOGOUT = LOGOUT_MR;
        }
        $input = $this->input->post();
        $this->Api_Auth_model->logOut($input);
        $this->Api_Auth_model->responseSuccess(1, $LOGOUT, '');
    }

    public function changePassword()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $query = $this->Api_Auth_model->changePassword($input,$lan);
        if($query == 1)
        {
            if($lan == 'en')
            {
                $PASS_CHANGE = PASS_CHANGE;
            }else if($lan == 'hi')
            {
                $PASS_CHANGE = PASS_CHANGE_HI;
            }else if($lan == 'mr')
            {
                $PASS_CHANGE = PASS_CHANGE_MR;
            }
            $this->Api_Auth_model->responseSuccess(1, $PASS_CHANGE, '');
        }
        else if($query == 2)
        {
            if($lan == 'en')
            {
                $USER_NOT = USER_NOT_FOUND;
            }else if($lan == 'hi')
            {
                $USER_NOT = USER_NOT_FOUND_HI;
            }else if($lan == 'mr')
            {
                $USER_NOT = USER_NOT_FOUND_MR;
            }
            $this->Api_Auth_model->responseFailed(0, $USER_NOT);
        }
        else
        {
            if($lan == 'en')
            {
                $SOMTHING = SOMTHING;
            }else if($lan == 'hi')
            {
                $SOMTHING = SOMTHING_HI;
            }else if($lan == 'mr')
            {
                $SOMTHING = SOMTHING_MR;
            }
            $this->Api_Auth_model->responseFailed(0, $SOMTHING);
        }
    }

    public function deleteAccount()
    {
        $input = $this->input->post();
        $this->Api_Auth_model->deleteAccount($input);
    }

    public function checkUser(){
		$input = $this->input->post();
		$this->Api_Auth_model->checkUser($input);
	}
    

}
