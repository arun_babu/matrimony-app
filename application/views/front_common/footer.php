<?php
$social = $this->CmsManagement_model->socialMedia();
?>
      <footer>
            <div class="copyright">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <ul class="icon">
                           <li><a href="<?=$social['facebook']; ?>" target="_blank"><i class="fa fa-facebook-f"></i>
                              </a>
                           </li>
                           <li><a href="<?=$social['insta']; ?>" target="_blank"><i class="fa fa-instagram"></i>
                              </a>
                           </li>
                           <li><a href="<?=$social['linkedin']; ?>" target="_blank"><i class="fa fa-linkedin"></i>
                              </a>
                           </li>
                           <li><a href="<?=$social['twitter']; ?>" target="_blank"><i class="fa fa-twitter"></i>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="row icon-1">
                     <div class="col-md-4 text-center">
                        <p class="text-white" >
                           Copyright ©2020 All rights reserved | Powered by <a href="#" class="copy-r">Matrimonix</a>  </p class="text-white" >
                     </div>
                     <div class="col-md-4 text-center copy">
                     <p class="text-white">  <i class="fa fa-phone"></i>
                     <?=$social['contact']; ?></p>
                     </div>
                     <div class="col-md-4 text-center">
                        <p class="text-white"><i class="fa fa-envelope"></i>
                           <?=$social['email']; ?>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
      </footer>
      
     

      <!-- <script src='<?php echo base_url('front_assets'); ?>/js/jquery.min.js'></script> -->
      <script src="<?php echo base_url('front_assets'); ?>/js/bootstrap.min.js" ></script>
      <script src="<?php echo base_url('front_assets'); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src='<?php echo base_url('front_assets'); ?>/js/owl.carousel.min.js'></script>
      <script src='<?php echo base_url('front_assets'); ?>/js/main.js'></script>
      <script type="text/javascript"> 
          /********************** load js *******************/
         $(document).ready(function(){
            $("div").slice(0,3).show();
            $("#seeMore").click(function(e){
              e.preventDefault();
              $("div:hidden").slice(0,3).fadeIn("slow");
              
              if($("div:hidden").length == 0){
                 $("#seeMore").fadeOut("slow");
                }
            });
          })
          </script>
          <script type="text/javascript">
        
            $('.m-card-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            })
      </script>


<script type="text/javascript">
  $(document).ready(function() {

  $(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
      numPanelOpen = $(accordionId + ' .collapse.in').length;
    
    $(this).toggleClass("active");

    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })

  openAllPanels = function(aId) {
    console.log("setAllPanelOpen");
    $(aId + ' .panel-collapse:not(".in")').collapse('show');
  }
  closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    $(aId + ' .panel-collapse.in').collapse('hide');
  }
     
});
</script>



 <script>
         $(document).on("scroll", function(){
          if
              ($(document).scrollTop() > 86){
            $("#banner").addClass("shrink");
          }
          else
          {
            $("#banner").removeClass("shrink");
          }
         });
                
      </script>
      <script type="text/javascript">
         "use strict"; // Start of use strict
         (function($) {
         
         
           function bootstrapAnimatedLayer() {
         
               function doAnimations(elems) {
                   //Cache the animationend event in a variable
                   var animEndEv = "webkitAnimationEnd animationend";
         
                   elems.each(function() {
                       var $this = $(this),
                           $animationType = $this.data("animation");
                       $this.addClass($animationType).one(animEndEv, function() {
                           $this.removeClass($animationType);
                       });
                   });
               }
         
               //Variables on page load
               var $myCarousel = $("#minimal-bootstrap-carousel"),
                   $firstAnimatingElems = $myCarousel
                   .find(".carousel-item:first")
                   .find("[data-animation ^= 'animated']");
         
               //Initialize carousel
               $myCarousel.carousel();
         
               //Animate captions in first slide on page load
               doAnimations($firstAnimatingElems);
         
               //Other slides to be animated on carousel slide event
               $myCarousel.on("slide.bs.carousel", function(e) {
                   var $animatingElems = $(e.relatedTarget).find(
                       "[data-animation ^= 'animated']"
                   );
                   doAnimations($animatingElems);
               });
           }
         
           bootstrapAnimatedLayer();
         
         
         
         })(jQuery);
      </script>
      <script type="text/javascript">
          $(document).ready(function(){
            $('#action_menu_btn').click(function(){
              $('.action_menu').toggle();
            });
              });
      </script>

<script type="text/javascript">
     $(function(){
$("#addClass").click(function () {
          $('#qnimate').addClass('popup-box-on');
            });
          
            $("#removeClass").click(function () {
          $('#qnimate').removeClass('popup-box-on');
            });
  })
</script>

<script type="text/javascript">
     $(function(){
$("#addClass1").click(function () {
          $('#qnimate').addClass('popup-box-on');
            });
          
            $("#removeClass").click(function () {
          $('#qnimate').removeClass('popup-box-on');
            });
  })
</script>



       </body>
    </html>   