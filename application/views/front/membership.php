<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/bootstrap.min.css" >

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/style.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.carousel.min.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.theme.default.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
      <title>Matrimony</title>
   </head>
   <body>
      <header >
         <nav class="navbar navbar-expand-md navbar-dark" >
            <div class="container">
               <!-- Brand -->
               <a class="navbar-brand" href="index.php"><span>Matrimony</span>.com</a>
               <!-- Toggler/collapsibe Button -->
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
               Login
               </button>
               <!-- Navbar links -->
               <div class="collapse navbar-collapse" id="collapsibleNavbar">
                  <ul class="navbar-nav ml-auto">
                     <!-- <li class="nav-item">
                        <a class="nav-link" href="#popup1">Login</a>              
                        </li> -->
                     <!--   <li class="nav-item">
                        <a  href="#" class="nav-link dropdown-toggle" data-toggle="modal" data-target="#myModal">Login</a>
                        </li> -->
                     <li class="nav-item">
                        <a  href="#" class="nav-link dropdown-toggle" data-toggle="modal" data-target="#myModal" > <img src="<?php echo base_url('front_assets'); ?>/images/call-center.png" class="img-fluid help-center"> Help</a>
                     </li>
                     <!-- Dropdown -->
                     <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Help
                        </a>
                        
                        </li> -->
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <section class="pricing-table bg-light" id="price-bg">
         <div class="container">
            <div class="updated">
               <h3>Upgrade Now & Get premium benefits for upto 4 weeks extra FREE!</h3>
               <div class="update-1">
                  <h5></h5>
                  <div class="background-pr">
                     <div class="box">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <div class="content">
                           <h2>Save upto 50% on Premium Plans!!! Valid for limited period! </h2>
                        </div>
                     </div>
                  </div>
               </div>
               <a class="skip" href="<?php echo site_url('profile'); ?>">Do This later <i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
            </div>
         </div>
         <div class="container">
            <hr>
            <div class="row">
               <div class="col-xl-12">
                  <div class="pricing-table">
                    <?php
                    foreach ($package as $p) 
                    {
                    
                    ?>
                     <div class="pricing-card">
                        <h3 class="pricing-card-header"><?=$p->title; ?></h3>
                        <div class="price"><sup>INR</sup><?=$p->price; ?><span>/<?php if($p->subscription_type==0){ echo "Free"; ?>
                                    <?php }else if($p->subscription_type==1){ echo "Monthly"; ?>
                                    <?php  }else if($p->subscription_type==2){  echo "Quarterly"; ?>
                                    <?php  }else if($p->subscription_type==3){ echo "Halfyearly"; ?>
                                    <?php  }else if($p->subscription_type==4){ echo "Yearly"; ?>
                                    <?php  } ?></span></div>
                        <ul>
                           <li><?=$p->description; ?> </li>
                        </ul>
                        <a href="#" class="order-btn buy_now" data-days="<?=$p->no_of_days?>" data-amount="<?=$p->price?>" data-id="<?=$p->packages_id?>">Buy Now</a>
                     </div>
                     <?php
                    }
                     ?>
                  </div>
               </div>
            </div>
         </div>
      </section>
 <!--      <section id="plans">
         <div class="container">
          <div class="row justify-content">
              <div class="col-lg-10 order-md-3 order-lg-3 col-md-10 d-flex">
               <div class="card cardss card-primary plans">
                  <h4 class="text-center">Experience Personalised Matchmaking starting at ₹29,500
                  </h4>
                  <a href="#" class="btn button-1 ar" role="button">View Plans
                  </a>
               </div>
            </div>
          </div>
         </div>
      </section> -->

      <section class="que-plans">
         <div class="container">
          <div class="sec-title">
             <h2>You have questions. We have the answers…</h2>
          </div>
            <div class="row justify-content">
              <div class="col-lg-10"> 
                 <div class="row">
                     <div class="col-lg-6">
                      <div class="qu-content">
                         <h4>What are some of the benefits of Premium plans?</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris</p>
                      </div>
                   </div>
                   <div class="col-lg-6">
                      <div class="qu-content">
                         <h4>What are some of the benefits of Premium plans?</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris</p>
                      </div>
                   </div>
                   <div class="col-lg-6">
                      <div class="qu-content">
                         <h4>What are some of the benefits of Premium plans?</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris</p>
                      </div>
                   </div>
                   <div class="col-lg-6">
                      <div class="qu-content">
                         <h4>What are some of the benefits of Premium plans?</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris</p>
                      </div>
                   </div>
                 </div>
              </div>
              
            </div>
         </div>
      </section>
  <!----------------- and questions ------------------>

   <section id="marriage-card">
       <div class="container">
          <div class="row justify-content">
             <div class="col-lg-10">
               <div class="sec-title">
                 <h2>With Shaadi Premium they found their perfect match and so can you</h2>
              </div>
                <div class="marriage-card">
                    <div class="owl-carousel m-card-carousel owl-theme">
                            <div class="item">
                                <div class="mm-c">
                                    <div class="row">
                                          <div class="col-lg-5">
                                             <div class="mr-img">
                                                 <img src="<?php echo base_url('front_assets'); ?>/images/marriage/m-card.jpg" class="img-fluid">
                                             </div>
                                          </div>
                                          <div class="col-lg-7">
                                            <div class="right-m">
                                              <i class="fa fa-quote-left" aria-hidden="true"></i>
                                                 <h4>Kanchan & Venkatesh</h4>
                                                 <p>Thanks to Shaadi.com team!! I finally got my dream partner with whom i would love to walk a journey of life. She is truly a gods gift for me and Shaad</p>
                                            </div>
                                          </div>
                                      </div> 
                                </div>
                            </div>
                             <div class="item">
                                <div class="mm-c">
                                    <div class="row">
                                          <div class="col-lg-5">
                                             <div class="mr-img">
                                                 <img src="<?php echo base_url('front_assets'); ?>/images/marriage/m-card.jpg" class="img-fluid">
                                             </div>
                                          </div>
                                          <div class="col-lg-7">
                                            <div class="right-m">
                                              <i class="fa fa-quote-left" aria-hidden="true"></i>
                                                 <h4>Kanchan & Venkatesh</h4>
                                                 <p>Thanks to Shaadi.com team!! I finally got my dream partner with whom i would love to walk a journey of life. She is truly a gods gift for me and Shaad</p>
                                            </div>
                                          </div>
                                      </div> 
                                </div>
                            </div>
                        </div>
                </div> 
             </div>
          </div>  
       </div>
   </section>

      <script>
         wow = new WOW(
         {
         boxClass: 'stamp',
         animateClass: 'hidden stamped',
         offset: 0,          // default
         mobile: true,       // default
         live: true        // default
         }
         )
         wow.init();
         wow2 = new WOW(
         {
         boxClass: 'sign-up-strip',
         animateClass: 'animated fadeInUpBig',
         offset: 0,
         mobile: true,
         live: true
         }
         )
         wow2.init();
      </script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>     
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var SITEURL = "<?php echo base_url() ?>";
  $('body').on('click', '.buy_now', function(e){
    // alert();
    var no_of_days = $(this).attr("data-days");
    var totalAmount = $(this).attr("data-amount");
    var package_id =  $(this).attr("data-id");
    var options = {
    // "key": "rzp_test_WU7Kfj9LTTV6i6",
    "key": "<?=$razorpay['razorpay_key'];?>",
    "amount": (Number(totalAmount)*100), // 2000 paise = INR 20
    "name": "Tutsmake",
    "description": "Payment",
    "image": "https://www.tutsmake.com/wp-content/uploads/2018/12/cropped-favicon-1024-1-180x180.png",
    "handler": function (response){
      // alert(response.razorpay_payment_id);
          $.ajax({
            url: SITEURL + 'razorPaySuccess',
            type: 'post',
            data: {
                razorpay_payment_id: response.razorpay_payment_id , totalAmount : totalAmount ,package_id : package_id, no_of_days : no_of_days,
            }, 
            success: function (msg) {
        // alert(msg);
               swal("Success", "Payment successfully", "success");
            }
        });
      
    },
 
    "theme": {
        "color": "#528FF0"
    }
  };
  var rzp1 = new Razorpay(options);
  rzp1.open();
  e.preventDefault();
  });
 
</script>