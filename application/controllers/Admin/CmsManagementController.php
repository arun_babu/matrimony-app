<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CmsManagementController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function websiteBanner()
    {
		$res['banner'] = $this->CmsManagement_model->websiteBanner();
		echo load_page($this,'cms_manage/website_banner',$res);
    }

    public function submitWebsiteBanner()
    {
    	$input = $this->input->post();
        $image = $_FILES['image']['name'];
    	$id = $this->CmsManagement_model->submitWebsiteBanner($input,$image);
    	
    }

    public function updateWebsiteBanner()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->CmsManagement_model->updateWebsiteBanner($id);
        $data['banner'] = $this->CmsManagement_model->websiteBanner();
        echo load_page($this,'cms_manage/website_banner',$data);
    }

    public function changeWebsiteBannerStatus()
    {
        $id = $this->uri->segment('3');
        $this->CmsManagement_model->changeWebsiteBannerStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('websiteBanner');
    }

    public function deleteWebsiteBanner()
    {
        $id = $this->uri->segment('3');
        $this->CmsManagement_model->deleteWebsiteBanner($id);
        $this->session->set_flashdata('error', DATA_DELETE);
        redirect('websiteBanner');
    }

    public function happyStory()
    {
    	$res['story'] = $this->CmsManagement_model->happyStory();
		echo load_page($this,'cms_manage/happy_story',$res);
    }

    public function submitHappyStory()
    {
    	$input = $this->input->post();
        $image = $_FILES['image']['name'];
    	$id = $this->CmsManagement_model->submitHappyStory($input,$image);
    	
    }

    public function updateHappyStory()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->CmsManagement_model->updateHappyStory($id);
        $data['story'] = $this->CmsManagement_model->happyStory();
        echo load_page($this,'cms_manage/happy_story',$data);
    }

    public function changeHappyStoryStatus()
    {
        $id = $this->uri->segment('3');
        $this->CmsManagement_model->changeHappyStoryStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('happyStory');
    }

    public function socialMedia()
    {
    	$res['social'] = $this->CmsManagement_model->socialMedia();
		echo load_page($this,'cms_manage/social_media',$res);
    }

    public function updateSocialMedia()
    {
    	$input = $this->input->post();
    	$id = $this->CmsManagement_model->updateSocialMedia($input);
    }

}