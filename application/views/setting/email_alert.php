<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Email alert</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Email alert</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    
                <h3 class="box-title m-b-0">Email alert List</h3>
                    
                  <p class="text-muted m-b-30 font-13">Email alert list with url and descriptions</p>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h4 >Live: <font color="#008b8b">https://phpstack-390430-1543668.cloudwaysapps.com/</font></h4>
                                     
                                </div>
                                <div class="form-group">
                                    <h4 >01) User Signup (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/signup' <br>
                                        --header 'language: en' \<br>
                                        --form 'profile_for=Self' \<br>
                                        --form 'gender=1' \<br>
                                        --form 'dob=1994-01-05' \<br>
                                        --form 'height=18' \<br>
                                        --form 'state=20' \<br>
                                        --form 'district=310' \<br>
                                        --form 'city=bhopal' \<br>
                                        --form 'pin=275217' \<br>
                                        --form 'qualification=BE' \<br>
                                        --form 'working=1' \<br>
                                        --form 'blood_group=4' \<br>
                                        --form 'aadhaar=987654789' \<br>
                                        --form 'marital_status=1' \<br>
                                        --form 'gotra=hindu' \<br>
                                        --form 'gotra_nanihal=hindu' \<br>
                                        --form 'caste=1' \<br>
                                        --form 'manglik=' \<br>
                                        --form 'birth_time=' \<br>
                                        --form 'birth_place=' \<br>
                                        --form 'mobile=8359947894' \<br>
                                        --form 'email=rahulrajpoot14357@gmail.com' \<br>
                                        --form 'name=Rahul Kumar' \<br>
                                        --form 'password=123456' \<br>
                                        --form 'device_type=android' \<br>
                                        --form 'device_token=kdfgdjfgsf5873456873jhbsjdf'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >02) User ForgotPassword (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'https://phpstack-390430-1543668.cloudwaysapps.com/admin/api/forgotPassword' <br>
                                        --header 'language: en' \<br>
                                        --form 'email=rahulrajpoot14357@gmail.com'<br>
                                        </font>
                                    </p>
                                </div>
                                

                            </div>
                            
                        </div>
                            
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        
        </div>
        <!-- /.row -->
    </div>
    </div>