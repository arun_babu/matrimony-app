<!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Matrimony</span></h3> </div>
                <ul class="nav" id="side-menu">
                    <!-- <li class="user-pro">
                        <a href="javascript:void(0)" class="waves-effect"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span class="hide-menu"> Steve Gection<span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                            <li><a href="javascript:void(0)"><i class="ti-user"></i> <span class="hide-menu">My Profile</span></a></li>
                            <li><a href="javascript:void(0)"><i class="ti-wallet"></i> <span class="hide-menu">My Balance</span></a></li>
                            <li><a href="javascript:void(0)"><i class="ti-email"></i> <span class="hide-menu">Inbox</span></a></li>
                            <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">Account Setting</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> <span class="hide-menu">Logout</span></a></li>
                        </ul>
                    </li> -->
                    <li class="<?php echo(isset($page) && $page == 'home') ? 'selected': '' ?>"> 
                        <a href="<?php echo base_url('dashboard'); ?>" class="waves-effect <?php echo(isset($page) && $page == 'home') ? 'active': '' ?>"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard <span class="fa arrow"></span> <span class="label label-rouded label-inverse pull-right">4</span></span></a>
                        
                    </li>
                    <li> <a href="javascript:void(0)" class="waves-effect "><i class="mdi mdi-account fa-fw"></i> <span class="hide-menu">User<span class="fa arrow"></span><span class="label label-rouded label-warning pull-right">30</span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?=base_url('users');?>"><i class="mdi mdi-account-multiple fa-fw"></i> <span class="hide-menu"> All Users </span></a> </li>
                            <li> <a href="<?=base_url('requestUsers');?>"><i class="mdi mdi-account-convert fa-fw"></i> <span class="hide-menu"> Users request </span></a> </li>
                        </ul>
                    </li>
                    <!-- <li> <a href="<?=base_url('users');?>" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw" data-icon="v"></i> <span class="hide-menu"> Users </span></a>
                    </li> -->

                    <li> <a href="<?=base_url('news');?>" class="waves-effect"><i class="mdi mdi-book-multiple-variant fa-fw" data-icon="v"></i> <span class="hide-menu"> News </span></a>
                    </li>
                    <li> <a href="<?=base_url('packages');?>" class="waves-effect"><i class="mdi mdi-book-plus fa-fw" data-icon="v"></i> <span class="hide-menu"> Packages </span></a>
                    </li>
                    
                    
                    <li> <a href="<?=base_url('caste');?>" class="waves-effect"><i class="mdi mdi-yeast fa-fw" data-icon="v"></i> <span class="hide-menu"> Caste </span></a>
                    </li>
                    <li> <a href="<?=base_url('manglik');?>" class="waves-effect"><i class="mdi mdi-vector-triangle fa-fw" data-icon="v"></i> <span class="hide-menu"> Manglik </span></a>
                    </li>
                    <li> <a href="<?=base_url('occupation');?>" class="waves-effect"><i class="mdi mdi-wallet-travel fa-fw" data-icon="v"></i> <span class="hide-menu"> Occupation </span></a>
                    </li>
                    <li> <a href="<?=base_url('income');?>" class="waves-effect"><i class="mdi mdi-cash fa-fw" data-icon="v"></i> <span class="hide-menu"> Income </span></a>
                    </li>

                    <li> <a href="javascript:void(0)" class="waves-effect "><i class="mdi mdi-altimeter fa-fw"></i> <span class="hide-menu">CMS Management<span class="fa arrow"></span><span class="label label-rouded label-warning pull-right">30</span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?=base_url('websiteBanner');?>"><i class="mdi mdi-file-image fa-fw"></i> <span class="hide-menu"> Website Banner </span></a> </li>
                            <li> <a href="<?=base_url('happyStory');?>"><i class="mdi mdi-tooltip-text fa-fw"></i> <span class="hide-menu"> Happy Story </span></a> </li>
                            <li> <a href="<?=base_url('socialMedia');?>"><i class="mdi mdi-vpn fa-fw"></i> <span class="hide-menu">About & Social Media </span></a> </li>
                            
                        </ul>
                    </li>

                    <li> <a href="javascript:void(0)" class="waves-effect "><i class="mdi mdi-altimeter fa-fw"></i> <span class="hide-menu">Services<span class="fa arrow"></span><span class="label label-rouded label-warning pull-right">30</span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?=base_url('category');?>"><i class="mdi mdi-file-image fa-fw"></i> <span class="hide-menu"> Category </span></a> </li>
                            <li> <a href="<?=base_url('serviceProvider');?>"><i class="mdi mdi-tooltip-text fa-fw"></i> <span class="hide-menu"> Service Provider </span></a> </li>
                            
                        </ul>
                    </li>

                    <li> <a href="javascript:void(0)" class="waves-effect "><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Settings<span class="fa arrow"></span><span class="label label-rouded label-warning pull-right">30</span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?=base_url('notification');?>"><i class="mdi mdi-notification-clear-all fa-fw"></i> <span class="hide-menu"> Notification </span></a></li>
                            <li> <a href="<?=base_url('apiKeys');?>"><i class="mdi mdi-key fa-fw"></i> <span class="hide-menu"> Api Keys </span></a> </li>
                            <li> <a href="<?=base_url('language');?>"><i class="mdi mdi-message-draw fa-fw"></i> <span class="hide-menu"> Language </span></a> </li>
                            <li> <a href="<?=base_url('webservice');?>"><i class="mdi mdi-web fa-fw"></i> <span class="hide-menu"> Webservice </span></a> </li>
                            <li> <a href="<?=base_url('emailAlert');?>"><i class="mdi mdi-email fa-fw"></i> <span class="hide-menu"> Email Alert </span></a> </li>
                            <li> <a href="<?=base_url('faqContent');?>"><i class="mdi mdi-table-edit fa-fw"></i><span class="hide-menu">FAQ</span></a> </li>
                            <li> <a href="<?=base_url('privacyPolicyContent');?>"><i class="mdi mdi-certificate fa-fw"></i><span class="hide-menu">Privacy Policy</span></a> </li>
                            <li> <a href="<?=base_url('termsContent');?>"><i class="mdi mdi-certificate fa-fw"></i><span class="hide-menu">Terms & Condition</span></a> </li>
                        </ul>
                    </li>
                    
                    
                    <li class="devider"></li>
                    <!-- <li> <a href="widgets.html" class="waves-effect"><i  class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Widgets</span></a> </li> -->
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->