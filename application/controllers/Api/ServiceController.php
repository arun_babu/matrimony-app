<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getCategory()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 
        $getdata = $this->Api_Service_model->getCategory($lan);
        if($getdata)
        {

            $this->Api_Auth_model->responseSuccess(1, 'Category', $getdata);
        }else{
        	if($lan == 'en')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }else if($lan == 'hi')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
            }else if($lan == 'mr')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
            }else{
            	$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }
            $this->Api_Auth_model->responseFailed(0,$DATA_NOT_AVAIABLE);
        }
    }

    public function getServiceByCategory()
    {
    	foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 
        $category_id = $this->input->post('category_id');
        $getdata = $this->Api_Service_model->getServiceByCategory($category_id);
        if($getdata == 0)
        {
        	if($lan == 'en')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }else if($lan == 'hi')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
            }else if($lan == 'mr')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
            }else{
            	$DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }
            $this->Api_Auth_model->responseFailed(0,$DATA_NOT_AVAIABLE);
            
        }else{
        	$this->Api_Auth_model->responseSuccess(1, 'list', $getdata);
        	
        }
    }

    public function reportUser()
    {
        $input = $this->input->post();
        $this->Api_Service_model->reportUser($input);
        $this->Base_model->responseSuccessWethoutData(1, REPORT);
       
    }

}