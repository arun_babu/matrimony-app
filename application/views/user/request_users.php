<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Users Requests</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="active">Users Requests</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <h3 class="box-title m-b-0">ALL USER Requests</h3>
                    <p class="text-muted m-b-30 font-13">You can see all details of Requests List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Detail</th> 
                                    <th>Manage</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($user as $u)
                                {
                                    $user1 = $this->User_model->getUserData($u->user_id);
                                    $user_img1 = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $u->user_id ));
                                    $user2 = $this->User_model->getUserData($u->requested_id);
                                    $user_img2 = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $u->requested_id ));
                                ?>
                                <tr>
                                    <td>
                                        <?php
                                          if($user_img1 !='')
                                          {
                                          ?>
                                          <img alt="user" style="height: 175px;" src="<?=base_url('assets');?>/images/user/<?=$user_img1->image; ?>">
                                          <?php
                                          }else{
                                          ?>
                                          <img alt="user" style="height: 175px;" src="<?=base_url('assets');?>/images/image.png"> 
                                        <?php } ?>
                                        <br>
                                        <h3><?=$user1['name']; ?></h3>
                                        <!-- <br> -->
                                        <?php
                                        if($user1['gender'] == 1)
                                        {
                                            echo "Male";
                                        }else{
                                            echo "Female";
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <a title="Verified" class="buttonss" href="<?php echo base_url('user/changeRequestStatus');?>/<?php echo $u->interest_id;?>">Mark as Accepted</a>

                                        <img alt="arrow" style="margin-top: 52px;" src="<?=base_url('assets');?>/images/right-arrow.png">
                                    </td>
                                    <td>
                                        <?php
                                          if($user_img2 !='')
                                          {
                                          ?>
                                          <img alt="user" style="height: 175px;" src="<?=base_url('assets');?>/images/user/<?=$user_img2->image; ?>">
                                          <?php
                                          }else{
                                          ?>
                                          <img alt="user" style="height: 175px;" src="<?=base_url('assets');?>/images/image.png"> 
                                        <?php } ?>
                                        <br>
                                        <h3><?=$user2['name']; ?></h3>
                                        <!-- <br> -->
                                        <?php
                                        if($user2['gender'] == 1)
                                        {
                                            echo "Male";
                                        }else{
                                            echo "Female";
                                        }

                                        ?>
                                            
                                    </td>
                                    
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<style type="text/css">
.buttonss {
    display: block;
    /*width: 115px;*/
    height: 25px;
    background: #4E9CAF;
    /*padding: 10px;*/
    text-align: center;
    border-radius: 5px;
    color: white;
    font-weight: bold;
    line-height: 25px;
}

</style>    