<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChatController extends CI_Controller{

	public function __construct()
    {
    	require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function setMessage()
    {
    	foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $img = (isset($_FILES['media'])) ? $_FILES['media']['name'] : '';
        $message_head_id = $this->Api_Chat_model->set_message($input);

        // $toStudentExst = $this->db->where('student_id',$input['to_user_id'])->get('am_student');
        // if($toStudentExst->num_rows() > 0)
        // {
        //     $student = $toStudentExst->row_array();
        //     $to_user_id = $student['student_id'];
        // }else{
        //     $shopExist = $this->db->where('teacher_id',$input['to_user_id'])->get('am_teacher')->row_array();
        //     $to_user_id = $shopExist['teacher_id'];
        // }

        $to_user_id = $input['to_user_id'];

        if($img == '')
        {
            $this->form_validation->set_rules('message', 'message', 'required');
            if ($this->form_validation->run() == false) {
                $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
                exit();
            }
            $data = array(
                'message_head_id' => $message_head_id,
                'from_user_id' => $input['from_user_id'],
                'to_user_id' => $to_user_id,
                'message' => $input['message'],
                'type' => $input['type'],
                // 'created_at' => date('Y-m-d H:i:s'),
                // 'updated_at' => date('Y-m-d H:i:s'),
                );
            $this->db->insert('mm_message',$data);

        }else{

            $this->load->library('upload');
            $config['image_library']  = 'gd2';
            $config['upload_path']    = './assets/images/';
            $config['allowed_types']  = 'gif|jpg|jpeg|png';
            $config['max_size']       = 1000000000;
            $config['file_name']      = time();
            // $config['create_thumb']   = TRUE;
            // $config['maintain_ratio'] = TRUE;
            // $config['width']          = 250;
            // $config['height']         = 250;
            $this->upload->initialize($config);
            $updateduserimage = "";
            if ($this->upload->do_upload('media') && $this->load->library('image_lib', $config)) {
                $updateduserimage = 'assets/images/' . $this->upload->data('file_name');
            } else {
                //  echo $this->upload->display_errors();
            }
            
            if ($updateduserimage) {
                $media = $updateduserimage;
            } else {
                $media = "";
            }

            $data1 = array(
                'message_head_id' => $message_head_id,
                'from_user_id' => $input['from_user_id'],
                'to_user_id' => $to_user_id,
                'message' => $input['message'],
                'type' => $input['type'],
                'media' => $media,
                // 'created_at' => date('Y-m-d H:i:s'),
                // 'updated_at' => date('Y-m-d H:i:s'),
                );
            $this->db->insert('mm_message',$data1);
        }

        $su = $this->db->where('user_id',$input['from_user_id'])->get('mm_users')->row();
        
        $ru = $this->db->where('user_id',$to_user_id)->get('mm_users')->row();
        
        $msg  = $su->name . ':' . $input['message'];
        $type = CHAT_TYPE;
        $this->firebase_with_class($ru->device_token, $input['from_user_id'], $su->name, "Chat", $type, $msg);
        if($lan == 'en')
        {
            $MSG_SEND = MSG_SEND;
        }else if($lan == 'ar')
        {
            $MSG_SEND = MSG_SEND_HI;
        }else if($lan == 'mr')
        {
            $MSG_SEND = MSG_SEND_MR;
        }
        $this->Base_model->responseSuccessWethoutData(1, $MSG_SEND);
    }

    public function firebase_with_class($device_token, $sender_id, $senderName, $title, $type, $msg1)
    {
        $user = $this->db->where('device_token',$device_token)->get('mm_users')->row();
        
        $FIRE_BASE_KEY=$this->Base_model->getSingleRow('mm_firebase_key',array('id'=>1));
        $api_key=$FIRE_BASE_KEY->firebase_key;

        if ($user->device_type == "ios") {
            $API_ACCESS_KEY = $api_key;
            
            $msg    = array(
                'body' => $msg1,
                'title' => $title,
                'icon' => 'myicon',
                'type' => $type,
                /*Default Icon*/
                'sound' => 'default'
                /*Default sound*/
            );
            $fields = array(
                'to' => $device_token,
                'notification' => $msg
            );
            
            
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json'
            );
            #Send Reponse To FireBase Server    
            $ch      = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            // echo "<pre>";
            // print_r($result); die;
            curl_close($ch);
        } else {
            $FCMPushNotification = new \BD\FCMPushNotification($api_key);
            
            $sDeviceToken = $device_token;
            
            $aPayload = array(
                'data' => array(
                    'title' => $title,
                    'type' => $type,
                    "sender_id" => $sender_id,
                    'body' => $msg1,
                    "senderName" => $senderName
                )
            );
            
            $aOptions = array(
                'time_to_live' => 15 //means messages that can't be delivered immediately are discarded. 
            );
            
            $aResult = $FCMPushNotification->sendToDevice($sDeviceToken, $aPayload, $aOptions // optional
                );
            // echo "<pre>";
            // print_r($result); die;
            return $aResult;
        }
    }

    public function getMessageHistory()
    {
        $user_id = $this->input->post('user_id');
        $this->Api_Chat_model->get_message_history($user_id);

    }

    public function getMessage()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 
        $input = $this->input->post();
        $this->Api_Chat_model->get_message($input,$lan);
    }

    public function blockUser()
    {
        $input = $this->input->post();
        $this->Api_Chat_model->blockUser($input);
    }

    public function unblockUser()
    {
        $input = $this->input->post();
        $this->Api_Chat_model->unblockUser($input);
    }


}