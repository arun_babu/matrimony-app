<section id="breadcrumb">
   <div class="container">
      <ul class="breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">Visitors</a></li>
      </ul>
   </div>
</section>
<section id="user" class="user-profile">
   <div class="container">
      <div class="row">
         <?php
         foreach ($user as $u) 
         {
            // $user_img = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $u->user_id ));
         ?>
         <div class="col-lg-4 col-md-6 single-item">
            <div class="item">
               <div class="thumb">
                  <a href="<?=site_url('about-profile');?>?User-Details=<?=base64_encode($u->user_id); ?>&user-sno=<?=time().md5($u->user_id).time();?>">
                     <?php
                         if($u->user_avtar !='')
                         {
                         ?>
                         <img class="img-fluid" alt="user" src="<?=base_url('assets');?>/images/user/<?=$u->user_avtar; ?>">
                         <?php
                         }else{
                         ?>
                         <img class="img-fluid" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                     <?php } ?>
                  </a>
                  <div class="overlay">
                  </div>
                  <div class="info">
                     <div class="info-one">
                        <h4 class="text-center"><a href="<?=site_url('about-profile');?>?User-Details=<?=base64_encode($u->user_id); ?>&user-sno=<?=time().md5($u->user_id).time();?>"> <?=$u->name; ?> </a></h4>
                        <p class="text-center visitor-detail"><?=$this->User_model->getHeightById($u->height); ?>, <?=$u->language; ?> </p>
                        <p class="text-center text-white"><?=$u->city; ?></p>
                        <button class="visitors" href="#"> Connect Now</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php } ?>

      </div>
      <!-- <div class="row">
            <div class="col-12">
                <div class="pagination-wrap card-pagination">
                    <ul>
                        <li><a href="#">first</a></li>
                        <li class="active"><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a href="#">next</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
   </div>
   </div>
</section>