<?php
class CmsManagement_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function websiteBanner()
    {
    	$res = $this->db->get('mm_website_banner')->result();
    	return $res;
    }

    public function submitWebsiteBanner($input,$img)
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('websiteBanner');
            exit();
        }

        if($input['website_banner_id'] !='')
        {

            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/website_banner/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = ""; 
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'description' => $input['description'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else
            {
                $config['upload_path']   = 'assets/images/website_banner/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'description' => $input['description'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            
            $this->db->where('website_banner_id',$input['website_banner_id']);
            $this->db->update('mm_website_banner',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('websiteBanner');
        }
        else
        {
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/website_banner/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }

            $data = array(
                'title' => $input['title'],
                'description' => $input['description'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_website_banner',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('websiteBanner');
        }
    }

    public function changeWebsiteBannerStatus($id)
    {
        $this->db->where('website_banner_id',$id);
        $query = $this->db->get('mm_website_banner');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('website_banner_id',$id);
            $this->db->update('mm_website_banner',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('website_banner_id',$id);
            $this->db->update('mm_website_banner',$data);
        }
    }

    public function updateWebsiteBanner($website_banner_id)
    {
    	$query = $this->db->where('website_banner_id',$website_banner_id)->get('mm_website_banner')->row_array();
        return $query;
    }

    public function deleteWebsiteBanner($website_banner_id)
    {
        $this->db->where('website_banner_id',$website_banner_id);
        $this->db->delete('mm_website_banner');
    }

    public function happyStory()
    {
    	$res = $this->db->get('mm_happy_story')->result();
    	return $res;
    }

    public function submitHappyStory($input,$img)
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('happyStory');
            exit();
        }

        if($input['happy_story_id'] !='')
        {

            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/happy_story/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = ""; 
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'description' => $input['description'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else
            {
                $config['upload_path']   = 'assets/images/happy_story/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'description' => $input['description'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            
            $this->db->where('happy_story_id',$input['happy_story_id']);
            $this->db->update('mm_happy_story',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('happyStory');
        }
        else
        {
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/happy_story/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 1000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }

            $data = array(
                'title' => $input['title'],
                'description' => $input['description'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_happy_story',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('happyStory');
        }
    }

    public function changeHappyStoryStatus($id)
    {
        $this->db->where('happy_story_id',$id);
        $query = $this->db->get('mm_happy_story');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('happy_story_id',$id);
            $this->db->update('mm_happy_story',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('happy_story_id',$id);
            $this->db->update('mm_happy_story',$data);
        }
    }

    public function updateHappyStory($happy_story_id)
    {
    	$query = $this->db->where('happy_story_id',$happy_story_id)->get('mm_happy_story')->row_array();
        return $query;
    }

    public function socialMedia()
    {
    	$query = $this->db->where('social_media_id',1)->get('mm_social_media')->row_array();
        return $query;
    }

    public function updateSocialMedia($input)
    {
    	$this->form_validation->set_rules('social_media_id', 'social_media_id', 'required');
        $this->form_validation->set_rules('facebook', 'facebook', 'required');
        $this->form_validation->set_rules('insta', 'insta', 'required');
        $this->form_validation->set_rules('linkedin', 'linkedin', 'required');
        $this->form_validation->set_rules('twitter', 'twitter', 'required');
        $this->form_validation->set_rules('contact', 'contact', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('about', 'about', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('socialMedia');
            exit();
        }

        $data = array(
        	'facebook' => $input['facebook'],
        	'insta' => $input['insta'],
        	'linkedin' => $input['linkedin'],
        	'twitter' => $input['twitter'],
        	'contact' => $input['contact'],
        	'email' => $input['email'],
        	'about' => $input['about']
        );
        $this->db->where('social_media_id',$input['social_media_id']);
        $this->db->update('mm_social_media',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('socialMedia');
    }

    public function activeWebsiteBanner()
    {
    	$res = $this->db->where('status','1')->get('mm_website_banner')->result();
    	return $res;
    }

    public function activeHappyStory()
    {
    	$res = $this->db->where('status','1')->get('mm_happy_story')->result();
    	return $res;
    }

}