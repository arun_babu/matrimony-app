<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class UserController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library('aws3');
		error_reporting(0);
    }

    public function userUpdate()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $user_id = $this->input->post('user_id');
        $data = $this->Api_User_model->userUpdate($input,$lan);
        if($data)
        {
            
            // $userData = $this->Api_User_model->getUserDataById($user_id);
            if($lan == 'en')
            {
                $PROFILE_UPDATE = PROFILE_UPDATE;
            }else if($lan == 'hi')
            {
                $PROFILE_UPDATE = PROFILE_UPDATE_HI;
            }else if($lan == 'mr')
            {
                $PROFILE_UPDATE = PROFILE_UPDATE_MR;
            }
            $this->Base_model->responseSuccess(1, $PROFILE_UPDATE, $data);
            
        }
        else
        {
            if($lan == 'en')
            {
                $SOMTHING = SOMTHING;
            }else if($lan == 'hi')
            {
                $SOMTHING = SOMTHING_HI;
            }else if($lan == 'mr')
            {
                $SOMTHING = SOMTHING_MR;
            }
            $this->Base_model->responseFailed(0,$SOMTHING);
        }
    }

    public function imageUpdate()
    {
        foreach (getallheaders() as $name => $value)
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $user_id = $this->input->post('user_id');
//        $image = implode("@&@",$_FILES['files']['name']);
//        if($image !='')
        //if(count($_FILES['files']['name']))
        if($_FILES['user_avtar'])
        {
            // $imgExist = $this->db->where('user_id',$user_id)->get('mm_user_image');
            // if($imgExist->num_rows() > 0)
            // {
            //     $img_del = $this->db->where('user_id',$user_id)->delete('mm_user_image');

            //     $filesCount = count($_FILES['files']['name']);
            //     for ($i = 0; $i < $filesCount; $i++) {
            //         $_FILES['file']['name']     = $_FILES['files']['name'][$i];
            //         $_FILES['file']['type']     = $_FILES['files']['type'][$i];
            //         $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
            //         $_FILES['file']['error']    = $_FILES['files']['error'][$i];
            //         $_FILES['file']['size']     = $_FILES['files']['size'][$i];
            //         // File upload configuration                       
            //         $uploadPath                 = './assets/images/user';
            //         $config['upload_path']      = $uploadPath;
            //         $config['allowed_types']    = 'gif|jpg|jpeg|png';
            //         $config['encrypt_name']     = TRUE;
            //         // Load and initialize upload library
            //         $this->load->library('upload', $config);
            //         $this->upload->initialize($config);

            //         if ($this->upload->do_upload('file')) {
            //             // Uploaded file data
            //             $fileData = $this->upload->data();
            //             $fileData['file_name'];
            //             $uploadData[$i] = $fileData['file_name'];
            //         } //$this->upload->do_upload('file')
            //     } //$i = 0; $i < $filesCount; $i++
            //     foreach ($uploadData as $uploadData) {
            //     $datas['user_id'] = $user_id;
            //     $datas['image']      = $uploadData;
            //     $datas['created_at'] = date('Y-m-d H:i:s');
            //     $listID              = $this->db->insert("mm_user_image", $datas);
            //     }
            // }
            // else
            // {

//                $filesCount = count($_FILES['files']['name']);
                $filesCount = 1;
//                for ($i = 0; $i < $filesCount; $i++) {
//                    $_FILES['file']['name']     = $_FILES['files']['name'][$i];
//                    $_FILES['file']['type']     = $_FILES['files']['type'][$i];
//                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
//                    $_FILES['file']['error']    = $_FILES['files']['error'][$i];
//                    $_FILES['file']['size']     = $_FILES['files']['size'][$i];

                    // File upload configuration                       
                    $uploadPath                 = './assets/images/user';
                    $config['upload_path']      = $uploadPath;
                    $config['allowed_types']    = 'gif|jpg|jpeg|png';
                    $config['encrypt_name']     = TRUE;
                    // Load and initialize upload library
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

//                    if ($this->upload->do_upload('file')) {
                    if (true) {
                        // Uploaded file data
                        $fileData = $this->upload->data();
//                        $fileData['file_name'];
						$uploadData = $this->aws3->sendfile('bethelmatrimony-upload', $_FILES['user_avtar']);
//						$uploadData[$i] = $fileData['file_name'];
//						$uploadData = $fileData['file_name'];
                    } //$this->upload->do_upload('file')
//                } //$i = 0; $i < $filesCount; $i++
//                foreach ($uploadData as $uploadData) {
                $datas['user_id'] = $user_id;
                $datas['image']      = $uploadData;
                $datas['created_at'] = date('Y-m-d H:i:s');
                $listID              = $this->db->insert("mm_user_image", $datas);


//                }
            // }
        }

        if($lan == 'en')
        {
            $DATA_SAVE = DATA_SAVE;
        }
        else if($lan == 'ar')
        {
            $DATA_SAVE = DATA_SAVE_AR;
        }
		$data = $this->Api_User_model->myProfile($user_id);
        $this->Base_model->responseSuccess(1, $DATA_SAVE,$data);


    }

    public function getGallary()
    {
        $user_id = $this->input->post('user_id');
        $data = $this->Api_User_model->getGallary($user_id);
        if($data)
        {
            $this->Base_model->responseSuccess(1, 'List', $data);
        }
        else
        {
            $this->Base_model->responseFailed(0,"No data found");
        }
    }

    public function myProfile()
    {
        $user_id = $this->input->post('user_id');
        $data = $this->Api_User_model->myProfile($user_id);
        if($data)
        {
            $this->Base_model->responseSuccess(1, 'Profile', $data);
        }
        else
        {
            $this->Base_model->responseFailed(0,"No data found");
        }
    }

    public function matchesUser()
    {
        $user_id = $this->input->post('user_id');
        $data = $this->Api_User_model->matchesUser($user_id);
        if($data == 0)
        {
            $this->Base_model->responseFailed(2,DEACTIVE_NEW);
        }
        else if($data == 1)
        {
            $this->Base_model->responseFailed(0,COMPLETE_PROFILE);
        }
        else
        {
            $this->Base_model->responseSuccess(1, 'Matches', $data);
        }
    }

    public function justJoin()
    {
        $user_id = $this->input->post('user_id');
        $gender = $this->input->post('gender');
        $data = $this->Api_User_model->justJoin($user_id,$gender);
        if($data == 0)
        {
            $this->Base_model->responseFailed(2,DEACTIVE_NEW);
        }
        else
        {
            $this->Base_model->responseSuccess(1, 'Just Join', $data);
        }
    }

    public function getGender()
    {
        $res = $this->db->get('mm_gender')->result();
        $this->Base_model->responseSuccess(1, 'Gender', $res);
    }

    public function deleteImage()
    {
        $media_id = $this->input->post('media_id');
        $this->Api_User_model->deleteImage($media_id);
    }

    public function setProfileImage()
    {
        $user_id = $this->input->post('user_id');
        $image = $_FILES['user_avtar']['name'];
        $this->Api_User_model->setProfileImage($user_id,$image);
    }


}
