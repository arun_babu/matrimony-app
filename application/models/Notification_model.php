<?php
class Notification_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function activeUser()
    {
    	$res = $this->db->order_by('user_id','DESC')->get('mm_users')->result();
    	return $res;
    }

    public function firebase_broadcast($title,$msg1,$type)
    {
     
   		$firebaseKey=$this->db->where('id','1')->get('mm_firebase_key')->row();
   		// $Topic_name = "/topics/Users";
   
      	$API_ACCESS_KEY= $firebaseKey->firebase_key;
        $Topic_name = TOPICS_FOR_USERS;
        
         // $type="All";
          $msg = array
              (
                  'body'    => $msg1,
                  'title'   => $title,
                  'type'    =>  $type,
                  'icon'    => 'myicon',/*Default Icon*/
                  'sound'   =>  'mySound'/*Default sound*/
            );
          
          $fields = array
              (
                  'to'        => $Topic_name,
                  'notification'    => $msg
                 
              );  
      
          $headers = array
              (
                  'Authorization: key=' . $API_ACCESS_KEY,
                  'Content-Type: application/json'
              );

          
          #Send Reponse To FireBase Server    
          $ch = curl_init();
          curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
          curl_setopt( $ch,CURLOPT_POST, true );
          curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
          curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
          curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
          $result = curl_exec($ch );
          // print_r($result);
          // exit();
          curl_close( $ch );
     
    }

    public function firebase_with_class($device_token, $sender_id, $senderName,$type, $title, $msg1)
    {
        $user = $this->Base_model->getSingleRow('mm_users', array(
            'device_token' => $device_token
        ));

        $FIRE_BASE_KEY = $this->Base_model->getSingleRow('mm_firebase_key',array('id'=>1));
        $api_key = $FIRE_BASE_KEY->firebase_key;

        // $FIRE_BASE_KEY = FIRE_BASE_KEY;
        // $api_key = $FIRE_BASE_KEY;

        
            $API_ACCESS_KEY = $api_key;
            
            $msg = array(
                'body' => $msg1,
                'title' => $title,
                'icon' => 'myicon',
                'type' => $type,
                /*Default Icon*/
                'sound' => 'default'
                /*Default sound*/
            );

            if($user->device_type == "ios") 
            {
                $fields = array(
                    'to' => $device_token,
                    'notification' => $msg
                );
            }
            else
            {
                $fields = array(
                    'to' => $device_token,
                    'data' => $msg
                );
            }  
            
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json'
            );
            #Send Reponse To FireBase Server    
            $ch      = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
        
    } 

}