<?php
class Income_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function income()
    {
    	$res = $this->db->order_by('income_id','DESC')->get('mm_income')->result();
    	return $res;
    }

    public function changeIncomeStatus($id)
    {
        $this->db->where('income_id',$id);
        $query = $this->db->get('mm_income');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('income_id',$id);
            $this->db->update('mm_income',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('income_id',$id);
            $this->db->update('mm_income',$data);
        }
    }

    public function submitIncome($input)
    {
    	$this->form_validation->set_rules('income', 'income', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('income');
            exit();
        }

        if($input['income_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            $data = array(
                'income' => $input['income'],
                'language' => $input['language'],
                'updated_at' => date('Y-m-d H:i:s'),
                );
            
            $this->db->where('income_id',$input['income_id']);
            $this->db->update('mm_income',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('income');
        }
        else
        {
            
            $data = array(
                'income' => $input['income'],
                'language' => $input['language'],
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_income',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('income');
        }
    }

    public function updateIncome($income_id)
    {
    	$query = $this->db->where('income_id',$income_id)->get('mm_income')->row_array();
        return $query;
    }

}