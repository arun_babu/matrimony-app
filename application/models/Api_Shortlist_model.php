<?php
class Api_Shortlist_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function shortlist($input,$lan)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('short_listed_id', 'short_listed_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Base_model->responseFailed(0,'Please fill user id field');
            exit();
        }

        $favExist = $this->db->where('short_listed_id',$input['short_listed_id'])->where('user_id',$input['user_id'])->get('mm_shortlists');
        if($favExist->num_rows() > 0)
        {
        	$this->db->where('short_listed_id',$input['short_listed_id']);
        	$this->db->where('user_id',$input['user_id']);
        	$this->db->delete('mm_shortlists');
        }
        else
        {
	        $data = array(
	        	'short_listed_id' => $input['short_listed_id'], 
	        	'user_id' => $input['user_id'], 
	        	'created_at' => date('Y-m-d H:i:s')
	    	);
	    	$this->db->insert('mm_shortlists',$data);
    	}


        if($lan == 'en')
        {
            $DATA_SAVE = DATA_SAVE;
        }else if($lan == 'hi')
        {
            $DATA_SAVE = DATA_SAVE_HI;
        }else if($lan == 'mr')
        {
            $DATA_SAVE = DATA_SAVE_MR;
        }
        $this->Base_model->responseSuccessWethoutData(1,$DATA_SAVE);
        exit();
        
    }

    public function getShortlist($input,$lan)
    {
    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        $userExist = $this->db->where('user_id',$input['user_id'])->where('status','1')->get('mm_users')->num_rows();
        if($userExist > 0)
        {
	        $user = $this->getShortlistUser($input['user_id']);   
            $users = array();
            foreach ($user as $user) {
                
                $user->gender = $this->User_model->getGenderById($user->gender);
                $blood_group = $this->User_model->getBloodGroupById($user->blood_group);
                $user->blood_group = $blood_group;
                $caste = $this->User_model->getCasteById($user->caste);
                $user->caste = $caste;
                $height = $this->User_model->getHeightById($user->height);
                $user->height = $height;
                $occupation = $this->User_model->getOccupation($user->occupation);
                $user->occupation = $occupation;
                $income = $this->User_model->getIncomeById($user->income);
                $user->income = $income;
                $state = $this->User_model->getStatesById($user->state);
                $user->state = $state;
                $district = $this->User_model->getDistrictsById($user->district);
                $user->district = $district;
                $marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
                $user->marital_status = $marital_status;
                $manglik = $this->User_model->getManglikById($user->manglik);
                $user->manglik = $manglik;
                $request = $this->Api_User_model->getRequested($input['user_id'],$user->user_id);
                $user->request = $request;
                $user->shortlisted = 1;
                $user->user_avtar = $this->config->base_url().'assets/images/user/' . $user->user_avtar;
                $user_img    = $this->Base_model->getAllDataWhere(array(
                    'user_id' => $user->user_id,
                ), 'mm_user_image');
                $user_imgs  = array();
                foreach ($user_img as $user_img) {
                    
                    $user_img->image = $this->config->base_url().'assets/images/user/' . $user_img->image;
                   
                    array_push($user_imgs, $user_img);
                } 
                
                $user->user_imgs = $user_imgs;
                array_push($users, $user);
            } 
            return $users;
            
        }
	    else
	    {
            if($lan == 'en')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW;
            }else if($lan == 'hi')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_HI;
            }else if($lan == 'mr')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_MR;
            }
	    	$this->Base_model->responseFailed(3, $DEACTIVE_NEW);
            exit();
	    }
    }

    public function getShortlistUser($user_id)
    {
        $this->db->select('u.*');
        $this->db->from('mm_users u');
        $this->db->join('mm_shortlists s','s.short_listed_id = u.user_id');
        $this->db->where('s.user_id',$user_id);
        $this->db->where('u.status','1');
        $this->db->order_by('u.s_no', 'desc');
        $query = $this->db->get();
        $user = $query->result();
        return $user;
    }

    public function sendInterest($input,$lan)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('requested_id', 'requested_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Base_model->responseFailed(0,'Please fill user id field');
            exit();
        }

        $favExist = $this->db->where('requested_id',$input['requested_id'])->where('user_id',$input['user_id'])->get('mm_interest');
        if($favExist->num_rows() > 0)
        {
            
            $this->Base_model->responseFailed(0,INTEREST_SENT);
            exit();
        }
        else
        {
            $data = array(
                'requested_id' => $input['requested_id'], 
                'user_id' => $input['user_id'], 
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_interest',$data);

            //Notification
            $usrToken     = $this->Base_model->getSingleRow('mm_users', array(
            'user_id' => $input['user_id']
            ));

            $getToken     = $this->Base_model->getSingleRow('mm_users', array(
            'user_id' => $input['requested_id']
            ));

            $name = $usrToken->name;
            $device_token = $getToken->device_token;
            $msg = " $name sent you an interest .";
            $title = "Send Interest.";
            $type = INTEREST_SEND_TYPE;
            $this->Notification_model->firebase_with_class($device_token, '', '',$type, $title, $msg);
            $data1['user_id']    = $input['requested_id'];
            $data1['title']      = $title;
            $data1['message']    = $msg;
            $data1['type']       = $type;
            $data1['created_at']   = date('Y-m-d H:i:s');
            $this->db->insert('mm_notification', $data1);
        }


        if($lan == 'en')
        {
            $DATA_SAVE = DATA_SAVE;
        }else if($lan == 'hi')
        {
            $DATA_SAVE = DATA_SAVE_HI;
        }else if($lan == 'mr')
        {
            $DATA_SAVE = DATA_SAVE_MR;
        }
        $this->Base_model->responseSuccessWethoutData(1,$DATA_SAVE);
        exit();
        
    }

    public function getInterest($input,$lan)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        $user_id = $input['user_id'];
        $type = $input['type'];

        $userExist = $this->db->where('user_id',$user_id)->where('status','1')->get('mm_users')->num_rows();
        if($userExist > 0)
        {
            if($type == '0') //Sent interest
            {
                $user = $this->Site_model->getSendInterest($user_id);   
                $users = array();
                foreach ($user as $user) {
                    
                    $user->gender = $this->User_model->getGenderById($user->gender);
                    $blood_group = $this->User_model->getBloodGroupById($user->blood_group);
                    $user->blood_group = $blood_group;
                    $caste = $this->User_model->getCasteById($user->caste);
                    $user->caste = $caste;
                    $height = $this->User_model->getHeightById($user->height);
                    $user->height = $height;
                    $occupation = $this->User_model->getOccupation($user->occupation);
                    $user->occupation = $occupation;
                    $income = $this->User_model->getIncomeById($user->income);
                    $user->income = $income;
                    $state = $this->User_model->getStatesById($user->state);
                    $user->state = $state;
                    $district = $this->User_model->getDistrictsById($user->district);
                    $user->district = $district;
                    $marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
                    $user->marital_status = $marital_status;
                    $manglik = $this->User_model->getManglikById($user->manglik);
                    $user->manglik = $manglik;
                    $user->request = 1;
                    $shortlisted = $this->Api_User_model->getShortlisted($user_id,$user->user_id);
                    $user->shortlisted = (int)$shortlisted;
                    $user->user_avtar = $this->config->base_url().'assets/images/user/' . $user->user_avtar;
                    $user_img    = $this->Base_model->getAllDataWhere(array(
                        'user_id' => $user->user_id,
                    ), 'mm_user_image');
                    $user_imgs  = array();
                    foreach ($user_img as $user_img) {
                        
                        $user_img->image = $this->config->base_url().'assets/images/user/' . $user_img->image;
                       
                        array_push($user_imgs, $user_img);
                    } 
                    
                    $user->user_imgs = $user_imgs;
                    array_push($users, $user);
                } 
                return $users;
            }
            else if($type == '1')
            {
                //Received Interest
                $user = $this->Site_model->getReceivedInterest($user_id);   
                $users = array();
                foreach ($user as $user) {
                    
                    $user->gender = $this->User_model->getGenderById($user->gender);
                    $blood_group = $this->User_model->getBloodGroupById($user->blood_group);
                    $user->blood_group = $blood_group;
                    $caste = $this->User_model->getCasteById($user->caste);
                    $user->caste = $caste;
                    $height = $this->User_model->getHeightById($user->height);
                    $user->height = $height;
                    $occupation = $this->User_model->getOccupation($user->occupation);
                    $user->occupation = $occupation;
                    $income = $this->User_model->getIncomeById($user->income);
                    $user->income = $income;
                    $state = $this->User_model->getStatesById($user->state);
                    $user->state = $state;
                    $district = $this->User_model->getDistrictsById($user->district);
                    $user->district = $district;
                    $marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
                    $user->marital_status = $marital_status;
                    $manglik = $this->User_model->getManglikById($user->manglik);
                    $user->manglik = $manglik;
                    $user->request = 1;
                    $shortlisted = $this->Api_User_model->getShortlisted($user_id,$user->user_id);
                    $user->shortlisted = (int)$shortlisted;
                    $user->user_avtar = $this->config->base_url().'assets/images/user/' . $user->user_avtar;
                    $user_img    = $this->Base_model->getAllDataWhere(array(
                        'user_id' => $user->user_id,
                    ), 'mm_user_image');
                    $user_imgs  = array();
                    foreach ($user_img as $user_img) {
                        
                        $user_img->image = $this->config->base_url().'assets/images/user/' . $user_img->image;
                       
                        array_push($user_imgs, $user_img);
                    } 
                    
                    $user->user_imgs = $user_imgs;
                    array_push($users, $user);
                } 
                return $users;
            }
        }
        else
        {
            if($lan == 'en')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW;
            }else if($lan == 'hi')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_HI;
            }else if($lan == 'mr')
            {
                $DEACTIVE_NEW = DEACTIVE_NEW_MR;
            }
            $this->Base_model->responseFailed(3, $DEACTIVE_NEW);
            exit();
        }
    }

    public function acceptedInterest($input,$lan)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('requested_id', 'requested_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Base_model->responseFailed(0,'Please fill user id field');
            exit();
        }

        $favExist = $this->db->where('requested_id',$input['requested_id'])->where('user_id',$input['user_id'])->get('mm_interest');
        if($favExist->num_rows() > 0)
        {
            $favExist1 = $this->db->where('requested_id',$input['requested_id'])->where('user_id',$input['user_id'])->where('status','1')->or_where('status','2')->get('mm_interest');
            if($favExist1->num_rows() > 0)
            {
                $this->Base_model->responseFailed(0,INTEREST_ACCEPT);
                exit();
            }
            else
            {
                $data = array( 
                    'status' => '1', 
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->db->where('user_id',$input['user_id']);
                $this->db->where('requested_id',$input['requested_id']);
                $this->db->update('mm_interest',$data);

                if($lan == 'en')
                {
                    $DATA_SAVE = DATA_SAVE;
                }else if($lan == 'hi')
                {
                    $DATA_SAVE = DATA_SAVE_HI;
                }else if($lan == 'mr')
                {
                    $DATA_SAVE = DATA_SAVE_MR;
                }
                $this->Base_model->responseSuccessWethoutData(1,$DATA_SAVE);
                exit();
            }
        }
        else
        {
            if($lan == 'en')
            {
                $SOMTHING = SOMTHING;
            }else if($lan == 'hi')
            {
                $SOMTHING = SOMTHING_HI;
            }else if($lan == 'mr')
            {
                $SOMTHING = SOMTHING_MR;
            }
            $this->Base_model->responseFailed(0,$SOMTHING);
            exit();
        }

        
        
    }

    

}