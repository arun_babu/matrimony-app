<section id="breadcrumb">
   <div class="container">
      <ul class="breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">Interest</a></li>
      </ul>
   </div>
</section>
<section class="user-view">
   <div class="container">
      <div class="main-body">
         <div class="row gutters-sm">
            <div class="col-md-3 mb-3">
               <div class="card">
                  <div class="card-body">
                     <div class="d-flex flex-column align-items-center text-center">

                       <!-- <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150"> -->
                       <?php
                      if($user['user_avtar'] !='')
                      {
                      ?>
                      <img class="rounded-circle" width="150" alt="user" src="<?=base_url('assets');?>/images/user/<?=$user['user_avtar']; ?>">
                      <?php
                      }else{
                      ?>
                      <img class="rounded-circle" width="150" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                        <?php } ?>
                        <div class="mt-3">
                           <h4><?=$user['name']; ?></h4>
                           <p class="text-secondary mb-1"><?=$this->User_model->getOccupation($user['occupation']); ?></p>
                           <p class="text-muted font-size-sm"><?=$user['city']; ?>, <?=$this->User_model->getStatesById($user['status']); ?></p>
                           <button class="btn button-1">Connect</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--      <div class="card mt-3">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Full Name</h6>
                      <span class="text-secondary">Kenneth Valdez</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Email</h6>
                      <span class="text-secondary">example@gmail.com</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Phone</h6>
                      <span class="text-secondary">(91) 816-9029</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Age </h6>
                      <span class="text-secondary">bootdey</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Marital Status</h6>
                      <span class="text-secondary">Never Married</span>
                    </li>
                  </ul>
                  </div> -->
            </div>
            <div class="col-md-9">
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info">Basic Info</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Full Name</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['name']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Email</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                <?=$user['email']; ?>
                              </div>
                           </div>
                           <!-- <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Age</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 24 Year
                              </div>
                           </div> -->
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Marital Status</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getMaritalStatusById($user['marital_status']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Height</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getHeightById($user['height']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Gotra</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['gotra']; ?>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Gotra Nanihal</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['gotra_nanihal']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">State</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getStatesById($user['status']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Distric</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getDistrictsById($user['district']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">City</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['city']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Annual Income</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getIncomeById($user['income']); ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
               <!----------------- basic info end---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info">Life Style</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Date of Birth</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['dob']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Aadhaar No.</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['aadhaar']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Manglik</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getManglikById($user['manglik']); ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Birth Time</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['birth_time']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Birth Place</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['birth_place']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
                <!----------------- life style  end---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info">About Me</h5>
                     <div class="row">
                       <div class="col-md-12 col-sm-12 text-secondary">
                                  <h6 class="mb-0">About</h6>
                                <?=$user['about_me']; ?>
<hr>

                              </div>

                        <div class="col-md-6">
        
                           
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Weight(kg)</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                <?=$user['weight']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Body Type</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                               <?=$user['body_type']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Complexion</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                <?=$user['complexion']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Physical Challenged</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 None
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Blood Group</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$this->User_model->getBloodGroupById($user['blood_group']); ?>
                              </div>
                           </div>
                             <hr>
                        </div>
                     </div>
                  </div>
               </div>
               <!----------------- About me  end---------->
                <!----------------- Important ---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info"> Important  Details</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Highest Education</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['qualification']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Work Experience</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?php
                                if($user['working'] == '1')
                                {
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                                ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Work place</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['work_place']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Occupation</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$this->User_model->getOccupation($user['occupation']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Organization name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['organisation_name']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
                              <!-----------------about more  end---------->
                <!----------------- About Family start ---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info"> About more</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Dietary Habits</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['dietary']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Drinking Habits</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['drinking']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Smoking Habits</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['smoking']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Language/Speak</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['language']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Hobbies</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['hobbies']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Interests</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['interests']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
                    <!----------------- Important me  end---------->
                <!----------------- About more start ---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info"> About Family</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Grandfather Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['grand_father_name']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Maternal Grandfather Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['maternal_grand_father_name_address']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Father Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['father_name']; ?>
                              </div>
                           </div>
                           <hr>
                               <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Mother Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                             <?=$user['mother_name']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Address</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_address']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Father's Occupation</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['father_occupation']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Mother' Occupation</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['mother_occupation']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0"> Brother(s)</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['brother']; ?>
                              </div>
                           </div>
                           <hr>
                          
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Sister</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['sister']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Family Income</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['family_income']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Family Type</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                             <?=$user['family_type']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Family Value</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_value']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">State</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_state']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">District</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_district']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">City </h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_city']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Pincode</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_pin']; ?>
                              </div>
                           </div>
                           <hr>
                          
                        </div>
                     </div>
                  </div>
               </div>
               <!------------- end family details -------------->
           
            </div>
         </div>
      </div>
   </div>

</section>