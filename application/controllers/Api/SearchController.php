<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function searchUsers()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 
        $input = $this->input->post();
        $user_id = $this->input->post('user_id');
        $user = $this->Api_Search_model->searchUsers($input);
        if($user)
        {
            if($user_id !='')
            {
            	$users = array();
                foreach ($user as $user) {
                    
                    $user->gender = $this->User_model->getGenderById($user->gender);
                    $blood_group = $this->User_model->getBloodGroupById($user->blood_group);
                    $user->blood_group = $blood_group;
                    $caste = $this->User_model->getCasteById($user->caste);
                    $user->caste = $caste;
                    $height = $this->User_model->getHeightById($user->height);
                    $user->height = $height;
                    $occupation = $this->User_model->getOccupation($user->occupation);
                    $user->occupation = $occupation;
                    $income = $this->User_model->getIncomeById($user->income);
                    $user->income = $income;
                    $state = $this->User_model->getStatesById($user->state);
                    $user->state = $state;
                    $district = $this->User_model->getDistrictsById($user->district);
                    $user->district = $district;
                    $marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
                    $user->marital_status = $marital_status;
                    $manglik = $this->User_model->getManglikById($user->manglik);
                    $user->manglik = $manglik;
                    $shortlisted = $this->Api_User_model->getShortlisted($user_id,$user->user_id);
                    $user->shortlisted = (int)$shortlisted;
                    $request = $this->Api_User_model->getRequested($user_id,$user->user_id);
                    $user->request = (int)$request;
                    $user->user_avtar = $this->config->base_url().'assets/images/user/' . $user->user_avtar;
                    $user_img    = $this->Base_model->getAllDataWhere(array(
                        'user_id' => $user->user_id,
                    ), 'mm_user_image');
                    $user_imgs  = array();
                    foreach ($user_img as $user_img) {
                        
                        $user_img->image = $this->config->base_url().'assets/images/user/' . $user_img->image;
                       
                        array_push($user_imgs, $user_img);
                    } 
                    
                    $user->user_imgs = $user_imgs;
                    array_push($users, $user);
                } 
                $this->Api_Auth_model->responseSuccess(1, 'Search', $users);
            }
            else
            {
                $users = array();
                foreach ($user as $user) {
                    
                    $user->gender = $this->User_model->getGenderById($user->gender);
                    $blood_group = $this->User_model->getBloodGroupById($user->blood_group);
                    $user->blood_group = $blood_group;
                    $caste = $this->User_model->getCasteById($user->caste);
                    $user->caste = $caste;
                    $height = $this->User_model->getHeightById($user->height);
                    $user->height = $height;
                    $occupation = $this->User_model->getOccupation($user->occupation);
                    $user->occupation = $occupation;
                    $income = $this->User_model->getIncomeById($user->income);
                    $user->income = $income;
                    $state = $this->User_model->getStatesById($user->state);
                    $user->state = $state;
                    $district = $this->User_model->getDistrictsById($user->district);
                    $user->district = $district;
                    $marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
                    $user->marital_status = $marital_status;
                    $manglik = $this->User_model->getManglikById($user->manglik);
                    $user->manglik = $manglik;
                    $user->shortlisted = 0;
                    $user->request = 0;
                    $user->user_avtar = $this->config->base_url().'assets/images/user/' . $user->user_avtar;
                    $user_img    = $this->Base_model->getAllDataWhere(array(
                        'user_id' => $user->user_id,
                    ), 'mm_user_image');
                    $user_imgs  = array();
                    foreach ($user_img as $user_img) {
                        
                        $user_img->image = $this->config->base_url().'assets/images/user/' . $user_img->image;
                       
                        array_push($user_imgs, $user_img);
                    } 
                    
                    $user->user_imgs = $user_imgs;
                    array_push($users, $user);
                } 
                $this->Api_Auth_model->responseSuccess(1, 'Search', $users);
            }
        }else{
        	if($lan == 'en')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }else if($lan == 'hi')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
            }else if($lan == 'mr')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
            }
            $this->Api_Auth_model->responseFailed(0,$DATA_NOT_AVAIABLE);
        }
    }

}