<?php
class Caste_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function caste()
    {
    	$res = $this->db->order_by('caste_id','DESC')->get('mm_caste')->result();
    	return $res;
    }

    public function changeCasteStatus($id)
    {
        $this->db->where('caste_id',$id);
        $query = $this->db->get('mm_caste');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('caste_id',$id);
            $this->db->update('mm_caste',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('caste_id',$id);
            $this->db->update('mm_caste',$data);
        }
    }

    public function submitCaste($input)
    {
    	$this->form_validation->set_rules('caste', 'caste', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('caste');
            exit();
        }

        if($input['caste_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            $data = array(
                'caste' => $input['caste'],
                'language' => $input['language'],
                'updated_at' => date('Y-m-d H:i:s'),
                );
            
            $this->db->where('caste_id',$input['caste_id']);
            $this->db->update('mm_caste',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('caste');
        }
        else
        {
            
            $data = array(
                'caste' => $input['caste'],
                'language' => $input['language'],
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_caste',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('caste');
        }
    }

    public function updateCaste($caste_id)
    {
    	$query = $this->db->where('caste_id',$caste_id)->get('mm_caste')->row_array();
        return $query;
    }


}