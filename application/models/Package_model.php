<?php
class Package_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function packages()
    {
    	$res = $this->db->order_by('packages_id','DESC')->get('mm_packages')->result();
    	return $res;
    }

    public function changePackagesStatus($id)
    {
        $this->db->where('packages_id',$id);
        $query = $this->db->get('mm_packages');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('packages_id',$id);
            $this->db->update('mm_packages',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('packages_id',$id);
            $this->db->update('mm_packages',$data);
        }
    }

    public function submitPackages($input)
    {
    	$this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('subscription_type', 'subscription_type', 'required');
        $this->form_validation->set_rules('language', 'language', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('news');
            exit();
        }

        if($input['subscription_type'] == '0'){
            $no_of_days = 500;
        }else if($input['subscription_type'] == '1'){
            $no_of_days = 30;
        }else if($input['subscription_type'] == '2'){
            $no_of_days = 90;
        }else if($input['subscription_type'] == '3'){
            $no_of_days = 180;
        }else if($input['subscription_type'] == '4'){
            $no_of_days = 360;
        }

        if($input['packages_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            $data = array(
                'title' => $input['title'],
                'description' => $input['description'],
                'price' => $input['price'],
                'subscription_type' => $input['subscription_type'],
                'no_of_days' => $no_of_days,
                'language' => $input['language'],
                'updated_at' => date('Y-m-d H:i:s'),
                );
            
            $this->db->where('packages_id',$input['packages_id']);
            $this->db->update('mm_packages',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('packages');
        }
        else
        {
            
            $data = array(
                'title' => $input['title'],
                'description' => $input['description'],
                'price' => $input['price'],
                'subscription_type' => $input['subscription_type'],
                'no_of_days' => $no_of_days,
                'language' => $input['language'],
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('mm_packages',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('packages');
        }
    }

    public function updatePackages($packages_id)
    {
    	$query = $this->db->where('packages_id',$packages_id)->get('mm_packages')->row_array();
        return $query;
    }


}