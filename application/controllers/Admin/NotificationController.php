<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotificationController extends CI_Controller{

	public function __construct()
    {
    	require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();

    }

    public function notification()
	{
      	if (isset($_SESSION['id'])) {

			$res['user'] = $this->Notification_model->activeUser();
			echo load_page($this,'notification/notification',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function add_broadcast()
    {
        $data['title']= $this->input->post('title', TRUE);
        $data['message']= $this->input->post('message', TRUE);
        $data['type']= "1";
        $data['created_at']=time(); 

         $title=$data['title'];
         $msg1=$data['message'];


         $this->Notification_model->firebase_broadcast($title,$msg1,BRODCAST_NOTIFICATION);

        $this->db->insert('mm_notification',$data);
        redirect('notification');
    }

    public function send_notification()
    {
  
      $type=ADMIN_NOTIFICATION;
      $user_id=$this->input->post('uid');
      $title=$this->input->post('title');
      $msg1=$this->input->post('message');

            $user = $this->db->where('user_id',$user_id)->get('mm_users')->row();

            $data['user_id']= $user->user_id;
            $data['title']= $title;
            $data['message']= $msg1;
            $data['type']= "2";
            $data['created_at']= time();

            $this->db->insert('mm_notification',$data);

           	$firebaseKey=$this->db->where('id','1')->get('mm_firebase_key')->row();
            
            $API_ACCESS_KEY= $firebaseKey->firebase_key;
            

          $device_token =$user->device_token;

          $msg = array(
                'body' => $msg1,
                'title' => $title,
                'icon' => 'myicon',
                'type' => $type,
                /*Default Icon*/
                'sound' => 'default'
                /*Default sound*/
            );

            if($user->device_type == "ios") 
            {
                $fields = array(
                    'to' => $device_token,
                    'notification' => $msg
                );
            }
            else
            {
                $fields = array(
                    'to' => $device_token,
                    'data' => $msg
                );
            }  
            
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json'
            );
            #Send Reponse To FireBase Server    
            $ch      = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);           
      
        // echo"<pre>";
        // print_r($result); die;
          redirect('notification');
    } 

}