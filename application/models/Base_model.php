<?php

class Base_model extends CI_Model
{

	function __construct()
	{
		parent:: __construct();
	}


	public function login($email, $password)
	{
		$data['email'] = $email;
		$data['password'] = $password;

		$sess_array = array();
		$getdata = $this->Base_model->getSingleRow('mm_admin', $data);
		if ($getdata) {
			$this->session->unset_userdata($sess_array);
			$sess_array = array(
				'email' => $getdata->email,
				'id' => $getdata->id,
				'name' => $getdata->name
			);

			$this->session->set_userdata($sess_array);
			$dataget['get_data'] = $getdata;
			$dataget['see_data'] = $sess_array;
			redirect('admin/dashboard');
		} else {

			$this->session->set_flashdata('error', LOGIN_FLASH);
			redirect('admin/login');

		}
	}

	/*Get single row data*/
	public function getSingleRow($table, $condition)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->row();
	}

	public function resetPassword($email)
	{
		$q = $this->db->where('email', $email)->get('mm_admin');
		if ($q->num_rows() > 0) {
			$password = rand(111111, 999999);
			$data = array(
				'password' => $password,
				'update_dt' => date('Y-m-d H:i:s')
			);
			$this->db->update('mm_admin', $data);
			return 1;
		} else {
			return 0;
		}
	}

	/*Add new data*/
	public function insert($table, $data)
	{
		if ($this->db->insert($table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function update($table, $data, $id)
	{
		if ($id != '') {
			$this->db->where('user_id', $id);
			$this->db->update($table, $data);
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function ckeck_row($table, $condition)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->row();
	}

	public function getAllData($table)
	{
		$this->db->select("*");
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllDataLanguage($table)
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where('language', 'en');
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllDataWhere($where, $table)
	{
		$this->db->where($where);
		$this->db->select("*");
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllSettingValue($lan)
	{
		//Body_type
		$body_type = $this->Base_model->getAllData('mm_body_type');
		$body_types = array();
		foreach ($body_type as $body_type) {

			$body_type->id = $body_type->body_type_id;
			$body_type->name = $body_type->body_type;

			array_push($body_types, $body_type);
		}
		$data['body_types'] = $body_types;

		//Complexion
		$complexion = $this->Base_model->getAllData('mm_complexion');
		$complexions = array();
		foreach ($complexion as $complexion) {

			$complexion->id = $complexion->complexion_id;
			$complexion->name = $complexion->complexion;

			array_push($complexions, $complexion);
		}
		$data['complexions'] = $complexions;

		//challanged
		$challanged = $this->Base_model->getAllData('mm_challanged');
		$challangeds = array();
		foreach ($challanged as $challanged) {

			$challanged->id = $challanged->challanged_id;
			$challanged->name = $challanged->challanged;

			array_push($challangeds, $challanged);
		}
		$data['challangeds'] = $challangeds;

		//diet
		$diet = $this->Base_model->getAllData('mm_diet');
		$diets = array();
		foreach ($diet as $diet) {

			$diet->id = $diet->diet_id;
			$diet->name = $diet->diet;

			array_push($diets, $diet);
		}
		$data['diets'] = $diets;

		//drinking_smoking
		$drinking_smoking = $this->Base_model->getAllData('mm_drinking_smoking');
		$drinking_smokings = array();
		foreach ($drinking_smoking as $drinking_smoking) {

			$drinking_smoking->id = $drinking_smoking->drinking_smoking_id;
			$drinking_smoking->name = $drinking_smoking->drinking_smoking;

			array_push($drinking_smokings, $drinking_smoking);
		}
		$data['drinking_smokings'] = $drinking_smokings;

		//fathers_occupation
		$fathers_occupation = $this->Base_model->getAllData('mm_fathers_occupation');
		$fathers_occupations = array();
		foreach ($fathers_occupation as $fathers_occupation) {

			$fathers_occupation->id = $fathers_occupation->fathers_occupation_id;
			$fathers_occupation->name = $fathers_occupation->fathers_occupation;

			array_push($fathers_occupations, $fathers_occupation);
		}
		$data['fathers_occupations'] = $fathers_occupations;

		//mothers_occupation
		$mothers_occupation = $this->Base_model->getAllData('mm_mothers_occupation');
		$mothers_occupations = array();
		foreach ($mothers_occupation as $mothers_occupation) {

			$mothers_occupation->id = $mothers_occupation->mothers_occupation_id;
			$mothers_occupation->name = $mothers_occupation->mothers_occupation;

			array_push($mothers_occupations, $mothers_occupation);
		}
		$data['mothers_occupations'] = $mothers_occupations;

		//brothers_sister
		$brothers_sister = $this->Base_model->getAllData('mm_brothers_sisters');
		$brothers_sisters = array();
		foreach ($brothers_sister as $brothers_sister) {

			$brothers_sister->id = $brothers_sister->brothers_sisters_id;
			$brothers_sister->name = $brothers_sister->brothers_sisters;

			array_push($brothers_sisters, $brothers_sister);
		}
		$data['brothers_sisterss'] = $brothers_sisters;

		//family_status
		$family_status = $this->Base_model->getAllData('mm_family_status');
		$family_statuss = array();
		foreach ($family_status as $family_status) {

			$family_status->id = $family_status->family_statu_id;
			$family_status->name = $family_status->family_status;

			array_push($family_statuss, $family_status);
		}
		$data['family_statuss'] = $family_statuss;

		//family_type
		$family_type = $this->Base_model->getAllData('mm_family_type');
		$family_types = array();
		foreach ($family_type as $family_type) {

			$family_type->id = $family_type->family_type_id;
			$family_type->name = $family_type->family_type;

			array_push($family_types, $family_type);
		}
		$data['family_types'] = $family_types;

		//family_values
		$family_values = $this->Base_model->getAllData('mm_family_values');
		$family_valuess = array();
		foreach ($family_values as $family_values) {

			$family_values->id = $family_values->family_values_id;
			$family_values->name = $family_values->family_values;

			array_push($family_valuess, $family_values);
		}
		$data['family_valuess'] = $family_valuess;

		$where = array('language' => $lan, 'status' => '1');

		//Caste
		$caste = $this->Base_model->getAllDataWhere($where, 'mm_caste');
		$castes = array();
		foreach ($caste as $caste) {

			$caste->id = $caste->caste_id;
			$caste->name = $caste->caste;

			array_push($castes, $caste);
		}
		$data['castes'] = $castes;

		//manglik
		$manglik = $this->Base_model->getAllDataWhere($where, 'mm_manglik');
		$mangliks = array();
		foreach ($manglik as $manglik) {

			$manglik->id = $manglik->manglik_id;
			$manglik->name = $manglik->manglik;

			array_push($mangliks, $manglik);
		}
		$data['mangliks'] = $mangliks;

		//occupation
		$occupation = $this->Base_model->getAllDataWhere($where, 'mm_occupation');
		$occupations = array();
		foreach ($occupation as $occupation) {

			$occupation->id = $occupation->occupation_id;
			$occupation->name = $occupation->occupation;

			array_push($occupations, $occupation);
		}
		$data['occupations'] = $occupations;

		//income
		$income = $this->Base_model->getAllDataWhere($where, 'mm_income');
		$incomes = array();
		foreach ($income as $income) {

			$income->id = $income->income_id;
			$income->name = $income->income;

			array_push($incomes, $income);
		}
		$data['incomes'] = $incomes;

		//blood_group
		$blood_group = $this->Base_model->getAllData('mm_blood_group');
		$blood_groups = array();
		foreach ($blood_group as $blood_group) {

			$blood_group->name = $blood_group->blood_group;

			array_push($blood_groups, $blood_group);
		}
		$data['blood_groups'] = $blood_groups;

		//heights
		$heights = $this->Base_model->getAllData('mm_heights');
		$heightss = array();
		foreach ($heights as $heights) {
			$heights->name = $heights->height;

			array_push($heightss, $heights);
		}
		$data['heightss'] = $heightss;

		//states
		$states = $this->Base_model->getAllData('mm_states');
		$statess = array();
		foreach ($states as $states) {
			$states->name = $states->name;

			array_push($statess, $states);
		}
		$data['statess'] = $statess;

		//marital_status
		$marital_status = $this->Base_model->getAllDataWhere($where, 'mm_marital_status');
		$marital_statuss = array();
		foreach ($marital_status as $marital_status) {

			$marital_status->name = $marital_status->marital_status;

			array_push($marital_statuss, $marital_status);
		}
		$data['marital_statuss'] = $marital_statuss;

		//gender
		$gender = $this->Base_model->getAllData('mm_gender');
		$genders = array();
		foreach ($gender as $gender) {
			$gender->id = $gender->gender_id;
			$gender->name = $gender->gender;

			array_push($genders, $gender);
		}
		$data['genders'] = $genders;

		//language
		$where1 = array('status' => '1');
		$language = $this->Base_model->getAllDataWhere($where1, 'mm_language');
		$languages = array();
		foreach ($language as $language) {
			$language->id = $language->language_id;
			$language->name = $language->language_name;

			array_push($languages, $language);
		}
		$data['languages'] = $languages;

		//hobbies
		$data['hobbiess'] = $this->Base_model->getAllData('mm_hobbies');

		//interests
		$data['interests'] = $this->Base_model->getAllData('mm_interest_user');

		return $data;
	}

	public function responseSuccessWethoutData($status, $message)
	{
		$arr = array('status' => $status, 'message' => $message);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function responseSuccess($status, $message, $data)
	{
		$arr = array('status' => $status, 'message' => $message, 'data' => $data);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function responseSuccessPercentage($status, $message, $data, $percentage)
	{
		$arr = array('status' => $status, 'message' => $message, 'data' => $data, 'percentage' => $percentage);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function responseFailed($status, $message)
	{
		$arr = array('status' => $status, 'message' => $message);
		header('Content-Type: application/json');
		echo json_encode($arr);
	}

	public function getDataByCaste($id)
	{
		$this->db->select("*,sub_caste as name");
		$this->db->from('mm_sub_caste');
		$this->db->where('language', 'en');
		$this->db->where('caste_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

}
