<section id="breadcrumb">
   <div class="container">
      <ul class="breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">Interest</a></li>
      </ul>
   </div>
</section>
<section class="interest-sec">
   <div class="container ">
      <!-- Nav pills -->
      <ul class="nav nav-pills justify-content-center" role="tablist">
         <li class="nav-item">
            <a class="nav-link btn fourth active" data-toggle="pill" href="#menu1">Send Interest</a>
         </li>
         <li class="nav-item">
            <a class="nav-link btn fourth " data-toggle="pill" href="#menu2">Received Interest</a>
         </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
         <div id="menu1" class="container tab-pane active">
            <div id="user" class="user-profile">
               <div class="container">
                  <div class="row">
                     <?php
                     foreach ($send as $s) 
                     {
                        // $user_img = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $s->user_id ));
                        $short = $this->Site_model->getShortlistByLoginUserId($user_id,$s->user_id);
                     ?>
                     <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                           <div class="thumb">
                              <a href="<?=site_url('about-profile');?>?User-Details=<?=base64_encode($s->user_id); ?>&user-sno=<?=time().md5($s->user_id).time();?>">
                                 <?php
                                  if($s->user_avtar !='')
                                  {
                                  ?>
                                  <img class="img-fluid" alt="user" src="<?=base_url('assets');?>/images/user/<?=$s->user_avtar; ?>">
                                  <?php
                                  }else{
                                  ?>
                                  <img class="img-fluid" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                              <?php } ?>
                              </a>
                              <div class="overlay">
                              </div>
                              <div class="info">
                                 <div class="user-details">
                                    <h4 class="text-left"><?=$s->name; ?> </h4>
                                    <p class="profession"><?=$this->User_model->getOccupation($s->occupation); ?></p>
                                    <div class="row">
                                       <div class="col-lg-6 col-6">
                                          <p>  <span class="year"> </span> <span class="year"> <?=$this->User_model->getHeightById($s->height); ?></span>  <span><?=$s->family_status?></span></p>
                                       </div>
                                       <div class="col-lg-6 col-6">
                                          <span class="profession-1"><?=$s->qualification; ?></span>
                                       </div>
                                       <div class="col-lg-6 col-6">
                                          <h6 class="address"><?=$s->city; ?></h6>
                                       </div>
                                       <div class="col-lg-6 col-6">
                                          <h6 class="marital-status"><?=$this->User_model->getMaritalStatusById($s->marital_status); ?></h6>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="user-bottom">
                              <ul class="profile-list">
                                 <li>
                                    <a href="<?=site_url('shortlistAction');?>?User-Details=<?=base64_encode($user_id); ?>&short-listed=<?=base64_encode($s->user_id);?>&user-sno=<?=time().md5($s->user_id).time();?>" class="decorate"><i class="fa fa-star" aria-hidden="true" <?php if($short == '1'){ ?> style="color: yellow;" <?php }else{} ?> ></i>
                                    <span class="text-white">Shortlist</span>
                                    </a>
                                 </li>
                                 <!-- <li><a href="#"><i class="fa fa-check" aria-hidden="true"></i>
                                    <span class="text-white">Interest Sent</span>
                                    </a>
                                 </li>
                                 <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i>
                                    <span class="text-white">Contact</span>
                                    </a>
                                 </li>
                                 <li><a href="<?php echo site_url('chat'); ?>"><i class="fa fa-comment" aria-hidden="true"></i>
                                    <span class="text-white">Chat</span>
                                    </a>
                                 </li> -->
                              </ul>
                           </div>
                        </div>
                     </div>
                     <?php
                     }
                     ?>
                     

                  </div>

                  <!-- <div class="row">
                     <div class="col-12">
                        <div class="pagination-wrap card-pagination">
                           <ul>
                              <li><a href="#">first</a></li>
                              <li class="active"><a href="#">01</a></li>
                              <li><a href="#">02</a></li>
                              <li><a href="#">next</a></li>
                           </ul>
                        </div>
                     </div>
                  </div> -->
               </div>
            </div>
         </div>
         <div id="menu2" class="container tab-pane fade">
            <div id="user" class="user-profile">
               <div class="container">
                  <div class="row">
                     <?php
                     foreach ($receive as $r) 
                     {
                        // $user_img = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $r->user_id ));
                        $shorts = $this->Site_model->getShortlistByLoginUserId($user_id,$r->user_id);
                     ?>
                     <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                           <div class="thumb">
                              <a href="<?=site_url('about-profile');?>?User-Details=<?=base64_encode($r->user_id); ?>&user-sno=<?=time().md5($r->user_id).time();?>">
                                 <?php
                                  if($r->user_avtar !='')
                                  {
                                  ?>
                                  <img class="img-fluid" alt="user" src="<?=base_url('assets');?>/images/user/<?=$r->user_avtar; ?>">
                                  <?php
                                  }else{
                                  ?>
                                  <img class="img-fluid" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                              <?php } ?>
                              </a>
                              <div class="overlay">
                              </div>
                              <div class="info">
                                 <div class="user-details">
                                    <h4 class="text-left"><?=$r->name; ?></h4>
                                    <p class="profession"><?=$this->User_model->getOccupation($r->occupation); ?></p>
                                    <div class="row">
                                       <div class="col-lg-6">
                                          <p>  <span class="year"> </span> <span class="year"> <?=$this->User_model->getHeightById($r->height); ?></span><span><?=$r->family_status?></span> </p>
                                       </div>
                                       <div class="col-lg-6">
                                          <span class="profession-1"><?=$r->qualification; ?></span>
                                       </div>
                                       <div class="col-lg-6">
                                          <h6 class="address"><?=$r->city; ?></h6>
                                       </div>
                                       <div class="col-lg-6">
                                          <h6 class="marital-status"><?=$this->User_model->getMaritalStatusById($r->marital_status); ?></h6>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="user-bottom">
                              <ul class="profile-list">
                                 <li>
                                    <a href="<?=site_url('shortlistAction');?>?User-Details=<?=base64_encode($user_id); ?>&short-listed=<?=base64_encode($r->user_id);?>&user-sno=<?=time().md5($r->user_id).time();?>"><i class="fa fa-star" aria-hidden="true" <?php if($shorts == '1'){ ?> style="color: yellow;" <?php }else{} ?> ></i>
                                    <span class="text-white">Shortlist</span>
                                    </a>
                                 </li>
                                 <!-- <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    <span class="text-white">Interest</span>
                                    </a>
                                 </li>
                                 <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i>
                                    <span class="text-white">Contact</span>
                                    </a>
                                 </li>
                                 <li><a href="<?php echo site_url('chat'); ?>"><i class="fa fa-comment" aria-hidden="true"></i>
                                    <span class="text-white">Chat</span>
                                    </a>
                                 </li> -->
                              </ul>
                           </div>
                        </div>
                     </div>
                     <?php
                     }
                     ?>
                     
                  </div>
                  <div class="row">
                     <!-- <div class="col-12">
                        <div class="pagination-wrap card-pagination">
                           <ul>
                              <li><a href="#">first</a></li>
                              <li class="active"><a href="#">01</a></li>
                              <li><a href="#">02</a></li>
                              <li><a href="#">next</a></li>
                           </ul>
                        </div>
                     </div> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script src='<?php echo base_url('front_assets'); ?>/js/jquery.min.js'></script>