<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Users List</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="active">Users List</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <h3 class="box-title m-b-0">ALL USER</h3>
                    <p class="text-muted m-b-30 font-13">You can see all details of User List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Gender</th>
                                    <th>Date of Joined</th>
                                    <th>Status</th>  
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($user as $u)
                                {
                                ?>
                                <tr>
                                    <td><?=$u->name; ?></td>
                                    <td><?=$u->email; ?></td>
                                    <td><?=$u->mobile; ?></td>
                                    <td>
                                        <?php
                                        if($u->gender == 1)
                                        {
                                            echo "Male";
                                        }else{
                                            echo "Female";
                                        }

                                        ?>
                                            
                                    </td>
                                    <td><?=$u->created_at; ?></td>
                                    <td>
                                        <?php if($u->status==1){ ?>
                                        <label class="badge badge-teal">Active</label>
                                        <?php }else if($u->status==0){ ?>
                                        <label class="badge badge-danger">Deactive</label>
                                        <?php  } ?> 
                                    </td>
                                    <td >
                                        <div class="dropdown">
                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a title="Verified" class="<?=$u->status==1?'disabled':''?>" href="<?php echo base_url('user/changeUserStatus');?>/<?php echo $u->user_id;?>">Activate</a>
                                            </li>
                                            <li>
                                                <a title="Not Verified" class="<?=$u->status==0?'disabled':''?>" href="<?php echo base_url('user/changeUserStatus');?>/<?php echo $u->user_id;?>" >Deactivate</a>
                                            </li>
											<li>
												<a title="Not Verified" class="<?=$u->status==0?'disabled':''?>" href="<?php echo base_url('user/userDelete');?>/<?php echo $u->user_id;?>" >Delete</a>
											</li>
                                            <!-- <li class="divider"></li>
                                            <li>
                                                <a title="Edit" href="<?=base_url()?>user/updateUser/<?php echo $u->user_id; ?>">Edit</a>
                                            </li> -->
                                            <li class="divider"></li>
                                            <li>
                                                <a title="Detail" href="<?=base_url()?>user/viewDetail/<?php echo $u->user_id; ?>">View Detail</a>
                                            </li>
                                        </ul> 
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>    
