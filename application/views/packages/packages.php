<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Packages</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Packages</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        
        <div class="row">
            
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['packages_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update Packages</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Add Packages</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('packages/submitPackages'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Title</label>
                                    <input type="hidden" name="packages_id" value="<?=@$row['packages_id'];?>">
                                    <input type="text" name="title" class="form-control" value="<?=@$row['title']?>" placeholder="Enter title" required>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Price</label>
                                    <input type="number" class="form-control" name="price" value="<?=@$row['price'] ;?>" placeholder="Enter Price" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Type</label>
                                    <select name="subscription_type" id="subscription_type" class="form-control" required>
                                        <option value="">--Select--</option>
                                        <option value="0">Free</option>
                                        <option value="1">Monthly</option>
                                        <option value="2">Quarterly</option>
                                        <option value="3">Halfyearly</option>
                                        <option value="4">Yearly</option>
                                    </select>
                                    <script type="text/javascript">
                                        setTimeout(function(){
                                        $("#subscription_type option[value='<?=@$row['subscription_type']?>']").prop('selected',true);
                                    }, 100);
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Language</label>
                                    <select name="language" id="language" class="form-control" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach($language as $l)
                                        {
                                            $select = '';
                                            if ($l->language_code == @$row['language']) {
                                                $select = 'selected';
                                            }
                                            ?>
                                            <option value="<?=$l->language_code; ?>" <?=$select; ?>><?=$l->language_name; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Description</label>
                                    <textarea class="form-control" name="description" placeholder="Enter description" required><?=@$row['description'];?></textarea>
                                </div>
                            </div>

                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['packages_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
            
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">All Packages</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of Packages List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Language</th>
                                    <th>Subscription Type</th>
                                    <th>Status</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($packages as $n) 
                            { 
                                $i++; 
                            ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $n->title; ?></td>
                                <td><?php echo $n->description; ?></td>
                                <td><?=$n->price; ?></td>
                                <td><?=$n->language; ?></td>
                                <td>
                                    <?php if($n->subscription_type==0){ ?>
                                    <label class="badge badge-teal">Free</label>
                                    <?php }else if($n->subscription_type==1){ ?>
                                    <label class="label label-success label-rouded">Monthly</label>
                                    <?php  }else if($n->subscription_type==2){ ?>
                                    <label class="label label-info label-rouded">Quarterly</label>
                                    <?php  }else if($n->subscription_type==3){ ?>
                                    <label class="badge badge-danger">Halfyearly</label>
                                    <?php  }else if($n->subscription_type==4){ ?>
                                    <label class="badge badge-teal">Yearly</label>
                                    <?php  } ?>
                                </td>
                                <td>
                                    <?php if($n->status==1){ ?>
                                    <label class="badge badge-teal">Active</label>
                                    <?php }else if($n->status==0){ ?>
                                    <label class="badge badge-danger">Deactive</label>
                                    <?php  } ?> 
                                </td>
                                <td >
                                    <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a title="Verified" class="<?=$n->status==1?'disabled':''?>" href="<?php echo base_url('packages/changePackagesStatus');?>/<?php echo $n->packages_id;?>">Activate</a>
                                        </li>
                                        <li>
                                            <a title="Not Verified" class="<?=$n->status==0?'disabled':''?>" href="<?php echo base_url('packages/changePackagesStatus');?>/<?php echo $n->packages_id;?>" >Deactivate</a>
                                        </li>
                                    
                                    <li class="divider"></li>
                                    <li>
                                        <a title="Edit" href="<?=base_url()?>packages/updatePackages/<?php echo $n->packages_id; ?>">Edit</a>
                                    </li>
                                     </ul> 
                                    </div>
                                </td>
                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>
