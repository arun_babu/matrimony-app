<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Language</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Language</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['language_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update Language</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Add Language</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('language/submitLanguage'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Language Code</label>
                                    <input type="hidden" name="language_id" value="<?=@$row['language_id'];?>">
                                    <input type="text" name="language_code" class="form-control" value="<?=@$row['language_code']?>" placeholder="Enter language code" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Language name</label>
                                    <input type="text" name="language_name" class="form-control" value="<?=@$row['language_name']?>" placeholder="Enter language name" required>
                                </div>
                            </div>
                            
                            
                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['language_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">All Language</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of Language List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Language Code</th>
                                    <th>Language Name</th>
                                    <th>Status</th>  
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($language as $c) 
                            { 
                                $i++; 
                            ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?=$c->language_code?></td>
                                <td><?php echo $c->language_name; ?></td>
                                <td>
                                    <?php if($c->status==1){ ?>
                                    <label class="badge badge-teal">Active</label>
                                    <?php }else if($c->status==0){ ?>
                                    <label class="badge badge-danger">Deactive</label>
                                    <?php  } ?> 
                                </td>
                                <td >
                                    <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                    <li>
                                        <a title="Verified" class="<?=$c->status==1?'disabled':''?>" href="<?php echo base_url('language/changeLanguageStatus');?>/<?php echo $c->language_id;?>">Activate</a>
                                    </li>
                                    <li>
                                        <a title="Not Verified" class="<?=$c->status==0?'disabled':''?>" href="<?php echo base_url('language/changeLanguageStatus');?>/<?php echo $c->language_id;?>" >Deactivate</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a title="Edit" href="<?=base_url()?>language/updateLanguage/<?php echo $c->language_id; ?>">Edit</a>
                                    </li>
                                     </ul> 
                                  </div>
                              </td>

                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>
