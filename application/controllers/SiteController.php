<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function index()
    {
        $res['banner'] = $this->CmsManagement_model->activeWebsiteBanner();
        $res['story'] = $this->CmsManagement_model->activeHappyStory();
        $res['social'] = $this->CmsManagement_model->socialMedia();
        $this->load->view('front/index',$res);
        $this->load->view('front_common/footer');
    }

    public function userLogin()
    {
    	$input = $this->input->post();
    	$getdata = $this->Site_model->userLogin($input);
    	if($getdata == '1')
        {
            $this->session->set_flashdata('error_red',USER_NOT_FOUND);
            redirect();
        }
        else if($getdata == '2')
        {
            $this->session->set_flashdata('error_red',VALID_PASS);
            redirect();
        }
        else if($getdata == '3')
        {
            $this->session->set_flashdata('error_red',DEACTIVE_NEW);
            redirect();
        }
    }

    public function userSignUp()
    {
    	$input = $this->input->post();
    	$this->Site_model->userSignUp($input);
    }

    public function userResetPassword()
    {
    	$email = $this->input->post('email');
    	$query = $this->Site_model->userResetPassword($email);
    	if($query == 1)
        {
        	$this->session->set_flashdata('error',PASS_SEND);
            redirect();
        }
        else if($query == 2)
        {
        	$this->session->set_flashdata('error_red',VALID_EMAIL);
            redirect();
        }
    }

    public function signOut()
    {
        $this->session->sess_destroy();
        redirect();
    }

    public function membership()
    {
    	$data['package'] = $this->Base_model->getAllDataLanguage('mm_packages');
        $data['razorpay'] = $this->db->where('status','1')->get('mm_razorpay_key')->row_array();
        $this->load->view('front/membership',$data);
        $this->load->view('front_common/footer');
    }

    public function razorPaySuccess()
    { 
        $sesData = $this->session->userdata;
        $user_id = $sesData['user_id'];
        $package_id = $this->input->post('package_id');
        $no_of_days = $this->input->post('no_of_days');
        $totalAmount = $this->input->post('totalAmount');
        $pkg = $this->db->where('packages_id',$package_id)->get('mm_packages')->row_array();
        $no = $no_of_days+'1';
        $date = date('Y-m-d', strtotime("+$no days"));

        $length = 6;
        $order_id = "";
        $codeAlphabet = time();
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $order_id .= $codeAlphabet[random_int(0, $max-1)];
        }
 
        $data = [
               'order_id' => $order_id,
               'user_id' => $user_id,
               'txn_id' => $this->input->post('razorpay_payment_id'),
               'subscription_type' => $pkg['subscription_type'],
               'price' => $totalAmount,
               'packages_id' => $package_id,
               'end_subscription_date' => $date,
               'created_at' => date('Y-m-d H:i:s'),
            ];

        $insert = $this->db->insert('mm_subscription_history', $data);
        if($insert)
        {
            $usr_sub = $this->db->where('user_id',$user_id)->get('mm_user_subscription')->num_rows();
            if($usr_sub > 0)
            {
                $data2 = array(
                    'user_id' => $user_id,
                    'subscription_type' => $pkg['subscription_type'],
                    'start_subscription_date' => date('Y-m-d'),
                    'end_subscription_date' => $date,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->db->where('user_id',$user_id);
                $this->db->update('mm_user_subscription',$data2);
            }
            else
            {
                $data2 = array(
                    'user_id' => $user_id,
                    'subscription_type' => $pkg['subscription_type'],
                    'start_subscription_date' => date('Y-m-d H:i:s'),
                    'end_subscription_date' => $date,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('mm_user_subscription',$data2);
            }
        }

        $user = $this->db->where('user_id',$user_id)->get('mm_users')->row_array();
        if($user['end_subscription_date'] !='')
        {
            if($user['end_subscription_date'] >= date('Y-m-d'))
            {
                $end_subscription_date = $user['end_subscription_date'];
                $now = time(); // or your date as well
                $your_date = strtotime("$end_subscription_date");
                $datediff =  $your_date - $now;

                $day = round($datediff / (60 * 60 * 24));
                $num = $day+$no;
                $dates = date('Y-m-d', strtotime("+$num days"));
                // echo $dates; die;
            }
            else
            {
                $no = $no_of_days+'1';
                $dates = date('Y-m-d', strtotime("+$no days"));
            }
            
        }
        else
        {
            $no = $no_of_days+'1';
            $dates = date('Y-m-d', strtotime("+$no days"));
        }

        $data1 = array(
            'end_subscription_date' => $dates
        );
        $sucess = $this->db->where('user_id',$user_id)->update('mm_users',$data1);
        if($sucess)
        {
            $email = $user['email'];
            $name = $user['name'];
            $subject = SUBSCRIPTION_HEAD;
            $msg = "Hello $name this is your subscription confirmation message, Your subscription package is $no_of_days days it cost you $totalAmount Rs. and it will be available till $dates, thank you.";
            $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
        }
        $arr = array('msg' => 'Payment successfully credited', 'status' => true);  
    }

    public function profile()
    {
    	$sesData = $this->session->userdata;
    	$data['user'] = $this->User_model->getUserData($sesData['user_id']);
    	// $data['user_img'] = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $sesData['user_id'] ));
    	$this->load->view('front_common/user-header');
        $this->load->view('front/profile',$data);
        $this->load->view('front_common/footer');
    }

    public function edit_profile()
    {
    	$sesData = $this->session->userdata;
    	$data['user'] = $this->User_model->getUserData($sesData['user_id']);
    	$data['height'] = $this->Base_model->getAllData('mm_heights');
    	$data['state'] = $this->Base_model->getAllData('mm_states');
    	$data['district'] = $this->Base_model->getAllData('mm_districts');
    	$data['income'] = $this->Base_model->getAllDataLanguage('mm_income');
    	$data['marital'] = $this->Base_model->getAllDataLanguage('mm_marital_status');
    	$data['manglik'] = $this->Base_model->getAllDataLanguage('mm_manglik');
    	$data['blood'] = $this->Base_model->getAllData('mm_blood_group');
    	$data['occupation'] = $this->Base_model->getAllDataLanguage('mm_occupation');
    	$data['caste'] = $this->Base_model->getAllDataLanguage('mm_caste');
    	$this->load->view('front_common/user-header');
        $this->load->view('front/edit-profile',$data);
        $this->load->view('front_common/footer');
    }

    public function updateUserRecord()
    {
    	$input = $this->input->post();
    	$this->User_model->updateUserRecord($input);
    }

    public function shortlist()
    {
    	$sesData = $this->session->userdata;
    	$data['user'] = $this->Site_model->getShortlistUser($sesData['user_id']);
    	$this->load->view('front_common/user-header');
        $this->load->view('front/shortlist',$data);
        $this->load->view('front_common/footer');
    }

    public function about_profile()
    {
    	$user_id = base64_decode($this->input->get('User-Details'));
    	$data['user'] = $this->User_model->getUserData($user_id);
    	// $data['user_img'] = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $user_id ));
    	$this->load->view('front_common/user-header');
        $this->load->view('front/about-profile',$data);
        $this->load->view('front_common/footer');
    }

    public function interest()
    {
    	$sesData = $this->session->userdata;
    	$user_id = $sesData['user_id'];
    	$data['send'] = $this->Site_model->getSendInterest($user_id);
    	$data['receive'] = $this->Site_model->getReceivedInterest($user_id);
    	$data['user_id'] = $user_id;
    	$this->load->view('front_common/user-header');
        $this->load->view('front/interest',$data);
        $this->load->view('front_common/footer');
    }

    public function shortlistAction()
    {
    	$user_id = base64_decode($this->input->get('User-Details'));
    	$short_listed_id = base64_decode($this->input->get('short-listed'));
    	$this->Site_model->shortlistAction($user_id,$short_listed_id);
    }

    public function matches()
    {
    	$sesData = $this->session->userdata;
    	$data['user'] = $this->Site_model->getUserMatches($sesData['user_id']);
    	$this->load->view('front_common/user-header');
        $this->load->view('front/matches',$data);
        $this->load->view('front_common/footer');
    }

    public function visitor()
    {
    	$data['user'] = $this->User_model->getActiveUsers();
    	$this->load->view('front_common/user-header');
        $this->load->view('front/visitor',$data);
        $this->load->view('front_common/footer');
    }

    public function news()
    {
    	$data['news'] = $this->db->where('status','1')->where('language','en')->get('mm_events')->row_array();
    	$this->load->view('front_common/user-header');
        $this->load->view('front/news',$data);
        $this->load->view('front_common/footer');
    }

    public function chat()
    {
        $sesData = $this->session->userdata;
        $data['list_user'] = $this->Site_model->getChatList($sesData['user_id']);
        $data['user_img'] = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $sesData['user_id'] ));
        $data['user'] = $this->Base_model->getSingleRow('mm_users', array('user_id' => $sesData['user_id'] ));
    	$this->load->view('front_common/user-header');
        $this->load->view('front/chat',$data);
        $this->load->view('front_common/footer');
    }

    public function getChatHistory()
    {
        $sesData = $this->session->userdata;
        $to_user_id = $sesData['user_id'];
        $user_id = $this->input->post('user_id');
        // echo $user_id; die;
        $data['message'] = $this->Site_model->getChatHistory($to_user_id,$user_id);
        // $data['user_img'] = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $user_id ));
        $data['user'] = $this->Base_model->getSingleRow('mm_users', array('user_id' => $user_id ));
        $data['user_id'] = $to_user_id;
        $data['to_user_id'] = $user_id;
        $this->load->view('front/chat_msg',$data);
    }

    public function setChatMsg()
    {
        $to_user_id = $this->input->post('to_user_id');
        $from_user_id = $this->input->post('from_user_id');
        $message = $this->input->post('message');
        $message_head_id = $this->Site_model->setChatMsg($to_user_id,$from_user_id);
        $data = array(
            'message_head_id' => $message_head_id,
            'from_user_id' => $from_user_id,
            'to_user_id' => $to_user_id,
            'message' => $message,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            );
        $success = $this->db->insert('mm_message',$data);
        if($success)
        {
            $data1['message'] = $this->Site_model->getChatHistory($from_user_id,$to_user_id);
            $data1['user_img'] = $this->Base_model->getSingleRow('mm_user_image', array('user_id' => $to_user_id ));
            $data1['user'] = $this->Base_model->getSingleRow('mm_users', array('user_id' => $to_user_id ));
            $data1['user_id'] = $from_user_id;
            $data1['to_user_id'] = $to_user_id;
            $this->load->view('front/chat_msg',$data1);
        }
    }

    public function userImageUpdate()
    {
        // echo "<pre>";
        // print_r($_FILES['image']['name']); die;
        $input = $this->input->post();
        $image = $_FILES['image']['name'];
        $id = $this->Site_model->userImageUpdate($input,$image);
        
    }

}