<section id="breadcrumb">
   <div class="container">
      <ul class="breadcrumb">
         <li><a href="#">Home</a></li>
         <li><a href="#">Profile</a></li>
      </ul>
   </div>
</section>
<section class="user-view">
   <div class="container">
      <div class="main-body">
         <div class="row gutters-sm">
            <div class="col-md-3 mb-3">
               <div class="card">
                  <div class="card-body">
                     <div class="d-flex flex-column align-items-center text-center">
                        <div class="card__profile">
                           <div class="avatar-upload">
                              <div class="avatar-edit">
                                 <!-- <form action="" method="post" id="form-image"> -->
                                    <input data-toggle="modal" data-target="#myModal" id="imageUpload" />
                                    <label for="imageUpload"></label>
                                 <!-- </form> -->
                              </div>
                              <div class="avatar-preview">
                                <?php
                                  if($user['user_avtar'] !='')
                                  {
                                  ?>
                                  <img class="profile-user-img img-fluid img-circle" id="imagePreview" alt="user" src="<?=base_url('assets');?>/images/user/<?=$user['user_avtar']; ?>">
                                  <?php
                                  }else{
                                  ?>
                                  <img class="profile-user-img img-fluid img-circle" id="imagePreview" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                              <?php } ?>
                                 <!-- <img class="profile-user-img img-fluid img-circle" id="imagePreview" src="<?php echo base_url('front_assets'); ?>/images/pr-img.jpg" alt="User profile picture"> -->
                              </div>
                           </div>
                        </div>
                        <div class="mt-3">
                           <h4><?=$user['name']; ?></h4>
                           <p class="text-secondary mb-1"><?=$this->User_model->getOccupation($user['occupation']); ?></p>
                           <p class="text-muted font-size-sm"><?=$user['city']; ?>, <?=$this->User_model->getStatesById($user['status']); ?></p>
                           <button class="btn button-1 "><a href="<?php echo site_url('edit-profile'); ?>" class="button-1 h-ed">Edit</a></button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--      <div class="card mt-3">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Full Name</h6>
                      <span class="text-secondary">Kenneth Valdez</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Email</h6>
                      <span class="text-secondary">example@gmail.com</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Phone</h6>
                      <span class="text-secondary">(91) 816-9029</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Age </h6>
                      <span class="text-secondary">bootdey</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                      <h6 class="mb-0"> Marital Status</h6>
                      <span class="text-secondary">Never Married</span>
                    </li>
                  </ul>
                  </div> -->
            </div>
            <div class="col-md-9">
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info">Basic Info</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Full Name</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['name']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Email</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['email']; ?>
                              </div>
                           </div>
                           <hr>
                           <!-- <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Age</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 24 Year
                              </div>
                           </div>
                           <hr> -->
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Marital Status</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getMaritalStatusById($user['marital_status']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Height</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getHeightById($user['height']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Gotra</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['gotra']; ?>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Gotra Nanihal</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['gotra_nanihal']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">State</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getStatesById($user['status']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Distric</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getDistrictsById($user['district']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">City</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['city']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Annual Income</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getIncomeById($user['income']); ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
               <!----------------- basic info end---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info">Life Style</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Date of Birth</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['dob']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Aadhaar No.</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['aadhaar']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Manglik</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$this->User_model->getManglikById($user['manglik']); ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Birth Time</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['birth_time']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Birth Place</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                 <?=$user['birth_place']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
                <!----------------- life style  end---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info">About Me</h5>
                     <div class="row">
                       <div class="col-md-12 col-sm-12 text-secondary">
                                  <h6 class="mb-0">About</h6>
                                <?=$user['about_me']; ?>
<hr>

                              </div>

                        <div class="col-md-6">
        
                           
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Weight(kg)</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                <?=$user['weight']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Body Type</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                               <?=$user['body_type']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-4">
                                 <h6 class="mb-0">Complexion</h6>
                              </div>
                              <div class="col-sm-8 text-secondary">
                                <?=$user['complexion']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Physical Challenged</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 None
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Blood Group</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$this->User_model->getBloodGroupById($user['blood_group']); ?>
                              </div>
                           </div>
                             <hr>
                        </div>
                     </div>
                  </div>
               </div>
               <!----------------- About me  end---------->
                <!----------------- Important ---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info"> Important  Details</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Highest Education</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['qualification']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Work Experience</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?php
                                if($user['working'] == '1')
                                {
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                                ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Work place</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['work_place']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Occupation</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$this->User_model->getOccupation($user['occupation']); ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Organization name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['organisation_name']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
                              <!-----------------about more  end---------->
                <!----------------- About Family start ---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info"> About more</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Dietary Habits</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['dietary']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Drinking Habits</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['drinking']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Smoking Habits</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['smoking']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Language/Speak</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['language']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Hobbies</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                 <?=$user['hobbies']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Interests</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['interests']; ?>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
               </div>
                    <!----------------- Important me  end---------->
                <!----------------- About more start ---------->
               <div class="card mb-3">
                  <div class="card-body">
                     <h5 class="basic-info"> About Family</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Grandfather Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">  
                                <?=$user['grand_father_name']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Maternal Grandfather Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['maternal_grand_father_name_address']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Father Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                                <?=$user['father_name']; ?>
                              </div>
                           </div>
                           <hr>
                               <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Mother Name</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                             <?=$user['mother_name']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Address</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_address']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Father's Occupation</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['father_occupation']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Mother' Occupation</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['mother_occupation']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0"> Brother(s)</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['brother']; ?>
                              </div>
                           </div>
                           <hr>
                          
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Sister</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                              <?=$user['sister']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Family Income</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_income']; ?>
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Family Type</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                             <?=$user['family_type']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Family Value</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_value']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">State</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_state']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">District</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_district']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">City </h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_city']; ?>
                              </div>
                           </div>
                           <hr>
                            <div class="row">
                              <div class="col-sm-6">
                                 <h6 class="mb-0">Pincode</h6>
                              </div>
                              <div class="col-sm-6 text-secondary">
                               <?=$user['family_pin']; ?>
                              </div>
                           </div>
                           <hr>
                          
                        </div>
                     </div>
                  </div>
               </div>
               <!------------- end family details -------------->
           
            </div>
         </div>
      </div>
   </div>

</section>
<div class="modal" id="myModal">
         <div class="modal-dialog">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="first">
                  <?php
                      if($user['user_avtar'] !='')
                      {
                      ?>
                      <img class="profile-user-img img-fluid img-circle" id="imagePreview" alt="user" src="<?=base_url('assets');?>/images/user/<?=$user['user_avtar']; ?>">
                      <?php
                      }else{
                      ?>
                      <img class="profile-user-img img-fluid img-circle" id="imagePreview" alt="user" src="<?=base_url('assets');?>/images/image.png"> 
                  <?php } ?>
                  <!-- <img class="img-fluid po-logo" src="<?php echo base_url('front_assets'); ?>/images/m-logop.png" > -->

                  <h5 class="text-center p-welcome">Update User Image</h5>
                  <h5 class="text-center" id="erroLog"></h5>
                  <form class="text-center" method="post" action="<?=site_url('userImageUpdate'); ?>" enctype="multipart/form-data">
                     
                     <div class="form-group">
                        <input type="hidden" name="user_id" value="<?=$user['user_id']; ?>">
                         <input class="form-control" type="file" name="image" required>
                     </div>
                      
                     <div class="form-group mb-10">
                        <button type="submit" class="button-4 ar">Update</button>
                       <!-- <a href="" class="button-1 ar">Submit</a> -->
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
<style type="text/css">
.button-4.ar {
background-color: #e43f3b;
border: 2px solid #e43f3b;
padding: 6px 0;
color: #fff;
margin: 0px auto 0;
width: 180px;
border-radius: 23px;
display: block;
text-align: center;
}
</style>      
<script src='<?php echo base_url('front_assets'); ?>/js/jquery.min.js'></script>