<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">All Users</h4> </div>
        <!-- /.col-lg-12 -->
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <div class="float-left d-none d-md-block">
                <div class="send_msg" style="float: right; padding: 5px 0;">
                    <input class="btn btn-primary" id="send_msgAll" type="submit" value="Message to all" data-toggle="modal" data-target="#responsive-modal"/>
                </div>
            </div> 
            <!-- <div class="float-right d-none d-md-block">
                <div class="send_msg" style="float: right; padding: 5px 0;">
                    <input disabled="disabled" id="send_msg" class="btn btn-primary" type="submit" value="Send Message" data-toggle="modal" data-target="#responsive-modal-Rahul"/>
                </div>
            </div>  -->
          </div>
 
          
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                <h3 class="box-title m-b-0">All Users</h3>
                <div class="table-responsive">
                    <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>S. No.</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Mobile</th>
                              <th>Send Message</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $i=0; 
                        foreach($user as $u) 
                        { 
                          $i++;

                        ?> 
                          <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $u->name; ?></td>       
                              <td><?php echo $u->email; ?></td>
                              <td><?=$u->mobile?></td>
                              <td>
                                <!-- <input class="notification_check mychechk" type="checkbox" value="<?=$u->user_id ?>" name="check" style="opacity:1;position:inherit;"> -->
                                <input id="send_msg" class="btn btn-primary" type="submit" value="Send Message" data-toggle="modal" data-target="#responsive-modal-Rahul<?=$i?>"/>

                                <div id="responsive-modal-Rahul<?=$i?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Send Notification</h4> 
                                          </div>
                                          <form method="post" action="<?=base_url();?>send_notification">
                                          <div class="modal-body">
                                              
                                            <div class="form-group">
                                                <label for="recipient-name" class="control-label">Title:</label>
                                                <input type="hidden" id="uid11" name="uid" value="<?=$u->user_id ?>">
                                                <input type="text" name="title" class="form-control" id="recipient-name" required placeholder="Enter Title"> 
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Message:</label>
                                                <textarea class="form-control" id="message-text" name="message" required placeholder="Enter Message"></textarea>
                                            </div>
                                              
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-danger waves-effect waves-light">Send</button>
                                          </div>
                                        </form>
                                      </div>
                                  </div>
                              </div>

                              </td>
                              
                          </tr>
                        <?php 
                        } 
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>

    <!-- sample modal content -->
    <!-- /.modal -->
    <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Send notification to all</h4> 
              </div>
              <form method="post" action="<?=base_url();?>add_broadcast">
                <div class="modal-body">
                    
                  <div class="form-group">
                      <label for="recipient-name" class="control-label">Title:</label>
                      <input type="hidden" id="uid" name="uid" value="send_all">
                      <input type="text" name="title" class="form-control" id="recipient-name" required placeholder="Enter Title"> 
                  </div>
                  <div class="form-group">
                      <label for="message-text" class="control-label">Message:</label>
                      <textarea class="form-control" id="message-text" name="message" required placeholder="Enter Message"></textarea>
                  </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Send</button>
                </div>
              </form>
            </div>
        </div>
    </div>

    
    <!-- Button trigger modal -->