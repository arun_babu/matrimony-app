<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManglikController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function manglik()
	  {
      if (isset($_SESSION['id'])) {
      $res['language'] = $this->Setting_model->activeLanguage();
			$res['manglik'] = $this->Manglik_model->manglik();
			echo load_page($this,'manglik/manglik',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function submitManglik()
  	{
  		$input = $this->input->post();
    	$id = $this->Manglik_model->submitManglik($input);
  	}

  	public function changeManglikStatus()
    {
        $id = $this->uri->segment('3');
        $this->Manglik_model->changeManglikStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('manglik');
    }

    public function updateManglik()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Manglik_model->updateManglik($id);
        $data['language'] = $this->Setting_model->activeLanguage();
        $data['manglik'] = $this->Manglik_model->manglik();
        echo load_page($this,'manglik/manglik',$data);
    }

}