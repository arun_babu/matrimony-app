<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">About & Social Media</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">About & Social Media</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        
        <div class="row">
            
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <h3 class="box-title m-b-0">Update About & Social Media</h3>
                  <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('socialMedia/updateSocialMedia'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Facebook</label>
                                    <input type="hidden" name="social_media_id" value="<?=@$social['social_media_id'];?>">
                                    <input type="text" name="facebook" class="form-control" value="<?=@$social['facebook']?>" placeholder="Enter facebook url" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Instagram </label>
                                    <input type="text" class="form-control" name="insta" value="<?=@$social['insta']?>" placeholder="Enter instagram url" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Linkedin </label>
                                    <input type="text" class="form-control" name="linkedin" value="<?=@$social['linkedin']?>" placeholder="Enter linkedin url" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Twitter </label>
                                    <input type="text" class="form-control" name="twitter" value="<?=@$social['twitter']?>" placeholder="Enter twitter url" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Contact </label>
                                    <input type="number" class="form-control" name="contact" value="<?=@$social['contact']?>" placeholder="Enter contact no" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Email </label>
                                    <input type="email" class="form-control" name="email" value="<?=@$social['email']?>" placeholder="Enter email" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">About Matrimony</label>
                                    <textarea class="form-control" name="about" placeholder="Enter About Matrimony" rows="4" cols="50" required><?=@$social['about'];?></textarea>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
            
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        
        </div>
        <!-- /.row -->
    </div>
    </div>
