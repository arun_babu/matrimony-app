<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShortlistController extends CI_Controller{

	public function __construct()
    {
        require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function shortlist()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $data = $this->Api_Shortlist_model->shortlist($input,$lan);
    }

    public function getShortlist()
    {
    	foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        }
        $input = $this->input->post();
        $data = $this->Api_Shortlist_model->getShortlist($input,$lan);
        if (empty($data)) {
            if($lan == 'en')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }else if($lan == 'hi')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
            }else if($lan == 'mr')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
            }
            $this->Base_model->responseFailed(0, $DATA_NOT_AVAIABLE);
        } //empty($data)
        else {
            $this->Base_model->responseSuccess(1, 'All Data', $data);
        }
    }

    public function sendInterest()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $data = $this->Api_Shortlist_model->sendInterest($input,$lan);
    }

    public function getInterest()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $data = $this->Api_Shortlist_model->getInterest($input,$lan);
        if (empty($data)) {
            if($lan == 'en')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }else if($lan == 'hi')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
            }else if($lan == 'mr')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
            }
            $this->Base_model->responseFailed(0, $DATA_NOT_AVAIABLE);
        } //empty($data)
        else {
            $this->Base_model->responseSuccess(1, 'All Data', $data);
        }
    }

    public function acceptedInterest()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $data = $this->Api_Shortlist_model->acceptedInterest($input,$lan);
    }

}