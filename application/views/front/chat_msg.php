<div class="contact-profile">
   <?php
    if(@$user->user_avtar !='')
    {
    ?>
    <img alt="user" src="<?=base_url('assets');?>/images/user/<?=$user->user_avtar; ?>" style="height: 43px; width: 42px;">
    <?php
    }else{
    ?>
    <img alt="user" src="<?=base_url('assets');?>/images/image.png"> 
   <?php } ?>
   <!-- <img src="<?php echo base_url('front_assets'); ?>/images/chat/harveyspecter.png" alt="" /> -->
   <p><?=$user->name; ?></p>
</div>
<div class="messages">
   <ul>
      <?php
      foreach ($message as $m) 
      {
         if($m['from_user_id'] != $user_id)
         {
            $to_user_img = $this->Base_model->getSingleRow('mm_users', array('user_id' => $m['from_user_id']));
      ?>
      <li class="sent">
         <?php
          if($to_user_img->user_avtar !='')
          {
          ?>
          <img alt="user" src="<?=base_url('assets');?>/images/user/<?=$to_user_img->user_avtar; ?>">
          <?php
          }else{
          ?>
          <img alt="user" src="<?=base_url('assets');?>/images/image.png"> 
         <?php } ?>
         <p><?=$m['message'];?></p>
      </li>
      <?php
      } else{
         $from_user_img = $this->Base_model->getSingleRow('mm_users', array('user_id' => $user_id));
      ?>
      <li class="replies">
         <?php
          if($from_user_img->user_avtar !='')
          {
          ?>
          <img alt="user" src="<?=base_url('assets');?>/images/user/<?=$from_user_img->user_avtar; ?>">
          <?php
          }else{
          ?>
          <img alt="user" src="<?=base_url('assets');?>/images/image.png"> 
         <?php } ?>
         <p><?=$m['message'];?></p>
      </li>
      <?php
      }
      }
      ?>
   </ul>
</div>
<div class="message-input">
   <div class="wrap">
      <input type="hidden" id="to_user_id" name="to_user_id" value="<?=$to_user_id; ?>">
      <input type="hidden" id="from_user_id" name="from_user_id" value="<?=$user_id; ?>">
      <input type="text" id="message1" name="message" class="send_message" placeholder="Write your message..." />
      <!-- <i class="fa fa-paperclip attachment" aria-hidden="true"></i> -->
      <button class="submit" id="myBtn" onclick="setChatMsg();"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
   </div>
</div>