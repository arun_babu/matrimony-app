<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubscribeController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function subscriptionCheck()
    {
        $user_id = $this->input->post('user_id');
        $data = $this->Api_Subscribe_model->subscriptionCheck($user_id);
    }

    public function userSubscribe()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $data = $this->Api_Subscribe_model->userSubscribe($input,$lan);
    }

    public function subscribeHistory()
    {
    	foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $data = $this->Api_Subscribe_model->subscribeHistory($input,$lan);
    }


}