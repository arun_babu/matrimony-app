<?php
class Setting_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getFirebaseKey()
    {
        $row = $this->db->get('mm_firebase_key')->row_array();
        return $row;
    }

    public function getRazorpayKey()
    {
        $row = $this->db->get('mm_razorpay_key')->row_array();
        return $row;
    }

    public function updateFirebaseKey($input)
    {
        $this->form_validation->set_rules('firebase_key', 'firebase_key', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('apiKeys');
            exit();
        }

        // UPDATE DATA HEAR...!!

        $data = array(
            'firebase_key' => $input['firebase_key'],
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('id',$input['id']);
        $this->db->update('mm_firebase_key',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('apiKeys');
        
    }

    public function updateRazorpayKey($input)
    {
        $this->form_validation->set_rules('razorpay_key', 'razorpay_key', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('apiKeys');
            exit();
        }

        // UPDATE DATA HEAR...!!

        $data = array(
            'razorpay_key' => $input['razorpay_key'],
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('id',$input['id']);
        $this->db->update('mm_razorpay_key',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('apiKeys');
    }

    public function language()
    {
        $res = $this->db->get('mm_language')->result();
        return $res;
    }

    public function submitLanguage($input)
    {
        $this->form_validation->set_rules('language_code', 'language_code', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('language');
            exit();
        }

        if($input['language_id'] !='')
        {
            //Update
            $data = array(
                'language_code' => $input['language_code'],
                'language_name' => $input['language_name'],
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $this->db->where('language_id',$input['language_id']);
            $update = $this->db->update('mm_language',$data);
            if($update)
            {
                $data1 = array(
                    'language' => $input['language_code'],
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                $this->db->where('language_id',$input['language_id']);
                $update = $this->db->update('mm_faq',$data1);

                $data2 = array(
                    'language' => $input['language_code'],
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                $this->db->where('language_id',$input['language_id']);
                $update = $this->db->update('mm_privacy_policy',$data2);

                $data3 = array(
                    'language' => $input['language_code'],
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                $this->db->where('language_id',$input['language_id']);
                $this->db->update('mm_terms',$data3);
            }
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('language');
        }
        else
        {
            //Insert
            $data = array(
                'language_code' => $input['language_code'],
                'language_name' => $input['language_name'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('mm_language',$data);
            $language_id = $this->db->insert_id();
            if($language_id)
            {
                $data1 = array(
                    'language' => $input['language_code'],
                    'language_id' => $language_id,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $insert = $this->db->insert('mm_faq',$data1);

                $data2 = array(
                    'language' => $input['language_code'],
                    'language_id' => $language_id,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('mm_privacy_policy',$data2);

                $data3 = array(
                    'language' => $input['language_code'],
                    'language_id' => $language_id,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('mm_terms',$data3);
            }

            $this->session->set_flashdata('error', DATA_INSERT);
            redirect('language');
        }

        
    }

    public function changeLanguageStatus($id)
    {
        $this->db->where('language_id',$id);
        $query = $this->db->get('mm_language');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('language_id',$id);
            $this->db->update('mm_language',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('language_id',$id);
            $this->db->update('mm_language',$data);
        }
    }

    public function updateLanguage($language_id)
    {
        $query = $this->db->where('language_id',$language_id)->get('mm_language')->row_array();
        return $query;
    }

    public function activeLanguage()
    {
        $res = $this->db->where('status','1')->get('mm_language')->result();
        return $res;
    }

    public function faqData()
    {
        $this->db->select('l.language_name, f.*')
         ->from('mm_language l')
         ->join('mm_faq f', 'f.language_id = l.language_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function submitfaq($faq,$faq_id,$language)
    {
        $this->form_validation->set_rules('faq', 'faq', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('faqContent');
            exit();
        }

        $data = array(
            'faq' => $faq,
            'language' => $language,
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('faq_id',$faq_id);
        $this->db->update('mm_faq',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('faqContent');

    }

    public function privacyPolicyData()
    {
        $this->db->select('l.language_name, p.*')
         ->from('mm_language l')
         ->join('mm_privacy_policy p', 'p.language_id = l.language_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function submitPrivacyPolicy($privacy_policy,$privacy_policy_id,$language)
    {
        $this->form_validation->set_rules('privacy_policy', 'privacy_policy', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('privacyPolicyContent');
            exit();
        }

        $data = array(
            'privacy_policy' => $privacy_policy,
            'language' => $language,
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('privacy_policy_id',$privacy_policy_id);
        $this->db->update('mm_privacy_policy',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('privacyPolicyContent');

    }

    public function termsContentData()
    {
        $this->db->select('l.language_name, t.*')
         ->from('mm_language l')
         ->join('mm_terms t', 't.language_id = l.language_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function submitTerms($terms,$terms_id,$language)
    {
        $this->form_validation->set_rules('terms', 'terms', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('termsContent');
            exit();
        }

        $data = array(
            'terms' => $terms,
            'language' => $language,
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('terms_id',$terms_id);
        $this->db->update('mm_terms',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('termsContent');

    }

    public function faqPage($lan)
    {
        $q = $this->db->where('language',$lan)->get('mm_faq')->row_array();
        return $q['faq'];
    }

    public function privacyPolicyPage($lan)
    {
        $q = $this->db->where('language',$lan)->get('mm_privacy_policy')->row_array();
        return $q['privacy_policy'];
    }

    public function termsConditionPage($lan)
    {
        $q = $this->db->where('language',$lan)->get('mm_terms')->row_array();
        return $q['terms'];
    }


}