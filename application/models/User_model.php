<?php
class User_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function users()
    {
    	$res = $this->db->order_by('user_id','DESC')->get('mm_users')->result();
    	return $res;
    }

    public function getActiveUsers()
    {
        $sesData = $this->session->userdata;
        $res = $this->db->where('user_id !=',$sesData['user_id'])->where('status','1')->order_by('user_id','DESC')->get('mm_users')->result();
        return $res;
    }

    public function changeUserStatus($id)
    {
        $this->db->where('user_id',$id);
        $query = $this->db->get('mm_users');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('user_id',$id);
            $this->db->update('mm_users',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('user_id',$id);
            $this->db->update('mm_users',$data);
        }
    }
	public function userDelete($id)
	{
		$this->db->where('user_id',$id)->delete('mm_users');
		$this->db->where('user_id', $id)->delete('mm_user_image');


	}

    public function getUserData($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('mm_users')->row_array();
        return $query;
    }

    public function getOccupation($id)
    {
        if($id !='')
        {
            $q = $this->db->where('occupation_id',$id)->get('mm_occupation')->row_array();
            $occupation = $q['occupation'];
        }else{
            $occupation = '';
        }
        return $occupation;
    }

    public function getHeightById($id)
    {
        if($id !='')
        {
            $q = $this->db->where('id',$id)->get('mm_heights')->row_array();
            $height = $q['height'];
        }else{
            $height = '';
        }
        return $height;
    }

    public function getMaritalStatusById($id)
    {
        if($id !='')
        {
            $q = $this->db->where('id',$id)->get('mm_marital_status')->row_array();
            $marital_status = $q['marital_status'];
        }else{
            $marital_status = '';
        }
        return $marital_status;
    }

    public function getStatesById($id)
    {
        if($id !='')
        {
            $q = $this->db->where('id',$id)->get('mm_states')->row_array();
            $name = $q['name'];
        }else{
            $name = '';
        }
        return $name;
    }

    public function getDistrictsById($id)
    {
        if($id !='')
        {
            $q = $this->db->where('id',$id)->get('mm_districts')->row_array();
            $name = $q['name'];
        }else{
            $name = '';
        }
        return $name;
    }

    public function getIncomeById($id)
    {
        if($id !='')
        {
            $q = $this->db->where('income_id',$id)->get('mm_income')->row_array();
            $income = $q['income'];
        }else{
            $income = '';
        }
        return $income;
    }

    public function getManglikById($id)
    {
        if($id !='')
        {
            $q = $this->db->where('manglik_id',$id)->get('mm_manglik')->row_array();
            $manglik = $q['manglik'];
        }else{
            $manglik = '';
        }
        return $manglik;
    }

    public function getUserImageById($user_id)
    {
        $result = $this->db->where('user_id',$user_id)->get('mm_user_image')->result();
        return $result;
    }

    public function getBloodGroupById($id)
    {
        if($id!='')
        {
            $q = $this->db->where('id',$id)->get('mm_blood_group')->row_array();
            $blood_group = $q['blood_group'];
        }else{
            $blood_group = '';
        }
        return $blood_group;
    }

    public function getCasteById($caste_id)
    {
        if($caste_id!='')
        {
            $q = $this->db->where('caste_id',$caste_id)->get('mm_caste')->row_array();
            $caste = $q['caste'];
        }else{
            $caste = '';
        }
        return $caste;
    }

	public function getSubCasteById($caste_id)
	{
		if($caste_id!='')
		{
			$q = $this->db->where('id',$caste_id)->get('mm_sub_caste')->row_array();
			$caste = $q['sub_caste'];
		}else{
			$caste = '';
		}
		return $caste;
	}

    public function getDietaryById($gender_id)
    {
        if($gender_id !='')
        {
            $q = $this->db->where('diet_id',$gender_id)->get('mm_diet')->row_array();
            $gender = $q['diet'];
        }else{
            $gender = '';
        }
        return $gender;
    }
    public function getDrinkingById($gender_id)
    {
        if($gender_id !='')
        {
            $q = $this->db->where('drinking_smoking_id',$gender_id)->get('mm_drinking_smoking')->row_array();
            $gender = $q['drinking_smoking'];
        }else{
            $gender = '';
        }
        return $gender;
    }
    public function getGenderById($gender_id)
    {
        if($gender_id !='')
        {
            $q = $this->db->where('gender_id',$gender_id)->get('mm_gender')->row_array();
            $gender = $q['gender'];
        }else{
            $gender = '';
        }
        return $gender;
    }

    public function updateUserRecord($input)
    {
        $user_id = $input['user_id'];
        $name = $input['name'];
        // $email = $input['email'];
        $mobile = $input['mobile'];
        $gender = $input['gender'];
        $profile_for = $input['profile_for'];
        // $area_of_interest = $input['area_of_interest'];
        $blood_group = $input['blood_group'];
        // $caste = $input['caste'];
        $complexion = $input['complexion'];
        $body_type = $input['body_type'];
        $weight = $input['weight'];
        $about_me = $input['about_me'];
        $height = $input['height'];
        $qualification = $input['qualification'];
        $occupation = $input['occupation'];
        $income = $input['income'];
        $work_place = $input['work_place'];
        $organisation_name = $input['organisation_name'];
        $city = $input['city'];
        $state = $input['state'];
        $district = $input['district'];
        // $current_address = $input['current_address'];
        // $pin = $input['pin'];
        // $whatsapp_no = $input['whatsapp_no'];
        // $challenged = $input['challenged'];
        // $permanent_address = $input['permanent_address'];
        $marital_status = $input['marital_status'];
        $aadhaar = $input['aadhaar'];
        $dob = $input['dob'];
        $birth_place = $input['birth_place'];
        $birth_time = $input['birth_time'];
        $manglik = $input['manglik'];
        $gotra = $input['gotra'];
        $gotra_nanihal = $input['gotra_nanihal'];
        $family_address = $input['family_address'];
        $father_name = $input['father_name'];
        $father_occupation = $input['father_occupation'];
        $family_status = $input['family_status'];
        $family_district = $input['family_district'];
        $mother_name = $input['mother_name'];
        $grand_father_name = $input['grand_father_name'];
        $family_state = $input['family_state'];
        $family_pin = $input['family_pin'];
        $mother_occupation = $input['mother_occupation'];
        $family_type = $input['family_type'];
        $family_income = $input['family_income'];
        $family_city = $input['family_city'];
        $maternal_grand_father_name_address = $input['maternal_grand_father_name_address'];
        $family_value = $input['family_value'];
        $mobile2 = $input['mobile2'];
        $brother = $input['brother'];
        $sister = $input['sister'];
        $drinking = $input['drinking'];
        $dietary = $input['dietary'];
        $hobbies = $input['hobbies'];
        $smoking = $input['smoking'];
        $interests = $input['interests'];
        $working = $input['working'];

        $data = array(
            'name' => $name,
            // 'email' => $email,
            'mobile' => $mobile,
            'gender' => $gender,
            'profile_for' => $profile_for,
            // 'area_of_interest' => $area_of_interest,
            'blood_group' => $blood_group,
            // 'caste' => $caste,
            'complexion' => $complexion,
            'body_type' => $body_type,
            'weight' => $weight,
            'about_me' => $about_me,
            'height' => $height,
            'qualification' => $qualification,
            'occupation' => $occupation,
            'income' => $income,
            'work_place' => $work_place,
            'organisation_name' => $organisation_name,
            'city' => $city,
            'state' => $state,
            'district' => $district,
            // 'current_address' => $current_address,
            // 'pin' => $pin,
            // 'whatsapp_no' => $whatsapp_no,
            // 'challenged' => $challenged,
            // 'permanent_address' => $permanent_address,
            'marital_status' => $marital_status,
            'aadhaar' => $aadhaar,
            'dob' => $dob,
            'birth_place' => $birth_place,
            'birth_time' => $birth_time,
            'manglik' => $manglik,
            'gotra' => $gotra,
            'gotra_nanihal' => $gotra_nanihal,
            'family_address' => $family_address,
            'father_name' => $father_name,
            'father_occupation' => $father_occupation,
            'family_status' => $family_status,
            'family_district' => $family_district,
            'mother_name' => $mother_name,
            'grand_father_name' => $grand_father_name,
            'family_state' => $family_state,
            'family_pin' => $family_pin,
            'mother_occupation' => $mother_occupation,
            'family_type' => $family_type,
            'family_income' => $family_income,
            'family_city' => $family_city,
            'maternal_grand_father_name_address' => $maternal_grand_father_name_address,
            'family_value' => $family_value,
            'mobile2' => $mobile2,
            'brother' => $brother,
            'sister' => $sister,
            'drinking' => $drinking,
            'dietary' => $dietary,
            'hobbies' => $hobbies,
            'smoking' => $smoking,
            'interests' => $interests,
            'working' => $working,
            'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('user_id',$user_id);
        $this->db->update('mm_users',$data);
        redirect('profile');
        
    }

    public function requestUsers()
    {
        $res = $this->db->where('status !=','2')->get('mm_interest')->result();
        return $res;
    }

    public function changeRequestStatus($interest_id)
    {
        $data = array(
            'status' => '2'
        );
        $this->db->where('interest_id',$interest_id);
        $this->db->update('mm_interest',$data);
    }

}
