<?php
class Dashboard_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getUserCount()
    {
        $num = $this->db->where('status','1')->get('mm_users')->num_rows();
        return $num;
    }

    public function getTotalIncome()
    {
        $this->db->select_sum('price');
        $result = $this->db->get('mm_subscription_history')->row();  
        return $result->price;
    }

    public function getPackagesCount()
    {
    	$num = $this->db->where('status','1')->get('mm_packages')->num_rows();
        return $num;
    }

    public function getLanguageCount()
    {
    	$num = $this->db->where('status','1')->get('mm_language')->num_rows();
        return $num;
    }

    public function getAndroidUser()
    {
    	$num = $this->db->where('status','1')->where('device_type','android')->get('mm_users')->num_rows();
        return $num;
    }

    public function getIosUser()
    {
    	$num = $this->db->where('status','1')->where('device_type','ios')->get('mm_users')->num_rows();
        return $num;
    }

    public function getAdmin()
    {
    	$num = $this->db->get('mm_admin')->num_rows();
        return $num;
    }

    public function getLastUser()
    {
        $res = $this->db->where('status','1')->limit('10')->order_by('s_no','desc')->get('mm_users')->result();
        return $res;
    }


}