<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['admin/fileup'] 							= 'Admin/DashboardController/upload';
$route['admin/dashboard'] 							= 'Admin/DashboardController/index';
$route['admin/login']								= 'Admin/AuthController/index';
$route['log_in'] 									= 'Admin/AuthController/login';
$route['logout'] 									= 'Admin/AuthController/logout';

//Users 
$route['users'] 									= 'Admin/UserController/users';
$route['user/changeUserStatus/(:any)'] 				= 'Admin/UserController/changeUserStatus';
$route['user/userDelete/(:any)'] 				    = 'Admin/UserController/userDelete';
$route['user/viewDetail/(:any)'] 					= 'Admin/UserController/viewDetail';
$route['requestUsers'] 								= 'Admin/UserController/requestUsers';
$route['user/changeRequestStatus/(:any)'] 			= 'Admin/UserController/changeRequestStatus';

//News 
$route['news'] 										= 'Admin/NewsController/news';
$route['news/submitNews'] 							= 'Admin/NewsController/submitNews';
$route['news/changeNewsStatus/(:any)'] 				= 'Admin/NewsController/changeNewsStatus';
$route['news/updateNews/(:any)'] 					= 'Admin/NewsController/updateNews';
$route['news/deleteNews/(:any)'] 					= 'Admin/NewsController/deleteNews';

//Packages 
$route['packages'] 									= 'Admin/PackageController/packages';
$route['packages/submitPackages'] 					= 'Admin/PackageController/submitPackages';
$route['packages/changePackagesStatus/(:any)'] 		= 'Admin/PackageController/changePackagesStatus';
$route['packages/updatePackages/(:any)'] 			= 'Admin/PackageController/updatePackages';

//Caste 
$route['caste'] 									= 'Admin/CasteController/caste';
$route['caste/submitCaste'] 						= 'Admin/CasteController/submitCaste';
$route['caste/changeCasteStatus/(:any)'] 			= 'Admin/CasteController/changeCasteStatus';
$route['caste/updateCaste/(:any)'] 					= 'Admin/CasteController/updateCaste';

//Manglik 
$route['manglik'] 									= 'Admin/ManglikController/manglik';
$route['manglik/submitManglik'] 					= 'Admin/ManglikController/submitManglik';
$route['manglik/changeManglikStatus/(:any)'] 		= 'Admin/ManglikController/changeManglikStatus';
$route['manglik/updateManglik/(:any)'] 				= 'Admin/ManglikController/updateManglik';

//Occupation 
$route['occupation'] 								= 'Admin/OccupationController/occupation';
$route['occupation/submitOccupation'] 				= 'Admin/OccupationController/submitOccupation';
$route['occupation/changeOccupationStatus/(:any)'] 	= 'Admin/OccupationController/changeOccupationStatus';
$route['occupation/updateOccupation/(:any)'] 		= 'Admin/OccupationController/updateOccupation';

//Income 
$route['income'] 									= 'Admin/IncomeController/income';
$route['income/submitIncome'] 						= 'Admin/IncomeController/submitIncome';
$route['income/changeIncomeStatus/(:any)'] 			= 'Admin/IncomeController/changeIncomeStatus';
$route['income/updateIncome/(:any)'] 				= 'Admin/IncomeController/updateIncome';
$route['test'] 									= 'Admin/IncomeController/test';

//Notification 
$route['notification'] 								= 'Admin/NotificationController/notification';
$route['send_notification'] 						= 'Admin/NotificationController/send_notification';
$route['add_broadcast'] 							= 'Admin/NotificationController/add_broadcast';

//CMSManagement
$route['websiteBanner'] 									= 'Admin/CmsManagementController/websiteBanner';
$route['websiteBanner/submitWebsiteBanner'] 				= 'Admin/CmsManagementController/submitWebsiteBanner';
$route['websiteBanner/changeWebsiteBannerStatus/(:any)'] 	= 'Admin/CmsManagementController/changeWebsiteBannerStatus';
$route['websiteBanner/updateWebsiteBanner/(:any)'] 			= 'Admin/CmsManagementController/updateWebsiteBanner';
$route['websiteBanner/deleteWebsiteBanner/(:any)'] 			= 'Admin/CmsManagementController/deleteWebsiteBanner';

$route['happyStory'] 								= 'Admin/CmsManagementController/happyStory';
$route['happyStory/submitHappyStory'] 				= 'Admin/CmsManagementController/submitHappyStory';
$route['happyStory/changeHappyStoryStatus/(:any)'] 	= 'Admin/CmsManagementController/changeHappyStoryStatus';
$route['happyStory/updateHappyStory/(:any)'] 		= 'Admin/CmsManagementController/updateHappyStory';

$route['socialMedia'] 								= 'Admin/CmsManagementController/socialMedia';
$route['socialMedia/updateSocialMedia'] 			= 'Admin/CmsManagementController/updateSocialMedia';

$route['category'] 									= 'Admin/ServiceController/category';
$route['service/submitCategory'] 					= 'Admin/ServiceController/submitCategory';
$route['service/changeCategoryStatus/(:any)'] 		= 'Admin/ServiceController/changeCategoryStatus';
$route['service/updateCategory/(:any)'] 			= 'Admin/ServiceController/updateCategory';

$route['serviceProvider'] 							= 'Admin/ServiceController/serviceProvider';
$route['service/submitServiceProvider'] 			= 'Admin/ServiceController/submitServiceProvider';
$route['service/changeServiceStatus/(:any)'] 		= 'Admin/ServiceController/changeServiceStatus';
$route['service/updateServiceProvider/(:any)'] 		= 'Admin/ServiceController/updateServiceProvider';

//Setting 
$route['apiKeys'] 									= 'Admin/SettingController/apiKeys';
$route['apiKeys/updateFirebaseKey'] 				= 'Admin/SettingController/updateFirebaseKey';
$route['apiKeys/updateRazorpayKey'] 				= 'Admin/SettingController/updateRazorpayKey';
$route['language'] 									= 'Admin/SettingController/language';
$route['language/submitLanguage'] 					= 'Admin/SettingController/submitLanguage';
$route['language/changeLanguageStatus/(:any)'] 		= 'Admin/SettingController/changeLanguageStatus';
$route['language/updateLanguage/(:any)'] 			= 'Admin/SettingController/updateLanguage';
$route['faqContent'] 								= 'Admin/SettingController/faqContent';
$route['faq/submitfaq'] 							= 'Admin/SettingController/submitfaq';
$route['privacyPolicyContent'] 						= 'Admin/SettingController/privacyPolicyContent';
$route['submitPrivacyPolicy'] 						= 'Admin/SettingController/submitPrivacyPolicy';
$route['termsContent'] 								= 'Admin/SettingController/termsContent';
$route['submitTerms'] 								= 'Admin/SettingController/submitTerms';
$route['webservice'] 								= 'Admin/SettingController/webservice';
$route['emailAlert'] 								= 'Admin/SettingController/emailAlert';

//APIS
//AuthController
$route['api/generateOtp'] 						= 'Api/AuthController/generateOtp';
$route['api/verifyOtp'] 						= 'Api/AuthController/verifyOtp';
$route['api/signup'] 							= 'Api/AuthController/signup';
$route['api/login'] 							= 'Api/AuthController/login';
$route['api/forgotPassword'] 					= 'Api/AuthController/forgotPassword';
$route['api/logOut'] 							= 'Api/AuthController/logOut';
$route['api/changePassword'] 					= 'Api/AuthController/changePassword';
$route['api/userActive'] 						= 'Api/AuthController/userActive';
$route['api/deleteAccount'] 					= 'Api/AuthController/deleteAccount';

//Custom API
$route['api/checkUser'] 						= 'Api/AuthController/checkUser';

$route['api/getCaste'] 							= 'Api/SettingController/getCaste';
$route['api/getSubCasteById'] 				= 'Api/SettingController/getCasteByReligion';
$route['api/getManglik'] 						= 'Api/SettingController/getManglik';
$route['api/getOccupation'] 					= 'Api/SettingController/getOccupation';
$route['api/getIncome'] 						= 'Api/SettingController/getIncome';
$route['api/getLanguage'] 						= 'Api/SettingController/getLanguage';
$route['api/getBloodGroup'] 					= 'Api/SettingController/getBloodGroup';
$route['api/getHeight'] 						= 'Api/SettingController/getHeight';
$route['api/getStates'] 						= 'Api/SettingController/getStates';
$route['api/getDistricts'] 						= 'Api/SettingController/getDistricts';
$route['api/getDistrict'] 						= 'Api/SettingController/getDistrict';
$route['api/getMaritalStatus'] 					= 'Api/SettingController/getMaritalStatus';
$route['api/getPackages'] 						= 'Api/SettingController/getPackages';
$route['api/getNews'] 							= 'Api/SettingController/getNews';

$route['api/getBodyType'] 						= 'Api/SettingController/getBodyType';
$route['api/getComplexion'] 					= 'Api/SettingController/getComplexion';
$route['api/getChallanged'] 					= 'Api/SettingController/getChallanged';
$route['api/getDiet'] 							= 'Api/SettingController/getDiet';
$route['api/getDrinkingSmoking'] 				= 'Api/SettingController/getDrinkingSmoking';
$route['api/getFathersOccupation'] 				= 'Api/SettingController/getFathersOccupation';
$route['api/getMothersOccupation'] 				= 'Api/SettingController/getMothersOccupation';
$route['api/getBrothersSisters'] 				= 'Api/SettingController/getBrothersSisters';
$route['api/getFamilyStatus'] 					= 'Api/SettingController/getFamilyStatus';
$route['api/getFamilyType'] 					= 'Api/SettingController/getFamilyType';
$route['api/getFamilyValues'] 					= 'Api/SettingController/getFamilyValues';
$route['api/getAllSettingValue'] 				= 'Api/SettingController/getAllSettingValue';

//ChatController
$route['api/setMessage'] 						= 'Api/ChatController/setMessage';
$route['api/getMessageHistory'] 				= 'Api/ChatController/getMessageHistory';
$route['api/getMessage'] 						= 'Api/ChatController/getMessage';
$route['api/blockUser'] 						= 'Api/ChatController/blockUser';
$route['api/unblockUser'] 						= 'Api/ChatController/unblockUser';

//UserController
$route['api/userUpdate'] 						= 'Api/UserController/userUpdate';
$route['api/imageUpdate'] 						= 'Api/UserController/imageUpdate';
$route['api/getGallary'] 						= 'Api/UserController/getGallary';
$route['api/matchesUser'] 						= 'Api/UserController/matchesUser';
$route['api/justJoin'] 							= 'Api/UserController/justJoin';
$route['api/myProfile'] 						= 'Api/UserController/myProfile';
$route['api/getGender'] 						= 'Api/UserController/getGender';
$route['api/deleteImage'] 						= 'Api/UserController/deleteImage';
$route['api/setProfileImage'] 					= 'Api/UserController/setProfileImage';

//ShortlistController
$route['api/shortlist'] 						= 'Api/ShortlistController/shortlist';
$route['api/getShortlist'] 						= 'Api/ShortlistController/getShortlist';

$route['api/sendInterest'] 						= 'Api/ShortlistController/sendInterest';
$route['api/getInterest'] 						= 'Api/ShortlistController/getInterest';
$route['api/acceptedInterest'] 					= 'Api/ShortlistController/acceptedInterest';
//SearchController
$route['api/searchUsers'] 						= 'Api/SearchController/searchUsers';
//VisitorsController
$route['api/addVisitors'] 						= 'Api/VisitorsController/addVisitors';
$route['api/getVisitors'] 						= 'Api/VisitorsController/getVisitors';
//SubscribeController
$route['api/subscriptionCheck'] 				= 'Api/SubscribeController/subscriptionCheck';
$route['api/userSubscribe'] 					= 'Api/SubscribeController/userSubscribe';
$route['api/subscribeHistory'] 					= 'Api/SubscribeController/subscribeHistory';
//ServiceController
$route['api/getCategory'] 						= 'Api/ServiceController/getCategory';
$route['api/getServiceByCategory'] 				= 'Api/ServiceController/getServiceByCategory';
$route['api/reportUser'] 						= 'Api/ServiceController/reportUser';

//PageController
$route['api/faq'] 								= 'Api/PageController/faq';
$route['api/faqPage/(:any)'] 					= 'Api/PageController/faqPage';
$route['api/privacyPolicy'] 					= 'Api/PageController/privacyPolicy';
$route['api/privacyPolicyPage/(:any)'] 			= 'Api/PageController/privacyPolicyPage';
$route['api/termsCondition'] 					= 'Api/PageController/termsCondition';
$route['api/termsConditionPage/(:any)'] 		= 'Api/PageController/termsConditionPage';



//HomeController
$route['api/getHomeData'] 						= 'Api/HomeController/getHomeData';

//Front
$route['userLogin'] 							= 'SiteController/userLogin';
$route['signOut'] 								= 'SiteController/signOut';
$route['userSignUp'] 							= 'SiteController/userSignUp';
$route['userResetPassword'] 					= 'SiteController/userResetPassword';
$route['membership'] 							= 'SiteController/membership';
$route['razorPaySuccess'] 						= 'SiteController/razorPaySuccess';
$route['profile'] 								= 'SiteController/profile';
$route['edit-profile'] 							= 'SiteController/edit_profile';
$route['userImageUpdate'] 						= 'SiteController/userImageUpdate';
$route['updateUserRecord'] 						= 'SiteController/updateUserRecord';
$route['shortlist'] 							= 'SiteController/shortlist';
$route['about-profile'] 						= 'SiteController/about_profile';
$route['interest'] 								= 'SiteController/interest';
$route['shortlistAction'] 						= 'SiteController/shortlistAction';
$route['matches'] 								= 'SiteController/matches';
$route['visitor'] 								= 'SiteController/visitor';
$route['newss'] 								= 'SiteController/news';
$route['chat'] 									= 'SiteController/chat';
$route['getChatHistory'] 						= 'SiteController/getChatHistory';
$route['setChatMsg'] 							= 'SiteController/setChatMsg';

// $route['default_controller'] = 'Admin/AuthController/index';
$route['default_controller'] = 'SiteController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
