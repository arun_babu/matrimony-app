<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VisitorsController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function addVisitors()
    {
        foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        } 

        $input = $this->input->post();
        $data = $this->Api_Visitors_model->addVisitors($input,$lan);
    }

    public function getVisitors()
    {
    	foreach (getallheaders() as $name => $value) 
        { 
            if($name == 'Language')
            {
                $lan = $value;
            }
        }
        $input = $this->input->post();
        $data = $this->Api_Visitors_model->getVisitors($input,$lan);
        if (empty($data)) {
            if($lan == 'en')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }else if($lan == 'hi')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_AR;
            }else if($lan == 'mr')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
            }
            $this->Base_model->responseFailed(0, $DATA_NOT_AVAIABLE);
        } //empty($data)
        else {
            $this->Base_model->responseSuccess(1, 'All Data', $data);
        }
    }

}