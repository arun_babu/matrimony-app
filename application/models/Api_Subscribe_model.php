<?php
class Api_Subscribe_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function subscriptionCheck($user_id)
    {
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        $query = $this->db->where('user_id',$user_id)->get('mm_user_subscription')->num_rows();
        if($query > 0)
        {
        	$res = $this->db->where('user_id',$user_id)->order_by('id','desc')->get('mm_subscription_history')->row_array();
	        
    		$pkg = $this->Package_model->updatePackages($res['packages_id']);
    		$date = strtotime($res['created_at']);

        	$userData = array(
                    'id' => $res['id'],
                    'order_id' => $res['order_id'],
                    'txn_id' => $res['txn_id'],
                    'user_id' => $res['user_id'],
                    'subscription_type' => $res['subscription_type'],
                    'price' => $res['price'],
                    'packages_id' => $res['packages_id'],
                    'end_subscription_date' => $res['end_subscription_date'],
                    'created_at' => $res['created_at'],
                    'subscription_title' => $pkg['title'],
                    'subscription_name' => $pkg['description'],
                    'days' => (int)$pkg['no_of_days'],
                    'subscription_start_date' => date('Y-m-d', $date),
                    'subscription_end_date' => $res['end_subscription_date'],
                );
	        	
        	$this->Api_Auth_model->responseSuccess(1, 'You are already subscribed.', $userData);
	        exit();
        }
        else
        {
        	$this->Base_model->responseFailed(0, 'You are not a subscribed user.');
	        exit();
        }
    }

    public function userSubscribe($input,$lan)
    {
    	$this->form_validation->set_rules('order_id', 'order_id', 'required');
		$this->form_validation->set_rules('txn_id', 'txn_id', 'required'); 
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		$this->form_validation->set_rules('price', 'price', 'required');
		$this->form_validation->set_rules('packages_id', 'packages_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        $order_id = $input['order_id'];
        $txn_id = $input['txn_id'];
        $user_id = $input['user_id'];
        $price = $input['price'];
        $packages_id = $input['packages_id'];

        $pkgs = $this->db->where('packages_id',$packages_id)->where('status',1)->get('mm_packages');
        if($pkgs->num_rows() > 0)
        {

        	$pkg = $pkgs->row_array();
	        $subscription_type = $pkg['subscription_type'];
	        if($subscription_type == 0)
	        {
	        	$no_of_days = 1000;
	        }elseif($subscription_type == 1)
	        {
	        	$no_of_days = 30;
	        }elseif($subscription_type == 2)
	        {
	        	$no_of_days = 90;
	        }elseif($subscription_type == 3)
	        {
	        	$no_of_days = 180;
	        }elseif($subscription_type == 4)
	        {
	        	$no_of_days = 365;
	        }

	        $no = $no_of_days+'1';
	        $date = date('Y-m-d', strtotime("+$no days"));

	        $data = array(
	        	'order_id' => $order_id,
	        	'txn_id' => $txn_id,
	        	'user_id' => $user_id,
	        	'price' => $price,
	        	'packages_id' => $packages_id,
	        	'subscription_type' => $subscription_type,
	        	'end_subscription_date' => $date,
	        	'created_at' => date('Y-m-d H:i:s')
	        );
	        $success = $this->db->insert('mm_subscription_history',$data);
	        if($success)
	        {
	        	$subscribe = $this->db->where('user_id',$user_id)->get('mm_user_subscription');
	        	if($subscribe->num_rows() > 0)
	        	{
	        		$userSub = $subscribe->row_array();

	        		if($userSub['end_subscription_date'] >= date('Y-m-d'))
		            {
		                $end_subscription_date = $userSub['end_subscription_date'];
		                $now = time(); // or your date as well
		                $your_date = strtotime("$end_subscription_date");
		                $datediff =  $your_date - $now;

		                $day = round($datediff / (60 * 60 * 24));
		                $num = $day+$no;
		                $dates = date('Y-m-d', strtotime("+$num days"));
		                // echo $dates; die;
		            }
		            else
		            {
		                $no = $no_of_days+'1';
		                $dates = date('Y-m-d', strtotime("+$no days"));
		            }

		            $data1 = array(
			            'end_subscription_date' => $dates
			        );
			        $this->db->where('user_id',$user_id)->update('mm_user_subscription',$data1);
			        
		        	$user = $this->db->where('user_id',$user_id)->get('mm_users')->row_array();
		        	$email = $user['email'];
		            $name = $user['name'];
		            $subject = SUBSCRIPTION_HEAD;
		            $msg = "Hello $name this is your subscription confirmation message, Your subscription package is $no_of_days days it cost you $price Rs. and it will be available till $dates, thank you.";
		            $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
			        
	        	}
	        	else
	        	{
	        		$no = $no_of_days+'1';
	            	$dates = date('Y-m-d', strtotime("+$no days"));

	            	$insData = array(
	            		'user_id' => $user_id,
	            		'subscription_type' => $subscription_type,
	            		'start_subscription_date' => date('Y-m-d'),
	            		'end_subscription_date' => $dates,
	            		'created_at' => date('Y-m-d H:i:s')
	            	);
	            	$this->db->insert('mm_user_subscription',$insData);

	            	$user = $this->db->where('user_id',$user_id)->get('mm_users')->row_array();
		        	$email = $user['email'];
		            $name = $user['name'];
		            $subject = SUBSCRIPTION_HEAD;
		            $msg = "Hello $name this is your subscription confirmation message, Your subscription package is $no_of_days days it cost you $price Rs. and it will be available till $dates, thank you.";
		            $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
	        	}

	        	if($lan == 'en')
	            {
	                $DATA_SAVE = DATA_SAVE;
	            }else if($lan == 'ar')
	            {
	                $DATA_SAVE = DATA_SAVE_HI;
	            }else if($lan == 'mr')
	            {
	                $DATA_SAVE = DATA_SAVE_MR;
	            }
	            $this->Base_model->responseSuccessWethoutData(1, $DATA_SAVE);
	            exit();
	        }
	        else{
	        	if($lan == 'en')
	            {
	                $SOMTHING = SOMTHING;
	            }else if($lan == 'ar')
	            {
	                $SOMTHING = SOMTHING_HI;
	            }else if($lan == 'mr')
	            {
	                $SOMTHING = SOMTHING_MR;
	            }
	        	$this->Base_model->responseFailed(0, $SOMTHING);
	            exit();
	        }
	    }
	    else
	    {
        	if($lan == 'en')
            {
                $SOMTHING = SOMTHING;
            }else if($lan == 'ar')
            {
                $SOMTHING = SOMTHING_HI;
            }else if($lan == 'mr')
            {
                $SOMTHING = SOMTHING_MR;
            }
        	$this->Base_model->responseFailed(0, $SOMTHING);
            exit();
        }

    }


    public function subscribeHistory($input,$lan)
    {
    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Base_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        $userExist = $this->db->where('user_id',$input['user_id'])->get('mm_subscription_history');
        if($userExist->num_rows() > 0)
        {
        	$history = $userExist->result();
        	$historys = array();
        	foreach($history as $history)
        	{
        		$pkg = $this->Package_model->updatePackages($history->packages_id);
        		$history->subscription_title = $pkg['title'];
        		$history->subscription_name = $pkg['description'];
        		$history->days = (int)$pkg['no_of_days'];
        		$date = strtotime($history->created_at);
        		$history->subscription_start_date = date('Y-m-d', $date);
        		$history->subscription_end_date = $history->end_subscription_date;
        		array_push($historys, $history);
        	}
        	
        	$this->Api_Auth_model->responseSuccess(1, 'list', $historys);
        }
        else
        {
        	if($lan == 'en')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE;
            }else if($lan == 'ar')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_HI;
            }else if($lan == 'mr')
            {
                $DATA_NOT_AVAIABLE = DATA_NOT_AVAIABLE_MR;
            }
        	$this->Base_model->responseFailed(0, $DATA_NOT_AVAIABLE);
            exit();
        }
    }

}