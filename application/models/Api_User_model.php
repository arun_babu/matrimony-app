<?php

class Api_User_model extends CI_Model
{

	function __construct()
	{
		parent:: __construct();
		$this->load->library('aws3');

	}

	public function userUpdate($input, $lan)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		if ($this->form_validation->run() == false) {
			$this->Base_model->responseFailed(0, 'Please fill user id field');
			exit();
		}

		$user_id = $input['user_id'];
		$user = $this->db->where('user_id', $user_id)->where('status', '1')->get('mm_users');
		if ($user->num_rows() > 0) {
			$u = $user->row_array();
			if ($input['name'] == '') {
				$name = $u['name'];
			} else {
				$name = $input['name'];
			}
			if ($input['email'] == '') {
				$email = $u['email'];
			} else {
				$email = $input['email'];
			}
			if ($input['mobile'] == '') {
				$mobile = $u['mobile'];
			} else {
				$mobile = $input['mobile'];
			}
			if ($input['gender'] == '') {
				$gender = $u['gender'];
			} else {
				$gender = $input['gender'];
			}
			if ($input['profile_for'] == '') {
				$profile_for = $u['profile_for'];
			} else {
				$profile_for = $input['profile_for'];
			}
			if ($input['area_of_interest'] == '') {
				$area_of_interest = $u['area_of_interest'];
			} else {
				$area_of_interest = $input['area_of_interest'];
			}
			if ($input['blood_group'] == '') {
				$blood_group = $u['blood_group'];
			} else {
				$blood_group = $input['blood_group'];
			}
			if ($input['caste'] == '') {
				$caste = $u['caste'];
			} else {
				$caste = $input['caste'];
			}
			if ($input['sub_caste'] == '') {
				$sub_caste = $u['sub_caste'];
			} else {
				$sub_caste = $input['sub_caste'];
			}
			if ($input['complexion'] == '') {
				$complexion = $u['complexion'];
			} else {
				$complexion = $input['complexion'];
			}
			if ($input['body_type'] == '') {
				$body_type = $u['body_type'];
			} else {
				$body_type = $input['body_type'];
			}
			if ($input['weight'] == '') {
				$weight = $u['weight'];
			} else {
				$weight = $input['weight'];
			}
			if ($input['about_me'] == '') {
				$about_me = $u['about_me'];
			} else {
				$about_me = $input['about_me'];
			}
			if ($input['height'] == '') {
				$height = $u['height'];
			} else {
				$height = $input['height'];
			}
			if ($input['qualification'] == '') {
				$qualification = $u['qualification'];
			} else {
				$qualification = $input['qualification'];
			}
			if ($input['occupation'] == '') {
				$occupation = $u['occupation'];
			} else {
				$occupation = $input['occupation'];
			}
			if ($input['income'] == '') {
				$income = $u['income'];
			} else {
				$income = $input['income'];
			}
			if ($input['work_place'] == '') {
				$work_place = $u['work_place'];
			} else {
				$work_place = $input['work_place'];
			}
			if ($input['organisation_name'] == '') {
				$organisation_name = $u['organisation_name'];
			} else {
				$organisation_name = $input['organisation_name'];
			}
			if ($input['city'] == '') {
				$city = $u['city'];
			} else {
				$city = $input['city'];
			}
			if ($input['state'] == '') {
				$state = $u['state'];
			} else {
				$state = $input['state'];
			}
			if ($input['district'] == '') {
				$district = $u['district'];
			} else {
				$district = $input['district'];
			}
			if ($input['current_address'] == '') {
				$current_address = $u['current_address'];
			} else {
				$current_address = $input['current_address'];
			}
			if ($input['pin'] == '') {
				$pin = $u['pin'];
			} else {
				$pin = $input['pin'];
			}
			if ($input['whatsapp_no'] == '') {
				$whatsapp_no = $u['whatsapp_no'];
			} else {
				$whatsapp_no = $input['whatsapp_no'];
			}
			if ($input['challenged'] == '') {
				$challenged = $u['challenged'];
			} else {
				$challenged = $input['challenged'];
			}
			if ($input['permanent_address'] == '') {
				$permanent_address = $u['permanent_address'];
			} else {
				$permanent_address = $input['permanent_address'];
			}
			if ($input['marital_status'] == '') {
				$marital_status = $u['marital_status'];
			} else {
				$marital_status = $input['marital_status'];
			}
			if ($input['aadhaar'] == '') {
				$aadhaar = $u['aadhaar'];
			} else {
				$aadhaar = $input['aadhaar'];
			}
			if ($input['dob'] == '') {
				$dob = $u['dob'];
			} else {
				$dob = $input['dob'];
			}
			if ($input['birth_place'] == '') {
				$birth_place = $u['birth_place'];
			} else {
				$birth_place = $input['birth_place'];
			}
			if ($input['birth_time'] == '') {
				$birth_time = $u['birth_time'];
			} else {
				$birth_time = $input['birth_time'];
			}
			if ($input['manglik'] == '') {
				$manglik = $u['manglik'];
			} else {
				$manglik = $input['manglik'];
			}
			if ($input['gotra'] == '') {
				$gotra = $u['gotra'];
			} else {
				$gotra = $input['gotra'];
			}
			if ($input['gotra_nanihal'] == '') {
				$gotra_nanihal = $u['gotra_nanihal'];
			} else {
				$gotra_nanihal = $input['gotra_nanihal'];
			}
			if ($input['family_address'] == '') {
				$family_address = $u['family_address'];
			} else {
				$family_address = $input['family_address'];
			}
			if ($input['father_name'] == '') {
				$father_name = $u['father_name'];
			} else {
				$father_name = $input['father_name'];
			}
			if ($input['father_occupation'] == '') {
				$father_occupation = $u['father_occupation'];
			} else {
				$father_occupation = $input['father_occupation'];
			}
			if ($input['family_status'] == '') {
				$family_status = $u['family_status'];
			} else {
				$family_status = $input['family_status'];
			}
			if ($input['family_district'] == '') {
				$family_district = $u['family_district'];
			} else {
				$family_district = $input['family_district'];
			}
			if ($input['mother_name'] == '') {
				$mother_name = $u['mother_name'];
			} else {
				$mother_name = $input['mother_name'];
			}
			if ($input['grand_father_name'] == '') {
				$grand_father_name = $u['grand_father_name'];
			} else {
				$grand_father_name = $input['grand_father_name'];
			}
			if ($input['family_state'] == '') {
				$family_state = $u['family_state'];
			} else {
				$family_state = $input['family_state'];
			}
			if ($input['family_pin'] == '') {
				$family_pin = $u['family_pin'];
			} else {
				$family_pin = $input['family_pin'];
			}
			if ($input['mother_occupation'] == '') {
				$mother_occupation = $u['mother_occupation'];
			} else {
				$mother_occupation = $input['mother_occupation'];
			}
			if ($input['family_type'] == '') {
				$family_type = $u['family_type'];
			} else {
				$family_type = $input['family_type'];
			}
			if ($input['family_income'] == '') {
				$family_income = $u['family_income'];
			} else {
				$family_income = $input['family_income'];
			}
			if ($input['family_city'] == '') {
				$family_city = $u['family_city'];
			} else {
				$family_city = $input['family_city'];
			}
			if ($input['maternal_grand_father_name_address'] == '') {
				$maternal_grand_father_name_address = $u['maternal_grand_father_name_address'];
			} else {
				$maternal_grand_father_name_address = $input['maternal_grand_father_name_address'];
			}
			if ($input['family_value'] == '') {
				$family_value = $u['family_value'];
			} else {
				$family_value = $input['family_value'];
			}
			if ($input['mobile2'] == '') {
				$mobile2 = $u['mobile2'];
			} else {
				$mobile2 = $input['mobile2'];
			}
			if ($input['brother'] == '') {
				$brother = $u['brother'];
			} else {
				$brother = $input['brother'];
			}
			if ($input['sister'] == '') {
				$sister = $u['sister'];
			} else {
				$sister = $input['sister'];
			}
			if ($input['drinking'] == '') {
				$drinking = $u['drinking'];
			} else {
				$drinking = $input['drinking'];
			}
			if ($input['dietary'] == '') {
				$dietary = $u['dietary'];
			} else {
				$dietary = $input['dietary'];
			}
			if ($input['hobbies'] == '') {
				$hobbies = $u['hobbies'];
			} else {
				$hobbies = $input['hobbies'];
			}
			if ($input['smoking'] == '') {
				$smoking = $u['smoking'];
			} else {
				$smoking = $input['smoking'];
			}
			if ($input['interests'] == '') {
				$interests = $u['interests'];
			} else {
				$interests = $input['interests'];
			}
			if ($input['working'] == '') {
				$working = $u['working'];
			} else {
				$working = $input['working'];
			}
			if ($input['language'] == '') {
				$language = $u['language'];
			} else {
				$language = $input['language'];
			}

			$data = array(
				'name' => $name,
				'email' => $email,
				'mobile' => $mobile,
				'gender' => $gender,
				'profile_for' => $profile_for,
				'area_of_interest' => $area_of_interest,
				'blood_group' => $blood_group,
				'caste' => $caste,
				'sub_caste' => $sub_caste,
				'complexion' => $complexion,
				'body_type' => $body_type,
				'weight' => $weight,
				'about_me' => $about_me,
				'height' => $height,
				'qualification' => $qualification,
				'occupation' => $occupation,
				'income' => $income,
				'work_place' => $work_place,
				'organisation_name' => $organisation_name,
				'city' => $city,
				'state' => $state,
				'district' => $district,
				'current_address' => $current_address,
				'pin' => $pin,
				'whatsapp_no' => $whatsapp_no,
				'challenged' => $challenged,
				'permanent_address' => $permanent_address,
				'marital_status' => $marital_status,
				'aadhaar' => $aadhaar,
				'dob' => $dob,
				'birth_place' => $birth_place,
				'birth_time' => $birth_time,
				'manglik' => $manglik,
				'gotra' => $gotra,
				'gotra_nanihal' => $gotra_nanihal,
				'family_address' => $family_address,
				'father_name' => $father_name,
				'father_occupation' => $father_occupation,
				'family_status' => $family_status,
				'family_district' => $family_district,
				'mother_name' => $mother_name,
				'grand_father_name' => $grand_father_name,
				'family_state' => $family_state,
				'family_pin' => $family_pin,
				'mother_occupation' => $mother_occupation,
				'family_type' => $family_type,
				'family_income' => $family_income,
				'family_city' => $family_city,
				'maternal_grand_father_name_address' => $maternal_grand_father_name_address,
				'family_value' => $family_value,
				'mobile2' => $mobile2,
				'brother' => $brother,
				'sister' => $sister,
				'drinking' => $drinking,
				'dietary' => $dietary,
				'hobbies' => $hobbies,
				'smoking' => $smoking,
				'interests' => $interests,
				'working' => $working,
				'language' => $language,
				'updated_at' => date('Y-m-d H:i:s')
			);
			$this->db->where('user_id', $user_id);
			$q = $this->db->update('mm_users', $data);
			if ($q) {

				$res = $this->db->where('user_id', $user_id)->get('mm_users')->row_array();
				$user_img = $this->Base_model->getAllDataWhere(array(
					'user_id' => $res['user_id'],
				), 'mm_user_image');
				$user_imgs = array();
				foreach ($user_img as $user_img) {

//					$user_img->image = $this->config->base_url() . 'assets/images/user/' . $user_img->image;
					$user_img->image = $user_img->image;

					array_push($user_imgs, $user_img);
				}

				$user_imgs = $user_imgs;

				$userData = array(
					's_no' => $res['s_no'],
					'user_id' => $res['user_id'],
					'name' => $res['name'],
					'email' => $res['email'],
					'mobile' => $res['mobile'],
					'gender' => $this->User_model->getGenderById($res['gender']),
					'profile_for' => $res['profile_for'],
					'area_of_interest' => $res['area_of_interest'],
					'blood_group' => $this->User_model->getBloodGroupById($res['blood_group']),
					'caste' => $this->User_model->getCasteById($res['caste']),
					'sub_caste' => $this->User_model->getSubCasteById($res['sub_caste']),
					'complexion' => $res['complexion'],
					'body_type' => $res['body_type'],
					'weight' => $res['weight'],
					'about_me' => $res['about_me'],
					'height' => $this->User_model->getHeightById($res['height']),
					'qualification' => $res['qualification'],
					'occupation' => $this->User_model->getOccupation($res['occupation']),
					'income' => $this->User_model->getIncomeById($res['income']),
					'work_place' => $res['work_place'],
					'organisation_name' => $res['organisation_name'],
					'city' => $res['city'],
					'state' => $this->User_model->getStatesById($res['state']),
					'district' => $this->User_model->getDistrictsById($res['district']),
					'current_address' => $res['current_address'],
					'pin' => $res['pin'],
					'whatsapp_no' => $res['whatsapp_no'],
					'challenged' => $res['challenged'],
					'permanent_address' => $res['permanent_address'],
					'marital_status' => $this->User_model->getMaritalStatusById($res['marital_status']),
					'aadhaar' => $res['aadhaar'],
					'dob' => $res['dob'],
					'birth_place' => $res['birth_place'],
					'birth_time' => $res['birth_time'],
					'manglik' => $this->User_model->getManglikById($res['manglik']),
					'gotra' => $res['gotra'],
					'gotra_nanihal' => $res['gotra_nanihal'],
					'family_address' => $res['family_address'],
					'father_name' => $res['father_name'],
					'father_occupation' => $res['father_occupation'],
					'family_status' => $res['family_status'],
					'family_district' => $res['family_district'],
					'mother_name' => $res['mother_name'],
					'grand_father_name' => $res['grand_father_name'],
					'family_state' => $res['family_state'],
					'family_pin' => $res['family_pin'],
					'mother_occupation' => $res['mother_occupation'],
					'family_type' => $res['family_type'],
					'family_income' => $res['family_income'],
					'family_city' => $res['family_city'],
					'maternal_grand_father_name_address' => $res['maternal_grand_father_name_address'],
					'family_value' => $res['family_value'],
					'mobile2' => $res['mobile2'],
					'brother' => $res['brother'],
					'sister' => $res['sister'],
					'drinking' => $res['drinking'],
					'dietary' => $res['dietary'],
					'hobbies' => $res['hobbies'],
					'smoking' => $res['smoking'],
					'language' => $res['language'],
					'interests' => $res['interests'],
					'working' => $res['working'],
					'shortlisted' => (int)$res['shortlisted'],
					'status' => $res['status'],
					'critical' => $res['critical'],
					'device_type' => $res['device_type'],
					'device_token' => $res['device_token'],
					'end_subscription_date' => $res['end_subscription_date'],
//					'user_avtar' => $this->config->base_url() . 'assets/images/user/' . $res['user_avtar'],
					'user_avtar' => $res['user_avtar'],
					'created_at' => $res['created_at'],
					'updated_at' => $res['updated_at'],
					'user_imgs' => $user_imgs
				);
				return $userData;
			}
		} else {
			if ($lan == 'en') {
				$USER_NOT_FOUND = USER_NOT_FOUND;
			} else if ($lan == 'hi') {
				$USER_NOT_FOUND = USER_NOT_FOUND_HI;
			} else if ($lan == 'mr') {
				$USER_NOT_FOUND = USER_NOT_FOUND_MR;
			}
			$this->Base_model->responseFailed(0, $USER_NOT_FOUND);
			exit();
		}

	}

	public function matchesUser($user_id)
	{
		$userExist = $this->db->where('user_id', $user_id)->where('status', '1')->get('mm_users');
		if ($userExist->num_rows() > 0) {
			$user1 = $userExist->row_array();
			if ($user1['gender'] == '1') {
				//Femail Data Show
				$manglik = $user1['manglik'];
				$user = $this->db->query("SELECT * FROM mm_users where user_id NOT IN (select report_user_id from mm_report_user where user_id = '$user_id') AND manglik = '$manglik' AND gender = '2' AND status = '1' ")->result();
				// $this->db->where('manglik',$user1['manglik']);
				// $this->db->where('gender','2');
				// $this->db->where('status','1');
				// $user = $this->db->get('mm_users')->result();
				$users = array();
				foreach ($user as $user) {
					$user->gender = $this->User_model->getGenderById($user->gender);
					$blood_group = $this->User_model->getBloodGroupById($user->blood_group);
					$user->blood_group = $blood_group;
					$caste = $this->User_model->getCasteById($user->caste);
					$user->caste = $caste;
					$height = $this->User_model->getHeightById($user->height);
					$user->height = $height;
					$occupation = $this->User_model->getOccupation($user->occupation);
					$user->occupation = $occupation;
					$income = $this->User_model->getIncomeById($user->income);
					$user->income = $income;
					$state = $this->User_model->getStatesById($user->state);
					$user->state = $state;
					$district = $this->User_model->getDistrictsById($user->district);
					$user->district = $district;
					$marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
					$user->marital_status = $marital_status;
					$manglik = $this->User_model->getManglikById($user->manglik);
					$user->manglik = $manglik;
					$request = $this->getRequested($user_id, $user->user_id);
					$user->request = $request;
					$shortlisted = $this->getShortlisted($user_id, $user->user_id);
					$user->shortlisted = (int)$shortlisted;
					$user->user_avtar = $user->user_avtar;
					$user_img = $this->Base_model->getAllDataWhere(array(
						'user_id' => $user->user_id,
					), 'mm_user_image');
					$user_imgs = array();
					foreach ($user_img as $user_img) {

//						$user_img->image = $this->config->base_url() . 'assets/images/user/' . $user_img->image;
						$user_img->image = $user_img->image;

						array_push($user_imgs, $user_img);
					}

					$user->user_imgs = $user_imgs;
					array_push($users, $user);
				}
				return $users;
			} else if ($user1['gender'] == '2') {
				//Male Data Show
				$manglik = $user1['manglik'];
				$user = $this->db->query("SELECT * FROM mm_users where user_id NOT IN (select report_user_id from mm_report_user where user_id = '$user_id') AND manglik = '$manglik' AND gender = '1' AND status = '1' ")->result();
				// $this->db->where('manglik',$user1['manglik']);
				// $this->db->where('gender','1');
				// $this->db->where('status','1');
				// $user = $this->db->get('mm_users')->result();
				$users = array();
				foreach ($user as $user) {

					$user->gender = $this->User_model->getGenderById($user->gender);
					$blood_group = $this->User_model->getBloodGroupById($user->blood_group);
					$user->blood_group = $blood_group;
					$caste = $this->User_model->getCasteById($user->caste);
					$user->caste = $caste;
					$height = $this->User_model->getHeightById($user->height);
					$user->height = $height;
					$occupation = $this->User_model->getOccupation($user->occupation);
					$user->occupation = $occupation;
					$income = $this->User_model->getIncomeById($user->income);
					$user->income = $income;
					$state = $this->User_model->getStatesById($user->state);
					$user->state = $state;
					$district = $this->User_model->getDistrictsById($user->district);
					$user->district = $district;
					$marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
					$user->marital_status = $marital_status;
					$manglik = $this->User_model->getManglikById($user->manglik);
					$user->manglik = $manglik;
					$request = $this->getRequested($user_id, $user->user_id);
					$user->request = $request;
					$shortlisted = $this->getShortlisted($user_id, $user->user_id);
					$user->shortlisted = (int)$shortlisted;
					$user->user_avtar = $user->user_avtar;
					$user_img = $this->Base_model->getAllDataWhere(array(
						'user_id' => $user->user_id,
					), 'mm_user_image');
					$user_imgs = array();
					foreach ($user_img as $user_img) {

//						$user_img->image = $this->config->base_url() . 'assets/images/user/' . $user_img->image;
						$user_img->image = $user_img->image;

						array_push($user_imgs, $user_img);
					}

					$user->user_imgs = $user_imgs;
					array_push($users, $user);
				}
				return $users;
			} else {
				return 1;
			}
		} else {
			return 0;
		}

	}

	public function getGallary($user_id)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		if ($this->form_validation->run() == false) {
			$this->Base_model->responseFailed(0, 'Please fill user id field');
			exit();
		}

		$userExist = $this->db->where('user_id', $user_id)->where('status', '1')->get('mm_users');
		if ($userExist->num_rows() > 0) {
			$res = $this->db->select('*,image')->where('user_id', $user_id)->get('mm_user_image')->result();
			return $res;
		} else {
			$this->Base_model->responseFailed(2, DEACTIVE_NEW);
		}

	}

	public function myProfile($user_id)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		if ($this->form_validation->run() == false) {
			$this->Base_model->responseFailed(0, 'Please fill user id field');
			exit();
		}
		$userExist = $this->db->where('user_id', $user_id)->where('status', '1')->get('mm_users');
		if ($userExist->num_rows() > 0) {
			$res = $this->db->where('user_id', $user_id)->get('mm_users')->row_array();
			$user_img = $this->Base_model->getAllDataWhere(array(
				'user_id' => $res['user_id'],
			), 'mm_user_image');
			$user_imgs = array();
			foreach ($user_img as $user_im) {

//				$user_img->image = $this->config->base_url() . 'assets/images/user/' . $user_img->image;
//				$user_im->image = $user_im;

//				array_push($user_imgs, $user_im->image);
			}

//			$user_imgs = $user_imgs;
//			$user_imgs = ['http://13.126.96.200/front_assets/images/logoFront.png'];

			$userData = array(
				's_no' => $res['s_no'],
				'user_id' => $res['user_id'],
				'name' => $res['name'],
				'email' => $res['email'],
				'mobile' => $res['mobile'],
				'gender' => $this->User_model->getGenderById($res['gender']),
				'profile_for' => $res['profile_for'],
				'area_of_interest' => $res['area_of_interest'],
				'blood_group' => $this->User_model->getBloodGroupById($res['blood_group']),
				'caste' => $this->User_model->getCasteById($res['caste']),
				'sub_caste' => $this->User_model->getSubCasteById($res['sub_caste']) ? $this->User_model->getSubCasteById($res['sub_caste']) : "",
				'complexion' => $res['complexion'],
				'body_type' => $res['body_type'],
				'weight' => $res['weight'],
				'about_me' => $res['about_me'],
				'height' => $this->User_model->getHeightById($res['height']),
				'qualification' => $res['qualification'],
				'occupation' => $this->User_model->getOccupation($res['occupation']),
				'income' => $this->User_model->getIncomeById($res['income']),
				'work_place' => $res['work_place'],
				'organisation_name' => $res['organisation_name'],
				'city' => $res['city'],
				'state' => $this->User_model->getStatesById($res['state']),
				'district' => $this->User_model->getDistrictsById($res['district']),
				'current_address' => $res['current_address'],
				'pin' => $res['pin'],
				'whatsapp_no' => $res['whatsapp_no'],
				'challenged' => $res['challenged'],
				'permanent_address' => $res['permanent_address'],
				'marital_status' => $this->User_model->getMaritalStatusById($res['marital_status']),
				'aadhaar' => $res['aadhaar'],
				'dob' => $res['dob'],
				'birth_place' => $res['birth_place'],
				'birth_time' => $res['birth_time'],
				'manglik' => $this->User_model->getManglikById($res['manglik']),
				'gotra' => $res['gotra'],
				'gotra_nanihal' => $res['gotra_nanihal'],
				'family_address' => $res['family_address'],
				'father_name' => $res['father_name'],
				'father_occupation' => $res['father_occupation'],
				'family_status' => $res['family_status'],
				'family_district' => $res['family_district'],
				'mother_name' => $res['mother_name'],
				'grand_father_name' => $res['grand_father_name'],
				'family_state' => $res['family_state'],
				'family_pin' => $res['family_pin'],
				'mother_occupation' => $res['mother_occupation'],
				'family_type' => $res['family_type'],
				'family_income' => $res['family_income'],
				'family_city' => $res['family_city'],
				'maternal_grand_father_name_address' => $res['maternal_grand_father_name_address'],
				'family_value' => $res['family_value'],
				'mobile2' => $res['mobile2'],
				'brother' => $res['brother'],
				'sister' => $res['sister'],
				'drinking' => $res['drinking'],
				'dietary' => $res['dietary'],
				'hobbies' => $res['hobbies'],
				'smoking' => $res['smoking'],
				'language' => $res['language'],
				'interests' => $res['interests'],
				'working' => $res['working'],
				'shortlisted' => (int)$res['shortlisted'],
				'status' => $res['status'],
				'critical' => $res['critical'],
				'device_type' => $res['device_type'],
				'device_token' => $res['device_token'],
				'end_subscription_date' => $res['end_subscription_date'],
//				'user_avtar' => $this->config->base_url() . 'assets/images/user/' . $res['user_avtar'],
				'user_avtar' => $res['user_avtar'],
				'created_at' => $res['created_at'],
				'updated_at' => $res['updated_at'],
				'user_imgs' => $user_img
			);
			return $userData;
		} else {
			$this->Base_model->responseFailed(2, DEACTIVE_NEW);
		}
	}

	public function justJoin($user_id, $gender)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		// $this->form_validation->set_rules('gender', 'gender', 'required');
		if ($this->form_validation->run() == false) {
			$this->Base_model->responseFailed(0, 'Please fill user_id field');
			exit();
		}

		$userExist = $this->db->where('user_id', $user_id)->where('status', '1')->get('mm_users');
		if ($userExist->num_rows() > 0) {
			if ($gender != '') {
				$gender = $gender == 1 ? 2 : 1;
				$user = $this->db->query("SELECT * FROM mm_users where user_id NOT IN (select report_user_id from mm_report_user where user_id = '$user_id') AND user_id != '$user_id' AND gender = '$gender' AND status = '1' ")->result();

				// $this->db->where('user_id !=',$user_id);
				// $this->db->where('gender',$gender);
				// $this->db->where('status','1');
				// $user = $this->db->get('mm_users')->result();
				$users = array();
				foreach ($user as $user) {

					$user->dietary = $this->User_model->getDietaryById($user->dietary);
					$user->drinking = $this->User_model->getDrinkingById($user->drinking);
					$user->smoking = $this->User_model->getDrinkingById($user->smoking);
					$user->sub_caste = $this->User_model->getSubCasteById($user->sub_caste);
					$user->gender = $this->User_model->getGenderById($user->gender);
					$blood_group = $this->User_model->getBloodGroupById($user->blood_group);
					$user->blood_group = $blood_group;
					$caste = $this->User_model->getCasteById($user->caste);
					$user->caste = $caste;
					$height = $this->User_model->getHeightById($user->height);
					$user->height = $height;
					$occupation = $this->User_model->getOccupation($user->occupation);
					$user->occupation = $occupation;
					$income = $this->User_model->getIncomeById($user->income);
					$user->income = $income;
					$state = $this->User_model->getStatesById($user->state);
					$user->state = $state;
					$district = $this->User_model->getDistrictsById($user->district);
					$user->district = $district;
					$marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
					$user->marital_status = $marital_status;
					$manglik = $this->User_model->getManglikById($user->manglik);
					$user->manglik = $manglik;
					$request = $this->getRequested($user_id, $user->user_id);
					$user->request = $request;
					$shortlisted = $this->getShortlisted($user_id, $user->user_id);
					$user->shortlisted = (int)$shortlisted;
					$user->user_avtar = $user->user_avtar;
					$user_img = $this->Base_model->getAllDataWhere(array(
						'user_id' => $user->user_id,
					), 'mm_user_image');
					$user_imgs = array();
					foreach ($user_img as $user_img) {

//						$user_img->image = $this->config->base_url() . 'assets/images/user/' . $user_img->image;
						$user_img->image = $user_img->image;

						array_push($user_imgs, $user_img);
					}

					$user->user_imgs = $user_imgs;
					array_push($users, $user);
				}
				$users = $users;

				$percentage = 0;
				$query = $this->db->where('user_id', $user_id)->get('mm_users');
				if ($query->num_rows() > 0) {
					$notEmpty = 0;
					$totalField = 65;
					foreach ($query->result() as $row) {
						$notEmpty += ($row->name != '') ? 1 : 0;
						$notEmpty += ($row->email != '') ? 1 : 0;
						$notEmpty += ($row->country_code != '') ? 1 : 0;
						$notEmpty += ($row->mobile != '') ? 1 : 0;
						$notEmpty += ($row->gender != '') ? 1 : 0;
						$notEmpty += ($row->profile_for != '') ? 1 : 0;
						$notEmpty += ($row->password != '') ? 1 : 0;
						$notEmpty += ($row->area_of_interest != '') ? 1 : 0;
						$notEmpty += ($row->blood_group != '') ? 1 : 0;
						$notEmpty += ($row->caste != '') ? 1 : 0;
						$notEmpty += ($row->complexion != '') ? 1 : 0;
						$notEmpty += ($row->body_type != '') ? 1 : 0;
						$notEmpty += ($row->weight != '') ? 1 : 0;
						$notEmpty += ($row->about_me != '') ? 1 : 0;
						$notEmpty += ($row->height != '') ? 1 : 0;
						$notEmpty += ($row->qualification != '') ? 1 : 0;
						$notEmpty += ($row->occupation != '') ? 1 : 0;
						$notEmpty += ($row->income != '') ? 1 : 0;
						$notEmpty += ($row->work_place != '') ? 1 : 0;
						$notEmpty += ($row->organisation_name != '') ? 1 : 0;
						$notEmpty += ($row->city != '') ? 1 : 0;
						$notEmpty += ($row->state != '') ? 1 : 0;
						$notEmpty += ($row->district != '') ? 1 : 0;
						$notEmpty += ($row->current_address != '') ? 1 : 0;
						$notEmpty += ($row->pin != '') ? 1 : 0;
						$notEmpty += ($row->whatsapp_no != '') ? 1 : 0;
						$notEmpty += ($row->challenged != '') ? 1 : 0;
						$notEmpty += ($row->permanent_address != '') ? 1 : 0;
						$notEmpty += ($row->marital_status != '') ? 1 : 0;
						$notEmpty += ($row->aadhaar != '') ? 1 : 0;
						$notEmpty += ($row->dob != '') ? 1 : 0;
						$notEmpty += ($row->birth_place != '') ? 1 : 0;
						$notEmpty += ($row->birth_time != '') ? 1 : 0;
						$notEmpty += ($row->manglik != '') ? 1 : 0;
						$notEmpty += ($row->gotra != '') ? 1 : 0;
						$notEmpty += ($row->gotra_nanihal != '') ? 1 : 0;
						$notEmpty += ($row->family_address != '') ? 1 : 0;
						$notEmpty += ($row->father_name != '') ? 1 : 0;
						$notEmpty += ($row->father_occupation != '') ? 1 : 0;
						$notEmpty += ($row->family_status != '') ? 1 : 0;
						$notEmpty += ($row->family_district != '') ? 1 : 0;
						$notEmpty += ($row->mother_name != '') ? 1 : 0;
						$notEmpty += ($row->grand_father_name != '') ? 1 : 0;
						$notEmpty += ($row->family_state != '') ? 1 : 0;
						$notEmpty += ($row->family_pin != '') ? 1 : 0;
						$notEmpty += ($row->mother_occupation != '') ? 1 : 0;
						$notEmpty += ($row->family_type != '') ? 1 : 0;
						$notEmpty += ($row->family_income != '') ? 1 : 0;
						$notEmpty += ($row->family_city != '') ? 1 : 0;
						$notEmpty += ($row->maternal_grand_father_name_address != '') ? 1 : 0;
						$notEmpty += ($row->family_value != '') ? 1 : 0;
						$notEmpty += ($row->mobile2 != '') ? 1 : 0;
						$notEmpty += ($row->brother != '') ? 1 : 0;
						$notEmpty += ($row->sister != '') ? 1 : 0;
						$notEmpty += ($row->drinking != '') ? 1 : 0;
						$notEmpty += ($row->dietary != '') ? 1 : 0;
						$notEmpty += ($row->hobbies != '') ? 1 : 0;
						$notEmpty += ($row->smoking != '') ? 1 : 0;
						$notEmpty += ($row->language != '') ? 1 : 0;
						$notEmpty += ($row->interests != '') ? 1 : 0;
						$notEmpty += ($row->working != '') ? 1 : 0;
						$notEmpty += ($row->shortlisted != '') ? 1 : 0;
						$notEmpty += ($row->critical != '') ? 1 : 0;
						$notEmpty += ($row->user_avtar != '') ? 1 : 0;
						$notEmpty += ($row->end_subscription_date != '') ? 1 : 0;
						//do with all field
					}
					$percentage = $notEmpty / $totalField * 100;
				}
				$per = $percentage;

				$this->Base_model->responseSuccessPercentage(1, 'Just Join', $users, $per);
				exit();
			} else {
				// $this->db->where('user_id !=',$user_id);
				// $this->db->where('status','1');
				// $user = $this->db->get('mm_users')->result();
				$user = $this->db->query("SELECT * FROM mm_users where user_id NOT IN (select report_user_id from mm_report_user where user_id = '$user_id') AND user_id != '$user_id' AND status = '1' ")->result();
				$users = array();
				foreach ($user as $user) {
					$user->dietary = $this->User_model->getDietaryById($user->dietary);
					$user->drinking = $this->User_model->getDrinkingById($user->drinking);
					$user->smoking = $this->User_model->getDrinkingById($user->smoking);
					$user->sub_caste = $this->User_model->getSubCasteById($user->sub_caste);

					$user->gender = $this->User_model->getGenderById($user->gender);
					$blood_group = $this->User_model->getBloodGroupById($user->blood_group);
					$user->blood_group = $blood_group;
					$caste = $this->User_model->getCasteById($user->caste);
					$user->caste = $caste;
					$height = $this->User_model->getHeightById($user->height);
					$user->height = $height;
					$occupation = $this->User_model->getOccupation($user->occupation);
					$user->occupation = $occupation;
					$income = $this->User_model->getIncomeById($user->income);
					$user->income = $income;
					$state = $this->User_model->getStatesById($user->state);
					$user->state = $state;
					$district = $this->User_model->getDistrictsById($user->district);
					$user->district = $district;
					$marital_status = $this->User_model->getMaritalStatusById($user->marital_status);
					$user->marital_status = $marital_status;
					$manglik = $this->User_model->getManglikById($user->manglik);
					$user->manglik = $manglik;
					$request = $this->getRequested($user_id, $user->user_id);
					$user->request = $request;
					$shortlisted = $this->getShortlisted($user_id, $user->user_id);
					$user->shortlisted = (int)$shortlisted;
					$user->user_avtar = $user->user_avtar;
					$user_img = $this->Base_model->getAllDataWhere(array(
						'user_id' => $user->user_id,
					), 'mm_user_image');
					$user_imgs = array();
					foreach ($user_img as $user_img) {

//						$user_img->image = $this->config->base_url() . 'assets/images/user/' . $user_img->image;
						$user_img->image = $user_img->image;

						array_push($user_imgs, $user_img);
					}

					$user->user_imgs = $user_imgs;
					array_push($users, $user);
				}


				$percentage = 0;
				$query = $this->db->where('user_id', $user_id)->get('mm_users');
				if ($query->num_rows() > 0) {
					$notEmpty = 0;
					$totalField = 65;
					foreach ($query->result() as $row) {
						$notEmpty += ($row->name != '') ? 1 : 0;
						$notEmpty += ($row->email != '') ? 1 : 0;
						$notEmpty += ($row->country_code != '') ? 1 : 0;
						$notEmpty += ($row->mobile != '') ? 1 : 0;
						$notEmpty += ($row->gender != '') ? 1 : 0;
						$notEmpty += ($row->profile_for != '') ? 1 : 0;
						$notEmpty += ($row->password != '') ? 1 : 0;
						$notEmpty += ($row->area_of_interest != '') ? 1 : 0;
						$notEmpty += ($row->blood_group != '') ? 1 : 0;
						$notEmpty += ($row->caste != '') ? 1 : 0;
						$notEmpty += ($row->complexion != '') ? 1 : 0;
						$notEmpty += ($row->body_type != '') ? 1 : 0;
						$notEmpty += ($row->weight != '') ? 1 : 0;
						$notEmpty += ($row->about_me != '') ? 1 : 0;
						$notEmpty += ($row->height != '') ? 1 : 0;
						$notEmpty += ($row->qualification != '') ? 1 : 0;
						$notEmpty += ($row->occupation != '') ? 1 : 0;
						$notEmpty += ($row->income != '') ? 1 : 0;
						$notEmpty += ($row->work_place != '') ? 1 : 0;
						$notEmpty += ($row->organisation_name != '') ? 1 : 0;
						$notEmpty += ($row->city != '') ? 1 : 0;
						$notEmpty += ($row->state != '') ? 1 : 0;
						$notEmpty += ($row->district != '') ? 1 : 0;
						$notEmpty += ($row->current_address != '') ? 1 : 0;
						$notEmpty += ($row->pin != '') ? 1 : 0;
						$notEmpty += ($row->whatsapp_no != '') ? 1 : 0;
						$notEmpty += ($row->challenged != '') ? 1 : 0;
						$notEmpty += ($row->permanent_address != '') ? 1 : 0;
						$notEmpty += ($row->marital_status != '') ? 1 : 0;
						$notEmpty += ($row->aadhaar != '') ? 1 : 0;
						$notEmpty += ($row->dob != '') ? 1 : 0;
						$notEmpty += ($row->birth_place != '') ? 1 : 0;
						$notEmpty += ($row->birth_time != '') ? 1 : 0;
						$notEmpty += ($row->manglik != '') ? 1 : 0;
						$notEmpty += ($row->gotra != '') ? 1 : 0;
						$notEmpty += ($row->gotra_nanihal != '') ? 1 : 0;
						$notEmpty += ($row->family_address != '') ? 1 : 0;
						$notEmpty += ($row->father_name != '') ? 1 : 0;
						$notEmpty += ($row->father_occupation != '') ? 1 : 0;
						$notEmpty += ($row->family_status != '') ? 1 : 0;
						$notEmpty += ($row->family_district != '') ? 1 : 0;
						$notEmpty += ($row->mother_name != '') ? 1 : 0;
						$notEmpty += ($row->grand_father_name != '') ? 1 : 0;
						$notEmpty += ($row->family_state != '') ? 1 : 0;
						$notEmpty += ($row->family_pin != '') ? 1 : 0;
						$notEmpty += ($row->mother_occupation != '') ? 1 : 0;
						$notEmpty += ($row->family_type != '') ? 1 : 0;
						$notEmpty += ($row->family_income != '') ? 1 : 0;
						$notEmpty += ($row->family_city != '') ? 1 : 0;
						$notEmpty += ($row->maternal_grand_father_name_address != '') ? 1 : 0;
						$notEmpty += ($row->family_value != '') ? 1 : 0;
						$notEmpty += ($row->mobile2 != '') ? 1 : 0;
						$notEmpty += ($row->brother != '') ? 1 : 0;
						$notEmpty += ($row->sister != '') ? 1 : 0;
						$notEmpty += ($row->drinking != '') ? 1 : 0;
						$notEmpty += ($row->dietary != '') ? 1 : 0;
						$notEmpty += ($row->hobbies != '') ? 1 : 0;
						$notEmpty += ($row->smoking != '') ? 1 : 0;
						$notEmpty += ($row->language != '') ? 1 : 0;
						$notEmpty += ($row->interests != '') ? 1 : 0;
						$notEmpty += ($row->working != '') ? 1 : 0;
						$notEmpty += ($row->shortlisted != '') ? 1 : 0;
						$notEmpty += ($row->critical != '') ? 1 : 0;
						$notEmpty += ($row->user_avtar != '') ? 1 : 0;
						$notEmpty += ($row->end_subscription_date != '') ? 1 : 0;
						//do with all field
					}
					$percentage = $notEmpty / $totalField * 100;
				}
				$per = $percentage;

				$this->Base_model->responseSuccessPercentage(1, 'Just Join', $users, $per);
				exit();

			}
		} else {
			return 0;
		}

	}

	public function getRequested($user_id, $requested_id)
	{
		$q = $this->db->where('user_id', $user_id)->where('requested_id', $requested_id)->get('mm_interest')->num_rows();
		if ($q > 0) {
			return 1;
		} else {
			return 0;
		}

	}

	public function getShortlisted($user_id, $short_listed_id)
	{
		$q = $this->db->where('user_id', $user_id)->where('short_listed_id', $short_listed_id)->get('mm_shortlists')->num_rows();
		if ($q > 0) {
			return 1;
		} else {
			return 0;
		}

	}

	public function deleteImage($media_id)
	{
		$this->form_validation->set_rules('media_id', 'media_id', 'required');
		if ($this->form_validation->run() == false) {
			$this->Base_model->responseFailed(0, 'Please fill media id field');
			exit();
		}

		$this->db->where('media_id', $media_id)->delete('mm_user_image');
		$this->Base_model->responseSuccessWethoutData(1, IMAGE_DELETE);
		exit();
	}

	public function setProfileImage($user_id, $img)
	{
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		if ($this->form_validation->run() == false) {
			$this->Base_model->responseFailed(0, 'Please fill user id field');
			exit();
		}

		if ($img == '') {
			$this->Base_model->responseFailed(0, 'Please fill image');
			exit();
		} else {
			$config['upload_path'] = 'assets/images/user/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['overwrite'] = TRUE;
//            $config['max_size']      = 1000000;
			$config['file_name'] = time();
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$image = "";
//			if ($this->upload->do_upload('user_avtar')) {
			if (true) {
				$uploadData = $this->upload->data();
				$uploadData['file_name'] = $this->aws3->sendfile('bethelmatrimony-upload', $_FILES['user_avtar']);
				$image = $uploadData['file_name'];
			}

			$data = array(
				'user_avtar' => $image,
//				'user_avtar' => $u['image'],
				'updated_at' => date('Y-m-d H:i:s'),
			);

			$this->db->where('user_id', $user_id);
			$this->db->update('mm_users', $data);
			$data = $this->myProfile($user_id);
			$this->Base_model->responseSuccess(1, DATA_SAVE, $data);

//			$this->Base_model->responseSuccessWethoutData(1, DATA_SAVE);
			exit();
		}
	}


}
