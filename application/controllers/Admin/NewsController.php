<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewsController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function news()
	{
      	if (isset($_SESSION['id'])) {
            $res['language'] = $this->Setting_model->activeLanguage();
			$res['news'] = $this->News_model->news();
			echo load_page($this,'news/news',$res);

	    } else {
	        redirect('login');
	    }
  	}

  	public function submitNews()
    {
    	$input = $this->input->post();
        $image = $_FILES['image']['name'];
    	$id = $this->News_model->submitNews($input,$image);
    	
    }

    public function updateNews()
    {
        $id = $this->uri->segment('3');
        $data['language'] = $this->Setting_model->activeLanguage();
        $data['row'] = $this->News_model->updateNews($id);
        $data['news'] = $this->News_model->news();
        echo load_page($this,'news/news',$data);
    }

    public function changeNewsStatus()
    {
        $id = $this->uri->segment('3');
        $this->News_model->changeNewsStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('news');
    }

    public function deleteNews()
    {
        $id = $this->uri->segment('3');
        $this->News_model->deleteNews($id);
        $this->session->set_flashdata('error', DATA_DELETE);
        redirect('news');
    }

}