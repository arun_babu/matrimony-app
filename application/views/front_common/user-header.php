<?php
$data = $this->session->userdata;
if($data['user_id'] == '')
{
    redirect();
}
?>
<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/plugins/images/favicon.png">
      <!-- Bootstrap CSS -->
      <!-- <script src='<?php echo base_url('front_assets'); ?>/js/jquery-3.5.1.min.js'></script> -->
      <!-- <script src='<?php echo base_url('front_assets'); ?>/js/jquery.min.js'></script> -->
     <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/bootstrap.min.css" >

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/style.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.carousel.min.css" >
      <link rel="stylesheet" href="<?php echo base_url('front_assets'); ?>/css/owl.theme.default.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">

      <!-- <script src='<?php echo base_url('front_assets'); ?>/js/jquery-3.5.1.slim.min.js'></script> -->
      <title>Matrimonix</title>
   </head>
   <body>
    <header class="user">
           <nav class="navbar navbar-expand-md navbar-dark">
         <div class="container">
            <!-- Brand -->
            <a class="navbar-brand" href="index.php"><span>Matrimonix</span></a>
            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            Login
            </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('profile'); ?>">My Profile</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('shortlist'); ?>">Shortlisted</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('interest'); ?>">Interest</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('matches'); ?>">My Matches</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('visitor'); ?>">Visitors</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('newss'); ?>">News</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('chat'); ?>">Chat</a>              
                  </li>
                  <li class="nav-item">
                     <a class="nav-link-1" href="<?php echo site_url('signOut'); ?>">Sign Out</a>              
                  </li>
               </ul>
            </div>
         </div>
      </nav>
    </header>



   
   